#!/bin/sh
set -e

RELEASE_DIR_BASE=$1
SILECS_VERSION=$2

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

RELEASE_DIR=${RELEASE_DIR_BASE}/${SILECS_VERSION}/silecs-model
if [ -d ${RELEASE_DIR} ]; then 
    echo "Error: ${RELEASE_DIR} already exists ...skipping"
    exit 1
fi

mkdir -p ${RELEASE_DIR}
cp -r ${SCRIPTPATH}/src/xml ${RELEASE_DIR}

# Make all files write-protected to prevent overwriting an old version by accident
chmod a-w -R ${RELEASE_DIR}