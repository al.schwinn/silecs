# silecs-codegen

This component of the SILECS PLC-framework is used in order to validate silecs-project-specific XML files.

# Disclaimer

The GSI Version of Silecs is a fork of the CERN Silecs version. It was forked 2018 and has evolved into a slightly different framework. The fork was required because CERN did not merge GSI merge requests, and generally does not respect the GPL v3 licence (source code and issue tracker only visible in the CERN walled garden, CERN account and special privileges required, software only builds and runs within CERN infrastructure ). That makes a collaboration/contributions impossible. 

## Getting Started

Please check the SILECS-Wiki for more information:

[GSI SILECS Wiki Page][GSI_Wiki]

## License

Licensed under the GNU GENERAL PUBLIC LICENSE Version 3. See the [LICENSE file][license] for details.

[license]: LICENSE
[GSI_Wiki]: https://www-acc.gsi.de/wiki/Frontend/SILECS