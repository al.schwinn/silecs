#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import zlib
import libxml2

import iecommon
import iefiles
import xmltemplate
import s7template
import virtualS7Template
import DIGITemplate

from iecommon import *
from model.Param.Param import *
from model.Param.Controller import *
from model.Class.Register import ParamRegister
from model.Class.Block import ParamBlock
from model.Class.Class import ParamClass
from model.Deploy.Deploy import *

#=========================================================================
# General remarks
#=========================================================================

#XML input documents well-formed is not checked! 
#This Python-script generates PLC sources document from XML Parameter documents
#which are supposed to be well-formed (generated with the 'genparam' script).

whichUnityFormat = {
    'uint8'      : 'BYTE',
    'int8'        : 'WORD',
    'uint16'    : 'WORD',
    'int16'      : 'INT',
    'uint32'    : 'DWORD',
    'int32'      : 'DINT',
    'float32'   : 'REAL',
    'string'     : 'STRING',
#    'uint64'      : '8',    not supported with PLC
#    'int64'        : '8',    not supported with PLC
#    'float64'     : '8',    not supported with PLC
    'date'       : 'DT',
    'char'       : 'WORD',
    'byte'       : 'BYTE',
    'word'      : 'WORD',
    'dword'    : 'DWORD',
    'int'          : 'INT',
    'dint'        : 'DINT',
    'real'        : 'REAL',
    'dt'           : 'DT'
}

whichTwincatFormat = {
    'uint8'      : 'BYTE',
    'int8'        : 'SINT',
    'uint16'    : 'WORD',
    'int16'      : 'INT',
    'uint32'    : 'DWORD',
    'int32'      : 'DINT',
    'float32'   : 'REAL',
    'string'     : 'STRING',
    'uint64'    : 'ULINT',
    'int64'     : 'LINT',
    'float64'   : 'LREAL',
    'date'       : 'DT',
    'char'       : 'SINT',
    'byte'       : 'BYTE',
    'word'      : 'WORD',
    'dword'    : 'DWORD',
    'int'          : 'INT',
    'dint'        : 'DINT',
    'real'        : 'REAL',
    'dt'           : 'DT',
    'string'     : 'STRING'
}

schneiderRegArray  = """ARRAY[0..%d] OF %s"""
schneiderRegArray2d  = """ARRAY[0..%d, 0..%d] OF %s"""

# Beckhoff templates (using newlines \r\n because TwinCAT compiler complains with line feed character)
beckhoffReg = """    %s_%04x_%s AT %%MW%s: %s;\r\n\r\n"""
beckhoffRegArray = """    %s_%04x_%s AT %%MW%s: ARRAY [0..%d] OF %s;\r\n\r\n"""
beckhoffRegArray2d = """    %s_%04x_%s AT %%MW%s: ARRAY [0..%d, 0..%d] OF %s;\r\n\r\n"""
beckhoffRegInit = """    %s_%04x_%s AT %%MW%s: %s:= %s;\r\n\r\n"""

#=========================================================================
# Sub-function
#=========================================================================

#-------------------------------------------------------------------------
# Schneider registers
#-------------------------------------------------------------------------

def xsyRegister(register):
    if register.isStringType():
        strLen = whichUnityFormat[register.format]+'['+str(register.stringLength)+']'
        if  register.isString():
            return strLen
        elif register.isStringArray():
            return schneiderRegArray %(register.dim1-1, strLen)
        else: #2D-Array
            return schneiderRegArray2d %(register.dim1-1, register.dim2-1, strLen)
    else:
        if  register.isScalar():
            return whichUnityFormat[register.format]
        elif register.isArray():
            return schneiderRegArray %(register.dim1-1, whichUnityFormat[register.format])
        else: #2D-Array
            return schneiderRegArray2d %(register.dim1-1, register.dim2-1, whichUnityFormat[register.format])
    

def decimalToBCD(value):
    return ((value//10)*16)+(value%10)
      
# convert date to DATE_AND_TIME string format (Siemens SIMATIC PLC)
def getDateTime(controller):
    dt = time.localtime(time.time())
    if controller.model == 'SIMATIC_S7-1200':
        # convert date to BCD byte-array (SIMATIC S7-12xx CPU does not support DATE_AND_TIME type)      
        return "%s,%s,%s,%s,%s,%s" %(decimalToBCD(dt[0]-2000),
                                     decimalToBCD(dt[1]),
                                     decimalToBCD(dt[2]),
                                     decimalToBCD(dt[3]),
                                     decimalToBCD(dt[4]),
                                     decimalToBCD(dt[5]))
    else:
        return "DT#%s-%s-%s-%s:%s:%s" %(dt[0],dt[1],dt[2],dt[3],dt[4],dt[5])


            
#-------------------------------------------------------------------------
# SIEMENS PLC code generation
#-------------------------------------------------------------------------
def generateSiemensSources(param, sourceFolderPath ,logTopics):
    stlString = ''
    symString = ''  # for .sdf Symbol file

    # Prepare the Simatic Symbols (<.sdf> file)
    DBnumber    = param.controller.address[MEM_TYPE]       #first data-block (SilecsHeader) starts from the base-address
    UDTnumber   = param.controller.address[MEM_TYPE]       #use base data-block address for UDT number as well (to be reserved by the developer)

    # Generate sources for each class that is deployed in the PLC
    for paramClass in param.controller.paramClasses:
        deviceDOMList = paramClass.getDeviceInstanceNodes() # get Device instances of that class
        # Generate the Blocks definition
        for blockIndex, block in enumerate(paramClass.getParamMemoryBlocks()):
            registerList = ''
            for register in block.getParamRegisters():
                
                #FLO: DO NOT LOG ERROR IF TWINCAT3 !!
                # If PLC does not supports this register format abort this generation and log the error
                # if(registerFormat in ['uint64','int64','float64']):
                    #iecommon.logGenerationResult(iecommon.opts.username,'ERROR: In register '+registerName+', '+registerFormat+' format not supported for current controller model.')
                    #iecommon.logError('ERROR: In register '+registerName+', '+registerFormat+' format not supported for current controller model.', True)
    
                #create register value if required (diagnostic registers assignment in particular)
                registerValue = s7template.stlRegisterValue(register.name, param.owner, getDateTime(param.controller), param.checksum,param.silecsVersion)
                
                if register.format == 'string':
                    registerList += s7template.stlRegister(register.name, register.format, register.dim1, register.dim2, registerValue, int(register.stringLength))
                else:                    
                    registerList += s7template.stlRegister(register.name, register.format, register.dim1, register.dim2, registerValue)

            stlString += s7template.stlBlockUDT(param.owner, paramClass.name, paramClass.version, block.name, registerList, (blockIndex == 0))
            symString += s7template.symBlockUDT(paramClass.name, paramClass.version, block.name, UDTnumber, (blockIndex == 0))
            UDTnumber += 1
   
        if param.controller.protocol == 'DEVICE_MODE':
        # Generate the Data-Blocks: one DB per Device instance
            for deviceIndex, deviceDOM in enumerate(deviceDOMList):
                blockList = ''
                for block in paramClass.getParamMemoryBlocks():
                    blockList += s7template.stlBlock(paramClass.name, block.name)
                deviceLabel = deviceDOM.prop('label')
                stlString += s7template.generateBlock(param.owner, paramClass.name, paramClass.version, param.controller.system, DBnumber, deviceLabel, blockList, (deviceIndex == 0),param.controller.protocol)
                symString += s7template.symDeviceDB(paramClass.name, paramClass.version, deviceLabel, DBnumber, (deviceIndex == 0))
                DBnumber += 1

        else:             # BLOCK_MODE
        # Generate the Data-Blocks: one DB per Block of registers
            for blockIndex, block in enumerate(paramClass.getParamMemoryBlocks()):
                deviceList = ''
                for deviceDOM in deviceDOMList:
                    deviceLabel = deviceDOM.prop('label')
                    deviceList += s7template.stlDevice(deviceLabel, paramClass.name, block.name)

                stlString += s7template.generateBlock(param.owner, paramClass.name, paramClass.version, param.controller.system, DBnumber, block.name, deviceList, (blockIndex == 0),param.controller.protocol)
                symString += s7template.symBlockDB(paramClass.name, paramClass.version, block.name, DBnumber, (blockIndex == 0))
                DBnumber += 1

    # write the String PLC generated code
    generateControllerFiles(sourceFolderPath,param.controller.hostName,".scl",stlString,logTopics);
        
    # Write the String PLC generated symbols into the <.sdf> symbols file
    generateControllerFiles(sourceFolderPath,param.controller.hostName,".sdf",symString,logTopics);

#-------------------------------------------------------------------------
# SCHNEIDER PLC code generation
#-------------------------------------------------------------------------

# ----------------------------------------------------
def generateSchneiderRegisters(xsydoc, paramClass, deviceDOM, block, deviceIndex, modeDevice, param, logTopics):
    
    deviceLabel = deviceDOM.prop('label')
    
    #Device and Block adresses come from the generated Paramerers file. Depends on the protocol mode:
    #DEVICE_MODE: relative address of the element within it parent node
    #BLOCK_MODE : absolute address in the entire PLC memory
    deviceAddress = int(deviceDOM.prop('address'))

    for register in block.getParamRegisters():
        # If PLC does not supports this register format abort this generation and log the error
        if(register.format in ['uint64','int64','float64']):
            iecommon.logError('ERROR: In register '+register.name+', '+register.format+' format not supported for current controller model.', True,logTopics)
        
        # Compute the Register address relying on the Class Parameters file data
        # Attention! Schneider uses WORD addressing while Parameters data are expressed in bytes to be PLC independent
        if modeDevice:
            totalAddress = (deviceAddress + block.address + register.address)/2
        else:
            totalAddress = (block.address + (deviceIndex * block.memSize) + register.address)/2
              
        # then insert the corresponding DOM element
        dataElt = xsydoc.xpathEval("//dataBlock")[0] #only one dataBlock node has been inserted so far
        regElt  = libxml2.newNode('variables')
        # Compute the base checksum of the class name
        classNameCRC = zlib.crc32((paramClass.name).encode(),0) & 0xffff
        # Create register name = regname_crc32(classname)_deviceid
        # 24 char (register name) + 1 (underscore) + 4 char (CRC classname) + 1 (underscore) + n (device id) = 30 + n (device id) 
        # possible problem when device id (n)> 99 --> string > 32 not compatible with Schneider PLCs
        regElt.setProp("name", "%s_%04x_%s" %(register.name, classNameCRC, deviceLabel))
        
        if register.format == 'string':
            regElt.setProp("typeName", "%s" %xsyRegister(register))
        else:
            regElt.setProp("typeName", "%s" %xsyRegister(register))
        
            
        regElt.setProp("topologicalAddress", "%%MW%ld" %totalAddress)
        if block.name == 'hdrBlk':
            initElt = libxml2.newNode('variableInit')
            if register.name == '_version': initElt.setProp("value", param.silecsVersion )
            if register.name == '_user'   : initElt.setProp("value", param.owner)
            if register.name == '_checksum': initElt.setProp("value", "%s" %param.checksum)
            if register.name == '_date'  : initElt.setProp("value", getDateTime(param.controller))
            regElt.addChild(initElt)
        commElt    = libxml2.newNode('comment')
        commElt.setContent(paramClass.name+"/"+deviceLabel+"/"+block.name)
        regElt.addChild(commElt)
        dataElt.addChild(regElt)
        
# ----------------------------------------------------
def generateSchneiderSources(param,sourceFolderPath,logTopics):

    # Create the Schneider DOM source (Unity <.XSY> XML document)
    xsyDoc  = libxml2.newDoc(version='1.0')
    rootElt = libxml2.newNode('VariablesExchangeFile')
    xsyDoc.addChild(rootElt)
    dataElt = libxml2.newNode('dataBlock')
    rootElt.addChild(dataElt)

    for paramClass in param.controller.paramClasses:
        deviceDOMList = paramClass.getDeviceInstanceNodes() # get Device instances of that class
        
        for block in paramClass.getParamMemoryBlocks():
            for deviceIndex,deviceDOM in enumerate(deviceDOMList):
                if param.controller.protocol == 'DEVICE_MODE': #-------------------
                    generateSchneiderRegisters(xsyDoc, paramClass, deviceDOM, block, deviceIndex, True, param, logTopics)
                else:            
                    generateSchneiderRegisters(xsyDoc, paramClass, deviceDOM, block, deviceIndex, False, param, logTopics)
    
    # Finally, write the DOM object (PLC generated code) into the XSY source file
    generateControllerFiles(sourceFolderPath,param.controller.hostName,".xsy",rootElt.serialize(format = True),logTopics);

    xsyDoc.freeDoc()      
    
# ----------------------------------------------------
def generateDIGISources(param,sourceFolderPath,logTopics):
    deviceLabel = ''
    cCodeString =  DIGITemplate.cHeader(param.silecsVersion, param.controller.address[MEM_TYPE])
    cTypeDefinitions = ''
    cDataAllocation  = DIGITemplate.cAllocationComment(param.controller.protocol)
    
    blockList = ''
    classList = ''
    NBdeviceDefinitionList = ''

    for paramClass in param.controller.paramClasses:
        cTypeDefinitions += DIGITemplate.cFirstBlockUDT(paramClass.name, paramClass.version)
        for block in paramClass.getParamMemoryBlocks():
            registerList = ''
            for register in block.getParamRegisters():
                if register.format == 'float64':
                    iecommon.logError('ERROR: In register '+register.name+', '+ register.format +' format not supported for current controller model.', True,logTopics)
                if register.format == 'string':
                    registerList += DIGITemplate.cRegister(register.name, register.format, register.dim1, register.dim2, register.stringLength)
                else:
                    registerList += DIGITemplate.cRegister(register.name, register.format, register.dim1, register.dim2)    

            cTypeDefinitions += DIGITemplate.cBlockUDT(paramClass.name, block.name, registerList)
            
        #==== Memory allocation ====
        if param.controller.protocol == 'DEVICE_MODE':
            # Generate the List of block in the class
            blockList =''
            for block in paramClass.getParamMemoryBlocks():
                blockList += DIGITemplate.cDeviceModeBlockInstantiation(paramClass.name, block.name)

            deviceList = ''
            for deviceDOM in paramClass.getDeviceInstanceNodes():
                deviceLabel = paramClass.name + "_" + deviceDOM.prop('label')
                deviceList += deviceLabel + ', ';
            deviceList = deviceList[:-2]  # remove last comma space
            classList += DIGITemplate.cDeviceModeClass_deviceList(blockList, deviceList)
        else:  # BLOCK_MODE
            for block in paramClass.getParamMemoryBlocks():
                    # DeviceList
                deviceList = ''
                for deviceDOM in paramClass.getDeviceInstanceNodes():
                    deviceLabel = deviceDOM.prop('label')
                    deviceList +=DIGITemplate.cBlockModeDeviceInstantiation_deviceList(paramClass.name, block.name, deviceLabel)
                # allocation is the same with device list or device number
                blockList += DIGITemplate.cBlockModeBlockInstantiation(deviceList, paramClass.name, block.name)

    # Predefine number of devices constant
    cDataAllocation += NBdeviceDefinitionList                

    if param.controller.protocol == 'DEVICE_MODE':
        cDataAllocation += DIGITemplate.cDeviceModeDataInstantiation(classList)
        cInitFunction    = DIGITemplate.cInitDeviceMode(param.silecsVersion,param.checksum,param.owner)
    else:
        cDataAllocation += DIGITemplate.cBlockModeDataInstantiation(blockList)
        cInitFunction    = DIGITemplate.cInitBlockMode(param.silecsVersion,param.checksum,param.owner)
    
              
    # Append data definitions
    cCodeString += cTypeDefinitions
    
    # Append data allocation
    cCodeString += cDataAllocation
    
    # Append initialization function
    cCodeString += cInitFunction
    
    # Generate and append sample example
    cCodeString += DIGITemplate.cExample(param.controller.protocol,paramClass.name, block.name, register.name, deviceLabel)
    
    # Finally, write the String C generated code into the temporal output file
    generateControllerFiles(sourceFolderPath,param.controller.hostName,".h",cCodeString,logTopics);

# ----------------------------------------------------
def generateVirtualS7Sources(param,sourceFolderPath,logTopics):   
    includeDesign  = ''
    allocDesign  = ''
    deleteDesign = ''
    getDesign = ''

    protocolMode = 'BlockMode'; 
    if param.controller.protocol == 'DEVICE_MODE':
        protocolMode = 'DeviceMode'; 
    duConstructor = virtualS7Template.vs7DuConstructor(param.deployName, param.deployVersion, protocolMode, param.controller.address[MEM_TYPE])
    
    for paramClass in param.controller.paramClasses:
        blocksCodeString = ''
         
        createBlockCodeString = '' 
        deleteBlockCodeString = '' 
        deviceCodeString = ''
         
        createDeviceCodeString = '' 
        deleteDeviceCodeString = '' 
        designCodeString = ''

        #DEPLOY-UNIT code generation ============================
        includeDesign += virtualS7Template.vs7DuDesignInclude(paramClass.name, paramClass.version)   
        allocDesign += virtualS7Template.vs7DuDesignAlloc(paramClass.name, paramClass.version)   
        deleteDesign += virtualS7Template.vs7DuDesignDelete(paramClass.name)
        getDesign += virtualS7Template.vs7DuDesignGet(paramClass.name, paramClass.version)

        #CLASSES code generation ================================
        for block in paramClass.getParamMemoryBlocks():
            getSetCodeString = ''
            dimsCodeString   = ''
            dataCodeString   = ''
            
            currentRegisterAddress = 0  #used to compute even/odd adress and insert align data by adding dummy registers
            previousRegisterMemSize = 0 #...
            dummyIndex = 0;

            createBlockCodeString += virtualS7Template.vs7ClassCreateBlock(paramClass.name, block.name)
            deleteBlockCodeString += virtualS7Template.vs7ClassDeleteBlock(paramClass.name, block.name)
            
            for register in block.getParamRegisters():
                getSetCodeString += virtualS7Template.vs7ClassGetSet(register.name, register.format, register.dim1, register.dim2,register.stringLength)
                dimsCodeString += virtualS7Template.vs7ClassDimension(register.name, register.format, register.dim1, register.dim2,register.stringLength)

                #Specific code to force 16bit alignment if not scalar 8bit register (==> insert dummy register if needed)
                dummyCodeString = virtualS7Template.vs7ClassDummyRegister(currentRegisterAddress, previousRegisterMemSize, register.memSize, dummyIndex)
                dataCodeString += dummyCodeString
                currentRegisterAddress += register.memSize 
                if dummyCodeString != "":
                    dummyIndex += 1
                    currentRegisterAddress += 1     
                #and now insert the normal data register     
                dataCodeString += virtualS7Template.vs7ClassDataRegister(register.name, register.format, register.dim1, register.dim2, register.stringLength)
                previousRegisterMemSize = register.memSize 
                
            blocksCodeString += virtualS7Template.vs7ClassBlock(param.silecsVersion, param.owner, param.checksum, paramClass.name, block.name, getSetCodeString, block.address, dimsCodeString, dataCodeString)
        
        for deviceIndex, deviceDOM in enumerate(paramClass.getDeviceInstanceNodes()):
            deviceLabel = deviceDOM.prop('label')
            createDeviceCodeString += virtualS7Template.vs7ClassCreateDevice(paramClass.name, deviceLabel, deviceIndex)
            deleteDeviceCodeString += virtualS7Template.vs7ClassDeleteDevice(paramClass.name, deviceLabel)
        
        deviceCodeString = virtualS7Template.vs7ClassDevice(createBlockCodeString, deleteBlockCodeString)
        designCodeString = virtualS7Template.vs7ClassDesign(paramClass.name, paramClass.version, createDeviceCodeString, deleteDeviceCodeString)
        
        classCodeString = virtualS7Template.vs7ClassHeader(paramClass.name, paramClass.version, blocksCodeString, deviceCodeString, designCodeString)
            
        # Finally, write the CLASS generated code into the temporal output file
        sourcesFile = os.path.normpath(sourceFolderPath + "/%s_%s.h" % (paramClass.name, paramClass.version))
        iecommon.logInfo("Generate PLC sources: %s" %sourcesFile,logTopics);
        fdesc = open(sourcesFile, "w")
        fdesc.write(classCodeString)
        fdesc.close()
    
    # Prepare the source (<.h> file) with its diagnostic data-block header
    duCodeString = virtualS7Template.vs7DuHeader(param.controller.hostName, param.deployVersion, duConstructor, includeDesign, allocDesign, deleteDesign, getDesign)    
             
    # Write the DEPLOY-UNIT generated code into the temporal output file
    sourcesFile = os.path.normpath(sourceFolderPath + "/%s_%s.h" % (param.deployName, param.deployVersion))
    iecommon.logInfo("Generate PLC sources: %s" %sourcesFile,logTopics);
    fdesc = open(sourcesFile, "w")
    fdesc.write(duCodeString)
    fdesc.close()

    # Prepare the source (<.h> file) with its diagnostic data-block header
    mainCodeString = virtualS7Template.vs7MainCode(param.deployName, param.deployVersion)
        
    # Finally, write the DEPLOY-UNIT generated code into the temporal output file
    sourcesFile = os.path.normpath(sourceFolderPath + "/%s_%s.cpp" % (param.controller.hostName, param.deployVersion))
    iecommon.logInfo("Generate PLC sources: %s" %sourcesFile,logTopics);
    fdesc = open(sourcesFile, "w")
    fdesc.write(mainCodeString)
    fdesc.close()
    
# ----------------------------------------------------
def generateBeckhoffRegisters(param, paramClass, deviceDOM, block, deviceIndex, logTopics):
    source = ''    
    deviceLabel = deviceDOM.prop('label')
    
    #Device and Block adresses come from the generated Paramerers file. Depends on the protocol mode:
    #DEVICE_MODE: relative address of the element within it parent node
    #BLOCK_MODE : absolute address in the entire PLC memory
    deviceAddress = int(deviceDOM.prop('address'))
    
    for register in block.getParamRegisters():
        # If PLC does not supports this register format abort this generation and log the error
        if(register.format in ['uint64','int64','float64']):
            iecommon.logError('ERROR: In register '+register.name+', '+register.format+' format not supported for current controller model.', True)
            return
    
        iecommon.logDebug("Processing: %s %s %s %s" %(paramClass.name, deviceLabel, block.name, register.name))

        # Compute the register address relying on the Class Parameters file data
        # Attention! Beckhoff uses WORD addressing while Parameters data are expressed in bytes to be PLC independent
        if param.controller.model == 'BC9020':
            totalAddress = (block.address - int(param.controller.address[MEM_TYPE]) + (deviceIndex * block.memSize) + register.address)/2
        elif param.controller.model == 'CX9020':
            totalAddress = (block.address - int(param.controller.address[MEM_TYPE]) + (deviceIndex * block.memSize) + register.address)
        else:
            raise "PLC model not supported: " + param.controller.model

        # add comment to source to identify block
        source += '    (*'+paramClass.name+'/'+deviceLabel+'/'+block.name+' *)\r\n'
        
        # Compute the base checksum of the class name
        classNameCRC = zlib.crc32((paramClass.name).encode(),0) & 0xffff
        
        if block.name == 'hdrBlk':
            if register.name == '_version':
                source+=beckhoffRegInit %(register.name, classNameCRC, deviceLabel, int(totalAddress), whichTwincatFormat[register.format]+"(16)", param.silecsVersion )
            if register.name == '_user':
                source+=beckhoffRegInit %(register.name, classNameCRC, deviceLabel, int(totalAddress), whichTwincatFormat[register.format]+"(16)", "'"+param.owner+"'") 
            if register.name == '_checksum':
                source+=beckhoffRegInit %(register.name, classNameCRC, deviceLabel, int(totalAddress), whichTwincatFormat[register.format], param.checksum)
            if register.name == '_date':
                source+=beckhoffRegInit %(register.name, classNameCRC, deviceLabel, int(totalAddress), whichTwincatFormat[register.format], getDateTime(param.controller))
        
        else:   # not header block
             # set data type - for string with fixed value registerLength
            if register.format == 'string':
                format = whichTwincatFormat[register.format]+'('+str(register.stringLength)+')'
            else:
                format = whichTwincatFormat[register.format]
                
            if register.dim1 == 1 and register.dim2 == 1:    # scalar
                source += beckhoffReg %(register.name, classNameCRC, deviceLabel, int(totalAddress), format)
            elif register.dim1 > 1 and register.dim2 == 1: # array
                source += beckhoffRegArray %(register.name, classNameCRC, deviceLabel, int(totalAddress), register.dim1-1, format)
            else:   # dim1>=1 and for whatever dim2, use double array syntax
                source += beckhoffRegArray2d %(register.name, classNameCRC, deviceLabel, int(totalAddress), register.dim1-1, register.dim2-1, format)
                    
    return source
# ----------------------------------------------------
def generateBeckhoffSources(param, sourceFolderPath, logTopics):
    
    # Prepare the global variables source (<.exp> file) with its diagnostic data-block header
    source ="""(* +-------------------------------------------------------------------\r\n"""
    source += """* | C.E.R.N Geneva, Switzerland\r\n"""
    source += """* | SILECS - BE/CO-FE\r\n"""
    source += """* | April 2015\r\n"""
    source += """* +-------------------------------------------------------------------\r\n"""
    source += """*\r\n"""
    source += """* Release : SILECS_%s\r\n"""%(param.silecsVersion)
    source += """*)"""
        
    source += '\r\nVAR_GLOBAL\r\n'

    for paramClass in param.controller.paramClasses:
        deviceDOMList = paramClass.getDeviceInstanceNodes() # get Device instances of that class
            
        # Device mode is not supported for Beckhoff PLCs - Only block mode available
        for block in paramClass.getParamMemoryBlocks():
            for deviceIndex, deviceDOM in enumerate(deviceDOMList):
                source += generateBeckhoffRegisters(param, paramClass, deviceDOM, block, deviceIndex,logTopics)
                
    source += 'END_VAR'

    # Write the source into the EXP source file
    generateControllerFiles(sourceFolderPath,param.controller.hostName,".exp",source,logTopics);

#-------------------------------------------------------------------------
# NI code generation
#-------------------------------------------------------------------------
def generateNISources(param, sourceFolderPath ,logTopics):
    iecommon.logInfo("PLC source generation not available for National Instruments equipment",logTopics)
    return

def generateControllerFiles(sourceFolderPath,hostName,fileExtention,source,logTopics={'errorlog': True}):
    fileName = hostName + fileExtention
    fullFilePath = os.path.normpath(sourceFolderPath + "/" + fileName )

    iecommon.logInfo("Generate PLC sources: %s" %fullFilePath,logTopics);
    fdesc = open(fullFilePath, "w")
    fdesc.write(source)
    fdesc.close()

# Needed for unit-testing
def generateControllerCode(controller, param, sourceFolderPath, logTopics={'errorlog': True}):
    if controller.plcNode.get_name() == 'Siemens-PLC':
        generateSiemensSources(param,sourceFolderPath,logTopics)
    elif controller.plcNode.get_name() == 'PC-Controller':
        generatePCLikeSources(param,sourceFolderPath,logTopics)
    elif controller.plcNode.get_name() == 'Virtual-Controller':
        generateVirtualS7Sources(param,sourceFolderPath,logTopics)
    elif controller.plcNode.get_name() == 'Schneider-PLC':
        generateSchneiderSources(param,sourceFolderPath,logTopics)
    elif controller.plcNode.get_name() == 'Rabbit-uC':
        generateDIGISources(param,sourceFolderPath,logTopics)
    elif controller.plcNode.get_name() == 'Beckhoff-PLC':
        generateBeckhoffSources(param,sourceFolderPath,logTopics)
    elif controller.plcNode.get_name() == 'NI-Controller':
        generateNISources(param,sourceFolderPath,logTopics)
    else:
        iecommon.logError("The PLC-Type: '" + controller.plcNode.get_name() + "' is not supported", True,logTopics)

#=========================================================================
# Entry Point
#=========================================================================
def genPlcSrc(workspacePath, deployName,silecsVersion,logTopics={'errorlog': True}):
    deployFilePath = iefiles.getSilecsDeployFilePath(workspacePath,deployName)
    iecommon.logInfo("Load Deployment document: %s" %deployFilePath,logTopics);

    # Create the source folder if needed
    sourceFolderPath = iefiles.getControllerSourcesDirectory(workspacePath, deployName)
    if not os.path.exists(sourceFolderPath):
        os.makedirs(sourceFolderPath)

    silecsDeploy = Deploy.getDeployFromFile(deployFilePath)
    for controller in silecsDeploy.controllers:
        paramsFile = iefiles.getParameterFile(workspacePath, deployName,controller.hostName)
        paramDOM = libxml2.parseFile(paramsFile)
        param = Param()
        param.initFromXMLNode(paramDOM)
        param.deployName = silecsDeploy.name
        param.deployVersion = silecsDeploy.version
        generateControllerCode(controller,param,sourceFolderPath,logTopics)
        paramDOM.freeDoc()
    
    return "Genplcsrc succeded!"
 
