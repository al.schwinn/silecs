#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from iecommon import *
from model.Class.Register import *
from Mapping import *
import libxml2


class IOType(object):
    ANALOG = "ANALOG-IO"
    DIGITAL = "DIGITAL-IO"
    MEMORY  = "MEMORY"
    
class Block(object):

    ___settingBlockName = "Setting-Block"
    ___acquisitionBlockName = "Acquisition-Block"
    ___commandBlockName = "Command-Block"
    ___configurationBlockName = "Configuration-Block"
    ___settingIOBlockName = "Setting-IO-Block"
    ___acquisitionIOBlockName = "Acquisition-IO-Block"
    ___commandIOBlockName = "Command-IO-Block"
    
    def __init__(self, xmlNode):
        self.xmlNode = xmlNode
        self.ioType = ""
        self.name = xmlNode.prop("name")
        if xmlNode.hasProp("ioType"):
            self.ioType = xmlNode.prop("ioType") #only available on io-blocks
        else:
            self.ioType = IOType.MEMORY
        self.___name = xmlNode.get_name()
        self.type = None
        if self.___name == self.___settingBlockName or self.___name == self.___acquisitionBlockName or self.___name == self.___commandBlockName or self.___name == self.___configurationBlockName:
            self.type = MEM_TYPE
        elif self.___name == self.___acquisitionIOBlockName and self.ioType == IOType.DIGITAL:
            self.type = DI_TYPE
        elif ( self.___name == self.___settingIOBlockName or self.___name == self.___commandIOBlockName) and self.ioType == IOType.DIGITAL:
            self.type = DO_TYPE
        elif self.___name == self.___acquisitionIOBlockName and self.ioType == IOType.ANALOG:
            self.type = AI_TYPE
        elif ( self.___name == self.___settingIOBlockName or self.___name == self.___commandIOBlockName ) and self.ioType == IOType.ANALOG:
            self.type = AO_TYPE
        else:
            raise Exception( "Cannot identify block-type of block:" + self.name )
                #xmlNode.shellPrintNode()

    def getNameCapitalized(self):
        return  iecommon.capitalizeString(self.name)
    
    def isReadable(self):
        return self.___name == self.___settingBlockName or self.___name == self.___acquisitionBlockName or self.___name == self.___configurationBlockName
    
    # note that configuration-blocks need to be writable, since they need to be sent to plc at least once, at startup
    def isWritable(self):
        return self.___name == self.___settingBlockName or self.___name == self.___commandBlockName or self.___name == self.___configurationBlockName

    def isIOBlock(self):
        return not self.isMEMBlock()
    
    def isMEMBlock(self):
        return self.type == MEM_TYPE
    
    def isAnanlogIO(self):
        return self.ioType == IOType.ANALOG
    
    def isDigitalIO(self):
        return self.ioType == IOType.DIGITAL
    
    def isAcquisition(self):
        return self.___name == self.___acquisitionBlockName or self.___name == self.___acquisitionIOBlockName
    
    def isSetting(self):
        return self.___name == self.___settingBlockName or self.___name == self.___settingIOBlockName
    
    def isCommand(self):
        return self.___name == self.___commandBlockName or self.___name == self.___commandIOBlockName
    
    def isConfiguration(self):
        return self.___name == self.___configurationBlockName
    
    def getMEMRegisterNodes(self):
        return self.xmlNode.xpathEval("*[name()='Acquisition-Register' or name()='Setting-Register' or name()='Configuration-Register']")
    
    def getIORegisterNodes(self):
        return self.xmlNode.xpathEval("*[name()='Acquisition-IO-Register' or name()='Setting-IO-Register']")
    
    def getRegisterNodes(self):
        return self.getMEMRegisterNodes() + self.getIORegisterNodes()
    
#has some additionalValues
class ParamBlock(Block):
    def __init__(self):
        self.size = 0
        self.address = 0
        self.memSize = 0
            
    def initWithParamBlockNode(self, xmlNode):
        super(ParamBlock, self).__init__(xmlNode)
        self.size = int(self.xmlNode.prop("size"))
        self.address = int(self.xmlNode.prop("address"))
        self.memSize = int(self.xmlNode.prop("mem-size"))
        
    def initWithDesignBlock(self, designBlock):
        newNode = libxml2.newNode(designBlock.xmlNode.get_name())
        newNode.newProp("name", designBlock.name)
        if designBlock.xmlNode.hasProp("ioType"):
            newNode.newProp("ioType", designBlock.xmlNode.prop("ioType"))
        print(newNode.get_name())
        super(ParamBlock, self).__init__(newNode)
        newNode.newProp("size", str(self.size))
        newNode.newProp("address", str(self.address))
        newNode.newProp("mem-size", str(self.memSize))
        
    def setSize(self,size):
        self.xmlNode.setProp("size", str(int(size)))
        self.size = size
        
    def setAddress(self,address):
        self.xmlNode.setProp("address", str(int(address)))
        self.address = address
        
    def setMemSize(self,memSize):
        self.xmlNode.setProp("mem-size", str(int(memSize)))
        self.memSize = memSize
        
    def getParamRegisters(self):
        paramRegisters = []
        for registerNode in self.getRegisterNodes():
            paramRegister = ParamRegister()
            paramRegister.initWithParamRegisterNode(registerNode)
            paramRegisters.append(paramRegister)
        return paramRegisters
    
    def getParamIORegisters(self):
        paramIORegisters = []
        for registerNode in self.getIORegisterNodes():
            paramRegister = ParamIORegister()
            paramRegister.initWithParamIORegisterNode(registerNode)
            paramIORegisters.append(paramRegister)
        return paramIORegisters
    
                
class DesignBlock(Block):
            
    def __init__(self, xmlNode):
        super(DesignBlock, self).__init__(xmlNode)
        self.generateFesaProperty = False
        self.fesaPropertyName = ""
        self.fesaGetServerActionName = ""
        self.fesaSetServerActionName = ""
    
        if self.xmlNode.hasProp("fesaPropertyName"):
            self.fesaPropertyName = xmlNode.prop("fesaPropertyName")
        else:
            self.fesaPropertyName = self.name
            
        if self.xmlNode.hasProp("generateFesaProperty"): #SilecsHEader does not have this attribute 
            self.generateFesaProperty = xsdBooleanToBoolean(xmlNode.prop("generateFesaProperty"))

        if self.xmlNode.hasProp("fesaGetServerActionName"):
            self.fesaGetServerActionName = xmlNode.prop("fesaGetServerActionName")
        else:
            self.fesaGetServerActionName = 'Get' + self.fesaPropertyName

        if self.xmlNode.hasProp("fesaSetServerActionName"):
            self.fesaSetServerActionName = xmlNode.prop("fesaSetServerActionName")
        else:
            self.fesaSetServerActionName = 'Set' + self.fesaPropertyName

    def getFesaName(self):
        return self.fesaPropertyName
    
    def getDesignMEMRegisters(self):
        designRegisters = []
        for registerNode in self.getMEMRegisterNodes():
            designRegisters.append(DesignRegister(registerNode))
        return designRegisters
        
    def getDesignIORegisters(self):
        designIORegisters = []
        for registerNode in self.getIORegisterNodes():
            designIORegisters.append(DesignRegister(registerNode))
        return designIORegisters
    
    def getDesignRegisters(self):
        return self.getDesignMEMRegisters() + self.getDesignIORegisters()
