#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
from iecommon import *

class SilecsDesign(object):
    
    def __init__(self):
        self.xmlNode = None
        self.name = ""
        self.version = ""

    def initFromXMLNode(self,xmlNode):
        self.xmlNode = xmlNode
        self.name = xmlNode.prop("silecs-design-name")
        self.version = xmlNode.prop("silecs-design-version")
        
    def getNameCapitalized(self):
        return iecommon.capitalizeString(self.name)