#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
from iecommon import *

class Device(object):

    def __init__(self):
        self.xmlNode = None
        self.silecsDeviceLabel = ""
        self.silecsDesignRef = ""
        self.fesaDeviceName = ""
        self.fesaFecName = ""
        self.silecsDesign = None #reference to SIlecsDesign object

            # not in init, in order to fake a silecsheader
    def initFromXMLNode(self,xmlNode):
        self.name = xmlNode.prop("silecs-design-name")
        self.version = xmlNode.prop("silecs-design-version")
        self.silecsDeviceLabel = xmlNode.prop("silecs-device-label")
        self.silecsDesignRef = xmlNode.prop("silecs-design-ref")
        if xmlNode.hasProp("fesa-device-name"):
            self.fesaDeviceName = xmlNode.prop("fesa-device-name")
        if xmlNode.hasProp("fesa-fec-name"):
            self.fesaFecName = xmlNode.prop("fesa-fec-name")
        #xmlNode.shellPrintNode()

    def getSilecsDeviceLabelCapitalizedNoUnderscore(self):
        return iecommon.capitalizeString(self.silecsDeviceLabel.replace("-","_"))