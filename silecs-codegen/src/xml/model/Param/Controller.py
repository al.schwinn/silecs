#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from model.Class.Class import ParamClass
from model.Deploy.Controller import Controller
from Mapping import *
import libxml2

class ParamController(object):

    def __init__(self):
        self.hostName = ""
        self.domain = ""
        self.system = ""
        self.model = ""
        self.protocol = ""
        self.brand = ""
        self.address = { MEM_TYPE: int(-1), DI_TYPE: int(-1), DO_TYPE: int(-1), AI_TYPE: int(-1), AO_TYPE: int(-1) }
        self.usedMem = ""
        self.xmlNode = None
        self.paramClasses = []

    def initAttributes(self, xmlNode):
        self.xmlNode = xmlNode
        self.hostName = xmlNode.prop("plc-name")
        self.domain = xmlNode.prop("domain")
        self.system = self.xmlNode.prop('plc-system')
        self.model = self.xmlNode.prop('plc-model')
        self.protocol = self.xmlNode.prop('protocol')
        self.brand = self.xmlNode.prop("plc-brand")
        self.address[MEM_TYPE] = int(float(self.xmlNode.prop("address")))
        self.address[DI_TYPE]  = int(float(self.xmlNode.prop("DI-address")))
        self.address[DO_TYPE]  = int(float(self.xmlNode.prop("DO-address")))
        self.address[AI_TYPE]  = int(float(self.xmlNode.prop("AI-address")))
        self.address[AO_TYPE]  = int(float(self.xmlNode.prop("AO-address")))
        self.usedMem = self.xmlNode.prop("used-mem")
            
    def initFromXMLNode(self, xmlNode):
        self.initAttributes(xmlNode)
        for classNode in self.xmlNode.xpathEval('SILECS-Class'):
            paramClass = ParamClass()
            paramClass.initWithParamClassNode(classNode)
            self.paramClasses.append(paramClass)
            
    def initFromDeployController(self, controller):
        newNode = libxml2.newNode("SILECS-Mapping")
        newNode.newProp("plc-name", controller.hostName)
        newNode.newProp("plc-brand", controller.brand)
        newNode.newProp("plc-system", str(controller.system))
        newNode.newProp("plc-model", controller.model)
        newNode.newProp("protocol", controller.protocol)
        newNode.newProp("address", str(int(controller.address[MEM_TYPE])))
        newNode.newProp("DI-address", str(int(controller.address[DI_TYPE])))
        newNode.newProp("DO-address", str(int(controller.address[DO_TYPE])))
        newNode.newProp("AI-address", str(int(controller.address[AI_TYPE])))
        newNode.newProp("AO-address", str(int(controller.address[AO_TYPE])))

        newNode.newProp("domain", controller.domain)
        newNode.newProp("used-mem", "TODO")
        self.initAttributes(newNode)
    
    def addParamClass(self,paramClass):
        self.xmlNode.addChild(paramClass.xmlNode)
        self.paramClasses.append(paramClass)
        