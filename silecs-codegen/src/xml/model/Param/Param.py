#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2
import datetime

from model.Param.Controller import ParamController

class Param(object):

    def __init__(self):
        self.xmlNode = None
        self.mappingNode = None
        self.silecsVersion = ""
        self.owner = ""
        self.genDate = ""
        self.checksum = 0
        self.controller = None
        self.deployName ="" #need to be explicitly set by codegen
        self.deployVersion ="" #need to be explicitly set by codegen

    def initBaseAttributes(self, paramDOM):
        paramNodes = paramDOM.xpathEval('/SILECS-Param')
        if len(paramNodes) != 1:
            raise Exception( "Error: param-document is corrupt" )
        self.xmlNode = paramNodes[0]
        ownderNodes = self.xmlNode.xpathEval('Mapping-Info/Owner')
        if len(ownderNodes) != 1:
            raise Exception( "Error: param-document is corrupt" )
        generationNodes = self.xmlNode.xpathEval('Mapping-Info/Generation')
        if len(generationNodes) != 1:
            raise Exception( "Error: param-document is corrupt" )
        deploymentNodes = self.xmlNode.xpathEval('Mapping-Info/Deployment')
        if len(deploymentNodes) != 1:
            raise Exception( "Error: param-document is corrupt" )
        self.silecsVersion = self.xmlNode.prop("silecs-version")
        self.owner = ownderNodes[0].prop("user-login")
        self.genDate = generationNodes[0].prop("date")
        self.checksum = int(deploymentNodes[0].prop('checksum'))
        
    def initFromXMLNode(self,paramDOM):
        self.initBaseAttributes(paramDOM)
        mappingNodes = self.xmlNode.xpathEval('SILECS-Mapping')
        if len(mappingNodes) != 1:
            raise Exception( "Error: param-document is corrupt" )
        self.mappingNode = mappingNodes[0]
        self.controller = ParamController()
        self.controller.initFromXMLNode(self.mappingNode)
        
    def addController(self,deployController):
        if self.controller != None:
            raise Exception("Cannot add controller '" + deployController.hostName + "'. A controller named '" + self.controller.hostName  + "' is already registered")
        paramController = ParamController()
        paramController.initFromDeployController(deployController)
        self.xmlNode.addChild(paramController.xmlNode)
        self.controller = paramController

                
    @staticmethod
    def createParam(silecsVersion, owner, checksum):
        paramDOM  = libxml2.newDoc(version='1.0')
        rootNode  = libxml2.newNode('SILECS-Param')
        rootNode.setProp("silecs-version", silecsVersion)
        paramDOM.addChild(rootNode)

        paramMappingInfoNode = libxml2.newNode("Mapping-Info")
        rootNode.addChild(paramMappingInfoNode)

        paramOwnerNode = libxml2.newNode('Owner')
        paramOwnerNode.setProp("user-login", owner)
        paramMappingInfoNode.addChild(paramOwnerNode)
        
        gen = libxml2.newNode('Generation')
        currentDate = str(datetime.datetime.now())
        gen.setProp("date",currentDate)
        paramMappingInfoNode.addChild(gen)
        
        deploy = libxml2.newNode('Deployment')
        deploy.setProp("checksum", str(checksum))
        paramMappingInfoNode.addChild(deploy)
        
        param = Param()
        param.initBaseAttributes(rootNode)
        #param.mappingNode = libxml2.newNode('SILECS-Mapping')
        #rootNode.addChild(param.mappingNode)
        return param
    
