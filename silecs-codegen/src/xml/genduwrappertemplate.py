#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import string
import iecommon
from model.Class.Register import DesignRegister
from model.Class.Block import DesignBlock

#=================================================================
# Deploy Unit Class
#=================================================================
deployUnitFileTemplate = string.Template("""/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef ${deployNameUpper}_H_
#define ${deployNameUpper}_H_

#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>

${deployCustomInclude}
namespace ${deployNameCapitalized}
{

typedef SilecsWrapper::DeployConfig DeployConfig;
typedef SilecsWrapper::DesignConfig DesignConfig;

class DeployUnit : public SilecsWrapper::DeployUnit
{
public:

    /*!
     * \\brief Use this method to create/get the unique instance of the Deploy Unit.
     *
     * \param logTopics This parameter can be used to enable/disable log topics
     * valid topics are ERROR[,INFO,DEBUG,SETUP,ALLOC,RECV,SEND,COMM,DATA,LOCK].
     *
     * \param globalConfig This parameter can be used to pass different parameters to
     * the library. I.e. enabling automatic connection.
     */
    static DeployUnit* getInstance(const std::string& logTopics = "",
            const SilecsWrapper::DeployConfig& globalConfig = SilecsWrapper::DeployConfig())
    {
        if (_instance == NULL)
        {
            _instance = new DeployUnit(logTopics, globalConfig);
        }
        else
        {
            if (logTopics.empty() == false)
            {
                _instance->getService()->setLogTopics(logTopics);
            }
        }
        return dynamic_cast<DeployUnit*>(_instance);
    }
    
    /*!
     * \\brief Use this method to create/get the unique instance of the Deploy Unit.
     *
     * \param globalConfig This parameter can be used to pass different parameters to
     * the library. I.e. enabling automatic connection.
     */
    static DeployUnit* getInstance(const SilecsWrapper::DeployConfig& globalConfig)
    {
        return getInstance("", globalConfig);
    }
    
${designGetters}\
private:

${designMemberDeclarations}\

    DeployUnit(const std::string& logTopics, const SilecsWrapper::DeployConfig& globalConfig) :
                    SilecsWrapper::DeployUnit("${deployNameCapitalized}", "${deployVersion}", logTopics, globalConfig)
    {
${constructorBody}\
    }
    
    ~DeployUnit()
    {
${destructorBody}\
    }
};

${controllerClass}

} /* namespace ${deployNameCapitalized} */

#endif /* ${deployNameUpper}_H_ */
""")

deployIncludeTemplate = string.Template("""\
#include "${designNameCapitalized}.h"
""")

designGetterTemplate = string.Template("""\
    /*!
     * \\brief Return pointer to the deployed design ${designName}.
     */
    ${designNameCapitalized}::Design* get${designNameCapitalized}()
    {
        return _${designName};
    }

""")

designAllocation = string.Template("""\
        // Construct Design ${designNameCapitalized}
        _${designName} = new ${designNameCapitalized}::Design((SilecsWrapper::DeployUnit*) this);
        
""")

designDeallocation = string.Template("""\
        delete _${designName};
""")

deployUnitMembersDeclaration = string.Template("""\
    ${designNameCapitalized}::Design* _${designName};
""")
        
def generateControllerFile(deployName, deployVersion, deployCustomInclude, constructorBody, destructorBody, designGetters, designMemberDeclarations, controllerClass):
    deployMapping = {'deployVersion' : deployVersion,
                     'deployNameCapitalized' :  iecommon.capitalizeString(deployName),
                     'deployNameUpper' :  deployName.upper(),
                     'deployCustomInclude' : deployCustomInclude,
                     'constructorBody' : constructorBody,
                     'destructorBody' : destructorBody,
                     'designGetters' : designGetters,
                     'designMemberDeclarations' : designMemberDeclarations,
                     'controllerClass' : controllerClass
                   }
    return deployUnitFileTemplate.substitute(deployMapping)

#=================================================================
# Design Class
#=================================================================

designFileTemplate = string.Template("""/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef ${designNameUpper}_H_
#define ${designNameUpper}_H_

#include <silecs-communication/wrapper/Block.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>
#include <silecs-communication/wrapper/Device.h>

namespace ${designNameCapitalized}
{

${classDeclarations}
} /* namespace ${designNameCapitalized} */

#endif /* ${designNameUpper}_H_ */
""")

designClassTemplate = string.Template("""
class Design : public SilecsWrapper::Design
{
public:

    Design(SilecsWrapper::DeployUnit *deployUnit) :
                    SilecsWrapper::Design("${designNameCapitalized}", "${designVersion}", deployUnit)
    {
    }

    ~Design()
    {
    }
};
""")

def getDesignClass(designName,designVersion):
    text = ""
    designClassmap = {'designName' : designName,
                      'designNameCapitalized' : iecommon.capitalizeString(designName),
                      'designVersion' : designVersion}
    return designClassTemplate.substitute(designClassmap)

designControllerInit = string.Template("""
            _controllerMap.insert(
                std::pair<std::string, Controller*>("${controllerName}",
                        new Controller("${controllerName}", "${controllerDomain}", this)));
""")

designReceiveTemplate = string.Template("""\
    /*!
     * \\brief Receive ${blockName} blocks from all connected controllers.
     */
    void receive${blockNameCapitalized}AllControllers()
    {
        _silecsCluster->recv("${blockName}");
    }
    
""")

designSendTemplate = string.Template("""\
    /*!
     * \\brief Send ${blockName} blocks to all connected controllers.
     */
    void send${blockNameCapitalized}AllControllers()
    {
        _silecsCluster->send("${blockName}");
    }
    
""")

def getDesignSendRecvBlocks(blockList):
    text = ""
    for blockNode in blockList:
        block = DesignBlock(blockNode)
        map = {'blockName' : block.name,
               'blockNameCapitalized' : block.getNameCapitalized()}
        if block.isReadable():
            text += designReceiveTemplate.substitute(map)
        if block.isWritable():
            text += designSendTemplate.substitute(map)
    return text

#=================================================================
# Controller Class
#=================================================================

controllerClassTemplate = string.Template("""
class Controller : public SilecsWrapper::Controller
{
public:
    Controller(SilecsWrapper::Design *design, const std::string parameterFile) :
                    SilecsWrapper::Controller("${controllerName}", "${controllerDomain}", design, parameterFile)
    {
${constructorBody}\
    }

    ~Controller()
    {
        map<std::string, SilecsWrapper::Device*>::iterator it;
        for (it = _deviceMap.begin(); it != _deviceMap.end(); it++)
        {
            delete it->second;
        }
    }

    /*!
     * \\brief Return pointer to the requested device.
     * \param label Device label.
     */
    SilecsWrapper::Device* getDevice(const std::string& label)
    {
        if (_deviceMap.find(label) != _deviceMap.end())
        {
            return _deviceMap[label];
        }
        throw Silecs::SilecsException(__FILE__, __LINE__, Silecs::PARAM_UNKNOWN_DEVICE_NAME, label);
    }

    std::map<std::string, SilecsWrapper::Device*>& getDeviceMap()
    {
        return _deviceMap;
    }
    
${deviceGetter}\
private:
    std::map<std::string, SilecsWrapper::Device*> _deviceMap;
};
""")

def getControllerClass(constructorBody,deviceGetter,controllerName,controllerDomain):
    text = ""
    designClassmap = {'constructorBody' : constructorBody,
                      'deviceGetter' : deviceGetter,
                        'controllerName' : controllerName,
                        'controllerDomain' : controllerDomain}
    return controllerClassTemplate.substitute(designClassmap)

controllerDeviceInit = string.Template("""\
            _deviceMap.insert(std::pair<std::string, SilecsWrapper::Device*>("${deviceName}", new SilecsWrapper::Device("${deviceName}", this)));
""")

deviceGetterTemplate = string.Template("""\
    /*!
     * \\brief Get pointer to device ${deviceName}.
     */
    SilecsWrapper::Device* get${deviceNameCapitalizedNoUndercore}()
    {
        return _deviceMap["${deviceName}"];
    }
    
""")

controllerReceiveTemplate = string.Template("""\
    /*!
     * \\brief Receive ${blockName} blocks from all devices of current controller.
     */
    void receive${blockNameCapitalized}AllDevices()
    {
        _silecsPLC->recv("${blockName}");
    }
    
""")

controllerSendTemplate = string.Template("""\
    /*!
     * \\brief Send ${blockName} blocks to all devices of current controller.
     */
    void send${blockNameCapitalized}AllDevices()
    {
        _silecsPLC->send("${blockName}");
    }
    
""")

def getControllerSendRecvBlocks(blockList):
    text = ""
    for blockNode in blockList:
        block = DesignBlock(blockNode)
        map = {'blockName' : block.name,
               'blockNameCapitalized' : block.getNameCapitalized()}
        if block.isReadable():
            text += controllerReceiveTemplate.substitute(map)
        if block.isWritable():
            text += controllerSendTemplate.substitute(map)
    return text

#=================================================================
# Device Class
#=================================================================

deviceClassTemplate = string.Template("""\
class Device : public SilecsWrapper::Device
{
public:
    Device(const std::string& label, SilecsWrapper::Controller *controller) :
                    SilecsWrapper::Device(label, controller)
    {
    }

    ~Device()
    {
    }
    
${blockGetter}
${sendRecvBlocks}
};
""")

def getDeviceClass(blockGetter,sendRecvBlocks):
    text = ""
    designClassmap = {'blockGetter' : blockGetter,
                      'sendRecvBlocks' : sendRecvBlocks}
    return deviceClassTemplate.substitute(designClassmap)


blockGetterTemplate = string.Template("""\
    /*!
     * \\brief Get ${blockName} block. 
     * \param block ${blockNameCapitalized} reference where to store returned value.
     */
    void get${blockNameCapitalized}(${blockNameCapitalized} &block)
    {
${blockInitialization}\
    }
    
""")

blockSetterTemplate = string.Template("""\
    /*!
     * \\brief Set ${blockName} block. 
     * \param block ${blockNameCapitalized} reference from where value are copied.
     */
    void set${blockNameCapitalized}(${blockNameCapitalized} &block)
    {
${blockInitialization}\
    }
    
""")

matrixRegisterAssignementGetter = string.Template("""\
        // Copy register ${regName}
        ${cType} *__${regName} = new ${cType}[block.${regName}Dim1 * block.${regName}Dim2];
        getSilecsDevice()->getRegister("${regName}")->getVal${silecsTypeCapitalized}Array2D(__${regName}, block.${regName}Dim1, block.${regName}Dim2);
        block.set${regNameCapitalized}(__${regName});
        delete[] __${regName};
""")
arrayRegisterAssignementGetter = string.Template("""\
        // Copy register ${regName}
        ${cType} *__${regName} = new ${cType}[block.${regName}Dim1];
        getSilecsDevice()->getRegister("${regName}")->getVal${silecsTypeCapitalized}Array(__${regName}, block.${regName}Dim1);
        block.set${regNameCapitalized}(__${regName});
        delete[] __${regName};
""")
scalarRegisterAssignementGetter = string.Template("""\
        // Copy register ${regName}
        block.set${regNameCapitalized}(getSilecsDevice()->getRegister("${regName}")->getVal${silecsTypeCapitalized}());
""")

matrixRegisterAssignementSetter = string.Template("""\
        // Copy register ${regName}
        ${cType} *__${regName} = new ${cType}[block.${regName}Dim1 * block.${regName}Dim2];
        block.get${regNameCapitalized}(__${regName});
        getSilecsDevice()->getRegister("${regName}")->setVal${silecsTypeCapitalized}Array2D(__${regName}, block.${regName}Dim1, block.${regName}Dim2);
        delete[] __${regName};
""")
arrayRegisterAssignementSetter = string.Template("""\
        // Copy register ${regName}
        ${cType} *__${regName} = new ${cType}[block.${regName}Dim1];
        block.get${regNameCapitalized}(__${regName});
        getSilecsDevice()->getRegister("${regName}")->setVal${silecsTypeCapitalized}Array(__${regName}, block.${regName}Dim1);
        delete[] __${regName};
""")
scalarRegisterAssignementSetter = string.Template("""\
        // Copy register ${regName}
        getSilecsDevice()->getRegister("${regName}")->setVal${silecsTypeCapitalized}(block.get${regNameCapitalized}());
""")

def getDeviceBlockGetterSetter(block):
    text = ""
    if block.isReadable():
        blockInitialization = ""
        for register in block.getDesignRegisters():
            map = {'regName' : register.name,
                   'regNameCapitalized' : register.getNameCapitalized(),
                   'silecsTypeCapitalized' : register.getSilecsTypeCapitalized(),
                   'cType' : register.getCType()}
            if register.isArray2D():
                blockInitialization += matrixRegisterAssignementGetter.substitute(map)
            elif register.isArray():
                blockInitialization += arrayRegisterAssignementGetter.substitute(map)
            else: #scalar
                blockInitialization += scalarRegisterAssignementGetter.substitute(map)
        
        blockMap = {'blockName' : block.name, 
                    'blockInitialization' : blockInitialization,
                    'blockNameCapitalized' : block.getNameCapitalized()}
        text +=  blockGetterTemplate.substitute(blockMap)
    if block.isWritable():
        blockInitialization = ""
        for register in block.getDesignRegisters():
            map = {'regName' : register.name,
                   'regNameCapitalized' : register.getNameCapitalized(),
                   'silecsTypeCapitalized' : register.getSilecsTypeCapitalized(),
                   'cType' : register.getCType()}
            if register.isArray2D():
                blockInitialization += matrixRegisterAssignementSetter.substitute(map)
            elif register.isArray():
                blockInitialization += arrayRegisterAssignementSetter.substitute(map)
            else: #scalar
                blockInitialization += scalarRegisterAssignementSetter.substitute(map)
        
        blockMap = {'blockName' : block.name, 
                    'blockInitialization' : blockInitialization,
                    'blockNameCapitalized' : block.getNameCapitalized()}
        text +=  blockSetterTemplate.substitute(blockMap)
    return text

deviceReceiveTemplate = string.Template("""\
    /*!
     * \\brief Receive ${blockName} block for current device.
     */
    void receive${blockNameCapitalized}()
    {
        _silecsDevice->recv("${blockName}");
    }
    
""")

deviceSendTemplate = string.Template("""\
    /*!
     * \\brief Send ${blockName} blocks to current device.
     */
    void send${blockNameCapitalized}()
    {
        _silecsDevice->send("${blockName}");
    }
    
""")

def getDeviceSendRecvBlocks(blockList):
    text = ""
    for blockNode in blockList:
        block = DesignBlock(blockNode)
        map = {'blockName' : block.name,
               'blockNameCapitalized' : block.getNameCapitalized()}
        if block.isReadable():
            text += deviceReceiveTemplate.substitute(map)
        if block.isWritable():
            text += deviceSendTemplate.substitute(map)
    return text

#=================================================================
# Block Class
#=================================================================

blockClassTemplate = string.Template("""
class ${blockNameCapitalized} : public SilecsWrapper::Block
{
public:
    /*!
     * \\brief ${blockName} constructor. It creates an empty block.
     */
    ${blockNameCapitalized}() :
                    SilecsWrapper::Block("${blockName}")${registerInitializerList}
    {
    }

    ~${blockNameCapitalized}()
    {
    }
${registerGetterSetter}\
${registersDimentionsDeclaration}\
private:
${registersDeclaration}
};
""")

def getBlockClass(block,registerInitializerList,registerGetterSetter,registersDimentionsDeclaration,registersDeclaration):
    map = {'blockName' : block.name,
           'registerInitializerList' : registerInitializerList,
           'blockNameCapitalized' : block.getNameCapitalized(),
           'registerGetterSetter' : registerGetterSetter,
           'registersDimentionsDeclaration' : registersDimentionsDeclaration,
           'registersDeclaration' : registersDeclaration}
    return blockClassTemplate.substitute(map)

registerInitializerListTemplate = string.Template("""\
,
                    ${registerName}(0)""")

def getBlockInitializerList(block):
    text = ""
    for register in block.getDesignRegisters():
        if register.isScalar():
            text += registerInitializerListTemplate.substitute({'registerName' : register.name})
    return text
    

matrixRegisterGetterTemplate = string.Template("""
    /*!
     * \\brief Get 2D array ${registerName} register.
     * \param value buffer where the value will be stored.
     */
    void get${registerNameCapitalized}(${cType}* value) const
    {
        memcpy(value, static_cast<const void *>(${registerName}), ${registerName}Dim1 * ${registerName}Dim2);
    }
""")

matrixRegisterSetterTemplate = string.Template("""
    /*!
     * \\brief Get array ${registerName} register.
     * \param value buffer where the value will be stored.
     */
    void set${registerNameCapitalized}(${cType}* value) const
    {
        memcpy((void *) ${registerName}, value, ${registerName}Dim1 * ${registerName}Dim2);
    }
""")

stringArrayRegisterGetterTemplate = string.Template("""
    /*!
     * \\brief Get std::string ${registerName} register.
     * \param value buffer where the value will be stored.
     */
    void get${registerNameCapitalized}(std::string* value) const
    {
        for (std::size_t i = 0; i <  ${registerName}Dim1; i++)
        {
            value[i] =  ${registerName}[i];
        }
    }
""")

stringArrayRegisterSetterTemplate = string.Template("""
    /*!
     * \\brief Set std::string ${registerName} register.
     * \param value to be set.
     */
    void set${registerNameCapitalized}(const std::string* value)
    {
        for (std::size_t i = 0; i < ${registerName}Dim1; i++)
        {
            ${registerName}[i] = value[i];
        }
    }
""")

arrayRegisterGetterTemplate = string.Template("""
    /*!
     * \\brief Set array ${registerName} register.
     * \param value to be set.
     */
    void get${registerNameCapitalized}(${cType}* value) const
    {
        memcpy(value, (void *) ${registerName}, ${registerName}Dim1);
    }
""")

arrayRegisterSetterTemplate = string.Template("""
    /*!
     * \\brief Set 2D array ${registerName} register.
     * \param value to be set.
     */
    void set${registerNameCapitalized}(${cType}* value) const
    {
        memcpy((void *) ${registerName}, value, ${registerName}Dim1);
    }
""")

scalarRegisterGetterTemplate = string.Template("""
    /*!
     * \\brief Get ${registerName} register.
     * \\return value.
     */
    ${cType} get${registerNameCapitalized}() const
    {
        return ${registerName};
    }
""")

scalarRegisterSetterTemplate = string.Template("""
    /*!
     * \\brief Set ${registerName} register.
     * \param value to be set.
     */
    void set${registerNameCapitalized}(${cType} value)
    {
        ${registerName} = value;
    }
""")

def getRegisterGetterSetter(block):
    text = ""
    for register in block.getDesignRegisters():
        map = {'registerName' : register.name,
               'registerNameCapitalized' : register.getNameCapitalized(),
               'cType' : register.getCType()}
        
        if register.isArray2D():
            text += matrixRegisterGetterTemplate.substitute(map)
            text += matrixRegisterSetterTemplate.substitute(map)
        elif register.isArray():
            if register.isStringType():
                text += stringArrayRegisterGetterTemplate.substitute(map)
                text += stringArrayRegisterSetterTemplate.substitute(map)
            else:
                text += arrayRegisterGetterTemplate.substitute(map)
                text += arrayRegisterSetterTemplate.substitute(map)
        else:
            if register.isStringType():
                map['cType'] = "const std::string&"
            text += scalarRegisterGetterTemplate.substitute(map)
            text += scalarRegisterSetterTemplate.substitute(map)
    return text

registersDimetionsDeclarationTemplate = string.Template("""\
    static const std::size_t ${registerName}Dim${index} = ${dimention};
""")

def getRegistersDimetionsDeclaration(block):
    text = ""
    for register in block.getDesignRegisters():
        map = {'registerName' : register.name}
        if register.isArray2D():
            map['index'] = "2"
            map['dimention'] = register.dim2
            text += registersDimetionsDeclarationTemplate.substitute(map)
            map['index'] = "1"
            map['dimention'] = register.dim1
            text += registersDimetionsDeclarationTemplate.substitute(map)
        elif register.isArray():
            map['index'] = "1"
            map['dimention'] = register.dim1
            text += registersDimetionsDeclarationTemplate.substitute(map)
    return text

matrixRegistersDeclarationTemplate = string.Template("""\
    ${cType} ${registerName}[${registerName}Dim1][${registerName}Dim2];
""")

arrayRegistersDeclarationTemplate = string.Template("""\
    ${cType} ${registerName}[${registerName}Dim1];
""")

scalarRegistersDeclarationTemplate = string.Template("""\
    ${cType} ${registerName};
""")

def getRegistersDeclaration(block):
    text = ""
    for register in block.getDesignRegisters():
        map = {'registerName' : register.name,
               'cType' : register.getCType()}
        if register.isArray2D():
            text += matrixRegistersDeclarationTemplate.substitute(map)
        elif register.isArray():
            text += arrayRegistersDeclarationTemplate.substitute(map)
        else:
            text += scalarRegistersDeclarationTemplate.substitute(map)
    return text
