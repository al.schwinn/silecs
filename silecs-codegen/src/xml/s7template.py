#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Description:
# This module implement the methods to build each components of the sources.
# A component source consists of one 'parameterizable' text that can
# be specialized on call to append the string to be stored in the file.
#

#=========================================================================
# Global definition
#=========================================================================

#-------------------------------------------------------------------------
# Hash-Table

whichSimaticFormat = {
	'uint8'   : 'BYTE',
	'int8'    : 'CHAR',
	'uint16'  : 'WORD',
	'int16'   : 'INT',
	'uint32'  : 'DWORD',
	'int32'   : 'DINT',
	'float32' : 'REAL',
    'string'   : 'STRING',
#	'uint64'  : '8',	not supported with PLC
#	'int64'   : '8',	not supported with PLC
#	'float64' : '8',    not supported with PLC
	'date'    : 'DT',
	'char'	:'CHAR',
    'byte'	:'BYTE',
    'word'	:'WORD',
    'dword'	:'DWORD',
    'int'	:'INT',
    'dint'	:'DINT',
    'real'	:'REAL',
    'dt'	:'DT'}


#=========================================================================
# STL Source template (.scl file)
#=========================================================================
firstBlockUDT = """\r
(* ---------------------------------------------------------------------\r
 * %s/ v%s\r
 * BLOCK Type definition\r
 * ---------------------------------------------------------------------\r
 *)\r
"""

blockUDT = """TYPE _%s_%s\r
AUTHOR:    %s\r
FAMILY:    SILECS\r
NAME:      UDTB\r
    STRUCT\r
%s\r
    END_STRUCT;\r
END_TYPE\r
\r
"""

regScalar = """        %s: %s%s;\r
"""

regArray  = """        %s: ARRAY[0..%d] OF %s%s;\r
"""

regArray2d =  """        %s: ARRAY[0..%d, 0..%d] OF %s%s;\r
"""

regFixedStringLen = """		%s: STRING[16]%s;\r
"""

firstBlock = """\r
(* ---------------------------------------------------------------------\r
 * %s/ v%s\r
 * BLOCK instance definition\r
 * ---------------------------------------------------------------------\r
 *)\r
"""

DB_TIAP = """DATA_BLOCK %s_%s\r
{ S7_Optimized_Access := 'FALSE' }\r
AUTHOR:    %s\r
FAMILY:    SILECS\r
NAME:      %s\r
STRUCT\r%s\r
END_STRUCT;\r
BEGIN\r
END_DATA_BLOCK\r
\r
"""

DB_STEP7 = """(* %s_%s ...........................................*)\r
DATA_BLOCK DB%s\r
{ S7_Optimized_Access := 'FALSE' }\r
AUTHOR:    %s\r
FAMILY:    SILECS\r
NAME:      %s\r
STRUCT\r
%s\r
END_STRUCT;\r
BEGIN\r
END_DATA_BLOCK\r
\r
"""

device = """    %s: _%s_%s;\r
"""

#=========================================================================
# SYMBOLS Source template (.sdf file)
#=========================================================================

diagSymbol = """"SILECS_HEADER",%s
"""

UDTSymbol  = """"_%s_%s","UDT %d","UDT %d","%s"
"""

DBSymbol   = """"%s_%s","DB %d","DB %d","%s"
"""

#=========================================================================
# Sub-function
#=========================================================================

# ------------------------------------------------------------------------
# Utils 
# ------------------------------------------------------------------------
def toCharArray(strg):
    charArray = ''
    for c in strg: charArray += "'%c'," %c
    return charArray

def toByteArray(strg):
    byteArray = ''
    for c in strg: byteArray += "16#%02x," %(ord(c))
    return byteArray

def toWordArray(strg):
    wordArray = ''
    for i in range(0, len(strg), 2):
        lsb = ord(strg[i])
        try: hsb = ord(strg[i+1]) 
        except: hsb = 0
        wordArray += "16#%02x%02x," %(hsb,lsb)
    return wordArray

# ------------------------------------------------------------------------
# STL code generation
# ------------------------------------------------------------------------

# REGISTER definition ----------------------------------------------------
def stlRegister(regName, regFormat, regDim, regDim2, regVal, regLen=1):
    if regLen > 1:    # register is a string or an array of strings
        strLen = whichSimaticFormat[regFormat]+'['+str(regLen)+']'
        if regDim == 1 and regDim2 == 1:    # scalar register
            return regScalar %(regName, strLen, regVal)        
        elif regDim > 1 and regDim2 == 1:    # array register
            return regArray %(regName, regDim-1, strLen, regVal)
        else:    # dim1>=1 and for whatever dim2, use double array syntax
            return regArray2d %(regName, regDim-1, regDim2-1, strLen, regVal)
    else:
        if regDim == 1 and regDim2 == 1:
            return regScalar %(regName, whichSimaticFormat[regFormat], regVal)
        elif regDim > 1 and regDim2 == 1:
            return regArray %(regName, regDim-1, whichSimaticFormat[regFormat], regVal)
        else:    # dim1>=1 and for whatever dim2, use double array syntax
            return regArray2d %(regName, regDim-1, regDim2-1, whichSimaticFormat[regFormat], regVal)

#This method is used to add default value to the diagnostic registers
def stlRegisterValue(regName, owner, date, checksum, silecsversion):
	if regName == '_version': return " := 'SILECS_%s'" %silecsversion
	if regName == '_checksum' : return " := DW#16#%x" %checksum
	if regName == '_user'   : return " := '%s'" %owner
	if regName == '_date'   : return " := %s" %date
	return ''    #all other registers cannot have default assignment for the time being

# BLOCK type definition (UDT) --------------------------------------------
def stlBlockUDT(owner, className, classVersion, blockName, regList, isFirst):
    if isFirst:
        return firstBlockUDT %(className, classVersion) + blockUDT %(className, blockName, owner, regList)
    else:
        return blockUDT %(className, blockName, owner, regList)
    
# BLOCK_MODE: data-block instance ----------------------------------------
def stlDevice(devLabel, className, blockName):
    return device %(devLabel, className, blockName)
	    
# DEVICE_MODE: data-block instance ---------------------------------------
def stlBlock(className, blockName):
    return device %(blockName, className, blockName)     
        
def generateBlock(owner, className, classVersion, system, DBnumber, blockName, blockList, isFirst, protocol):
    srcCore = ""
    mode = ''    
    if protocol == 'BLOCK_MODE':
        mode = 'BLK_MODE'
    elif protocol == 'DEVICE_MODE':
        mode = 'DEV_MODE'
    else:
        raise Exception( "Unknown PLC-protocol defined: " + protocol )

    if (system == 'TIA-PORTAL'):
        srcCore = DB_TIAP %(className, blockName, owner, mode, blockList)
    elif (system == 'STEP-7'):
        srcCore = DB_STEP7 %(className, blockName, DBnumber, owner, mode, blockList)
    else:
        raise Exception( "Unknown PLC-system defined: " + system )

    if isFirst:
        return firstBlock %(className, classVersion) + srcCore
    else:
        return srcCore
	
# ------------------------------------------------------------------------
# SYMBOLS code generation
# ------------------------------------------------------------------------

# HEADER file ------------------------------------------------------------
def symHeader(baseAddress):
    return DBSymbol %('SILECS', 'HEADER', baseAddress, baseAddress, "SILECS Diagnostic data-block")

# UDT Type definition ----------------------------------------------------
def symBlockUDT(className, classVersion, blockName, number, isFirst):
    comment = ''
    if isFirst: comment = "[%s/%s] UDT symbol: _<class-name>_<block-name>" %(className, classVersion)
    return UDTSymbol %(className, blockName, number, number, comment)

# BLOCK_MODE: data-block symbol ----------------------------
def symBlockDB(className, classVersion, blockName, number, isFirst):
    comment = ''
    if isFirst: comment = "[%s/%s] DB symbol: <class-name>_<block-name>" %(className, classVersion)
    return DBSymbol %(className, blockName, number, number, comment)

# DEVICE_MODE : data-block symbol ----------------------------
def symDeviceDB(className, classVersion, deviceLabel, number, isFirst):
    comment = ''
    if isFirst: comment = "[%s/%s] DB symbol: <class-name>_<device-label | device-id>" %(className, classVersion)
    return DBSymbol %(className, deviceLabel, number, number, comment)
