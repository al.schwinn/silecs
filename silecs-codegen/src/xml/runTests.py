#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from test.testBase import *

import test.fesa.fillFESADeployUnitTest
import test.fesa.generateFesaDesignTest
import test.fesa.generateSourceCodeTest
import migration.runTests

import test.general.iecommonTest
import test.general.genplcsrcTest
import test.general.genParamTest
import test.general.genDuWrapperTest

def runTests():
    migration.runTests.runAllTests()
    test.fesa.fillFESADeployUnitTest.runTests()
    test.fesa.generateFesaDesignTest.runTests()
    test.fesa.generateSourceCodeTest.runTests()
    test.general.iecommonTest.runTests()
    test.general.genParamTest.runTests()
    test.general.genplcsrcTest.runTests()
    test.general.genDuWrapperTest.runTests()


    print("################################################")
    print("# Test suite finished - no failures detected ! #")
    print("################################################")

    sys.exit(0)

# ********************** module stand alone code **********************
if __name__ == "__main__":
    runTests()
