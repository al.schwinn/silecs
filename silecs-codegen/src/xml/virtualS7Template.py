#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Project: SILECS
# Author: F.Locci/ BE-CO
# Date: August 2012
# Last modification: Jannuary 2015
#
# Description:
# This module implement the methods to build each components of the sources.
# A component source consists of one 'parameterizable' text that can
# be specialized on call to append the string to be stored in the file.
#

import string
import struct
import iecommon

#-------------------------------------------------------------------------
# Hash-Table

whichRegisterFormat = {
    'uint8'   : 'uint8_t',
    'int8'    : 'int8_t',
    'uint16'  : 'uint16_t',
    'int16'   : 'int16_t',
    'uint32'  : 'uint32_t',
    'int32'   : 'int32_t',
    'float32' : 'float',
    'uint64'  : 'uint64_t',
    'int64'   : 'int64_t',
    'string'  : 'string',    
    'float64' : 'double',
    'date'    : 'double',
       'char'    :'int8_t',
    'byte'    :'uint8_t',
    'word'    :'uint16_t',
    'dword'    :'uint32_t',
    'int'    :'int16_t',
    'dint'    :'int32_t',
    'real'    :'float',
    'dt'    :'double'
}

#=========================================================================
# Virtual S7 DEPLOY-UNIT template
#=========================================================================

duDesignInclude = """#include "%s_%s.h"
"""

duConstructor = """DeployUnit() : SilecsServer::DeployUnit("%s", "%s", SilecsServer::S7Protocol, SilecsServer::%s, %s)
"""

duDesignAlloc = """    mapDesigns_["%s"] = new %s_%s::Design();
"""

duDesignDelete = """    delete mapDesigns_["%s"];
"""

duDesignGet = """    %s_%s::Design* get%s()
    {
        return dynamic_cast<%s_%s::Design*>(mapDesigns_["%s"]);
    }
    
"""

duHeader = """
/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef %s_H_
#define %s_H_

#include <silecs-virtual-controller/interface/DeployUnit.h>
#include <silecs-virtual-controller/interface/Design.h>
%s

namespace %s_%s
{

class DeployUnit : public SilecsServer::DeployUnit
{
public:

    %s    {
    %s    }

    ~DeployUnit()
    {
    %s    }

%s};

}

#endif
"""

mainCode = """
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>

#include <silecs-virtual-controller/core/SilecsSnap7Server.h>
#include "%s_%s.h"

class UserSnap7Server : public SilecsServer::SilecsSnap7Server
{
public:
    UserSnap7Server(%s::DeployUnit* du) : SilecsSnap7Server(du, true) {}
    virtual ~UserSnap7Server() {}

    virtual void userFunction(PSrvEvent PEvent)
    {
      // Implement the specific process control here!
      // Look at SILECS Wikis: 'Create a virtual controller' chapter 
    }
};

int main(int argc, char*argv[])
{
    %s::DeployUnit du;
    UserSnap7Server server(&du);
    if (server.startServer() < 0)
    {
        std::cout << "Failed to start the VC server: " << du.getName() << std::endl;
        return -1;
    }
    return 0;
}
"""


#=========================================================================
# Virtual S7 CLASS template
#=========================================================================
classScalarGetSet = """    
    /*!
     * \\brief Get %s register.
     * \\return value.
     */
    %s get%s() const
    {
        return structData_.%s;
    }

    /*!
     * \\brief Set %s register.
     * \\param value to be set.
     */
    void set%s(%s value)
    {
        structData_.%s = value;
    }
"""

classArrayGetSet = """    
    /*!
     * \\brief Get array %s register.
     * \\return value.
     */
    void get%s(%s* value) const
    {
        memcpy(value, &structData_.%s, %sDim1_ * %sDim2_ * sizeof(%s));
    }

    /*!
     * \\brief Set array %s register.
     * \\param value to be set.
     */
    void set%s(%s* value)
    {
        memcpy(&structData_.%s, value, %sDim1_ * %sDim2_ * sizeof(%s));
    }
"""

classScalarStringGetSet = """    
    /*!
     * \\brief Get %s register.
     * \\return value.
     */
    std::string get%s() const
    {
        size_t len = (size_t)structData_.%s[1];
        return std::string((char*)&(structData_.%s[2]), len);
    }

    /*!
     * \\brief Set %s register.
     * \\param value to be set.
     */
    void set%s(const std::string &value)
    {
        size_t len = (value.length() < %sLen_) ? value.length() : %sLen_;
        memcpy((char*)&(structData_.%s[2]), value.c_str(), len);
        structData_.%s[0] = char(0);
        structData_.%s[1] = char(len);
    }
"""

classArrayStringGetSet = """    
    /*!
     * \\brief Get std::string %s register.
     * \\param value buffer where the value will be stored.
     */
    void get%s(std::string* value) const
    {
        for (std::size_t i = 0; i < %sDim1_; i++)
        {
            size_t len = (size_t)structData_.%s[i][1];
            value[i].assign(&(structData_.%s[i][2]), len);
        }
    }

    /*!
     * \\brief Set std::string %s register.
     * \\param value to be set.
     */
    void set%s(std::string* value)
    {
        for (std::size_t i = 0; i < %sDim1_; i++)
        {
            size_t len = (value[i].length() < %sLen_) ? value[i].length() : %sLen_;
            memcpy(&(structData_.%s[i][2]), value[i].c_str(), len);
            structData_.%s[i][0] = char(0);
            structData_.%s[i][1] = char(len);
        }
    }
"""

classHeaderInit = """    set_version("%s");
        set_checksum(%s);
        set_user("%s");
        set_date(0.0);"""

classBlock = """
class %s : public SilecsServer::Block
{
public:
    /*!
     * \\brief %s constructor. It creates an empty block.
     */
    %s() : SilecsServer::Block("%s:%s")
    {
    %s
    }

    ~%s()
    {
    }
    %s
    virtual inline size_t getSize() const
    {
        return sizeof(structData_);
    }

    virtual void getData(unsigned char * data) const
    {
        memcpy(data, &structData_, this->getSize());
    }

    virtual void setData(unsigned char * data)
    {
        memcpy(&structData_, data, this->getSize());
    }

    virtual inline size_t getOffset() const { return %s; }

%s
private:

#pragma pack(push, 1)
    struct
    {
%s
    } structData_;
#pragma pack(pop)
    
};
"""

classArrayDim = """    static const std::size_t %sDim1_ = %s;
    static const std::size_t %sDim2_ = %s;
"""

classStringLen = """    static const std::size_t %sLen_ = %s;
"""

classCreateBlock = """
        blockMap_["%s:%s"] = new %s();"""

classDeleteBlock = """
        delete (blockMap_["%s:%s"]);"""

classDevice = """
class Device : public SilecsServer::Device
{
public:
    Device(const std::string& label, size_t number):SilecsServer::Device(label, number)
    {    %s    
    }

    ~Device()
    {    %s    
    }
};"""

classCreateDevice = """
        deviceMap_["%s:%s"] = new Device("%s:%s", %s);"""

classDeleteDevice = """
        delete(deviceMap_["%s:%s"]);"""

classDesign = """
class Design : public SilecsServer::Design
{
public:

    Design():SilecsServer::Design("%s", "%s")
    {    %s    
    }

    ~Design()
    {    %s    
    }

    /*!
     * \\brief Return pointer to the requested device.
     * \\param label Device label.
     */
    %s_%s::Device* getDevice(const std::string& label)
    {
        if (deviceMap_.find(label) != deviceMap_.end())
        {
            return dynamic_cast<%s_%s::Device*>(deviceMap_[label]);
        }
        return NULL;
    }
};"""

classHeader = """
/* Copyright CERN 2015
 *
 * WARNING: This code is automatically generated from your SILECS deploy unit document.
 * You should never modify the content of this file as it would break consistency.
 * Furthermore, any changes will be overwritten in the next code generation.
 * Any modification shall be done using the SILECS development environment
 * and regenerating this source code.
 */

#ifndef %s_%s_H_
#define %s_%s_H_

#include <silecs-virtual-controller/interface/Block.h>
#include <silecs-virtual-controller/interface/DeployUnit.h>
#include <silecs-virtual-controller/interface/Design.h>
#include <silecs-virtual-controller/interface/Device.h>

namespace %s_%s
{

class Design;
%s

%s

%s

} /* namespace */
#endif
"""


#=========================================================================
# Virtual S7 generation Sub-function
#=========================================================================

# SERVER MAIN code generation ---------------------------------------------
def vs7MainCode(deployName, deployVersion):
    fullDeployName = deployName + '_' + deployVersion.replace(".", "_")
    return mainCode %(deployName, deployVersion, fullDeployName, fullDeployName)
     
# DEPLOY-UNIT generation -------------------------------------------------
def vs7DuHeader(deployName, deployVersion, duConstructor, designIncludes, designAllocs, designDeletes, designGetters):
    deployVersion = deployVersion.replace(".", "_")
    return duHeader %(deployName.upper(), deployName.upper(), designIncludes, deployName, deployVersion, duConstructor, designAllocs, designDeletes, designGetters)  
   
def vs7DuDesignInclude(designName, designVersion):
    return duDesignInclude %(designName, designVersion)

def vs7DuConstructor(deployName, deployVersion, mode, baseAddr):
    return duConstructor %(deployName, deployVersion, mode, baseAddr)
   
def vs7DuDesignAlloc(designName, designVersion):
    designVersion = designVersion.replace(".", "_")
    return duDesignAlloc %(designName, designName, designVersion)
   
def vs7DuDesignDelete(designName):
    return duDesignDelete %(designName)

def vs7DuDesignGet(designName, designVersion):
    designVersion = designVersion.replace(".", "_")
    return duDesignGet %(designName, designVersion, designName, designName, designVersion, designName)

# CLASSES generation -----------------------------------------------------
def hexSwap32(input):
    input = format(input, '#02X')
    hexswap = input[0:2]+input[8:10]+input[6:8]+input[4:6]+input[2:4]
    return hexswap  
   
def vs7ClassHeader(designName, designVersion, classBlocks, classDevice, classDesign):
    designVersion = designVersion.replace(".", "_")
    return classHeader %(designName.upper(), designVersion, designName.upper(), designVersion, designName, designVersion, classBlocks, classDevice, classDesign)  

def vs7ClassBlock(framework, owner, checksum, className, blockName, regsCode, blockOffset, regsDim, regsDef):
    BlockName = iecommon.capitalizeString(blockName);
    initCodeString = ""
    if className == "SilecsHeader":
        initCodeString = classHeaderInit %(framework, hexSwap32(checksum), owner)   
    return classBlock %(BlockName, blockName, BlockName, className, blockName, initCodeString, BlockName, regsCode, blockOffset, regsDim, regsDef)

def vs7ClassGetSet(regName, regFormat, regDim1, regDim2, regLen):
    RegName = iecommon.capitalizeString(regName)
    regFormat = whichRegisterFormat[regFormat]
    if regDim1 == 1 and regDim2 == 1:    # scalar
        if regFormat == 'string':
            return classScalarStringGetSet %(regName, RegName, regName, regName, regName, RegName, regName, regName, regName, regName, regName)
        else:
            return classScalarGetSet %(regName, regFormat, RegName, regName, regName, RegName, regFormat, regName)
    else:
        if regFormat == 'string':
            return classArrayStringGetSet %(regName, RegName, regName, regName, regName, regName, RegName, regName, regName, regName, regName, regName, regName)
        else:
            return classArrayGetSet %(regName, RegName, regFormat, regName, regName, regName, regFormat, regName, RegName, regFormat, regName, regName, regName, regFormat)

def vs7ClassDimension(regName, regFormat, regDim1, regDim2, regLen):
    dimCodeString = ''
    if regDim1 > 1 or regDim2 > 1:    # not a scalar ==> define dimension constants
        dimCodeString += classArrayDim %(regName, regDim1, regName, regDim2)
    if regFormat == 'string':
        dimCodeString += classStringLen %(regName, regLen)
    return dimCodeString

def vs7ClassDummyRegister(curRegAddress, prevRegMemSize, regMemSize, dummyIndex):
    dummyCodeString = ''
    if curRegAddress % 2 != 0: #is current register address an odd address?
        if prevRegMemSize > 1 or regMemSize > 1:   #only 8bit scalar (not following array) uses 8bit alignment! 
            dummyCodeString = "        uint8_t dummy%s;\n" %(dummyIndex)
    return dummyCodeString

def vs7ClassDataRegister(regName, regFormat, regDim1, regDim2, regLen):
    length = ''
    regFormat = whichRegisterFormat[regFormat]
    if regFormat == 'string':
        length = "[%sLen_+2]" %(regName)
        regFormat = 'char'
    dataCodeString = "        %s %s" %(regFormat, regName)
    if regDim1 > 1 or regDim2 > 1:    # not scalar
        dataCodeString += "[%sDim1_]" %(regName)    
        if regDim2 > 1:    # not scalar
            dataCodeString += "[%sDim2_]" %(regName)
    dataCodeString += "%s;\n" %(length)
    return dataCodeString

def vs7ClassCreateBlock(className, blockName):
    BlockName = iecommon.capitalizeString(blockName);
    return classCreateBlock %(className, blockName, BlockName)

def vs7ClassDeleteBlock(className, blockName):
    return classDeleteBlock %(className, blockName)

def vs7ClassCreateDevice(className, deviceLabel, deviceIndex):
    return classCreateDevice %(className, deviceLabel, className, deviceLabel, deviceIndex)

def vs7ClassDeleteDevice(className, deviceLabel):
    return classDeleteDevice %(className, deviceLabel)

def vs7ClassDevice(createBlocks, deleteBlocks):
    return classDevice %(createBlocks, deleteBlocks)

def vs7ClassDesign(className, classVersion, createDevices, deleteDevices):
    class_version = classVersion.replace(".", "_")
    return classDesign %(className, classVersion, createDevices, deleteDevices, className, class_version, className, class_version)
