#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import re
import os
import sys
import getpass
import codecs

import iecommon
import libxml2

#==================================================================
# LOGGING
#==================================================================
def logHeader(level):
    global project, program
    _time = time.strftime("%Y/%m/%d-%H:%M:%S")
    program = sys.argv[0]
    return "[%s SILECS %s %s] " %(_time, program, level)

def logError(msg, exit,logTopics={}):
    if 'errorlog' in logTopics:
        if logTopics['errorlog'] == True:
            _logMsg = "%s%s" %(logHeader("ERROR"), msg)
            print(_logMsg)
            if exit:
                raise Exception(_logMsg)

def logInfo(msg,logTopics={}):
    if 'infolog' in logTopics:
        if logTopics['infolog'] == True:
            _logMsg = "%s%s" %(logHeader("INFO"), msg)
            print(_logMsg)
    
def logDebug(msg,logTopics={}):   
    if 'debuglog' in logTopics:
        if logTopics['debuglog'] == True:
            _logMsg = "%s%s" %(logHeader("DEBUG"), msg)
            print(_logMsg)
            
# Append message to a text file
def logToFile(path,msg):
    with open(path, "a") as logFile:
        logFile.write(logHeader("CMD") + msg + "\n")           
#-------------------------------------------------------------------------
# General XML Parsing
#-------------------------------------------------------------------------

def xsdBooleanToBoolean(value):
    # Remove trailing and leading whitespaces
    value = value.strip()
    if value == 'true' or value == '1':
        return True
    elif value == 'false' or value == '0':
        return False
    raise ValueError(value + ' is not a valid xsd:boolean value')

def hasChildren(element):
    children = element.xpathEval("*")
    if len(children):
        return True
    return False

def getFirstChild(element):
    return element.xpathEval("*")[0]

def isChildElement(node,elementName):
    if not type(elementName) is str:
        raise Exception("Error: Wrong Type Parsed")
    for subnode in node:
        if subnode.get_name() == elementName:
            return True
    return False

def getChildElement(node,elementName):
    for subnode in node:
        if subnode.get_name() == elementName:
            return subnode
    return null

def getOrCreateChildElement(node,elementName):
    namedChilds = node.xpathEval(elementName)
    if len(namedChilds):
        return namedChilds[0]
    newElement = libxml2.newNode(elementName)
    node.addChild(newElement)
    return newElement

def getOrCreateNamedChildElement(node,elementName, attributeNameValue, attributeName="name"):
    elements = node.xpathEval(elementName + "[@" + attributeName + "='" + attributeNameValue + "']")
    if len(elements):
        return elements[0]
    newNode = libxml2.newNode(elementName)
    node.addChild(newNode)
    newNode.setProp(attributeName, attributeNameValue)
    return newNode

def getOrCreateNamedPreviousSiblingElement(parent,node,elementName, attributeNameValue):
    alreadyAvailable = node.get_parent().xpathEval(elementName + "[@name='" + attributeNameValue + "']")
    if len(alreadyAvailable):
        return alreadyAvailable[0]
    newNode = libxml2.newNode(elementName)
    node.addPrevSibling(newNode)
    newNode.setProp("name", attributeNameValue)
    return newNode

def getOrCreatePreviousSiblingElement(node,elementName):
    alreadyAvailable = node.get_parent().xpathEval(elementName)
    if len(alreadyAvailable):
        return alreadyAvailable[0]
    iecommon.logDebug("Not Found: Name:"+ elementName, {'debuglog': True})
    newNode = libxml2.newNode(elementName)
    node.addPrevSibling(newNode)
    return newNode

def getOrCreateNamedFirstChild(parent,elementName, attributeNameValue):
    elements = parent.xpathEval(elementName + "[@name='" + attributeNameValue + "']")
    if len(elements):
        return elements[0]
    newNode = libxml2.newNode(elementName)
    newNode.setProp('name', attributeNameValue)
    if parent.get_children():
        firstChild = parent.xpathEval("*")[0]
        firstChild.addPrevSibling(newNode)
    else:
        parent.addChild(newNode)
    return newNode

def fillAttributes(element, attrs):
    for name,value in attrs.items():
        if not type(value) is str:
            raise Exception("Error: Wrong Type Parsed for attribute: " + name)
        element.setProp(name, value)
    return element

def capitalizeString(text):
    str = ""
    if len(text) > 0:
        str += text[0].upper()
        str += text[1:]
    return str

#Major changes: Public API, database and PLC code generation may be impacted
def getMajorSilecsVersion(silecsVersionString):
    firstDot = silecsVersionString.find('.')
    return silecsVersionString[0:firstDot]

#Minor changes: New functionality: backward-compatible
def getMinorSilecsVersion(silecsVersionString):
    firstDot = silecsVersionString.find('.')
    secondDot = silecsVersionString.find('.',firstDot)
    return silecsVersionString[firstDot:secondDot]

#Patches and bug fixes: backward compatible
def getPatchSilecsVersion(silecsVersionString):
    firstDot = silecsVersionString.find('.')
    secondDot = silecsVersionString.find('.',firstDot)
    return silecsVersionString[secondDot:]


