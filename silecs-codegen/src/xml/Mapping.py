#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

global MEM_TYPE
global DI_TYPE
global DO_TYPE
global AI_TYPE
global AO_TYPE
global mappin2String

MEM_TYPE = 0
DI_TYPE = 1
DO_TYPE = 2
AI_TYPE = 3
AO_TYPE = 4
    
mappingType2String =  { MEM_TYPE: "MEM", DI_TYPE: "DI", DO_TYPE: "DO", AI_TYPE: "AI", AO_TYPE: "AO" }

class Mapping(object):
    def __init__(self,controllerAddress, type):
        self.classBaseAddress    = controllerAddress  # start-address of the data per area type (MEM, DI,DO, AI,AO) for the given class
        self.nbBlock             = 0  # number of block per area type (MEM, DI,DO, AI,AO)
        self.deviceDataSize      = 0  # global size of one class instance (sum of block size per area type)
        self.usedData             = ""  # data size summary per class and per area type#
        self.instanceAddress     = -1  # start-address of the device data
        self.isDefined = False  # True if the object related base-address has been defined
        self.type = type               # mapping-type (MEM, DI, DO,AI,AO )
        self.blockDataSize   = 0
        self.blockAddress    = 0
        
        if controllerAddress != -1:
            self.instanceAddress     = 0
            self.isDefined = True
            
    def clearInstanceAddressIfDefined(self):
        if self.isDefined:
            self.instanceAddress     = 0