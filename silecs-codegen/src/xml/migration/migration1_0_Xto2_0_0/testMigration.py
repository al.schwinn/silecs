#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from test.testBase import *

import libxml2
from migration.migration1_0_Xto2_0_0.migrators import *
import inspect #get caller name

def testdesignValueTypeMigrator():
    
    SilecsDesignOld = '''<?xml version="1.0" encoding="UTF-8"?>
    <SILECS-Design xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        silecs-version="0.10.0" created="03/21/16" updated="03/21/16"
        xsi:noNamespaceSchemaLocation="/common/usr/cscofe/silecs/0.10.0/silecs-model/src/xml/DesignSchema.xsd">
        <Information>
            <Owner user-login="schwinn" />
            <Editor user-login="schwinn" />
        </Information>
        <SILECS-Class name="PneuDrive" version="0.1.0" domain="TEST">
            <Block name="Acq" mode="READ-ONLY">
                <Register name="string1" synchro="MASTER" format="string" />
                <Register name="string2" synchro="MASTER" format="string" string-len="65"/>
                <Register name="string1D" synchro="MASTER" format="string" array-dim1="2" />
                <Register name="string2D" synchro="MASTER" format="string" array-dim1="2" array-dim2="4"/>
                <Register name="scalar" synchro="MASTER" format="int8" />
                <Register name="scalar1D" synchro="MASTER" format="int8" array-dim1="2" />
                <Register name="scalar2D" synchro="MASTER" format="int8" array-dim1="2" array-dim2="4"/>
            </Block>
        </SILECS-Class>
    </SILECS-Design>'''
    context = libxml2.parseDoc(SilecsDesignOld)

    designValueTypeMigrator(context)
    #context.shellPrintNode()   #for debug
    scalar = context.xpathEval("//Register[not(@format)]/scalar[@format='int8']")
    array = context.xpathEval("//Register[not(@array-dim1)]/array[@dim='2' and @format='int8']")
    array2D = context.xpathEval("//Register[not(@array-dim2)]/array2D[@dim1='2' and @dim2='4' and @format='int8']")
    string1 = context.xpathEval("//Register/string[@format='string' and @string-length='64']")
    string2 = context.xpathEval("//Register[not(@string-len)]/string[@format='string' and @string-length='65']")
    stringArray = context.xpathEval("//Register/stringArray[@dim='2' and @format='string' and @string-length='64']")
    stringArray2D = context.xpathEval("//Register/stringArray2D[@dim1='2' and @dim2='4' and @format='string' and @string-length='64']")


    assertEqual(len(scalar),1)
    assertEqual(len(array),1)
    assertEqual(len(array2D),1)
    assertEqual(len(string1),1)
    assertEqual(len(string2),1)
    assertEqual(len(stringArray),1)
    assertEqual(len(stringArray2D),1)



def testdesignBlockRegisterMigrator():
    SilecsDesignOld = '''<?xml version="1.0" encoding="UTF-8"?>
    <SILECS-Design xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        silecs-version="0.10.0" created="03/21/16" updated="03/21/16"
        xsi:noNamespaceSchemaLocation="/common/usr/cscofe/silecs/0.10.0/silecs-model/src/xml/DesignSchema.xsd">
        <Information>
            <Owner user-login="schwinn" />
            <Editor user-login="schwinn" />
        </Information>
        <SILECS-Class name="PneuDrive" version="0.1.0" domain="TEST">
            <Block name="block1" mode="READ-ONLY">
                <Register name="b1r1" synchro="MASTER" format="string" />
                <Register name="b1r2" synchro="NONE" format="string" array-dim1="2" />
            </Block>
            <Block name="block2" mode="READ-WRITE">
                <Register name="b2r1" synchro="MASTER" format="string" />
                <Register name="b2r2" synchro="SLAVE" format="string" />
                <Register name="b2r3" synchro="NONE" format="string" array-dim1="2" />
            </Block>
            <Block name="block3" mode="WRITE-ONLY">
                <Register name="b3r1" synchro="SLAVE" format="string" />
                <Register name="b3r2" synchro="NONE" format="string" array-dim1="2" />
            </Block>
        </SILECS-Class>
    </SILECS-Design>'''
    context = libxml2.parseDoc(SilecsDesignOld)
    designBlockRegisterMigrator(context)
    context.shellPrintNode()   #for debug
    
    oldBlocks = context.xpathEval("//Block")
    oldRegisters = context.xpathEval("//Register")
    assertEqual(len(oldBlocks),0)
    assertEqual(len(oldRegisters),0)
    
    block1 = context.xpathEval("/SILECS-Design/SILECS-Class/Acquisition-Block")
    block2 = context.xpathEval("/SILECS-Design/SILECS-Class/Setting-Block")
    block3 = context.xpathEval("/SILECS-Design/SILECS-Class/Command-Block")
    assertEqual(len(block1),1)
    assertEqual(len(block2),1)
    assertEqual(len(block3),1)
    
    b1r1 = context.xpathEval("/SILECS-Design/SILECS-Class/Acquisition-Block/Acquisition-Register[@name='b1r1']")
    b1r2 = context.xpathEval("/SILECS-Design/SILECS-Class/Acquisition-Block/Acquisition-Register[@name='b1r2']")
    
    b2r1 = context.xpathEval("/SILECS-Design/SILECS-Class/Setting-Block/Volatile-Register[@name='b2r1']")
    b2r2 = context.xpathEval("/SILECS-Design/SILECS-Class/Setting-Block/Setting-Register[@name='b2r2']") # synchro = master ? --> Missconfiguration --> We should have this reg in an acquisition-block
    b2r3 = context.xpathEval("/SILECS-Design/SILECS-Class/Setting-Block/Volatile-Register[@name='b2r3']")
    
    b3r2 = context.xpathEval("/SILECS-Design/SILECS-Class/Command-Block/Setting-Register[@name='b3r1']")
    b3r2 = context.xpathEval("/SILECS-Design/SILECS-Class/Command-Block/Setting-Register[@name='b3r2']")

def runTests():
    testdesignValueTypeMigrator()
    testdesignBlockRegisterMigrator()
    # print deployDoc # for debugging
