#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2

def designValueTypeMigrator(context):
    modified = False
    for register in context.xpathEval("//Register"):
        format = register.prop("format")
        register.unsetProp("format")
        dim1 = "1"
        dim2 = "1"
        stringLength ="64" #thats the default
        
        if register.hasProp("string-len"):
            stringLength = register.prop("string-len")
            register.unsetProp("string-len")
        if register.hasProp("array-dim1"):
            dim1 = register.prop("array-dim1")
            register.unsetProp("array-dim1")
        if register.hasProp("array-dim2"):
            dim2 = register.prop("array-dim2")
            register.unsetProp("array-dim2")
            
        valueType = None
        if(format == 'string'):
            if int(dim1) > 1:
                if int(dim2) > 1:
                    valueType = libxml2.newNode("stringArray2D")
                    valueType.newProp("dim1",dim1)
                    valueType.newProp("dim2",dim2)
                else:
                    valueType = libxml2.newNode("stringArray")
                    valueType.newProp("dim",dim1)
            else:
                valueType = libxml2.newNode("string")
            valueType.newProp("string-length",stringLength)
        else:
            if int(dim1) > 1:
                if int(dim2) > 1:
                    valueType = libxml2.newNode("array2D")
                    valueType.newProp("dim1",dim1)
                    valueType.newProp("dim2",dim2)
                else:
                    valueType = libxml2.newNode("array")
                    valueType.newProp("dim",dim1)
            else:
                valueType = libxml2.newNode("scalar")
        valueType.newProp("format",format)
        register.addChild(valueType)
        modified = True
    return modified

def designBlockRegisterMigrator(context):
    modified = False
    for blockNode in context.xpathEval("SILECS-Design/SILECS-Class/Block"):
        for childNode in blockNode.get_children():
            if childNode.get_name() == 'Register':
                if blockNode.prop("mode") == 'READ-ONLY':
                    childNode.setName("Acquisition-Register")
                elif blockNode.prop("mode") == 'WRITE-ONLY':
                    childNode.setName("Setting-Register")
                else: #READ-WRITE
                    if childNode.prop("synchro") == 'SLAVE':
                        childNode.setName("Setting-Register")
                    else:
                        childNode.setName("Volatile-Register")
                childNode.unsetProp("synchro")
                
        if blockNode.prop("mode") == 'READ-ONLY':
            blockNode.setName("Acquisition-Block")
        elif blockNode.prop("mode") == 'WRITE-ONLY':
            blockNode.setName("Command-Block")
        else: #READ-WRITE
            blockNode.setName("Setting-Block")
        blockNode.unsetProp("mode")
        modified = True
    return modified
