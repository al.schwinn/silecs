#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import fnmatch
import os
import sys
import libxml2
from argparse import ArgumentParser
import FileUtils

class MigrationBase(object):
    def __init__(self):
        self.parser = ArgumentParser(description='Migration script')
        self.parser.add_argument("silecsDocument", action="store", help="The SILECS document to migrate")
        self.parser.add_argument("xmlSchema", action="store", help="Path to the new schema")
        self.parser.add_argument("versionOld", action="store", help="old silecs version")
        self.parser.add_argument("versionNew", action="store", help="new silecs version")
        results = self.parser.parse_args()
        self.versionOld = results.versionOld
        self.versionNew = results.versionNew
        self.silecsDocument = results.silecsDocument
        self.xmlSchema = results.xmlSchema
        
    def fixSilecsVersion(self, context):
        root = context.xpathEval("/*")[0]
        if root.prop("silecs-version") != self.versionOld:
            raise IOError("Wrong Silecs Version. Document-version: '" + root.prop("silecs-version") + "' script start version: '" + self.versionOld + "' - migration cancelled")
        root.setProp("silecs-version",self.versionNew)
        print("Info: Replaced old silecs-versiong string: " + self.versionOld + " with: " + self.versionNew)

    def fixXMLScheman(self, context,xmlSchema):
        root = context.xpathEval("/*")[0]
        root.setProp("xsi:noNamespaceSchemaLocation",xmlSchema)
        print("Info: Replaced old silecs-xmlSchema")

    def backupOldFESAMakeSpecific(self):
        projectDir = FileUtils.getProjectDir(self.silecsDocument)
        makeSpecific = os.path.join(projectDir,"Makefile.specific")
        FileUtils.backupFile(makeSpecific)
            
    def updateFESAMakeSpecific(self):
        projectDir = FileUtils.getProjectDir(self.silecsDocument)
        makeSpecific = os.path.join(projectDir,"Makefile.specific")
        print("Migration-Info: Old Version-Strings in '" + makeSpecific + "' will be replaced\n")
        if os.path.isfile(makeSpecific):
            oldComm = "silecs-communication-cpp/" + self.versionOld
            newComm = "silecs-communication-cpp/" + self.versionNew
            FileUtils.replaceInFile(makeSpecific,oldComm,newComm)
            oldSnap7 = "snap7/" + self.versionOld
            newSnap7 = "snap7/" + self.versionNew
            FileUtils.replaceInFile(makeSpecific,oldSnap7,newSnap7)

    def removeGenCode(self):
        projectDir = FileUtils.getProjectDir(self.silecsDocument)
        print("Migration-Info: Generated-code folder of '" + projectDir + "' will be removed\n")
        clientFolder = projectDir + "/generated/client"
        controllerFolder = projectDir + "/generated/controller"
        wrapperFolder = projectDir + "/generated/wrapper"
        if os.path.isdir(clientFolder):
            shutil.rmtree(clientFolder)
            print("removed generation folder: " + clientFolder)
        if os.path.isdir(controllerFolder):
            shutil.rmtree(controllerFolder)
            print("removed generation folder: " + controllerFolder)
        if os.path.isdir(wrapperFolder):
            shutil.rmtree(wrapperFolder)
            print("removed generation folder: " + wrapperFolder)
            
    def migrate(self):
        print("INFO: Migration %s --> %s " % (self.versionOld, self.versionNew))
        
        #project directory path
        projectDir = FileUtils.getProjectDir(self.silecsDocument)
        
        # Design
        extension = FileUtils.getExtension(self.silecsDocument)
        modified = False
        if extension == '.silecsdesign':
            context = self._parse(self.silecsDocument)
            modified = self.migrateClass(context, projectDir)

        # Deploy
        elif extension == '.silecsdeploy':
            context = self._parse(self.silecsDocument)
            modified = self.migrateDeployUnit(context, projectDir)

        # Unknown
        else:
            raise IOError("Document type unknown: %r" % extension)

        # only chaning the version or the schema does not count as modification --> no backup needed
        self.fixSilecsVersion(context) 
        self.fixXMLScheman(context,self.xmlSchema)
        if modified:
            self._saveAndBackupFile(context, self.silecsDocument)
        else:
            self._saveFile(context, self.silecsDocument)
        
        print('File %r successfully migrated' % self.silecsDocument)

    def migrateClass(self, context, projectDir):
        return False

    def migrateDeployUnit(self, context, projectDir):
        return False
    
    def _parse(self, document):
        if not FileUtils.fileExists(document):
            raise IOError("File not found: %r" % document)
        else:
            print("Parsing file: %r" % document)
            return libxml2.parseFile(document)

    def _saveFile(self, context, silecsDocument):
        with open(silecsDocument, 'w') as fd:
            context.saveTo(fd)

    def _saveAndBackupFile(self, context, silecsDocument):
        FileUtils.backupFile(silecsDocument)
        with open(silecsDocument, 'w') as fd:
            context.saveTo(fd)


class MigrationAny(MigrationBase):
    def __init__(self, arguments):
        super(MigrationAny, self).__init__()

    def dummy(self):
        #doNothing
        print("dummy")

if __name__ == "__main__":
    migration = MigrationAny(sys.argv)
    migration.migrate()