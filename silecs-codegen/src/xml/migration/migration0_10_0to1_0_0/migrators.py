#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import libxml2

def deployRemoveSilecsHeaderMigrator(context):
    modified = False
    results = context.xpathEval('/SILECS-Deploy/Deploy-Classes/Class[name/text()="SilecsHeader"]')
    if len(results) != 0:
        silecsHeaderClassNode = results[0]
        silecsHeaderClassNode.unlinkNode()
        modified = True
    return modified

def deployReOrderControllerAndClassesMigrator(context):
    modified = False
    root = context.xpathEval("/SILECS-Deploy")[0]
    deployInstances = root.xpathEval("Deploy-Instances")[0]
    for controller in deployInstances.xpathEval("Controller"):
        controller.unlinkNode()
        root.addChild(controller)
        modified = True
    deployInstances.unlinkNode()

    deployUnitNode = root.xpathEval("Deploy-Unit")[0]
    oldPLCTypeNodes = deployUnitNode.xpathEval("*[@system]")
    if len(oldPLCTypeNodes) == 1: # Actually it is exactly one for a valid design
        oldPLCTypeNode = oldPLCTypeNodes[0]
        oldPLCTypeNode.unlinkNode()
        for newController in root.xpathEval("Controller"):
            newController.addChild(oldPLCTypeNode)
        modified = True

    # In order to get fresh, empty element ( emty content )
    newDuNode = libxml2.newNode("Deploy-Unit")
    newDuNode.newProp("name",deployUnitNode.prop("name"))
    newDuNode.newProp("version",deployUnitNode.prop("version"))
    deployUnitNode.replaceNode(newDuNode)

    deployClassesNodes = root.xpathEval("Deploy-Classes")
    if len(deployClassesNodes) != 1:# Actually it is exactly one for a valid design
        return modified
    deployClassesNode = deployClassesNodes[0]
    for oldClassNode in root.xpathEval("Deploy-Classes/Class"):
        modified = True
        className = oldClassNode.xpathEval("name")[0].getContent()
        classVersion = oldClassNode.xpathEval("version")[0].getContent()
        for newController in root.xpathEval("Controller"):
            silecsDesign = libxml2.newNode("SilecsDesign")
            newController.addChild(silecsDesign)
            silecsDesign.newProp('silecs-design-name', className)
            silecsDesign.newProp('silecs-design-version', classVersion)
            for oldDevice in oldClassNode.xpathEval("device-list/Device"):
                deviceName = oldDevice.prop("label")
                newDevice = libxml2.newNode("Device")
                newDevice.newProp('device-name', deviceName)
                silecsDesign.addChild(newDevice)
    deployClassesNode.unlinkNode()
    return modified

def deploySwapStep7TiaMigrator(context):
    modified = False
    for tiaEntry in context.xpathEval("/SILECS-Deploy/Deploy-Unit/Siemens-PLC[@system='TIA-PORTAL']"):
        tiaEntry.setProp("system","STEP-7")
        modified = True
    for s7Entry in context.xpathEval("/SILECS-Deploy/Deploy-Unit/Siemens-PLC[@system='STEP-7']"):
        s7Entry.setProp("system","TIA-PORTAL")
        modified = True
    return modified

def designGenerateFesaPropValueItemMigrator(context):
    modified = False
    for block in context.xpathEval("//Block"):
        if not block.hasProp("generateFesaProperty"):
            block.newProp("generateFesaProperty","true")
            modified = True
    for register in context.xpathEval("//Register"):
        if not register.hasProp("generateFesaValueItem"):
            register.newProp("generateFesaValueItem","true")
            modified = True
    return modified

def fesaInstanceFileMigrator(context):
    modified = False
    for classNode in context.xpathEval('/instantiation-unit/classes/*'):
        className = classNode.get_name()
        for deviceConfiguration in classNode.xpathEval('device-instance/configuration'):
            result1 = deviceConfiguration.xpathEval('parameterFile/value')
            result2 = deviceConfiguration.xpathEval('plcHostName/value')
            if len(result1) == 1 and len(result2) == 1:
                parameterFileValueNode = result1[0]
                controllerValueNode = result2[0]
                valueString = parameterFileValueNode.getContent()
                valueString = valueString.replace(className, controllerValueNode.getContent())
                parameterFileValueNode.setContent(valueString)
                modified = True
    return modified
