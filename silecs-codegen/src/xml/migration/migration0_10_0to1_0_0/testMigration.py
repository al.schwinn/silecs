#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from test.testBase import *

import libxml2
#from migration.0_10_0to1_0_0 import *
from migration.migration0_10_0to1_0_0.migrators import *
import inspect #get caller name

SilecsDesignOld = '''<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Design xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	silecs-version="0.10.0" created="03/21/16" updated="03/21/16"
	xsi:noNamespaceSchemaLocation="/common/usr/cscofe/silecs/0.10.0/silecs-model/src/xml/DesignSchema.xsd">
	<Information>
		<Owner user-login="apetit" />
		<Editor user-login="apetit" />
	</Information>
	<SILECS-Class name="PneuDrive" version="0.1.0" domain="TEST">
		<Block name="Setting" mode="READ-WRITE">
			<Register name="ExecuteDrive" synchro="MASTER" format="int8" />
			<Register name="RequestControl" synchro="MASTER" format="int8" />
			<Register name="ErrorAck" synchro="MASTER" format="int8" />
		</Block>
		<Block name="Acq" mode="READ-ONLY">
			<Register name="AirPressure" format="int8" synchro="MASTER" ></Register>
			<Register name="HardwareILk" synchro="MASTER" format="int8" />
			<Register name="Maintenance" synchro="MASTER" format="int8" />
			<Register name="MotionTime" synchro="MASTER" format="int32" />
			<Register name="DriveCounter" synchro="MASTER" format="int32" />
			<Register name="Position" synchro="MASTER" format="int8" />
			<Register name="Warnings" synchro="MASTER" format="int8" />
			<Register name="Errors" synchro="MASTER" format="int8" />
			<Register name="OperationalMode" synchro="MASTER" format="int8">
			</Register>
		</Block>
	</SILECS-Class>
</SILECS-Design>'''
SilecsDesignOldParsed = libxml2.parseDoc(SilecsDesignOld)

SilecsDeployOld = '''<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Deploy xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" silecs-version="0.10.0" created="03/22/16" updated="03/22/16" xsi:noNamespaceSchemaLocation="/common/usr/cscofe/silecs/0.10.0/silecs-model/src/xml/DeploySchema.xsd">
    <Information>
        <Owner user-login="apetit"/>
        <Editor user-login="apetit"/>
    </Information>
    <Deploy-Unit name="PneuDriveDU" version="0.1.0">
        <Siemens-PLC system="STEP-7" model="SIMATIC_S7-300" protocol="DEVICE_MODE" base-DB-number="1"/>
    </Deploy-Unit>
    <Deploy-Instances>
      <Controller host-name="sdaplc003"/>
      <Controller host-name="ilkio001" />
    </Deploy-Instances>
    <Deploy-Classes>
       <Class>
            <device-list><Device label="SilecsHeader"/></device-list><name>SilecsHeader</name>
            <version>1.0.0</version>
       </Class>
       <Class>
       	<device-list>
       		<Device label="YR07DC2"/><Device label="YR07DF2"/><Device label="YR11DC3"/><Device label="YR11DF3"/><Device label="YRE1DC1"/><Device label="YRE1DF1"/>
       	</device-list>
       	
       	<name>PneuDrive</name>
       	<version>0.1.0</version>
       </Class>
       <Class>
       	<device-list>
       		<Device label="dgsdfg"/>
       	</device-list>
       	<name>Whatever</name>
       	<version>0.1.0</version>
       </Class>
    </Deploy-Classes>
</SILECS-Deploy>'''
SilecsDeployOldParsed = libxml2.parseDoc(SilecsDeployOld)

fesaInstanceOldParsed = libxml2.parseFile("migration/migration0_10_0to1_0_0/DeviceData_PneuDriveDU.instance")

def testdeployRemoveSilecsHeaderMigrator(deployDoc):
    deployRemoveSilecsHeaderMigrator(deployDoc)
    silecsHeaders = deployDoc.xpathEval('/SILECS-Deploy/Deploy-Classes/Class/name[text()="SilecsHeader"]')
    assertEqual(len(silecsHeaders),0)

def testdeployReOrderControllerAndClassesMigrator(deployDoc):
    deployReOrderControllerAndClassesMigrator(deployDoc)
    print(deployDoc)
    oldClasses = deployDoc.xpathEval('/SILECS-Deploy/Deploy-Classes/Class')
    assertEqual(len(oldClasses),0)
    oldDeployInstances = deployDoc.xpathEval('/SILECS-Deploy/Deploy-Instances')
    assertEqual(len(oldDeployInstances),0)
    oldPLCNode = deployDoc.xpathEval('/SILECS-Deploy/Deploy-Unit/Siemens-PLC')
    assertEqual(len(oldPLCNode),0)

    newContollers = deployDoc.xpathEval('/SILECS-Deploy/Controller')
    assertEqual(len(newContollers),2)
    for newController in newContollers:
        assertTrue(newController.hasProp("host-name"))
        plcNodes = newController.xpathEval('Siemens-PLC')
        assertEqual(len(plcNodes),1)
        classNodes = newController.xpathEval('SilecsDesign')
        assertEqual(len(classNodes),2)
        pneuDrive = newController.xpathEval("SilecsDesign[@silecs-design-name='PneuDrive']")[0]
        pneuDriveDevices = pneuDrive.xpathEval('Device')
        assertEqual(len(pneuDriveDevices),6)
        whatever = newController.xpathEval('SilecsDesign[@silecs-design-name="Whatever"]')[0]
        whateverDevices = whatever.xpathEval('Device')
        assertEqual(len(whateverDevices),1)

def testdeploySwapStep7TiaMigrator(deployDoc):
    deploySwapStep7TiaMigrator(deployDoc)
    plcEntries = deployDoc.xpathEval("/SILECS-Deploy/Deploy-Unit/Siemens-PLC[@system='TIA-PORTAL']")
    assertEqual(len(plcEntries),1)

def testdesignGenerateFesaPropValueItemMigrator(context):
    designGenerateFesaPropValueItemMigrator(context)
    updatedBlocks = context.xpathEval("//Block[@generateFesaProperty='true']")
    updatedRegisters = context.xpathEval("//Register[@generateFesaValueItem='true']")
    assertEqual(len(updatedBlocks),2)
    assertEqual(len(updatedRegisters),12)

def testfesaInstanceFileMigrator(context):
    fesaInstanceFileMigrator(context)
    newValues = context.xpathEval("//device-instance[@name='YR11DF3']/configuration/parameterFile/value")
    assertEqual(len(newValues),1)
    assertEqual(newValues[0].getContent(),"../../../generated/client/sdaplc003.silecsparam")

def runTests():
    testdeploySwapStep7TiaMigrator(SilecsDeployOldParsed)
    testdeployRemoveSilecsHeaderMigrator(SilecsDeployOldParsed)
    testdeployReOrderControllerAndClassesMigrator(SilecsDeployOldParsed)
    testdesignGenerateFesaPropValueItemMigrator(SilecsDesignOldParsed)
    testfesaInstanceFileMigrator(fesaInstanceOldParsed)

    # print deployDoc # for debugging
