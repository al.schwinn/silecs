#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from test.testBase import *
import migration.migration_0_9_0to0_10_0.testMigration
import migration.migration0_10_0to1_0_0.testMigration
import migration.migration1_0_Xto2_0_0.testMigration
import migration.migration2_0_Xto2_1_X.testMigration

SilecsDeployOld = '''<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Deploy silecs-version="oldVersion" created="03/04/16" updated="03/04/16" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="oldSchemaLocation">
</SILECS-Deploy>
'''

# Test does not run in Jenkins, since "argparse" is not installed there

#class BaseTestFake(MigrationBase):
#    def __init__(self):
#        super(BaseTestFake, self).__init__("oldVersion", "newVersion")

#def runbaseTests():
#    baseClass = BaseTestFake()
#    context = libxml2.parseDoc(SilecsDeployOld)
#    baseClass.fixSilecsVersion(context)
#    baseClass.fixXMLScheman(context,"newSchemaLocation")
#
#    root = context.xpathEval("/*")[0]
#
#    assertEqual( root.prop('silecs-version'),"newVersion" )
#    assertEqual( root.prop('noNamespaceSchemaLocation'),"newSchemaLocation" )

def runAllTests():
#    runbaseTests()
    migration.migration_0_9_0to0_10_0.testMigration.runTests()
    migration.migration0_10_0to1_0_0.testMigration.runTests()
    migration.migration1_0_Xto2_0_0.testMigration.runTests()
    migration.migration2_0_Xto2_1_X.testMigration.runTests()
    allTestsOk()
