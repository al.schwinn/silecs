#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil

def fileExists(fileName):
    return os.path.isfile(fileName)

def write(string, filePath):
    with open(filePath, 'w') as f:
        f.write(string)

def backupFile(fileName):
    if not os.path.isfile(fileName):
        return
    src = fileName
    dst = fileName + ".backup"
    os.rename(src, dst)
    print("Backed up old file: " + dst)

def getExtension(fileName):
    '''Returns the file extension, dot included'''
    return fileName[fileName.rfind('.'):]

def getProjectDir(file_in_project):
    absPath = os.path.abspath(file_in_project)
    return os.path.dirname(os.path.dirname(absPath))

def getFesaSourceFiles(fesaClassName, projectDir):
    fesaCodeFolder = os.path.join(projectDir,"src",fesaClassName)
    if not os.path.exists(fesaCodeFolder):
        return []
    if not os.path.isdir(fesaCodeFolder):
        return []
    sourceFiles = []
    for root, subdirs, files in os.walk(fesaCodeFolder):
        for file in files:
            if file.endswith(".cpp") or file.endswith(".h"):
                print(os.path.join(root,file))
                sourceFiles.append(os.path.join(root,file))
    return sourceFiles

def replaceInFile(filePath,searchString,replaceWithString):
    with open(filePath, 'r') as file :
      filedata = file.read()
    filedata = filedata.replace(searchString, replaceWithString)
    with open(filePath, 'w') as file:
      file.write(filedata)