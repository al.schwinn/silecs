#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from migrationBase import MigrationBase
from migration0_10_0to1_0_0.migrators import *

import libxml2
import sys
import FileUtils
import shutil

class Migration(MigrationBase):
    def __init__(self, arguments):
        super(Migration, self).__init__()

    def migrateClass(self, context, projectDir ):
        modified = designGenerateFesaPropValueItemMigrator(context)
        return modified

    def migrateDeployUnit(self, context, projectDir ):
        modified = deploySwapStep7TiaMigrator(context)
        modified = deployRemoveSilecsHeaderMigrator(context)
        modified = deployReOrderControllerAndClassesMigrator(context) 
        self.removeGenCode()
        self.migrateFESAInstanceFile()
        return modified

    def migrateFESAInstanceFile(self):
        results = self.parser.parse_args()
        silecsDocument = results.silecsDocument
        projectDir = FileUtils.getProjectDir(silecsDocument)
        testFolder = projectDir + "/src/test"
        if not os.path.isdir(testFolder):
            return
        for f in os.listdir(testFolder):
            fecFolder = os.path.join(testFolder, f)
            if os.path.isdir( fecFolder ):
                for subFile in os.listdir(fecFolder):
                    filename, file_extension = os.path.splitext(subFile)
                    if file_extension == ".instance":
                        instanceFile = os.path.join(fecFolder, subFile)
                        print("parsing FESA instancee file: " + instanceFile)
                        context = self._parse(instanceFile)
                        if fesaInstanceFileMigrator(context):
                            self._saveAndBackupFile(context,instanceFile)

if __name__ == "__main__":
    migration = Migration(sys.argv)
    migration.migrate()
    migration.backupOldFESAMakeSpecific()
