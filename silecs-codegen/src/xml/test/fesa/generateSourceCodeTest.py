#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from test.testBase import *
import fesa.fesa_3_0_0.generateSourceCode
import fesa.fesa_3_1_0.generateSourceCode

import libxml2

testFolder = "test"
generationFolder = testFolder + "/generated_temp"
comparisonFolder = testFolder + "/generated_correct"
generatedHeaderName = "AllTypes.h"
generatedCppName = "AllTypes.cpp"

silecsDesignRoot = libxml2.parseFile("test/AllTypesFESA.silecsdesign")



def testGenHSource():
    fesaDesignRoot = libxml2.parseFile(generationFolder + "/AllTypesFESA.design" )
    fesa.fesa_3_1_0.generateSourceCode.genHSource('AllTypes', silecsDesignRoot,fesaDesignRoot,generationFolder,logTopics={'errorlog': True})
    assertFileEqual( generationFolder + "/" + generatedHeaderName, comparisonFolder + "/" + generatedHeaderName)

def testGenCppSource():
    fesaDesignRoot = libxml2.parseFile(generationFolder + "/AllTypesFESA.design" )
    fesa.fesa_3_1_0.generateSourceCode.genCppSource('AllTypes', silecsDesignRoot,fesaDesignRoot,generationFolder,logTopics={'errorlog': True})
    assertFileEqual( generationFolder + "/" + generatedCppName, comparisonFolder + "/" + generatedCppName)

def runTests():
    testGenHSource()
    testGenCppSource()
    allTestsOk()
