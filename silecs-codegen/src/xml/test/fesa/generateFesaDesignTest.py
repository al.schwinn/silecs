#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from test.testBase import *
import fesa.fesa_7_3_0.generateFesaDesign

fesa_version = "7.3.0"

import libxml2
import unittest

simpleSilecsDesign = '''<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Design silecs-version="0.10.0" created="03/02/16" updated="03/02/16" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="/common/home/bel/schwinn/lnx/workspace-silecs/silecs-model/src/xml/DesignSchema.xsd">
    <Information>
        <Owner user-login="schwinn"/>
        <Editor user-login="schwinn"/>
    </Information>
    <SILECS-Class name="Test123" version="0.1.0" domain="OPERATIONAL">
        <Setting-Block name="Setting" generateFesaProperty="true">
            <Setting-Register name="mySettingRegister" generateFesaValueItem="true">
                <scalar format="uint8"/>
            </Setting-Register>
        </Setting-Block>
         <Command-Block name="Command" generateFesaProperty="true">
            <Setting-Register name="mySettingRegister" generateFesaValueItem="true">
                <scalar format="uint8"/>
            </Setting-Register>
        </Command-Block>
        <Acquisition-Block name="Acquisition" generateFesaProperty="true">
            <Acquisition-Register name="myAcqRegister" generateFesaValueItem="true">
                <scalar format="uint8"/>
            </Acquisition-Register>
        </Acquisition-Block>
    </SILECS-Class>
</SILECS-Design>'''
simpleSilecsDesignRoot = libxml2.parseDoc(simpleSilecsDesign)

def testFillXML_EmptyTemplate(generator):
    fesaRoot = libxml2.parseFile("test/fesa/emptyTemplate.xml")
    generator.fillXML(fesa_version, 'MyClass', fesaRoot,simpleSilecsDesignRoot,logTopics={'errorlog': True})
    assertTrue( fesaRoot.xpathEval('/equipment-model/events') != None )
    assertTrue( fesaRoot.xpathEval('/equipment-model/scheduling-units') != None )

def testFillXML_GSITemplate(generator):
    fesaRoot = libxml2.parseFile("test/fesa/GSIClassTemplate.xml")
    generator.fillXML(fesa_version, 'MyClass', fesaRoot,simpleSilecsDesignRoot,logTopics={'errorlog': True})
    assertTrue( fesaRoot.xpathEval('/equipment-model/events') != None )
    assertTrue( fesaRoot.xpathEval('/equipment-model/scheduling-units') != None )

    acquisition = fesaRoot.xpathEval('/equipment-model/interface/device-interface/acquisition')[0]
    firstGSIAcqProp = acquisition.xpathEval('GSI-Acquisition-Property')[0]
    
    assertTrue( acquisition.xpathEval('*')[0].prop('name') != firstGSIAcqProp.prop('name') ) # check if generated at right position
    assertEqual( firstGSIAcqProp.prop('name'),"Acquisition" )
    valueItems = firstGSIAcqProp.xpathEval('value-item')
    assertTrue( valueItems != None )

    setting = fesaRoot.xpathEval('/equipment-model/interface/device-interface/setting')[0]
    firstGSISettingProp = setting.xpathEval('GSI-Setting-Property')[0]
    assertTrue( acquisition.xpathEval('*')[0].prop('name') != firstGSISettingProp.prop('name') ) # check if generated at right position

def testFillXML_GSITemplate_Exceptions(generator):
    fesaRoot = libxml2.parseFile("test/fesa/GSIClassTemplate.xml")
    myRoot = simpleSilecsDesignRoot
    commandBlock = myRoot.xpathEval('/SILECS-Design/SILECS-Class/Command-Block')[0]
    
    commandBlock.setProp("name", "Init")
    try:
        generator.fillXML(fesa_version, 'MyClass', fesaRoot,myRoot,logTopics={'errorlog': True})
    except:
        print("test passed")
    else:
        print("Test Failed: testFillXML_GSITemplateExceptions - Init \n No Exception was thrown")
        sys.exit(1)
        
    commandBlock.setProp("name", "Reset")
    try:
        generator.fillXML(fesa_version, 'MyClass', fesaRoot,myRoot,logTopics={'errorlog': True})
    except:
        print("test passed")
    else:
        print("Test Failed: testFillXML_GSITemplateExceptions - Reset \n No Exception was thrown")
        sys.exit(1)


def testFillXML_AllTypes(generator):
    fesaRoot = libxml2.parseFile("test/fesa/GSIClassTemplate.xml")
    silecsRoot = libxml2.parseFile("test/AllTypesFESA.silecsdesign")
    fesaRoot = generator.fillXML(fesa_version, 'MyClass', fesaRoot,silecsRoot,logTopics={'errorlog': True})
    fesaNewDocPath = "test/generated_temp/AllTypesFESA.design"
    fesaCompareDocPath = "test/generated_correct/AllTypesFESA.design"
    with open(fesaNewDocPath, 'w') as fd:
        fesaRoot.saveTo(fd,format = True)

    print('FESA design document saved successfully')
    assertFileEqual( fesaNewDocPath, fesaCompareDocPath)

def runTests():

    # Always only test the latest supported Fesa Version
    generator = fesa.fesa_7_3_0.generateFesaDesign.FESADesignGenerator7_3_0();
    testFillXML_EmptyTemplate(generator)
    testFillXML_GSITemplate(generator)
    testFillXML_AllTypes(generator)
    testFillXML_GSITemplate_Exceptions(generator)
    allTestsOk()
