import sys
import os
import difflib

import inspect #get caller name

def assertEqual( a, b ):
    if a != b:
        callerInfo = str(inspect.stack()[1][3]) + ":" + str(inspect.stack()[1][2])
        print("Test Failed: " + callerInfo + "\nVariables are different. Var1: '" + str(a) + "' Var2: '" + str(b) + "'")
        sys.exit(1)
    else:
         print("success: " + str(inspect.stack()[1][3]))

def assertTrue( condition ):
    if not condition:
        callerInfo = str(inspect.stack()[1][3]) + ":" + str(inspect.stack()[1][2])
        print("Test Failed: " + callerInfo + "\nCondition expected to be True, but was False")
        sys.exit(1)
    else:
         print("success: " + str(inspect.stack()[1][3]))

def assertFalse( condition ):
    if condition:
        callerInfo = str(inspect.stack()[1][3]) + ":" + str(inspect.stack()[1][2])
        print("Test Failed: " + callerInfo + "\nCondition expected to be False, but was True")
        sys.exit(1)
    else:
         print("success: " + str(inspect.stack()[1][3]))

def assertPLCCodeEqual(filePath1,filePath2):
    with open(filePath1) as a: a_content = a.readlines()
    with open(filePath2) as b: b_content = b.readlines()
    diff = difflib.unified_diff(a_content,b_content)
    isDifferent = False
    for i,line in enumerate(diff):
        if i < 2:
            print("first 2 lines ignored .. unknown reason why they differ")
        elif ("DT#" in line) or ("SILECS_set_dt" in  line) :  #ignore timestamps in silecsheader
            print("date ignored")
        elif line.startswith("-"):
            isDifferent = True
            print(i,' '+line)
        elif line.startswith("+"):
            isDifferent = True
            print(i,' '+line)
    if isDifferent:
        print("Test Failed: The files: " + filePath1 + ", " + filePath2 + " are different from each other !")
        sys.exit(1)
    else:
         print("success: " + str(inspect.stack()[1][3]))

def assertParameterFileEqual(filePath1,filePath2):
    with open(filePath1) as a: a_content = a.readlines()
    with open(filePath2) as b: b_content = b.readlines()
    diff = difflib.unified_diff(a_content,b_content)
    isDifferent = False
    for i,line in enumerate(diff):
        if i < 2:
            print("first 2 lines ignored .. unknown reason why they differ")
        elif ("<Generation date=" in line) :  #ignore timestamps
            print("date ignored")
        elif line.startswith("-"):
            isDifferent = True
            print(i,' '+line)
        elif line.startswith("+"):
            isDifferent = True
            print(i,' '+line)
    if isDifferent:
        print("Test Failed: The files: " + filePath1 + ", " + filePath2 + " are different from each other !")
        sys.exit(1)
    else:
         print("success: " + str(inspect.stack()[1][3]))

def assertFileEqual(filePath1,filePath2):
    with open(filePath1) as a: a_content = a.readlines()
    with open(filePath2) as b: b_content = b.readlines()
    diff = difflib.unified_diff(a_content,b_content)
    isDifferent = False
    for i,line in enumerate(diff):
        if i < 2:
            print("first 2 lines ignored .. unknown reason why they differ")
        elif line.startswith("-"):
            isDifferent = True
            print(i,' '+line)
        elif line.startswith("+"):
            isDifferent = True
            print(i,' '+line)
    if isDifferent:
        print("Test Failed: The files: " + filePath1 + ", " + filePath2 + " are different from each other !")
        sys.exit(1)
    else:
         print("success: " + str(inspect.stack()[1][3]))

def allTestsOk( ):
         print("All tests for module: " + str(inspect.stack()[1][1]) + " run green! ")
