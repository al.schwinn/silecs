
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>

#include <silecs-virtual-controller/core/SilecsSnap7Server.h>
#include "AllTypesDU_0.1.0.h"

class UserSnap7Server : public SilecsServer::SilecsSnap7Server
{
public:
    UserSnap7Server(AllTypesDU_0_1_0::DeployUnit* du) : SilecsSnap7Server(du, true) {}
    virtual ~UserSnap7Server() {}

    virtual void userFunction(PSrvEvent PEvent)
    {
      // Implement the specific process control here!
      // Look at SILECS Wikis: 'Create a virtual controller' chapter 
    }
};

int main(int argc, char*argv[])
{
    AllTypesDU_0_1_0::DeployUnit du;
    UserSnap7Server server(&du);
    if (server.startServer() < 0)
    {
        std::cout << "Failed to start the VC server: " << du.getName() << std::endl;
        return -1;
    }
    return 0;
}
