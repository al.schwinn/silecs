#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import difflib
import sys
import libxml2

from test.testBase import *
from genparam import *
import iecommon
import iefiles

import libxml2

testFolder = "test"
generationFolder = testFolder + "/generated_temp/client"
comparisonFolder = testFolder + "/generated_correct/client"

def fakeGetSilecsDesignFilePath(workspacePath,designName):
    return testFolder + "/AllTypes.silecsdesign"

def fakeGetParameterFile(workspacePath,deployName,controllerName):
    return generationFolder + "/" + controllerName + ".silecsparam" 

def fakeGetSilecsDeployFilePath(workspacePath,deployName):
    return testFolder + "/AllTypesDU.silecsdeploy"

def fakeGetParameterFileDirectory(workspacePath,deployName):
    return generationFolder

def CompareGeneratedFiles(hostName,fileExtension):
    fileGeneratedPath = generationFolder + "/" + hostName + fileExtension
    fileCorrectPath = comparisonFolder + "/" + hostName + fileExtension
    print("comparing: " + fileGeneratedPath + " with: " +  fileCorrectPath)
    assertParameterFileEqual( fileGeneratedPath, fileCorrectPath)

def SiemensSourcesTest():
    CompareGeneratedFiles("Siemens_TiaDevice",".silecsparam")
    CompareGeneratedFiles("Siemens_TiaBlock",".silecsparam")
    CompareGeneratedFiles("Siemens_Step7Device",".silecsparam")
    CompareGeneratedFiles("Siemens_Step7Block",".silecsparam")
    
def SiemensVirtualSourcesTest():
    CompareGeneratedFiles("Virtual_SiemensBlock",".silecsparam")
    CompareGeneratedFiles("Virtual_SiemensDevice",".silecsparam")

def BeckhoffSourcesTest():
    CompareGeneratedFiles("Beckhoff_BC9020",".silecsparam")
    CompareGeneratedFiles("Beckhoff_CX9020_TC2",".silecsparam")
    CompareGeneratedFiles("Beckhoff_CX9020_TC3",".silecsparam")

def SchneiderSourcesTest():
    CompareGeneratedFiles("Schneider_M340",".silecsparam")
    CompareGeneratedFiles("Schneider_PremiumQuantum",".silecsparam")

def RabbitSourcesTest():
    CompareGeneratedFiles("Rabbit_BlockMode",".silecsparam")
    CompareGeneratedFiles("Rabbit_DeviceMode",".silecsparam")
    

def runTests():

    genParamBase( fakeGetSilecsDesignFilePath, fakeGetParameterFile, fakeGetSilecsDeployFilePath, fakeGetParameterFileDirectory, "fake", "AllTypesDU", "fake", "DEV")
    SiemensSourcesTest()
    SiemensVirtualSourcesTest()
    BeckhoffSourcesTest()
    SchneiderSourcesTest()
    RabbitSourcesTest()
    allTestsOk()
