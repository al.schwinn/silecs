#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import difflib
import sys
import libxml2

from test.testBase import *
import genplcsrc
import iecommon
import iefiles
from model.Deploy.Deploy import Deploy
from model.Param.Param import Param

import libxml2



testFolder = "test"
generationFolder = testFolder + "/generated_temp"
comparisonFolder = testFolder + "/generated_correct"
generationFoldeController = generationFolder + "/controller"
comparisonFolderController = comparisonFolder + "/controller"
def generatePLCSources():
    silecsDeploy = Deploy.getDeployFromFile(testFolder + "/AllTypesDU.silecsdeploy")
    for controller in silecsDeploy.controllers:
        paramsFile = generationFolder + "/client/" + controller.hostName + ".silecsparam"
        paramDOM = libxml2.parseFile(paramsFile)
        param = Param()
        param.initFromXMLNode(paramDOM)
        param.deployName = silecsDeploy.name
        param.deployVersion = silecsDeploy.version
        genplcsrc.generateControllerCode(controller, param, generationFoldeController ,{'errorlog': True})


def CompareGeneratedFiles(hostName,fileExtension):
    fileGeneratedPath = generationFoldeController + "/" + hostName + fileExtension
    fileCorrectPath = comparisonFolderController + "/" + hostName + fileExtension
    print("comparing: " + fileGeneratedPath + " with: " +  fileCorrectPath)
    assertPLCCodeEqual( fileGeneratedPath, fileCorrectPath)

def SiemensSourcesTest():
    CompareGeneratedFiles("Siemens_TiaDevice",".scl")
    CompareGeneratedFiles("Siemens_TiaDevice",".sdf")
    CompareGeneratedFiles("Siemens_TiaBlock",".scl")
    CompareGeneratedFiles("Siemens_TiaBlock",".sdf")
    CompareGeneratedFiles("Siemens_Step7Device",".scl")
    CompareGeneratedFiles("Siemens_Step7Device",".sdf")
    CompareGeneratedFiles("Siemens_Step7Block",".scl")
    CompareGeneratedFiles("Siemens_Step7Block",".sdf")
    
def SiemensVirtualSourcesTest():
    CompareGeneratedFiles("SilecsHeader_1.0.0",".h")
    CompareGeneratedFiles("Virtual_SiemensBlock_0.1.0",".cpp")
    CompareGeneratedFiles("Virtual_SiemensDevice_0.1.0",".cpp")

def BeckhoffSourcesTest():
    CompareGeneratedFiles("Beckhoff_BC9020",".exp")
    CompareGeneratedFiles("Beckhoff_CX9020_TC2",".exp")
    CompareGeneratedFiles("Beckhoff_CX9020_TC3",".exp")

def SchneiderSourcesTest():
    CompareGeneratedFiles("Schneider_M340",".xsy")
    CompareGeneratedFiles("Schneider_PremiumQuantum",".xsy")

def RabbitSourcesTest():
    CompareGeneratedFiles("Rabbit_BlockMode",".h")
    CompareGeneratedFiles("Rabbit_DeviceMode",".h")

def runTests():

    generatePLCSources()

    SiemensSourcesTest()
    SiemensVirtualSourcesTest()
    BeckhoffSourcesTest()
    SchneiderSourcesTest()
    RabbitSourcesTest()
    allTestsOk()
