#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import libxml2

from test.testBase import *
from genduwrapper import *
import iecommon
import iefiles

import libxml2

testFolder = "test"
generationFolder = testFolder + "/generated_temp/wrapper"
comparisonFolder = testFolder + "/generated_correct/wrapper"
deployFilePath = testFolder + "/AllTypesDU.silecsdeploy"
duControllerWrapperGenerated = generationFolder + "/Siemens_Step7Device.h" # just check one of them, codegen is identical for all 
duControllerWrapperCorrect = comparisonFolder + "/Siemens_Step7Device.h"
duDesignWrapperGenerated = generationFolder + "/AllTypes.h"
duDesignWrapperCorrect = comparisonFolder + "/AllTypes.h"

def fakeGetSilecsDesignFilePath(workspacePath,designName):
    return testFolder + "/AllTypes.silecsdesign"

def fakeGetDuDesignWrapperFile(workspacePath,deployName,designName):
    return generationFolder + "/" + designName + ".h"

def fakeGetDuWrapperFile(workspacePath, deployName, controllerName):
    return generationFolder + "/" + controllerName + ".h"

def CompareGeneratedFiles():
    assertFileEqual( duDesignWrapperGenerated, duDesignWrapperCorrect)
    assertFileEqual( duControllerWrapperGenerated, duControllerWrapperCorrect)

def runTests():
    genDuWrapperBase(deployFilePath,fakeGetDuWrapperFile,"fake",fakeGetSilecsDesignFilePath, fakeGetDuDesignWrapperFile)
    CompareGeneratedFiles()
    allTestsOk()
