#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=========================================================================
# FESA .cproject file
#=========================================================================

cproject = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<?fileVersion 4.0.0?><cproject storage_type_id="org.eclipse.cdt.core.XmlProjectDescriptionStorage">
    <storageModule moduleId="org.eclipse.cdt.core.settings">
        <cconfiguration id="cern.fesa.plugin.build.configuration.537108490">
            <storageModule buildSystemId="org.eclipse.cdt.managedbuilder.core.configurationDataProvider" id="cern.fesa.plugin.build.configuration.537108490" moduleId="org.eclipse.cdt.core.settings" name="FESA Make Build">
                <externalSettings/>
                <extensions>
                    <extension id="org.eclipse.cdt.core.GASErrorParser" point="org.eclipse.cdt.core.ErrorParser"/>
                    <extension id="org.eclipse.cdt.core.GmakeErrorParser" point="org.eclipse.cdt.core.ErrorParser"/>
                    <extension id="org.eclipse.cdt.core.GLDErrorParser" point="org.eclipse.cdt.core.ErrorParser"/>
                    <extension id="org.eclipse.cdt.core.CWDLocator" point="org.eclipse.cdt.core.ErrorParser"/>
                    <extension id="org.eclipse.cdt.core.GCCErrorParser" point="org.eclipse.cdt.core.ErrorParser"/>
                </extensions>
            </storageModule>
            <storageModule moduleId="cdtBuildSystem" version="4.0.0">
                <configuration buildProperties="" description="" id="cern.fesa.plugin.build.configuration.537108490" name="FESA Make Build" parent="cern.fesa.plugin.build.configuration">
                    <folderInfo id="cern.fesa.plugin.build.configuration.537108490." name="/" resourcePath="">
                        <toolChain id="cern.fesa.plugin.build.toolchain.1156032800" name="FESA Toolchain" superClass="cern.fesa.plugin.build.toolchain">
                            <targetPlatform id="cern.fesa.plugin.build.targetplatform.67227612" isAbstract="false" superClass="cern.fesa.plugin.build.targetplatform"/>
                            <builder id="cern.fesa.plugin.build.builder.1302129891" name="Gnu Make Builder.FESA Make Build" superClass="cern.fesa.plugin.build.builder"/>
                            <tool id="cern.fesa.plugin.build.archiver.1068962328" name="GCC Archiver" superClass="cern.fesa.plugin.build.archiver"/>
                            <tool id="cern.fesa.plugin.build.compiler.648804671" name="GCC C++ Compiler" superClass="cern.fesa.plugin.build.compiler">
                                <option id="gnu.cpp.compiler.option.include.paths.12841593" name="Include paths (-I)" superClass="gnu.cpp.compiler.option.include.paths" valueType="includePath">
                                </option>
                                <inputType id="cdt.managedbuild.tool.gnu.cpp.compiler.input.2129597065" superClass="cdt.managedbuild.tool.gnu.cpp.compiler.input"/>
                            </tool>
                            <tool id="cdt.managedbuild.tool.gnu.c.compiler.base.662860517" name="GCC C Compiler" superClass="cdt.managedbuild.tool.gnu.c.compiler.base">
                                <inputType id="cdt.managedbuild.tool.gnu.c.compiler.input.1388561052" superClass="cdt.managedbuild.tool.gnu.c.compiler.input"/>
                            </tool>
                            <tool id="cdt.managedbuild.tool.gnu.c.linker.base.365658250" name="GCC C Linker" superClass="cdt.managedbuild.tool.gnu.c.linker.base"/>
                            <tool id="cern.fesa.plugin.build.linker.884627432" name="GCC C++ Linker" superClass="cern.fesa.plugin.build.linker">
                                <inputType id="cdt.managedbuild.tool.gnu.cpp.linker.input.2024588043" superClass="cdt.managedbuild.tool.gnu.cpp.linker.input">
                                    <additionalInput kind="additionalinputdependency" paths="$(USER_OBJS)"/>
                                    <additionalInput kind="additionalinput" paths="$(LIBS)"/>
                                </inputType>
                            </tool>
                            <tool id="cern.fesa.plugin.build.assembler.838409637" name="GCC Assembler" superClass="cern.fesa.plugin.build.assembler">
                                <inputType id="cdt.managedbuild.tool.gnu.assembler.input.1405521367" superClass="cdt.managedbuild.tool.gnu.assembler.input"/>
                            </tool>
                        </toolChain>
                    </folderInfo>
                </configuration>
            </storageModule>
            <storageModule moduleId="org.eclipse.cdt.core.externalSettings"/>
        </cconfiguration>
    </storageModule>
    <storageModule moduleId="cdtBuildSystem" version="4.0.0">
        <project id="" name=""/>
    </storageModule>
    <storageModule moduleId="org.eclipse.cdt.core.LanguageSettingsProviders"/>
    <storageModule moduleId="org.eclipse.cdt.make.core.buildtargets">
        <buildTargets>
            <target name="clean" path="" targetID="org.eclipse.cdt.build.MakeTargetBuilder">
                <buildCommand>make</buildCommand>
                <buildArguments/>
                <buildTarget>clean</buildTarget>
                <stopOnError>true</stopOnError>
                <useDefaultCommand>true</useDefaultCommand>
                <runAllBuilders>true</runAllBuilders>
            </target>
            <target name="all x86_64" path="" targetID="org.eclipse.cdt.build.MakeTargetBuilder">
                <buildCommand>make</buildCommand>
                <buildArguments/>
                <buildTarget>-j16 CPU=x86_64 </buildTarget>
                <stopOnError>true</stopOnError>
                <useDefaultCommand>true</useDefaultCommand>
                <runAllBuilders>true</runAllBuilders>
            </target>
        </buildTargets>
    </storageModule>
    <storageModule moduleId="scannerConfiguration"/>
</cproject>
"""

#=========================================================================
# H Source template (.h file)
#=========================================================================

htop = """/*
 * ${className}.h
 *
 * Generated by SILECS framework tools 
 */

#ifndef ${className}_${className}_H_
#define ${className}_${className}_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <fesa-core/Synchronization/MultiplexingContext.h>
#include <${className}/GeneratedCode/ServiceLocator.h>
"""

hTop2 = """
namespace ${className}
{

    /*---------------------------------------------------------------------------------------------------------
     * SETUP
     *---------------------------------------------------------------------------------------------------------
     *  Setup the SILECS service by calling the setup() method from the RTDeviceClass::specificInit()
     *  Stop and cleanup the SILECS service by calling the cleanup() method if needed (eg.: from ~RTDeviceClass())
     *
     *  In order to make use of the different blocks, defined in the silecsdesign, please make use of the static, block related variables of the class, defined on the bottom of this file !
     * --------------------------------------------------------------------------------------------------------
     */

    /*---------------------------------------------------------------------------------------------------------
     * COMMUNICATION
     *---------------------------------------------------------------------------------------------------------
     * General methods to synchronize the FESA fields and related PLC registers of the FESA server with or without
     * PLC side synchronization (send/recv) if requested. Each action is done for one particular block.
     * In case of BLOCK_MODE configuration (see SILECS doc.), the transaction is optimal with the following
     * 'AllDevices' and 'PLCDevices' methods.
     * Each method can be called in the appropriate server-action (set) and rt-action (get)
     *
     * getAllDevices  : [receive all devices of all connected PLCs +] update FESA fields with related SILECS registers
     * setAllDevices  : update SILECS registers with related FESA fields [+ send block to all connected PLCs]
     * getPLCDevices  : [receive all devices of one PLC +] update FESA fields with related SILECS registers
     * setPLCDevices  : update SILECS registers with related FESA fields [+ send block to the PLC]
     * getSomeDevices : [receive each device of the device-collection +] update FESA fields with related SILECS registers
     * setSomeDevices : update SILECS registers with related FESA fields [+ send block to each device of the device-collection]
     * getOneDevice   : [receive block of one PLC device +] update FESA fields with related SILECS registers
     * setOneDevice   : update SILECS registers with related FESA fields [+ send block to the PLC device]
     *
     * --------------------------------------------------------------------------------------------------------
     */

    class Abstract${className}
    {
        public:
            static inline Silecs::Service* theService()  { return pService_; }
            static inline Silecs::Cluster* theCluster()  { return pCluster_; }
            inline std::string& getBlockName()          { return blockName_; }

            static void setup(const ServiceLocator* serviceLocator);
            static void cleanup();
            static bool isInitialized(){ return Abstract${className}::isInitialized_; }
            static void updatePLCRegisters(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator);
            static void updateFesaFields(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator);

            Abstract${className}(std::string blockName);
            virtual ~Abstract${className}();

            void getAllDevices(const ServiceLocator* serviceLocator, const bool recvNow, MultiplexingContext* pContext);
            void getPLCDevices(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator, const bool recvNow, MultiplexingContext* pContext);
            void getSomeDevices(std::vector<Device*> deviceCol, const bool recvNow, MultiplexingContext* pContext);
            virtual void getOneDevice(Device* pDevice, const bool recvNow, MultiplexingContext* pContext);

            void setAllDevices(const ServiceLocator* serviceLocator, const bool sendNow, MultiplexingContext* pContext);
            void setPLCDevices(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator, const bool sendNow, MultiplexingContext* pContext);
            void setSomeDevices(std::vector<Device*> deviceCol, const bool sendNow, MultiplexingContext* pContext);
            virtual void setOneDevice(Device* pDevice, const bool sendNow, MultiplexingContext* pContext);

        protected:

            static Silecs::Service* pService_;
            static Silecs::Cluster* pCluster_;
            static bool isInitialized_;

            // Name of the silecs-block which is addressed
            std::string blockName_;

            // not copyable object
            Abstract${className}(const Abstract${className}&);
            Abstract${className}& operator=(const Abstract${className}&);
    };

    // -------------------------------------------------------------------------------------------------
    #define BLOCK_RO( blockName, propName )\t\\
    class blockName##_Type : public Abstract${className}\t\\
    {\t\\
        public:\t\\
            blockName##_Type(std::string name);\t\\
            ~blockName##_Type();\t\\
            void getOneDevice(Device* pDevice, const bool transmitNow, MultiplexingContext* pContext);\t\\
    }


    #define BLOCK_WO( blockName, propName )\t\\
    class blockName##_Type : public Abstract${className}\t\\
    {\t\\
        public:\t\\
            blockName##_Type(std::string name);\t\\
            ~blockName##_Type();\t\\
        void setOneDevice(Device* pDevice, const bool transmitNow, MultiplexingContext* pContext);\t\\
        void setOneDevice(Device* pDevice, propName##PropertyData& data, const bool transmitNow, MultiplexingContext* pContext);\t\\
    }

    #define BLOCK_RW( blockName, propName )\t\\
    class blockName##_Type : public Abstract${className}\t\\
    {\t\\
        public:\t\\
            blockName##_Type(std::string name);\t\\
            ~blockName##_Type();\t\\
            void getOneDevice(Device* pDevice, const bool transmitNow, MultiplexingContext* pContext);\t\\
            void setOneDevice(Device* pDevice, const bool transmitNow, MultiplexingContext* pContext);\t\\
            void setOneDevice(Device* pDevice, propName##PropertyData& data, const bool transmitNow, MultiplexingContext* pContext);\t\\
    }
    
    """

hBlock = """BLOCK_${mode}( ${blockName}, ${propName} );
    """

hBottom = """
    /*---------------------------------------------------------------------------------------------------------
     * INTERFACE
     *---------------------------------------------------------------------------------------------------------
     * This is the public interface used from the FESA code to access the PLC service.
     */
    class ${className}
    {
        public:
            static inline Silecs::Service* theService()  { return Abstract${className}::theService(); }
            static inline Silecs::Cluster* theCluster()  { return Abstract${className}::theCluster(); }
            static void setup(const ServiceLocator* serviceLocator) { Abstract${className}::setup(serviceLocator); }
            static void cleanup() { Abstract${className}::cleanup(); }
            static bool isInitialized(){ return Abstract${className}::isInitialized(); }
            static Silecs::PLC* getPLC(Device* pDevice)
            {
                return Abstract${className}::theCluster()->getPLC(pDevice->plcHostName.get());
            }
    """

hDeclBlocks = """
            static ${className}_Type ${className};"""

hClosing = """           
        };
    }

    #endif /* ${className}_${className}_H_ */
    """

#=========================================================================
# C++ Source template (.cpp file)
#=========================================================================
cTop = """/*
 * ${className}.cpp
 *
 * Generated by SILECS framework tools 
 */
 
#include <${className}/Common/${className}.h>
#include <fesa-core/Synchronization/NoneContext.h>
#include <fesa-core/Synchronization/MultiplexingContext.h>
 
namespace ${className}
{
    //Global objects of the SILECS class
    Silecs::Service* Abstract${className}::pService_ = NULL;
    Silecs::Cluster* Abstract${className}::pCluster_ = NULL;
    bool Abstract${className}::isInitialized_ = false;
    """

cGlobal = """${blockName}_Type\t${className}::${blockName}("${blockName}");
    """
cPart1 = """
    //-------------------------------------------------------------------------------------------------------------
    // Constructor & Destructor methods
    
    Abstract${className}::Abstract${className}(std::string blockName): blockName_(blockName) {}
    Abstract${className}::~Abstract${className}() {}
    """

cBlockConstr = """
    ${blockName}_Type::${blockName}_Type(std::string name): Abstract${className}(name) {}
    ${blockName}_Type::~${blockName}_Type() {}
    """

cPart2 = """
    //---------------------------------------------------------------------------------------------------------
    // Set-up the SILECS components for the Abstract${className} class (service & cluster)

    void Abstract${className}::setup(const ServiceLocator* serviceLocator)
    {
        try
        {
            // Instantiate the singleton of the SILECS Service
            pService_ = Silecs::Service::getInstance();

            // Enable the SILECS diagnostic with user topics if any
            pService_->setArguments(serviceLocator->getUsrCmdArgs());

            // Instantiate the SILECS Cluster object for the given Class/Version
            GlobalDevice* pGlobalDevice = serviceLocator->getGlobalDevice();
            pCluster_ = pService_->getCluster( "${className}", pGlobalDevice->plcClassVersion.get());
            isInitialized_ = true;

            // Connect each PLC of the Cluster that is referred from the FESA instance
            std::vector<Device*> pDeviceCol = serviceLocator->getDeviceCollection();
            for(std::vector<Device*>::iterator pDeviceIter=pDeviceCol.begin(); pDeviceIter!= pDeviceCol.end(); pDeviceIter++)
            {
                Device* pDevice = *pDeviceIter;

                // Retrieve the PLC related to the current FESA device
                // (from 'plcHostName' FESA field defined on that purpose).
                Silecs::PLC* pPLC = pCluster_->getPLC(pDevice->plcHostName.get());

                // Update PLC registers from related FESA fields just before synchronising done at connection time
                updatePLCRegisters(pPLC, serviceLocator);

                // Connect the PLC if not already connected
                if (!pPLC->isEnabled())
                {   pPLC->connect(/*synchroMode=*/Silecs::FULL_SYNCHRO, /*connectNow=*/true);
                    if (pPLC->isConnected())
                        updateFesaFields(pPLC, serviceLocator);
                }
            }
        }
        catch (const Silecs::SilecsException& ex)
        {
            throw fesa::FesaException(__FILE__, __LINE__, ex.getMessage());
        }
    }

    //---------------------------------------------------------------------------------------------------------
    // Release all the SILECS resources
    void Abstract${className}::cleanup()
    {
        // Attention! This method is responsible to stop all the PLC connections
        // and to remove all the SILECS resources (Clusters and related components: PLCs, Devices, Registers, ..)
        // Calling method must ensure that no process is currently accessing these resources before cleaning.
        //
        Silecs::Service::deleteInstance();
    }
"""

updatePLCRegisters_start = """
    //---------------------------------------------------------------------------------------------------------
    //automatically called by the setup method at connection time)
    void Abstract${className}::updatePLCRegisters(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator)
    {
        fesa::NoneContext noneContext;
    """
updatePLCRegisters_body = """
        ${className}::${blockName}.setPLCDevices(pPLC, serviceLocator, false, &noneContext);"""
updatePLCRegisters_end = """
    }
"""

updateFesaFields_start = """
    //---------------------------------------------------------------------------------------------------------
    //automatically called by the setup method at connection time)
    void Abstract${className}::updateFesaFields(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator)
    {
        fesa::NoneContext noneContext;
    """
updateFesaFields_body = """
        ${className}::${blockName}.getPLCDevices(pPLC, serviceLocator, false, &noneContext);"""
updateFesaFields_end = """
    }
"""

cPart4 = """
    
    //---------------------------------------------------------------------------------------------------------
    // General methods to synchronize the FESA fields and related PLC registers of the FESA server
    // with or without PLC side access (send/recv) if requested.
    // get_ : [receive block from PLC +] update FESA fields with related PLC registers
    // set_ : update PLC registers with related FESA fields [+ send block to PLC]

    //---------------------------------------------------------------------------------------------------------

    void Abstract${className}::getAllDevices(const ServiceLocator* serviceLocator, const bool recvNow, MultiplexingContext* pContext)
    {
        if (recvNow) theCluster()->recv(blockName_);

        std::vector<Device*> deviceCol = serviceLocator->getDeviceCollection();
        for(std::vector<Device*>::iterator pDeviceIter=deviceCol.begin(); pDeviceIter!= deviceCol.end(); pDeviceIter++)
        {  getOneDevice(*pDeviceIter, false, pContext);
        }
    }

    void Abstract${className}::getPLCDevices(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator, const bool recvNow, MultiplexingContext* pContext)
    {
        if (recvNow) pPLC->recv(blockName_);

        std::vector<Device*> deviceCol = serviceLocator->getDeviceCollection();
        for(std::vector<Device*>::iterator pDeviceIter=deviceCol.begin(); pDeviceIter!= deviceCol.end(); pDeviceIter++)
        {   if ((*pDeviceIter)->plcHostName.get() == pPLC->getName())
            {  getOneDevice(*pDeviceIter, false, pContext);
            }
        }
    }

    void Abstract${className}::getSomeDevices(std::vector<Device*> deviceCol, const bool sendNow, MultiplexingContext* pContext)
    {
        for(std::vector<Device*>::iterator pDeviceIter=deviceCol.begin(); pDeviceIter!= deviceCol.end(); pDeviceIter++)
        {    getOneDevice(*pDeviceIter, sendNow, pContext);
        }
    }

    void Abstract${className}::getOneDevice(Device* pDevice, const bool recvNow, MultiplexingContext* pContext) {};

    //---------------------------------------------------------------------------------------------------------

    void Abstract${className}::setAllDevices(const ServiceLocator* serviceLocator, bool sendNow, MultiplexingContext* pContext)
    {
        std::vector<Device*> deviceCol = serviceLocator->getDeviceCollection();
        for(std::vector<Device*>::iterator pDeviceIter=deviceCol.begin(); pDeviceIter!= deviceCol.end(); pDeviceIter++)
        {  setOneDevice(*pDeviceIter, false, pContext);
        }

        if (sendNow) theCluster()->send(blockName_);
    }

    void Abstract${className}::setPLCDevices(Silecs::PLC* pPLC, const ServiceLocator* serviceLocator, bool sendNow, MultiplexingContext* pContext)
    {
        std::vector<Device*> deviceCol = serviceLocator->getDeviceCollection();
        for(std::vector<Device*>::iterator pDeviceIter=deviceCol.begin(); pDeviceIter!= deviceCol.end(); pDeviceIter++)
        {   if ((*pDeviceIter)->plcHostName.get() == pPLC->getName())
            {  setOneDevice(*pDeviceIter, false, pContext);
            }
        }

        if (sendNow) pPLC->send(blockName_);
    }

    void Abstract${className}::setSomeDevices(std::vector<Device*> deviceCol, bool sendNow, MultiplexingContext* pContext)
    {
        for(std::vector<Device*>::iterator pDeviceIter=deviceCol.begin(); pDeviceIter!= deviceCol.end(); pDeviceIter++)
        {    setOneDevice(*pDeviceIter, sendNow, pContext);
        }
    }

    void Abstract${className}::setOneDevice(Device* pDevice, const bool sendNow, MultiplexingContext* pContext) {};
    
    //---------------------------------------------------------------------------------------------------------
    """

cCommonGet = """
    void ${blockName}_Type::getOneDevice(Device* pDevice, const bool recvNow, MultiplexingContext* pContext)
    {
        if( !isInitialized_ )
        {
            throw fesa::FesaException(__FILE__, __LINE__, "SILECS-Service not initialized yet - ${className}::setup needs to be called before any plc-interaction can be done");
        }
        Silecs::PLC* pPLC  = pCluster_->getPLC(pDevice->plcHostName.get());
        Silecs::Device* pPLCDevice = pPLC->getDevice(pDevice->plcDeviceLabel.get());
"""

cRecv = """        if (recvNow) pPLCDevice -> recv(blockName_);        
    """

cGetStringReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            pDevice->${fesaFieldName}.set(pRegister->getValString().c_str(), pContext);
        }
"""

cGetStringArrayReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            const std::string** stdStringArray = pRegister->getRefStringArray(dim1);
            for (unsigned int i=0; i<dim1; i++)
            {
                pDevice->${fesaFieldName}.setString(stdStringArray[i]->c_str(), i, pContext);
            }
        }
"""

cGetScalarReg = """
        pDevice->${fesaFieldName}.set( pPLCDevice->getRegister("${regName}")->getVal${regType}(), pContext);"""
        
cGetArrayReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            pDevice->${fesaFieldName}.set(pRegister->getRef${regType}Array(dim1), dim1, pContext);
        }
        """
cGetArray2DReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            uint32_t dim2 = pRegister->getDimension2();
            pDevice->${fesaFieldName}.set(pRegister->getRef${regType}Array2D(dim1, dim2), dim1, dim2, pContext);
        }
"""
cGetUnsignedArray2DReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            uint32_t dim2 = pRegister->getDimension2();
            ${fesaType}* ${regName} = (${fesaType}*)calloc(dim1*dim2, sizeof(${fesaType}));
            pRegister->getVal${regType}Array2D(${regName}, dim1, dim2);
            pDevice->${fesaFieldName}.set(${regName}, dim1, dim2, pContext);
            free(${regName});
        }
"""

cGetUnsignedArrayReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            ${fesaType}* ${regName} = (${fesaType}*)calloc(dim1, sizeof(${fesaType}));
            pRegister->getVal${regType}Array(${regName}, dim1);
            pDevice->${fesaFieldName}.set(${regName}, dim1, pContext);
            free(${regName});
        }
    """



cCommonSet = """    
    void ${blockName}_Type::setOneDevice(Device* pDevice, const bool sendNow, MultiplexingContext* pContext)
    {
        if( !isInitialized_ )
        {
            throw fesa::FesaException(__FILE__, __LINE__, "SILECS-Service not initialized yet - ${className}::setup needs to be called before any plc-interaction can be done");
        }
        Silecs::PLC* pPLC  = pCluster_->getPLC(pDevice->plcHostName.get());
        Silecs::Device* pPLCDevice = pPLC->getDevice(pDevice->plcDeviceLabel.get());
"""

cSend = """
        if (sendNow) pPLCDevice->send(blockName_);
"""

cDatatypeSet = """
    void ${blockName}_Type::setOneDevice(Device* pDevice, ${propName}PropertyData& data, bool sendNow, MultiplexingContext* pContext)
    {
        if( !isInitialized_ )
        {
            throw fesa::FesaException(__FILE__, __LINE__, "SILECS-Service not initialized yet - ${className}::setup needs to be called before any plc-interaction can be done");
        }
        Silecs::PLC* pPLC = pCluster_->getPLC(pDevice->plcHostName.get());
        Silecs::Device* pPLCDevice = pPLC->getDevice(pDevice->plcDeviceLabel.get());
"""

cSetStringReg = """
        pPLCDevice->getRegister("${regName}")->setValString(pDevice->${fesaFieldName}.get(${context}));
"""

cSetStringArrayReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            std::string stdStringArray[dim1];
            uint32_t fesaDim1;
            const char** cStringArray = pDevice->${fesaFieldName}.get(fesaDim1${context});
            for (unsigned int i=0; i<dim1; i++) stdStringArray[i] = (const char*)cStringArray[i];
            pRegister->setValStringArray(stdStringArray, dim1);
        }
"""

cSetScalarReg = """
        pPLCDevice->getRegister("${regName}")->setVal${regType}( ${cCast}pDevice->${fesaFieldName}.get(${context}));"""
            
cSetArrayReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            uint32_t fesaDim1;
            pRegister->setVal${regType}Array(pDevice->${fesaFieldName}.get(fesaDim1${context}), dim1);
        }
    """

cSetArray2DReg = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            uint32_t dim2 = pRegister->getDimension2();
            uint32_t fesaDim1,fesaDim2;
            pRegister->setVal${regType}Array2D(pDevice->${fesaFieldName}.get(fesaDim1, fesaDim2${context}), dim1, dim2);
        }
"""

cSetStringRegData = """
        (data.is${fesaFieldName_upper}Available()) ? pPLCDevice->getRegister("${regName}")->setValString(data.${fesaFieldName}.get()) :
                                     pPLCDevice->getRegister("${regName}")->setValString(pDevice->${fesaFieldName}.get(${context}));"""
                                    
cSetStringArrayRegData = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            std::string stdStringArray[dim1];
            uint32_t fesaDim1;
            const char** cStringArray = (data.is${fesaFieldName_upper}Available() ? data.${fesaFieldName}.get(fesaDim1) : pDevice->${fesaFieldName}.get(fesaDim1${context}));
            for (unsigned int i=0; i<dim1; i++) stdStringArray[i] = (const char*)cStringArray[i];
            pRegister->setValStringArray(stdStringArray, dim1);
        }
"""

cSetScalarRegData = """
        (data.is${fesaFieldName_upper}Available()) ? pPLCDevice->getRegister("${regName}")->setVal${regType}( ${cCast}data.${fesaFieldName}.get()) :
                                          pPLCDevice->getRegister("${regName}")->setVal${regType}( ${cCast}pDevice->${fesaFieldName}.get(${context}));"""
cSetArrayRegData = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            uint32_t fesaDim1;
            (data.is${fesaFieldName_upper}Available()) ? pRegister->setVal${regType}Array( data.${fesaFieldName}.get(fesaDim1), dim1) :
                                        pRegister->setVal${regType}Array( pDevice->${fesaFieldName}.get(fesaDim1${context}), dim1);
        }
    """
cSetArray2DRegData = """
        {
            Silecs::Register* pRegister = pPLCDevice->getRegister("${regName}");
            uint32_t dim1 = pRegister->getDimension1();
            uint32_t dim2 = pRegister->getDimension2();
            uint32_t fesaDim1,fesaDim2;
            (data.is${fesaFieldName_upper}Available()) ? pRegister->setVal${regType}Array2D(data.${fesaFieldName}.get(fesaDim1, fesaDim2), dim1, dim2) :
                                         pRegister->setVal${regType}Array2D(pDevice->${fesaFieldName}.get(fesaDim1, fesaDim2${context}), dim1, dim2);
        }
"""

makeDesign = """# Include SILECS library path
SILECS_PATH ?= ${centralMakefilePath}

# Additional compiler warning flags
override WARNFLAGS += 

# Additional compiler flags
COMPILER_FLAGS += -I$(SILECS_PATH)/include
COMPILER_FLAGS +=
LINKER_FLAGS +=

# Additional headers (Custom.h Specific.h ...) which need to be installed
EXTRA_HEADERS += 
"""
makeDeploy = """# Include SILECS library path
SILECS_PATH ?= ${silecsBasePath}

SNAP7_BASE = ${snap7BasePath}

# Additional compiler warning flags
override WARNFLAGS += 

# Additional libs and flags which are common to the Realtime and Server part
COMPILER_FLAGS +=
LINKER_FLAGS += -L$(SILECS_PATH)/lib/$(CPU) -lsilecs-comm
LINKER_FLAGS += -L$(SNAP7_BASE)/bin/$(CPU)-linux -lsnap7
#add default search path for dynamic snap7 library
LINKER_FLAGS += -Wl,-rpath,$(SNAP7_BASE)/bin/$(CPU)-linux,-rpath,/usr/lib
LINKER_FLAGS +=

# Additional libs and flags which are specific to the Realtime part
COMPILER_RT_FLAGS += 
LINKER_RT_FLAGS += 

# Additional libs and flags which are specific to the Server part
COMPILER_SERVER_FLAGS += 
LINKER_SERVER_FLAGS += 

# Additional headers (Custom.h Specific.h ...) which need to be released
EXTRA_HEADERS += 
"""
    
#=========================================================================
# FESA .cproject file
#=========================================================================

def genCProject():
    return cproject

#=========================================================================
# Header file (.h) code generation sub-functions
#================    =========================================================


def genHTop(className):
    return htop.replace('${className}', className )

def genHTop2(className):
    return hTop2.replace('${className}', className)
   
def genHBlock(mode, blockName, propName):
    return hBlock.replace('${mode}', mode).replace('${blockName}', blockName).replace('${propName}', propName)

def genHBottom(className):
    return hBottom.replace('${className}', className)

def genHDeclBlocks(className): 
    return hDeclBlocks.replace('${className}', className)

def genHClosing(className):
    return hClosing.replace('${className}', className)

#=========================================================================
# C++ file (.cpp) code generation sub-functions
#=========================================================================
def genCTop(className):
    return cTop.replace('${className}', className)

def genCGlobal(className, blockName):
    return cGlobal.replace('${className}', className).replace('${blockName}', blockName)

def genCPart1(className):
    return cPart1.replace('${className}', className)

def genCBlockConstr(blockName, className):
    return cBlockConstr.replace('${className}', className).replace('${blockName}', blockName)

def genCPart2(className):
    return cPart2.replace('${className}', className)

def genCCommonGet(blockName,className):
    return cCommonGet.replace('${className}', className).replace('${blockName}', blockName)

def updatePLCRegisters(className, blockList):
    code = updatePLCRegisters_start.replace('${className}', className)
    for block in blockList:
        if block.isConfiguration(): # only config Blocks are updated on PLC during startup !
            code += updatePLCRegisters_body.replace('${className}', className).replace('${blockName}', block.name)
    code += updatePLCRegisters_end
    return code

def updateFesaFields(className, blockList):
    code = updateFesaFields_start.replace('${className}', className)
    for block in blockList:
        if block.isReadable() and not block.isConfiguration():
            code += updateFesaFields_body.replace('${className}', className).replace('${blockName}', block.name)
    code += updateFesaFields_end
    return code

def genCPart4(className):
    return cPart4.replace('${className}', className)

def genCGetStringReg(register):
    return cGetStringReg.replace('${regName}', register.name).replace('${fesaFieldName}', register.getFesaFieldName())

def genCGetStringArrayReg(register):
    return cGetStringArrayReg.replace('${regName}', register.name).replace('${fesaFieldName}', register.getFesaFieldName())

def genCGetScalarReg(register):
    return cGetScalarReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName())

def genCGetArrayReg(register):
    if register.isUnsigned():
        return cGetUnsignedArrayReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${fesaType}', register.getFesaType())
    else:
        return cGetArrayReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName())

def genCGetArray2DReg(register):
    if register.isUnsigned():
        return cGetUnsignedArray2DReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaType}', register.getFesaType()).replace('${fesaFieldName}', register.getFesaFieldName())
    else:
        return cGetArray2DReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName())

def genCCommonSet(blockName,className):
    return cCommonSet.replace('${className}', className).replace('${blockName}', blockName)

def genCDatatypeSet(blockName,propName,className):
    return cDatatypeSet.replace('${className}', className).replace('${blockName}', blockName).replace('${propName}', propName)

def genCSetStringReg(register):
    context = "pContext"
    if register.isConfiguration():
        context = ""
    return cSetStringReg.replace('${regName}', register.name).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genCSetStringArrayReg(register):
    context = ", pContext"
    if register.isConfiguration():
        context = ""
    return cSetStringArrayReg.replace('${regName}', register.name).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genCSetScalarReg(register):
    cCast = ""
    context = "pContext"
    if register.isUnsigned():
        cCast = "(" + register.getCType() + ")"
    if register.isConfiguration():
        context = ""
    return cSetScalarReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${cCast}', cCast ).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genCSetArrayReg(register):
    context = ", pContext"
    if register.isConfiguration():
        context = ""
    return cSetArrayReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genCSetArray2DReg(register):
    context = ", pContext"
    if register.isConfiguration():
        context = ""
    return cSetArray2DReg.replace('${regName}', register.name).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)
        
def genCSetStringRegData(register):
    context = "pContext"
    if register.isConfiguration():
        context = ""
    return cSetStringRegData.replace('${regName}', register.name).replace('${fesaFieldName_upper}', register.getFesaFieldNameCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genCSetStringArrayRegData(register):
    context = ", pContext"
    if register.isConfiguration():
        context = ""
    return cSetStringArrayRegData.replace('${regName}', register.name).replace('${fesaFieldName_upper}', register.getFesaFieldNameCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genCSetScalarRegData(register):
    cCast = ""
    context = "pContext"
    if register.isUnsigned():
        cCast = "(" + register.getCType() + ")"
    if register.isConfiguration():
        context = ""
    return cSetScalarRegData.replace('${regName}', register.name).replace('${fesaFieldName_upper}', register.getFesaFieldNameCapitalized()).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${cCast}', cCast).replace('${context}', context)

def genCSetArrayRegData(register):
    context = ", pContext"
    if register.isConfiguration():
        context = ""
    return cSetArrayRegData.replace('${regName}', register.name).replace('${fesaFieldName_upper}', register.getFesaFieldNameCapitalized()).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genCSetArray2DRegData(register):
    context = ", pContext"
    if register.isConfiguration():
        context = ""
    return cSetArray2DRegData.replace('${regName}', register.name).replace('${fesaFieldName_upper}', register.getFesaFieldNameCapitalized()).replace('${regType}', register.getSilecsTypeCapitalized()).replace('${fesaFieldName}', register.getFesaFieldName()).replace('${context}', context)

def genMakeDesign(centralMakefilePath):
    return makeDesign.replace('${centralMakefilePath}', centralMakefilePath)

def genMakeDeploy(silecsBasePath,snap7BasePath):
    return makeDeploy.replace('${silecsBasePath}', silecsBasePath).replace('${snap7BasePath}', snap7BasePath)
