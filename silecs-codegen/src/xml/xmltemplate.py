#!/usr/bin/python
# Copyright 2016 CERN and GSI
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import getpass

#=========================================================
# SILECS Header
#=========================================================
silecsHeader = """<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Design silecs-version="%s" created="%s" updated="%s" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <Information>
        <Owner user-login="%s"/>
        <Editor user-login="%s"/>
    </Information>
  <SILECS-Class domain="TEST" name="SilecsHeader" version="1.0.0">
    <Acquisition-Block name="hdrBlk">
      <Acquisition-Register name="_version">
          <string string-length="16" format="string"/>
      </Acquisition-Register>
      <Acquisition-Register name="_checksum">
          <scalar format="uint32" />
      </Acquisition-Register>
      <Acquisition-Register name="_user">
          <string string-length="16" format="string"/>
      </Acquisition-Register>
      <Acquisition-Register name="_date">
          <scalar format="dt" />
      </Acquisition-Register>
    </Acquisition-Block>
  </SILECS-Class>
</SILECS-Design>
"""
def getSilecsHeader(silecsVersion):
	date = time.strftime("%x")
	owner = getpass.getuser()
	return silecsHeader % (silecsVersion, date, date, owner, owner)

#=========================================================
# DESIGN TEMPLATE
#=========================================================
designTemplate = """<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Design silecs-version="%s" created="%s" updated="%s" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="%s">
    <Information>
        <Owner user-login="%s"/>
        <Editor user-login="%s"/>
    </Information>
    <SILECS-Class name="%s" version="0.1.0" domain="OPERATIONAL" >
        <Acquisition-Block name="MyBlock" generateFesaProperty="true">
            <Acquisition-Register name="myRegister" generateFesaValueItem="true">
                <scalar format="int32"/>
            </Acquisition-Register>
        </Acquisition-Block>
    </SILECS-Class>
</SILECS-Design>
"""
def getDesignTemplate(designName, schemaPath, silecsVersion):
	date = time.strftime("%x")
	owner = getpass.getuser()
	return designTemplate % (silecsVersion, date, date, schemaPath, owner, owner, designName)


#=========================================================
# DEPLOY TEMPLATE
#=========================================================
deployTemplate = """<?xml version="1.0" encoding="UTF-8"?>
<SILECS-Deploy silecs-version="%s" created="%s" updated="%s" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="%s">
    <Information>
        <Owner user-login="%s"/>
        <Editor user-login="%s"/>
    </Information>
    <Deploy-Unit name="%s" version="0.1.0"/>
    <SilecsDesign silecs-design-name="" silecs-design-version=""/>
        
	<Controller host-name="">
	    <Siemens-PLC system="TIA-PORTAL" model="SIMATIC_S7-300" protocol="DEVICE_MODE" base-DB-number="1">
	        <Device silecs-device-label="dev0" silecs-design-ref="" fesa-device-name="" fesa-fec-name=""/>
	    </Siemens-PLC>
	</Controller>
</SILECS-Deploy>
"""

def getDeployTemplate(deployName, schemaPath, silecsVersion):
	date = time.strftime("%x")
	owner = getpass.getuser()
	return deployTemplate % (silecsVersion, date, date, schemaPath, owner, owner, deployName)
