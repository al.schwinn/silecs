/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-diagnostic/loginhandler.h>

#ifdef WITH_RBAC
    extern std::string UserName;

    loginHandler::loginHandler()
    {
    }

    bool loginHandler::requestLogin()
    {
        LoginDialog *loginDialog = new LoginDialog();

        for(int i=0;i<NUMBER_PASSWORD_ATTEMPTS;i++)
        {
            try
            {
                //Popup interface requesting password

                loginDialog->setModal(true);

                if(loginDialog->exec()==1)
                {
                    auto_ptr<cmw::rbac::TokenClass> token = cmw::rbac::LoginContext::login("SILECS Diagnostic Tool",
                                                                                           loginDialog->getUsername(),
                                                                                           loginDialog->getPassword());
                    if (token.get() != NULL)
                    {
                        //password correct
                        extern std::string UserName;
                        UserName = loginDialog->getUsername();
                        delete loginDialog;
                        return true;
                    }
                }
                else
                {
                    //cancell button was pressed
                    delete loginDialog;
                    return false;
                }
            }
            catch (const cmw::rbac::AuthenticationFailure & ex)
            {
                qDebug() << "Autentication failure!";
            }
            catch(...)
            {
                qDebug() << "unknown exception";
            }
        }

        delete loginDialog;
        return false;
    }
#endif
