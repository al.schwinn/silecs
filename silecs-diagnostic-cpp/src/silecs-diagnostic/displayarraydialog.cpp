/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-diagnostic/displayarraydialog.h>
#include <silecs-diagnostic/generated/ui_displayarraydialog.h>

#include <QtCore/QDebug>

DisplayArrayDialog::DisplayArrayDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DisplayArrayDialog)
{
    ui->setupUi(this);

}

DisplayArrayDialog::~DisplayArrayDialog()
{
    delete ui;
}

void DisplayArrayDialog::setDataVector(std::vector<QString> dataVector, bool editable, unsigned long dim2)
{
    this->dataVector = dataVector;

    // display informations
    if(dim2 <= 1)   // 1d array
    {
        int numberOfValues = this->dataVector.size();
        ui->tableWidget->setColumnCount(1);
        ui->tableWidget->setRowCount(numberOfValues);

        for(int i= 0; i< numberOfValues; i++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(dataVector[i],1);

            if(!editable) // default flag contain editable
                item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);

            ui->tableWidget->setItem(0,i,item);
        }
    }
    else    // 2d array
    {
        unsigned long dim1 = (this->dataVector.size())/dim2;
        ui->tableWidget->setColumnCount(dim2);
        ui->tableWidget->setRowCount(dim1);

        for(unsigned long i=0; i<dim1; i++)
        {
            for(unsigned long j=0; j<dim2; j++)
            {
                QTableWidgetItem *item = new QTableWidgetItem(dataVector[i*dim2+j],1);
                if(!editable)   // default flag contains editable
                    item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);

                ui->tableWidget->setItem(i,j,item);
            }
        }
    }
    // display informations end
}

std::vector<QString> DisplayArrayDialog::getDataVector()
{
    return this->dataVector;
}

void DisplayArrayDialog::on_buttonBox_accepted()
{
    int numberOfValues = this->dataVector.size();
    for(int i= 0; i< numberOfValues; i++)
        this->dataVector[i] = ui->tableWidget->item(0,i)->text();

}
