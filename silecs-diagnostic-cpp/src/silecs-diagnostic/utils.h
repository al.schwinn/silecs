/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_UTILS_H
#define SILECS_DIAG_UTILS_H

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>

#include <QtCore/QObject>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QLabel>

#include <silecs-diagnostic/item.h>

namespace Silecs
{
	class Cluster;
	class Register;
	class Device;
	class PLC;
}

namespace Utils {

	void logError(QTextBrowser* errorConsole, std::string message);

	void logInfo(QTextBrowser* errorConsole, std::string message);

	void logDebugIf(QTextBrowser* errorConsole, std::string message);

    /**
     * Convert an int number in the equivalent string
     */
    std::string toString(int number);

    /**
     * Convert an unsigned int number in the equivalent string
     */
    std::string toString(unsigned int number);

    /**
     * Convert a long number in the equivalent string
     */
    std::string toString(long number);

    /**
     * Convert a unsgined long number in the equivalent string
     */
    std::string toString(unsigned long number);

    /**
     * Convert a long number in the equivalent string (hex)
     */
    std::string toString(long number, int base);

    /**
     * Add an element to the tree
     */
    Item* addTreeItem(Item *parent,QString name ,QString description, QString type, void* linkedObject,QString icon);

    /**
     * Remove the last element from a string
     */
    char* chop(char *string);

    /**
     * Display information about the cluster in the given console
     */
    void displayClusterInformation(Silecs::Cluster *cluster,QTextEdit *console);

    /**
     * Display information about the plc in the given console
     */
    void displayPLCInformation(Silecs::PLC *plc,QTextEdit *console);

    /**
     * Display information about the device in the given console
     */
    void displayDeviceInformation(Silecs::Device *device,QTextEdit *console);

    /**
     * Display information about the register in the given console
     */
    void displayRegisterInformation(Silecs::Register *reg,QTextEdit *console);

    /**
     * Display register value in multiple formats
     */
    void displayRegisterValue(Silecs::Register *reg,
                              QLabel *binValueLabel,
                              QLabel *hexValueLabel,
                              QLabel *decValueLabel,
                              QLabel *asciiValueLabel
                              );

    /**
     * Return a map of the list of cluster the user can access
     * < ClassName, ClassVersionList >
     */
    std::map<std::string, std::vector<std::string>* >getUserAccessibleCluster(std::string userName);

}
#endif // SILECS_DIAG_UTILS_H
