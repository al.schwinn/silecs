/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <QtCore/QThread>
#include <QtWidgets/QApplication>
#include <QtWidgets/QSplashScreen>

#include <silecs-diagnostic/constants.h>
#include <silecs-diagnostic/diagnostictoolmainview.h>
#include <silecs-diagnostic/loginhandler.h>
#include <silecs-diagnostic/silecsmodule.h>
#include <silecs-communication/interface/utility/XMLParser.h>


silecsModule *mysilecs;

class I : public QThread
{
public:
        static void sleep(unsigned long secs) {
                QThread::sleep(secs);
        }
};

// Logged in user. (To be set at runtime)
std::string UserName;


    // main with splash screen and login
    int main(int argc, char *argv[])
    {
		mysilecs = NULL;
    	try
    	{
    		Silecs::XMLParser::init();
#ifdef WITH_RBAC
			//Login
			loginHandler* login = new loginHandler();
			if(!login->requestLogin())
			{
				return 0;
			}
#endif

            QApplication app(argc, argv);
			QPixmap pixmap( ":/Images/splash-silecs.png" );

			QSplashScreen splash(pixmap);
			splash.show();

			diagnosticToolMainView mainWin;
			mainWin.setWindowTitle("SILECS Diagnostic Tool");

			if (argc >= 3) // an argument was passed
			{
				if (strcmp(argv[1],"-d") == 0)
					mainWin.setDeployFile(argv[2]);
			}

			I::sleep(SPLASH_SCREEN_TIME);

			mainWin.showMaximized();
			splash.finish(&mainWin);
				return app.exec();
    	}
        catch(std::string *str)
        {
        	std::cout << str;
        }
        catch(std::exception& ex)
        {
        	std::cout << ex.what();
        }
    	catch(...)
    	{
    		std::cout << "Unexpected error caught in Main. Please notify SILECS support";
    	}
        return -1;
    }
