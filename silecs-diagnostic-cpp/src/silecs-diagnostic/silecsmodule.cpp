/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#include <QtCore/QDebug>

#include <silecs-diagnostic/silecsmodule.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/StringUtilities.h>

extern std::string UserName;


silecsModule::silecsModule(std::string logTopics, QTextBrowser *messageConsole):
		messageConsole_(messageConsole), debugLoggingEnabled_(false)
{
    silecsService = NULL;
    silecsCluster = NULL;
    counterConnectedPLC = 0;

    if(logTopics.length() == 0)
    {
    	Utils::logInfo(messageConsole_,"no log topics defined");
        silecsService  = Silecs::Service::getInstance(0, NULL);
    }
    else
    {
    	Utils::logInfo(messageConsole_,"the following log topics will be used: " + logTopics);
        char *a = new char[logTopics.size()+1];
        a[logTopics.size()]=0;
        memcpy(a,logTopics.c_str(),logTopics.size());
        char *argv[2] = { (char *)"-plcLog", a };
        silecsService  = Silecs::Service::getInstance(2,argv);
    }
}

silecsModule::~silecsModule()
{
    silecsService->deleteInstance();
    silecsService = NULL;
    silecsCluster = NULL;
    Utils::logInfo(messageConsole_,"silecsModule destructor executed");
}

void silecsModule::setLogTopics(std::string logTopics)
{
    if(silecsService != NULL)
    {
        if(!silecsService->setLogTopics(logTopics))
        	Utils::logError(messageConsole_,"Error while setting log topics ");
    }
}

bool silecsModule::isDebugLogEnabled()
{
	return debugLoggingEnabled_;
}

void silecsModule::enableDebugLog()
{
	debugLoggingEnabled_ = true;
}

void silecsModule::updatePLCRunState(Item *plcItem)
{
	int index = 1;
	try
	{
		Silecs::PLC* plc = (Silecs::PLC*)(plcItem->getLinkedObject());
		if( !plc->isConnected() )
		{
			plcItem->setText(index,"");
			plcItem->setBackgroundColor(index,Qt::white);
		}
		if( plc->isRunning() )
		{
			plcItem->setText(index,"running");
			plcItem->setBackgroundColor(index,Qt::green);
		}
		else
		{
			plcItem->setText(index,"stopped");
			plcItem->setBackgroundColor(index,Qt::yellow);
		}
	}
	catch(const Silecs::SilecsException& ex2)
	{
		plcItem->setText(index,"state unknown");
		plcItem->setBackgroundColor(index,Qt::red);
		std::string message = "Failed to obtain plc run-state: ";
		message +=  ex2.what();
		Utils::logError(messageConsole_,message);
	}

}

Item *silecsModule::generateTree(string className, string deployFile)
{
    Utils::logInfo(messageConsole_,"Loading deploy file: " + deployFile);
    XMLParser parserDeploy(deployFile,true);

    Item *root =  new Item(silecsCluster);
    if( className == HEADER_NAME )
    {
        silecsCluster = silecsService->getCluster(HEADER_NAME,HEADER_VERSION);
        root->setText(0,QString::fromStdString(HEADER_NAME+" v"+HEADER_VERSION));
    }
    else
    {
        ElementXML silecsDesign = parserDeploy.getFirstElementFromXPath("/SILECS-Deploy/SilecsDesign[@silecs-design-name='" + className + "']");
        string classVersion = silecsDesign.getAttribute("silecs-design-version");
        silecsCluster = silecsService->getCluster(className,classVersion);
        root->setText(0,QString::fromStdString(className+" v"+classVersion));
    }
    root->setWhatsThis(0,QString::fromStdString(CLUSTER_TYPE));
    root->setLinkedObject(silecsCluster);

    ElementXML deployUnitNode = parserDeploy.getFirstElementFromXPath("/SILECS-Deploy/Deploy-Unit");
    string deployName = deployUnitNode.getAttribute("name");
    std::size_t deployFolderPos = deployFile.find(deployName);
    string deployProjectPath = deployFile.substr(0,deployFolderPos) + deployName;
    boost::ptr_vector<ElementXML> controllerNodes = parserDeploy.getElementsFromXPath_throwIfEmpty("/SILECS-Deploy/Controller");
    boost::ptr_vector<ElementXML>::const_iterator controllerIter;
    for(controllerIter = controllerNodes.begin(); controllerIter != controllerNodes.end(); controllerIter++)
    {
        std::string plcName = controllerIter->getAttribute("host-name");
        Silecs::PLC *plc;
        string parameterFile = "";
        try
        {
            parameterFile = deployProjectPath + "/generated-silecs/client/" + plcName + ".silecsparam";
            Utils::logInfo(messageConsole_,"Loading parameter file: " + parameterFile);
            plc = silecsCluster->getPLC(plcName,parameterFile);
        }
        catch(const Silecs::SilecsException& ex2)
        {
            Utils::logError(messageConsole_,"Error while loading "+ plcName+". "+ ex2.what());
            continue;
        }

        // add plc on the tree
        Item *plcItem = Utils::addTreeItem(root,QString::fromStdString(plcName),"",QString::fromStdString(PLC_TYPE),plc,":/Images/PLC.png");

        boost::ptr_vector<ElementXML> instances;
        XMLParser parseParam(parameterFile,true);
        try
        {
            instances = parseParam.getElementsFromXPath_throwIfEmpty("/SILECS-Param/SILECS-Mapping/SILECS-Class[@name='" + className + "']/Instance");
        }
        catch(const Silecs::SilecsException& ex2)
        {
            Utils::logError(messageConsole_,"Failed to fetch instances for class '" + className + "' from parameter-file '" + parameterFile + "':" + ex2.what());
            break;
        }

        boost::ptr_vector<ElementXML>::iterator pInstanceIter;
        for(pInstanceIter = instances.begin(); pInstanceIter != instances.end(); ++pInstanceIter)
        {
            std::string deviceName = pInstanceIter->getAttribute("label");
            Utils::logInfo(messageConsole_,"found device: '" + deviceName + "' in parameter-file");
            Silecs::Device *device = plc->getDevice(deviceName);

            // add device on the tree
            Item *deviceItem = Utils::addTreeItem(plcItem,QString::fromStdString(deviceName),"",QString::fromStdString(DEVICE_TYPE),device,":/Images/DEV.png");

            // get register List for the current device
            std::string registerList = device->getRegisterList();
            istringstream registerListSplitted(registerList);

            do
            {
                std::string registerName;
                registerListSplitted >> registerName;

                //avoid last empty register
                if(registerName.compare("")==0) break;

                Utils::logInfo(messageConsole_,"found Register: '" + registerName + "' in parameter file");
                Silecs::Register *reg = device->getRegister(registerName);

                Item *registerItem = Utils::addTreeItem(deviceItem,QString::fromStdString(registerName),"",QString::fromStdString(REGISTER_TYPE),reg,":/Images/REG.png" );

                // Set the block name
                registerItem->setText(1 , QString::fromStdString(reg->getBlockName()));

                // Color background of input and output buffer
                registerItem->setBackgroundColor(2,QColor(255,255,204));//light yellow
                registerItem->setBackgroundColor(3,QColor(204,255,255));//light blue

            }while(registerListSplitted);
        }
    } // END OF PLC LOOP

    // Reset the number of connected PLC
    this->counterConnectedPLC =0;
    return root;
}


void silecsModule::setScalarDataInDeviceFromItem(Item *currentItem, std::string blockName) throw (std::string*)
{
    Silecs::Device* device = (Silecs::Device*)(currentItem->getLinkedObject());

    // PLC and device name just for error messages
    std::string plcName = ((Silecs::PLC*)dynamic_cast<Item*>(currentItem->parent())->getLinkedObject())->getName();
    std::string deviceName = device->getLabel();

    std::string registerList = device->getRegisterList();
    istringstream registerListSplitted(registerList);

    int registerIndex = -1;
    do
    {
        registerIndex++;
        std::string registerName;
        registerListSplitted >> registerName;

        qDebug() << "get Register"<<QString::fromStdString(registerName);
        //avoid last empty register

        if(registerName.compare("")==0) break;

        Silecs::Register *reg = device->getRegister(registerName);

        // analyse just the register within the selected block
        if(reg->getBlockName().compare(blockName)!=0)
        {
            qDebug() << "Continue";
            continue;
        }

        std::string stringValue = currentItem->child(registerIndex)->text(3).toStdString();
        if(reg->isScalar()) //TODO: What happens for non-scalar values ?
            reg->setScalarfromString(stringValue);

    }while(registerListSplitted);
}


// FIXME TODO move to class 'Silecs::SilecsRegister', like done for 'setScalarfromString', so it can be reused internally ( e.g. default values for configuration fields )
void silecsModule::setArrayRegValue(Item *currentItem, std::vector<QString> *dataVector)
{
    Silecs::Register* reg = (Silecs::Register*)currentItem->getLinkedObject();
    Silecs::FormatType format = reg->getFormat();

    //get device an plc name for message errors
    std::string deviceName = ((Silecs::Device*)dynamic_cast<Item*>(currentItem->parent())->getLinkedObject())->getLabel();
    std::string plcName = ((Silecs::PLC*)dynamic_cast<Item*>(currentItem->parent()->parent())->getLinkedObject())->getName();
    std::string registerName = reg->getName();

    // convert to the proper format
    switch(format)
    {
        case uInt8:
        {
            Utils::logError(messageConsole_,"format = uInt8");
            if(reg->isDoubleArray()) // is 2DArray
            {
                uint8_t vector[reg->getDimension1()][reg->getDimension2()];
                uint16_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<uint16_t>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt8");
                        else
                            vector[i][j] = (uint8_t)val;
                    }
                }
                reg->setValUInt8Array2D((const uint8_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "uInt8 2D array set";
            }
            else
            {
                uint8_t vector[reg->getDimension1()];
                // Cast first to uint16_t to avoid conversion to ASCII character
                uint16_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<uint16_t>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt8");
                    else
                        vector[i] = (uint8_t)val;
                }
                reg->setValUInt8Array(vector,reg->getDimension1());
                qDebug() << "uInt8 array set";
            }
            break;
        }
        case Int8:
        {
            qDebug() << "format = Char";
            if(reg->isDoubleArray())
            {
                int8_t vector[reg->getDimension1()][reg->getDimension2()];
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(QString((*dataVector)[i*reg->getDimension2()+j]).toStdString().length()!=1)
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int8");
                        else
                            vector[i][j] = QString((*dataVector)[i*reg->getDimension2()+j]).toStdString()[0];
                    }
                }
                reg->setValInt8Array2D((const int8_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "Int8 2D array set";
            }
            else
            {
                int8_t vector[reg->getDimension1()];
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(QString((*dataVector)[i]).toStdString().length()!=1)
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int8");
                    else
                        vector[i] = QString((*dataVector)[i]).toStdString()[0];
                }
                reg->setValInt8Array(vector,reg->getDimension1());
                qDebug() << "Int8 array set";
            }
            break;
        }
        case uInt16:
        {
            qDebug() << "format = uInt16";
            if(reg->isDoubleArray())
            {
                uint16_t vector[reg->getDimension1()][reg->getDimension2()];
                uint16_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<uint16_t>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt16");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValUInt16Array2D((const uint16_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "uInt16 2D array set";
            }
            else
            {
                uint16_t vector[reg->getDimension1()];
                uint16_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<uint16_t>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt16");
                    else
                        vector[i] = val;
                }
                reg->setValUInt16Array(vector,reg->getDimension1());
                qDebug() << "uInt16 array set";
            }
            break;
        }
        case Int16:
        {
            qDebug() << "format = Int16";
            if(reg->isDoubleArray())
            {
                int16_t vector[reg->getDimension1()][reg->getDimension2()];
                int16_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<short>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int16");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValInt16Array2D((const int16_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "Int16 2D array set";
            }
            else
            {
                int16_t vector[reg->getDimension1()];
                int16_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<short>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int16");
                    else
                        vector[i] = val;
                }
                reg->setValInt16Array(vector,reg->getDimension1());
                qDebug() << "Int16 array set";
            }
            break;
        }
        case uInt32:
        {
            qDebug() << "format = uInt32";
            if(reg->isDoubleArray())
            {
                uint32_t vector[reg->getDimension1()][reg->getDimension2()];
                uint32_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<uint32_t>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt32");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValUInt32Array2D((const uint32_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "uInt32 2D array set";
            }
            else
            {
                uint32_t vector[reg->getDimension1()];
                uint32_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<uint32_t>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt32");
                    else
                        vector[i] = val;
                }
                reg->setValUInt32Array(vector,reg->getDimension1());
                qDebug() << "uInt32 array set";
            }
            break;
        }
        case Int32:
        {
            qDebug() << "format = Int32";
            if(reg->isDoubleArray())
            {
                int32_t vector[reg->getDimension1()][reg->getDimension2()];
                int32_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<int32_t>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int32");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValInt32Array2D((const int32_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "Int32 2D array set";
            }
            else
            {
                int32_t vector[reg->getDimension1()];
                int32_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<int32_t>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int32");
                    else
                        vector[i] = val;
                }
                reg->setValInt32Array(vector,reg->getDimension1());
                qDebug() << "Int32 array set";
            }
            break;
        }
        case uInt64:
        {
            qDebug() << "format = uInt64";
            if(reg->isDoubleArray())
            {
                uint64_t vector[reg->getDimension1()][reg->getDimension2()];
                uint64_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<uint64_t>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt64");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValUInt64Array2D((const uint64_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "uInt64 2D array set";
            }
            else
            {
                uint64_t vector[reg->getDimension1()];
                uint64_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<uint64_t>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a uInt64");
                    else
                        vector[i] = val;
                }
                reg->setValUInt64Array(vector,reg->getDimension1());
                qDebug() << "uInt64 array set";
            }
            break;
        }
        case Int64:
        {
            qDebug() << "format = Int64";
            if(reg->isDoubleArray())
            {
                int64_t vector[reg->getDimension1()][reg->getDimension2()];
                int64_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<int64_t>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int64");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValInt64Array2D((const int64_t*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "Int64 2D array set";
            }
            else
            {
                int64_t vector[reg->getDimension1()];
                int64_t val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<int64_t>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Int64");
                    else
                        vector[i] = val;
                }
                reg->setValInt64Array(vector,reg->getDimension1());
                qDebug() << "Int64 array set";
            }
            break;
        }
        case Float32:
        {
            qDebug() << "format = Float32";
            if(reg->isDoubleArray())
            {
                float vector[reg->getDimension1()][reg->getDimension2()];
                float val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<float>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Float32");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValFloat32Array2D((const float*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "Float32 2D array set";
            }
            else
            {
                float vector[reg->getDimension1()];
                float val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<float>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Float32");
                    else
                        vector[i] = val;
                }
                reg->setValFloat32Array(vector,reg->getDimension1());
                qDebug() << "Float32 array set";
            }
            break;
        }
        case Float64:
        {
            qDebug() << "format = Float64";
            if(reg->isDoubleArray())
            {
                double vector[reg->getDimension1()][reg->getDimension2()];
                double val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<double>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Float64");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValFloat64Array2D((const double*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "Float64 2D array set";
            }
            else
            {
                double vector[reg->getDimension1()];
                double val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<double>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Float64");
                    else
                        vector[i] = val;
                }
                reg->setValFloat64Array(vector,reg->getDimension1());
                qDebug() << "Float64 array set";
            }
            break;
        }
        case String:
        {
            qDebug() << "format = String";
            if(reg->isDoubleArray())
            {
                void *vector = calloc(reg->getDimension1()*reg->getDimension2(), sizeof(void*));
                std::string** stringVector = static_cast<std::string**>(vector);
                for(unsigned long i=0; i<(reg->getDimension1()); i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        stringVector[(i*reg->getDimension2())+j] = new std::string(((*dataVector)[(i*reg->getDimension2())+j]).toStdString());
                    }
                }
                reg->setValStringArray2D((const std::string*)stringVector, reg->getDimension1(), reg->getDimension2());
                qDebug() << "String 2D array set";
                // cleanup
                for (unsigned int i=0; i<(reg->getDimension1()*reg->getDimension2()); i++)
                {
                    delete stringVector[i];
                }
                free(vector);
            }
            else
            {
                std::string vector[reg->getDimension1()];
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    vector[i] = ((*dataVector)[i]).toStdString();
                }
                reg->setValStringArray(vector, reg->getDimension1());
                qDebug() << "String array set";
            }
            break;
        }
        case Date:
        {
            qDebug() << "format = Date";
            if(reg->isDoubleArray())
            {
                double vector[reg->getDimension1()][reg->getDimension2()];
                double val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    for(unsigned long j=0; j<reg->getDimension2(); j++)
                    {
                        qDebug() << QString((*dataVector)[i*reg->getDimension2()+j]);
                        if(!StringUtilities::from_string<double>(val, QString((*dataVector)[i*reg->getDimension2()+j]).toStdString(), std::dec))
                            throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Date (Epoch value)");
                        else
                            vector[i][j] = val;
                    }
                }
                reg->setValDateArray2D((const double*)vector,reg->getDimension1(),reg->getDimension2());
                qDebug() << "Date 2D array set";
            }
            else
            {
                double vector[reg->getDimension1()];
                double val;
                for(unsigned long i=0;i<reg->getDimension1();i++)
                {
                    qDebug() << QString((*dataVector)[i]);
                    if(!StringUtilities::from_string<double>(val, QString((*dataVector)[i]).toStdString(), std::dec))
                        throw new std::string("Output buffer value of "+plcName+" / "+deviceName+" / "+registerName+ " must be a Date (Epoch value)");
                    else
                        vector[i] = val;
                }
                reg->setValDateArray(vector,reg->getDimension1());
                qDebug() << "Date array set";
            }
            break;
        }
        default:
        	Utils::logError(messageConsole_,"Error: no converter available for that format");
    }
}

void silecsModule::updateClusterItem(Item *Cluster,bool updateInputBufferOnly)
{
    int numberOfPLC = Cluster->childCount();
    for(int PLCIndex=0; PLCIndex < numberOfPLC;PLCIndex++)
    {
        this->updatePLCItem(dynamic_cast<Item*>(Cluster->child(PLCIndex)),updateInputBufferOnly);
    }
}


void silecsModule::updatePLCItem(Item *PLCItem,bool updateInputBufferOnly)
{
	updatePLCRunState(PLCItem);
	Silecs::PLC* plc = (Silecs::PLC*)(PLCItem->getLinkedObject());
	Utils::logInfo(messageConsole_,std::string("updating controller: '") + plc->getName() + "'");
    int numberOfdevice = PLCItem->childCount();
    for(int deviceIndex=0; deviceIndex<numberOfdevice;deviceIndex++)
    {
        this->updateDeviceItem(dynamic_cast<Item*>(PLCItem->child(deviceIndex)),updateInputBufferOnly);
    }
}


void silecsModule::updateDeviceItem(Item *deviceItem,bool updateInputBufferOnly)
{
    Silecs::Device *device = (Silecs::Device*)deviceItem->getLinkedObject();
    Utils::logInfo(messageConsole_,std::string("updating device: '") + device-> getLabel() + "'");

    int numberOfRegisters = deviceItem->childCount();
    for(int regIndex=0;regIndex<numberOfRegisters;regIndex++)
    {
        // Get the register Item in the tree view
        Item *regItem = dynamic_cast<Item*>(deviceItem->child(regIndex));
        Silecs::Register *reg = device->getRegister(regItem->text(0).toStdString());
        Utils::logInfo(messageConsole_,"updating register: '" + reg->getName() + "'of device: '" + device-> getLabel() + "'" );

        // PLC values
        try
        {
            if(reg->isReadable())
            {
                if(reg->isScalar())
                {
                    // Display the value for scalar
                    regItem->setText(2 , QString::fromStdString(reg->getInputValAsString()));
                }
                else
                {
                    // Display [...] for arrays
                    regItem->setText(2 , "[...]");
                }
            }
            else // is WRITE only for silecs
            {
                regItem->setText(2 , "WRITE-ONLY block");
            }
        }
        catch (std::exception& ex)
        {
            regItem->setText(2 , "?");
            std::ostringstream message;
            message << "Impossible to read the input buffer value of - '" << regItem->text(0).toStdString() << "'";
            Utils::logError(messageConsole_,message.str());
        }

        if(!updateInputBufferOnly)
        {
            // local values
            try{
                if(reg->isWritable()) // = is WRITE or READ+WRITE for silecs
                {
                    if(reg->isScalar())
                    {
                        // Display the value for scalar
                        regItem->setText(3 , QString::fromStdString(reg->getOutputValAsString()));
                    }
                    else
                    {
                        // Display [...] for arrays
                        regItem->setText(3 , "[...]");
                    }
                    // Output for type date not supported
                    if(reg->getFormat()==Date)
                    {
                        regItem->setText(3 , "Date format not supported.");
                    }
                }
                else // = is READ only for silecs
                {
                    regItem->setText(3 , "READ-ONLY block");
                }

            }
            catch (std::exception& ex)
            {
                regItem->setText(3 , "?");
                std::ostringstream message;
                message << "Impossible to read the output buffer value of - '" << regItem->text(0).toStdString() << "'";
                Utils::logError(messageConsole_, message.str());
            }
        }
    }
}

