/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-diagnostic/stderrredirect.h>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QString>
#include <QtCore/QTextStream>
#include <QtCore/QDebug>


StdErrRedirect::StdErrRedirect(QTextEdit *errorLog,QObject *parent): QObject(parent)
{
    // Store the pointer to the error log window
    m_errorLog = errorLog;

    // Create a temporary filename: first find the path:
    tmpFileNameQtFormat = QDir::tempPath();

    // Make sure the closing slash is present:
    if (!tmpFileNameQtFormat.endsWith(QChar('/')))
        tmpFileNameQtFormat.append(QChar('/'));

    // Add the file name itself:
    tmpFileNameQtFormat.append("QtDiagDump_"+QString::number(QCoreApplication::applicationPid()));

    qDebug()<< "file ->" << tmpFileNameQtFormat;

    // Obtain a version of the filename in the operating system's native format:
    tmpFileNameNativeFormat = QDir::toNativeSeparators(tmpFileNameQtFormat);

    // if file exists because previous section crashed i rename the file ***.old
    QFile fileToRename(tmpFileNameQtFormat);
    if(fileToRename.exists())
        fileToRename.rename(tmpFileNameQtFormat+".old");

    // Set up redirection to this file:
    freopen(tmpFileNameNativeFormat.toLatin1().constData(), "a+", stderr);

    // Initialise the QFileSystemWatcher:
    connect(&watcher, SIGNAL(fileChanged(const QString &)),
            this,     SLOT(fileChanged(const QString &)));
    watcher.addPath(tmpFileNameQtFormat);

    tmp.setFileName(tmpFileNameQtFormat);
}

StdErrRedirect::~StdErrRedirect()
{
    // Ensure the temporary file is properly deleted:
    fclose(stderr);
    tmp.close();
    tmp.open(QIODevice::ReadWrite);
    tmp.remove();
}

void StdErrRedirect::fileChanged(const QString &filename)
{
    (void)filename;
    tmp.open(QIODevice::ReadOnly);
    QTextStream stream(&tmp);
    QString content = stream.readAll();
    tmp.close();

    // Identify what's new, and just send this to the window:
    int newchars = content.size() - oldContent.size();
    if (newchars)
    {
        m_errorLog -> append(content.right(newchars));
        oldContent = content;
    }
}
