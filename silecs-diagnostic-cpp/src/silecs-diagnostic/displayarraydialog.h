/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_DISPLAYARRAYDIALOG_H
#define SILECS_DIAG_DISPLAYARRAYDIALOG_H

#include <QtWidgets/QDialog>

namespace Ui {
    class DisplayArrayDialog;
}

class DisplayArrayDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DisplayArrayDialog(QWidget *parent = 0);
    ~DisplayArrayDialog();

    void setDataVector(std::vector<QString> dataVector, bool editable, unsigned long dim2);

    std::vector<QString> getDataVector();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::DisplayArrayDialog *ui;

    std::vector<QString> dataVector;

};

#endif // SILECS_DIAG_DISPLAYARRAYDIALOG_H
