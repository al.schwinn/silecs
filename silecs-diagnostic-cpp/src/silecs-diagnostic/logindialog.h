/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_LOGINDIALOG_H
#define SILECS_DIAG_LOGINDIALOG_H

#include <QtWidgets/QDialog>

#include <silecs-diagnostic/constants.h>

namespace Ui {
    class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();

    //set and get username
//    void setUsername(std::string username);
    std::string getUsername();

    //set and get password
//    void setPassword(std::string password);
    std::string getPassword();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::LoginDialog *ui;

    // login and password
    std::string username;
    std::string password;
};

#endif // SILECS_DIAG_LOGINDIALOG_H
