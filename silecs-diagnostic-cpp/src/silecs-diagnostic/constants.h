/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_CONSTANTS_H
#define SILECS_DIAG_CONSTANTS_H
#include <string>

/*
 * Different string type
 */
const std::string CLUSTER_TYPE = "CLUSTER";
const std::string PLC_TYPE = "PLC";
const std::string DEVICE_TYPE = "DEVICE";
const std::string REGISTER_TYPE = "REGISTER";

// SILECS HEADER
const std::string HEADER_NAME = "SilecsHeader";
const std::string HEADER_VERSION = "1.0.0";
const std::string HEADER_DEVICE_NAME = "SilecsHeader";
const std::string HEADER_BLOCK = "hdrBlk";
const std::string HEADER_RELEASE_REG = "_version";
const std::string HEADER_OWNER_REG = "_user";
const std::string HEADER_DATE_REG = "_date";
const std::string HEADER_CHECKSUM_REG = "_checksum";

//splash screen time (in seconds)
const unsigned long SPLASH_SCREEN_TIME = 1;
const int NUMBER_PASSWORD_ATTEMPTS = 3;

// max buffer lenght for PLC name
const int MAX_BUFFER = 1024;

#endif // SILECS_DIAG_CONSTANTS_H
