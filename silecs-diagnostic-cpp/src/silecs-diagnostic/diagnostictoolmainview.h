/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_DIAGNOSTICTOOLMAINVIEW_H
#define SILECS_DIAG_DIAGNOSTICTOOLMAINVIEW_H

#include <QtWidgets/QMainWindow>

#include <silecs-diagnostic/displayarraydialog.h>
#include <silecs-diagnostic/item.h>
#include <silecs-diagnostic/silecsmodule.h>
#include <silecs-diagnostic/stderrredirect.h>

#include <iostream>
#include <typeinfo>
#include <map>


namespace Ui {
    class diagnosticToolMainView;
}

using namespace Silecs;


class diagnosticToolMainView : public QMainWindow
{
    Q_OBJECT

public:
    explicit diagnosticToolMainView(QWidget *parent = 0);
    ~diagnosticToolMainView();

    void setDeployFile(std::string file);

private slots:

    // Tree view
    void on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);
    void on_treeWidget_doubleClicked(const QModelIndex &index);
    void on_treeWidget_itemExpanded(QTreeWidgetItem *item);

    // Class name combo
    void on_classNameComboBox_currentIndexChanged(const QString &arg1);

    // Buttons
    void on_sendButton_clicked();
    void on_receiveButton_clicked();
    void on_connectButton_clicked();
    void on_loadDeployButton_clicked();
    void on_disconnectButton_clicked();
    void on_copyButton_clicked();

    // Context menu for the console
    void ctxMenu(const QPoint &pos);
    void clearConsole_slot();

    // different way of receiving
    void receiveDevice(std::string blockName,Item* currentItem);
    void periodicReceiveDevice();

    // monitor
    void on_periodicReceiveButton_toggled(bool checked);

    // Log Topics change
    void on_checkERROR_stateChanged(int arg1);
    void on_checkDEBUG_stateChanged(int arg1);
    void on_checkSETUP_stateChanged(int arg1);
    void on_checkALLOC_stateChanged(int arg1);
    void on_checkLOCK_stateChanged(int arg1);
    void on_checkCOMM_stateChanged(int arg1);
    void on_checkSEND_stateChanged(int arg1);
    void on_checkRECV_stateChanged(int arg1);
    void on_checkDATA_stateChanged(int arg1);
    void on_checkDIAG_stateChanged(int arg1);

    void on_treeWidget_clicked();

private:

    // GSI-Hack - load directly from file
    std::string designName;
    std::string deployFile;
    void resetGUI();
    void loadFiles();

    void fillClassNameComboBox();

    void logBlockReceived(std::string blockName, int elapsedTime);
    void logCommunicationSuccess(int elapsedTime);

    std::vector<QString> openArrayDialogBase(Silecs::Register* reg, std::vector<QString> dataVector, bool localData);
    std::vector<QString> open1DArrayDialog(Silecs::Register* reg, bool localData);
    std::vector<QString> open2DArrayDialog(Silecs::Register* reg, bool localData);
    void markItemNotEdiable(QTreeWidgetItem *item);

    Ui::diagnosticToolMainView *ui;

    //BlockName and current item for periodic receive
    std::string blockNameForPeriodicReceive;
    Item* currentItemForPeriodicReceive;
    QTimer *periodicReceiveTimer;

    StdErrRedirect *redirector;
    std::string generateLogTopics();
};

#endif // SILECS_DIAG_DIAGNOSTICTOOLMAINVIEW_H
