/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_ITEM_H
#define SILECS_DIAG_ITEM_H

#include <QtWidgets/QTreeWidgetItem>

class QDomDocument;
class Item : public QTreeWidgetItem {
//  Q_PROPERTY( QString Name READ name WRITE setName );
 private:
        void *linkedObject;
 public:
        Item(void* linkedObject);
        Item();

        // set the pointer to the related object
        void setLinkedObject(void* linkedObject);

        // return the pointer to the related object
        void* getLinkedObject();
};

#endif // SILECS_DIAG_ITEM_H
