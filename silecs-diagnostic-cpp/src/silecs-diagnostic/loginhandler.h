/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECS_DIAG_LOGINHANDLER_H
#define SILECS_DIAG_LOGINHANDLER_H

#include <silecs-diagnostic/logindialog.h>
#include <silecs-diagnostic/constants.h>

#include <iostream>

#ifdef WITH_RBAC
    #include <cmw-rbac/LoginContext.h>
    #include <cmw-rbac/AuthenticationFailure.h>
#endif

using namespace std;

#ifdef WITH_RBAC
    extern std::string USER_NAME;

    class loginHandler
    {
    public:
        loginHandler();
        bool requestLogin();
    };
#endif


#endif // SILECS_DIAG_LOGINHANDLER_H
