/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QScrollBar>

#include <silecs-diagnostic/utils.h>
#include <silecs-diagnostic/silecsmodule.h>
#include <iomanip>


extern silecsModule *mysilecs;

void Utils::logError(QTextBrowser* errorConsole, std::string message)
{
    errorConsole->insertHtml(QString::fromStdString("<font color='red'>DIAG-TOOL [ERROR] " + message + "</font><br>"));
    errorConsole->verticalScrollBar()->setValue(errorConsole->verticalScrollBar()->maximum());
}

void Utils::logInfo(QTextBrowser* errorConsole, std::string message)
{
    errorConsole->insertHtml(QString::fromStdString("<font color='black'>DIAG-TOOL [INFO] " + message + "</font><br>"));
    errorConsole->verticalScrollBar()->setValue(errorConsole->verticalScrollBar()->maximum());
}

void Utils::logDebugIf(QTextBrowser* errorConsole, std::string message)
{
    if (mysilecs->isDebugLogEnabled())
    {
        errorConsole->insertHtml(QString::fromStdString("<font color='black'>DIAG-TOOL [DEBUG] " + message + "</font><br>"));
        errorConsole->verticalScrollBar()->setValue(errorConsole->verticalScrollBar()->maximum());
    }
}

//convert int to string
std::string Utils::toString(int number)
{
    char tmp[32];
    sprintf(tmp, "%d", number);
    return tmp;
}

//convert unsigned int to string
std::string Utils::toString(unsigned int number)
{
    char tmp[32];
    sprintf(tmp, "%ud", number);
    return tmp;
}

//convert long to string
std::string Utils::toString(long number)
{
    char tmp[32];
    sprintf(tmp, "%ld", number);
    return tmp;
}

//convert unsigned long to string
std::string Utils::toString(unsigned long number)
{
    char tmp[32];
    sprintf(tmp, "%lud", number);
    return tmp;
}

//convert long to a given base
std::string Utils::toString(long number, int base)
{
    std::stringstream stream;
    stream << std::setbase(base) << number;
    return stream.str();
}

Item* Utils::addTreeItem(Item *parent, QString name, QString description, QString type, void* linkedObject, QString icon)
{
    Item *itm = new Item(linkedObject);
    itm->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
    itm->setText(0, name);
    itm->setText(2, description);
    itm->setWhatsThis(0, type);

    if (icon.compare("") != 0)
        itm->setIcon(0, QIcon(QPixmap(icon)));

    parent->addChild(itm);
    return itm;
}

char* Utils::chop(char *string)
{
    size_t i, len;
    len = strlen(string);
    char *newstring;
    newstring = (char *)malloc(len);
    for (i = 0; i < strlen(string) - 1; i++)
    {
        newstring[i] = string[i];
    }
    newstring[len] = '\0';

    return newstring;
}

void Utils::displayClusterInformation(Silecs::Cluster *cluster, QTextEdit *console)
{
    std::string text = "";

    text.append("<h3>Cluster general information</h3>");
    text.append("<ul>");
    text.append("<li>Class name:    " + cluster->getClassName() + "</li>");
    text.append("<li>Class version: " + cluster->getClassVersion() + "</li>");
    text.append("<li>Host name:     " + cluster->getHostName() + "</li>");
    text.append("</ul>");

    console->setText(QString::fromStdString(text));

}

void Utils::displayPLCInformation(Silecs::PLC *plc, QTextEdit *console)
{
    std::string text = "";

    if (plc->isConnected(false))
    {
        text.append("<h3><font color ='green'>This PLC is currently connected</font></h3><hr/>");

        if (plc->getPLCType() == BusCoupler)
        {
            text.append("<h3><font color ='green'>PLC Runtime information</font></h3>");
            text.append("<ul>");
            text.append("<li>No runtime information available: Bus controller (no memory)</li>");
            text.append("</ul><hr/>");

        }
        else
        {
            Silecs::Device *device   = plc->getDevice(HEADER_DEVICE_NAME);
            text.append("<h3><font color ='green'>PLC Runtime information</font></h3>");
            text.append("<ul>");
            text.append("<li>PLC HEADER DATA uploaded from: " + plc->getName() + "</li>");
            text.append("<li>Software Release: " + device->getRegister(HEADER_RELEASE_REG)->getValString() + "</li>");
            text.append("<li>Mapping Owner:    " + device->getRegister(HEADER_OWNER_REG)->getValString() + "</li>");
            text.append("<li>Mapping Checksum: " + device->getRegister(HEADER_CHECKSUM_REG)->getInputValAsString() + "</li>");
            text.append("<li>Generation Date:  " + device->getRegister(HEADER_DATE_REG)->getInputValAsString() + "</li>");
            text.append("</ul><hr/>");
        }
    }
    else
        text.append("<h3>This PLC is currently disconnected</h3><hr/>");

    text.append("<h3>PLC general information</h3>");
    text.append("<ul>");
    text.append("<li>Name:    " + plc->getName() + "</li>");
    text.append("<li>Address: " + plc->getIPAddress() + "</li>");
    text.append("<li>Brand:   " + plc->getBrand() + "</li>");
    text.append("<li>Model:   " + plc->getModel() + "</li>");
    text.append("<li>Owner:   " + plc->getLocalOwner() + "</li>");
    text.append("</ul><hr/>");

    text.append("<h3>PLC configuration information</h3>");
    text.append("<ul>");
    text.append("<li>Protocol type: " + plc->getProtocolType() + "</li>");
    text.append("<li>Protocol mode: " + plc->getProtocolMode() + "</li>");

    if (plc->getMemBaseAddress() > -1)
        text.append("<li>Memory Base address:  " + toString(plc->getMemBaseAddress()) + " (0x" + toString(plc->getMemBaseAddress(), 16) + ")" + "</li>");
    else
        text.append("<li>Memory Base address:  Not applicable</li>");

    if (plc->getDIBaseAddress() > -1)
        text.append("<li>DI Base address:  " + toString(plc->getDIBaseAddress()) + " (0x" + toString(plc->getDIBaseAddress(), 16) + ")" + "</li>");
    else
        text.append("<li>DI Base address:  Not applicable</li>");

    if (plc->getDOBaseAddress() > -1)
        text.append("<li>DO Base address:  " + toString(plc->getDOBaseAddress()) + " (0x" + toString(plc->getDOBaseAddress(), 16) + ")" + "</li>");
    else
        text.append("<li>DO Base address:  Not applicable</li>");

    if (plc->getAIBaseAddress() > -1)
        text.append("<li>AI Base address:  " + toString(plc->getAIBaseAddress()) + " (0x" + toString(plc->getAIBaseAddress(), 16) + ")" + "</li>");
    else
        text.append("<li>AI Base address:  Not applicable</li>");

    if (plc->getAOBaseAddress() > -1)
        text.append("<li>AO Base address:  " + toString(plc->getAOBaseAddress()) + " (0x" + toString(plc->getAOBaseAddress(), 16) + ")" + "</li>");
    else
        text.append("<li>AO Base address:  Not applicable</li>");

    text.append("</ul><hr/>");

    text.append("<h3>PLC generation information</h3>");
    text.append("<ul>");
    text.append("<li>Release:  " + plc->getLocalRelease() + "</li>");
    text.append("<li>Date:     " + plc->getLocalDate() + "</li>");
    text.append("<li>Checksum: " + toString(plc->getLocalChecksum()) + "</li>");
    text.append("</ul><hr/>");

    console->setText(QString::fromStdString(text));
}

void Utils::displayDeviceInformation(Silecs::Device *device, QTextEdit *console)
{
    std::string text = "";

    text.append("<h3>Device general information</h3>");
    text.append("<ul>");
    text.append("<li>Name:    " + device->getLabel() + "</li>");
    text.append("</ul>");

    console->setText(QString::fromStdString(text));
}

void Utils::displayRegisterInformation(Silecs::Register *reg, QTextEdit *console)
{
    std::string text = "";

    text.append("<h3>Register general information</h3>");
    text.append("<ul>");
    text.append("<li>Name:        " + reg->getName() + "</li>");
    text.append("<li>Format:      " + reg->getFormatAsString() + "</li>");
    if (reg->getFormat() == String)
        text.append("<li>String length: " + toString(reg->getLength()) + "</li>");
    text.append("<li>Dimension1:   " + toString(reg->getDimension1()) + "</li>");
    text.append("<li>Dimension2:  " + toString(reg->getDimension2()) + "</li>");
    text.append("<li>Block name:  " + reg->getBlockName() + "</li>");
    text.append("<li>Last update: " + reg->getTimeStampAsString() + "</li>");

    // Access method
    if (reg->hasInputAccess() && reg->hasOutputAccess())
        text.append("<li>Access method: Read / Write </li>");
    else
    {
        if (reg->hasInputAccess())
            text.append("<li>Access method: Read only </li>");
        else
            text.append("<li>Access method: Write only </li>");
    }

    text.append("<li>Access area: " + reg->whichAccessArea(reg->getAccessArea()) + "</li>");
    text.append("</ul>");

    console->setText(QString::fromStdString(text));
}

void Utils::displayRegisterValue(Silecs::Register *reg, QLabel *binValueLabel, QLabel *hexValueLabel, QLabel *decValueLabel, QLabel *asciiValueLabel)
{

    if (!reg->isReadable())
    {
        binValueLabel->setText("--Write only register--");
        hexValueLabel->setText("--Write only register--");
        decValueLabel->setText("--Write only register--");
        asciiValueLabel->setText("--Write only register--");
    }
    else
    {
        // The register has input access
        if (reg->getDimension1() > 1 || reg->getDimension2() > 1)
        {
            binValueLabel->setText("--Vector--");
            hexValueLabel->setText("--Vector--");
            decValueLabel->setText("--Vector--");
            asciiValueLabel->setText("--Vector--");
        }
        else
        {
            //Scalar with input access
            switch (reg->getFormat())
            {
                case uInt8:
                {

                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt8(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt8(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt8(), 0, 10));
                    //QString temp;
                    //asciiValueLabel->setText(temp.sprintf("%c",reg->getValUInt8()));
                    QChar c = reg->getValUInt8();
                    //if(!c.isPrint())
                    //    c='?';
                    //asciiValueLabel->setText(QObject::tr("%1").arg(c));
                    asciiValueLabel->setText(c.isPrint() ? QObject::tr("%1").arg(c) : "--Not printable--");

                    break;
                }
                case Int8:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValInt8(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValInt8(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValInt8(), 0, 10));
                    QChar c = reg->getValInt8();
                    asciiValueLabel->setText(c.isPrint() ? QObject::tr("%1").arg(c) : "--Not printable--");
                    break;
                }
                case uInt16:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt16(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt16(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt16(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case Int16:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValInt16(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValInt16(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValInt16(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case uInt32:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt32(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt32(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt32(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case Int32:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValInt32(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValInt32(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValInt32(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case uInt64:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt64(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt64(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValUInt64(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case Int64:
                {
                    binValueLabel->setText(QObject::tr("%1").arg(reg->getValInt64(), 0, 2));
                    hexValueLabel->setText(QObject::tr("%1").arg(reg->getValInt64(), 0, 16));
                    decValueLabel->setText(QObject::tr("%1").arg(reg->getValInt64(), 0, 10));
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                case String:
                {
                    binValueLabel->setText("--Not relevant--");
                    hexValueLabel->setText("--Not relevant--");
                    decValueLabel->setText("--Not relevant--");
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }

                default: // Float32,Float64,Date
                {
                    binValueLabel->setText("--Not relevant--");
                    hexValueLabel->setText("--Not relevant--");
                    decValueLabel->setText("--Not relevant--");
                    asciiValueLabel->setText("--Not relevant--");
                    break;
                }
            }
        }
    }
}
