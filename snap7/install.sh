#!/bin/sh
set -e

RELEASE_DIR_BASE=$1
SILECS_VERSION=$2

SNAP7_VERSION=1.4.2

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

RELEASE_DIR=${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION}
if [ -d ${RELEASE_DIR} ]; then 
    echo "Error: ${RELEASE_DIR} already exists ...skipping"
    exit 1
fi

mkdir -p ${RELEASE_DIR}
cp -r ${SCRIPTPATH}/snap7-full/build/bin/x86_64-linux ${RELEASE_DIR}

# Make sure everybody can read all installed files
chmod a+r -R ${RELEASE_DIR}
chmod a+x -R ${RELEASE_DIR}

# Make all files write-protected to prevent overwriting an old version by accident
chmod a-w -R ${RELEASE_DIR}
    
# Update link to latest version
if [ -L ${RELEASE_DIR_BASE}/snap7/latest ]; then
    rm ${RELEASE_DIR_BASE}/snap7/latest
fi
ln -fvs ${RELEASE_DIR_BASE}/snap7/${SNAP7_VERSION} ${RELEASE_DIR_BASE}/snap7/latest
