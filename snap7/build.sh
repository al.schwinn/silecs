#!/bin/sh
set -e

SNAP7_VERSION=1.4.0
SNAP7_FOLDER=snap7-full-${SNAP7_VERSION}

archiveFile=${SNAP7_FOLDER}.tar.gz
if [ ! -f "$archiveFile" ]
then
   rm -rf snap7-full*
   wget http://downloads.sourceforge.net/project/snap7/${SNAP7_VERSION}/${archiveFile}
   tar -vxzf ${archiveFile}
   mv ${SNAP7_FOLDER} snap7-full
fi
cd snap7-full/build/unix
make -f x86_64_linux.mk
