#!/bin/bash

# release locations
GLOBAL_RELEASE_DIR=/common/usr/cscofe/silecs

function help {
    echo -e ""
    echo -e "Script in order to build and install the complete silecs framework"
    echo -e ""
    echo -e "Usage:"
    echo -e ""
    echo -e "\t Install version 1.2.3 to default directory ($GLOBAL_RELEASE_DIR):"
    echo -e "\t\t ./build_and_install.sh 1.2.3"
    echo -e ""
    echo -e "\t Install version 1.2.3 to custom directory:"
    echo -e "\t\t ./build_and_install.sh 1.2.3 /home/user/MyCustomDir"
    echo -e ""
    exit 1;
}

if [ $# -lt 1 ]
  then
    echo "Error: Wrong number of arguments supplied."
    help
fi

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

# Build and install instructions for modbus library:
# ./configure --prefix=/common/usr/cscofe/silecs/modbus/3.1.7 --enable-static
# make -j4
# make install

# List of silecs packages which can be released
PACKAGES="silecs-codegen silecs-model snap7 silecs-communication-cpp silecs-diagnostic-cpp silecs-cli silecs-cli-client"

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

SILECS_VERSION=$1

if [ -z "$2" ]
  then
    RELEASE_DIR_BASE=$GLOBAL_RELEASE_DIR
  else
    RELEASE_DIR_BASE=$2
fi

for PACKAGE in $PACKAGES; do
    echo "installing package ${PACKAGE} into ${RELEASE_DIR_BASE}"
    mkdir -p ${RELEASE_DIR_BASE}
    ${PACKAGE}/install.sh ${RELEASE_DIR_BASE} ${SILECS_VERSION}
    if [ "$?" -eq "1" ]; then
        echo "Error: Package ${PACKAGE} skipped"
    fi
done

exit 0
