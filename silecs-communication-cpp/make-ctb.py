#!/usr/bin/python

import os
import sys

depVersion = 'SILECS_MAKEFILE_VERSION=DEV RDA_VERSION=DEV'
ctbPaths = 'RELEASE_LOCATION=/local/RELEASE_LOCATION'
makeFlags = ' RELEASE_GROUP=si ' + depVersion + ' ' + ctbPaths

# compile first as we can't use -j in local-devrelease
cmd = 'make -j4' + makeFlags
rc = os.system(cmd + ' CPU=L865')
if(rc != 0):
  sys.exit(-1)
  
rc = os.system(cmd + ' CPU=L866')
if(rc != 0):
  sys.exit(-1)
  
# now release ==========================================
cmd = 'make local-devrelease' + makeFlags + ' TMP_DIR=.'
rc = os.system(cmd + ' CPU=L865')
if(rc != 0):
  sys.exit(-1)
  
rc = os.system(cmd + ' CPU=L866')
if(rc != 0):
  sys.exit(-1)
 