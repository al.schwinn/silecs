/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/wrapper/Controller.h>
#include <silecs-communication/wrapper/Design.h>

namespace SilecsWrapper
{

Controller::Controller(const std::string& name, const std::string& domain, Design *design, const std::string parameterFile) :
                _name(name),
                _domain(domain),
                _design(design),
                _silecsPLC(_design->getSilecsCluster()->getPLC(_name))
{
    if (_design->getDeployUnit()->getGlobalConfig().getAutomaticConnect())
    {
        connect(); //Using default parameters: MASTER registers download, connect now
    }
}

Controller::~Controller()
{

}

void Controller::connect()
{
    //By default: Controller connected right-away and Master registers download.
    _silecsPLC->connect(Silecs::MASTER_SYNCHRO, true);
}

void Controller::connect(Silecs::SynchroMode synchroMode, bool connectNow)
{
    //Connect parameters can be set in manual connection mode (Automatic connect==false)
    _silecsPLC->connect(synchroMode, connectNow);
}

void Controller::disconnect()
{
    _silecsPLC->disconnect();
}

} //namespace SilecsWrapper
