/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECSWRAPER_DEVICE_H_
#define SILECSWRAPER_DEVICE_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/wrapper/Controller.h>

#include <string>

namespace SilecsWrapper
{

class Device
{
public:
    Device(const std::string& label, Controller *controller) :
                    _label(label),
                    _controller(controller)
    {
        _silecsDevice = _controller->getSilecsPLC()->getDevice(label);
    }

    const std::string& getLabel() const
    {
        return _label;
    }

    Silecs::Device* getSilecsDevice()
    {
        return _silecsDevice;
    }

    virtual ~Device()
    {
    }

protected:
    std::string _label;
    Controller* _controller;
    Silecs::Device* _silecsDevice;

private:
    // Non copyable
    Device(const Device& other);
    Device& operator=(const Device&);
};

} //namespace SilecsWrapper

#endif
