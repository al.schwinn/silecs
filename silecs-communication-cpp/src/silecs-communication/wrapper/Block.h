/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECSWRAPER_BLOCK_H_
#define SILECSWRAPER_BLOCK_H_

#include <string>

namespace SilecsWrapper
{
class Block
{
public:
    Block(const std::string& name) :
                    _name(name)
    {
    }

    virtual ~Block()
    {
    }

    const std::string& getName() const
    {
        return _name;
    }

protected:
    const std::string _name;
};

} //namespace SilecsWrapper

#endif
