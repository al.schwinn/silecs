/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef SILECSWRAPER_CONTROLLER_H_
#define SILECSWRAPER_CONTROLLER_H_

#include <silecs-communication/interface/equipment/SilecsPLC.h>

#include <string>

namespace SilecsWrapper
{

class Design;

class Controller
{
public:
    Controller(const std::string& name, const std::string& domain, Design *design, const std::string parameterFile = "");

    const std::string& getName() const
    {
        return _name;
    }

    Silecs::PLC* getSilecsPLC()
    {
        return _silecsPLC;
    }

    void connect();
    void connect(Silecs::SynchroMode synchroMode, bool connectNow);

    void disconnect();

    virtual ~Controller();

protected:
    const std::string _name;
    const std::string _domain;
    Design* _design;
    Silecs::PLC* _silecsPLC;

private:
    // Non copyable
    Controller(const Controller& other);
    Controller& operator=(const Controller&);
};

} //namespace SilecsWrapper

#endif
