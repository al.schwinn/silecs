/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>

namespace SilecsWrapper
{

Design::Design(const std::string& name, const std::string& version, DeployUnit *deployUnit) :
                _name(name),
                _version(version),
                _deployUnit(deployUnit)
{
    _silecsCluster = _deployUnit->getService()->getCluster(_name, _version);
}

Design::~Design()
{

}

const std::string& Design::getName() const
{
    return _name;
}

const std::string& Design::getVersion() const
{
    return _version;
}

Silecs::Cluster* Design::getSilecsCluster() const
{
    return _silecsCluster;
}

DeployUnit* Design::getDeployUnit() const
{
    return _deployUnit;
}

void Design::setConfiguration(const DesignConfig& configuration)
{
    Silecs::plcMapType plcMap = _silecsCluster->getPLCMap();
    Silecs::plcMapType::iterator plcMapIter;
    for (plcMapIter = plcMap.begin(); plcMapIter != plcMap.end(); ++plcMapIter)
    {
        //if (plcMapIter->second->isConnected({*connectNow = *}false))
        if (plcMapIter->second->isEnabled())
        {
            //at least one PLC is already enabled (PLC object created). Does not make sense to set configuration after.
            throw Silecs::SilecsException(__FILE__, __LINE__, Silecs::COMM_ALREADY_ENABLED, std::string("Design configuration must be set before instantiating any controller! Disable the 'Automatic connect' flag of the deploy configuration if needed."));
        }
    }
    _silecsCluster->configuration = configuration;
}

const DesignConfig& Design::getConfiguration()
{
    return _silecsCluster->configuration;
}

} //namespace SilecsWrapper
