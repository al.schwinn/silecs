/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _WRAPPER_ACTION_H_
#define _WRAPPER_ACTION_H_

#include <silecs-communication/interface/core/SilecsAction.h>

namespace Silecs
{
class Context;

/*!
 * \class WrapperAction
 * \brief
 */
class WrapperAction
{

public:
    WrapperAction()
    {
    }
    ;
    WrapperAction(Action* plcAction) :
                    pAction_(plcAction)
    {
    }
    ;
    virtual ~WrapperAction()
    {
    }
    ;

    /*!
     * \fn function
     * It contains the code to be executed for this action.
     * Has to be overriden for concrete PLC action.
     */
    static int function(WrapperAction plcWrapperAction, Context* pContext)
    {
        return plcWrapperAction.pAction_->execute(pContext);
    }

private:
    Action *pAction_;

};

} // namespace

#endif // _WRAPPER_ACTION_H_

