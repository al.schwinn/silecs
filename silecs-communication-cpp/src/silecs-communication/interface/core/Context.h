/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _CONTEXT_H_
#define _CONTEXT_H_

namespace Silecs
{
class Device;

/*!
 * \class Context
 * \brief This class defines the context of the Silecs Transaction.
 * A transaction can be done from Cluster or PLC levels for all-devices
 * and from Device level for a particular device.
 * A transaction can be executed by the client process (normal transaction) or
 * can be executed at the Cluster set-up for retentive registers synchronization.
 */
class Context
{
public:
    Context(Device* pDev, bool isForSynchronization);
    virtual ~Context();

    inline bool isForAllDevices()
    {
        return pDevice_ == NULL;
    }
    inline bool isForOneDevice()
    {
        return pDevice_ != NULL;
    }
    inline Device* getDevice()
    {
        return pDevice_;
    }

    inline bool isForSynchronization()
    {
        return isForSynchro_;
    }

private:
    /// Device reference used for Device context
    Device* pDevice_;

    /// true for synchronization transaction.
    /// In this case only retentive registers will be updated (Master & Slave).
    bool isForSynchro_;
};

} // namespace

#endif // _USER_CONTEXT_H_
