/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <unistd.h>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

/* ----------------------------------------------------------------------
 * RELEASE
 * 3.1.0: initial
 * 3.2.0: Code refactor to easily integrate new hardware equipment;
 *        NI via shared library supported (32 bit only);
 *        64bits CPU support (for SLC6 migration);
 * 3.2.1: Missing and safe free (Mutex, Thread)
 *        Safe free in libS7
 *        Fix in libS7:
 *        Do not recv/send empty packet in case of data-size multiple frame size.
 * 3.3.0: Library Conditional compilation WITH_CHN={YES/NO}
 *        Silecsop is editor of each design and deploy
 * 		  SVN Support (can be enable from ieglobal.py)
 * 		  Automated compilation procedure
 * 		  CNV receive via buffer subscriber
 * 3.3.1: Fix memory leak using correct disposal of CNV data
 * 3.4.0: Support Rabbit Microcontroller via MB connection.
 *        MB read baseAddress instead of address 000 at connection time.
 * 3.4.1: FIX: consider uint8 as numeric and int8 as ASCII value while setting register value
 * 3.5.0: FIX: add missing method to allow dynamic resizing of the send/recv data-block.
 *        Look at: PLC::[set/reset][Read/Write]BlockAttributes() methods and related components (SilecsPLC.h)
 * 3.5.1: FIX: Dynamic resizing didn't work properly with SCHNEIDER using BLOKC-MODE.
 *        Full custom adressing is required to fix the problem.
 * 3.6.0: FEATURE: Input arguments setting for FESA3 development.                        
 * 3.7.0: Support Beckhoff PLC via MB connection.
 *        FIX: single init/cleanup of libxml2 (not reentrant)
 * 3.8.5: Look at release 3.8.5 content
 * 3.8.6: Look at release 3.8.6 content
 * --------------------------------------------------------------------*/

/* **********************************************************************
 * Global data definition related to the software install and versioning
 * Must be up-to-date for each release in connection with others packages
 * (Java tool and Environment software (Python scripts, ..)
 * **********************************************************************
 */
#ifndef MAJOR
#error "Silecs::Service: MAJOR SYMBOL NOT DEFINED! Make sure to define symbols MAJOR, MINOR and PATCH in your Makefile."
#endif

namespace Silecs
{
#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

//Semantic versioning for SILECS packages (Tools, Environment and client library have consistent major number)
const std::string Service::semverMajor_ = STRINGIFY(MAJOR); //Major release, not backward compatible
const std::string Service::semverMinor_ = STRINGIFY(MINOR); //Minor release, backward compatible
const std::string Service::semverPatch_ = STRINGIFY(PATCH); //Bug fixes, backward compatible
const std::string Service::developmentVersion_ = "DEV"; // Version which is used for unreleased code

const std::string Service::paramFilesPath_ = "/dsc/data/silecs/" + Service::semverMajor_ + ".m.p/delivery/";
const std::string Service::paramFilesPathFallback_ = "/acc/dsc/mcr/data/silecs/" + Service::semverMajor_ + ".m.p/delivery/";

} // namespace

/* **********************************************************************
 * **********************************************************************
 */
namespace Silecs
{
// static definition
Service* Service::instance_ = NULL;
bool Service::isShutingDown_ = false;

Service* Service::getInstance(int argc, char ** argv)
{
    if (instance_ == NULL)
    {
        instance_ = new Service(argc, argv);
        LOG((ALLOC)) << "SILECS Service create";

        XMLParser::init();

        //ACET start-up message (once per process)
        TRACE("info") << "SILECS service startup: libsilecs-comm " << semverMajor_ << "." << semverMinor_ << "." << semverPatch_;
    }
    return instance_;
}

void Service::deleteInstance()
{
    if (!isShutingDown_)
    {
        isShutingDown_ = true;
        LOG((ALLOC)) << "SILECS Resources release";

        TRACE("info") << "SILECS service shutdown: libsilecs-comm " << semverMajor_ << "." << semverMinor_ << "." << semverPatch_;

        XMLParser::cleanUp();

        if (instance_ != NULL)
            delete instance_;
        instance_ = NULL;
    }
}

void Service::setArguments(std::string usrArgs)
{
    size_t pos = usrArgs.find("-plcLog");
    if (pos != std::string::npos)
    {
        std::string topics = usrArgs.substr(pos + 8);
        char *argv[3] = {(char*)"dummy", (char*)"-plcLog", (char*)topics.c_str()};
        if (setArgs(3, argv) == false)
        {
            throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_ARGUMENTS);
        }
    }
}

bool Service::setLogTopics(std::string topics)
{
    bool isOK = Log::getTopicsFromString(topics);
    return isOK;
}

Service::Service(int argc, char ** argv)
{
    isShutingDown_ = false;

    //Default client process name (in case arguments are not transmitted)
    char *ident = (char*)"SILECS-Client";

    //Start the sys-logger at first
    if (argc > 0)
    { //Real process name
        ident = basename(argv[0]);
    }

    //No particular topics to log to syslog in addition to the mandatory ones (if any)
    Log::startSyslog(ident, 0);

    //Then, interpret and set the 'plc' parameters of the command line, if any
    if ( (argc > 0) && (argv != NULL))
    {
        if (setArgs(argc, argv) == false)
        {
            printArgs();
            /* or as following if required
             std::cerr << std::endl;
             std::cerr << "PLC expected arguments: " << std::endl;
             std::cerr << Silecs::whichArgs() << std::endl;
             */
            throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_ARGUMENTS);
        }
    }
}

Service::~Service()
{
    //Disconnect and remove all shared-connection if any (one per PLC)
    PLC::deleteConnection();

    // Remove all the Cluster instances of this SILECS Service
    clusterMapType::iterator clusterMapIter;
    for (clusterMapIter = clusterMap_.begin(); clusterMapIter != clusterMap_.end(); clusterMapIter++)
    {
        delete clusterMapIter->second;
    }
    clusterMap_.clear();

    //close the syslog resource
    Log::stopSyslog();
}

Cluster* Service::getCluster(std::string className, std::string classVersion)
{
    std::string clusterName = className + "_" + classVersion;
    clusterMapType::iterator iter = clusterMap_.find(clusterName);
    if (iter == clusterMap_.end())
    {
        // Does not exist yet, create a new one for this className
        Cluster* theCluster = new Cluster(className, classVersion);
        clusterMap_.insert(std::make_pair(clusterName, theCluster));
        return theCluster;
    }
    // Already created, return the unique reference
    return iter->second;
}

std::string Service::getSemverMajor()
{
    return semverMajor_;
}

std::string Service::getSemverMinor()
{
    return semverMinor_;
}

std::string Service::getSemverPatch()
{
    return semverPatch_;
}

std::string Service::getVersion()
{
    if( semverMajor_ == "0" && semverMinor_ == "0" && semverPatch_ == "0" )
    {
        return developmentVersion_;
    }

    return semverMajor_ + "." + semverMinor_ + "." + semverPatch_;
}

bool Service::isVersionSupported(std::string version)
{
    if( version == getVersion())
        return true;

    std::size_t pos = version.find(".");
    if (pos == std::string::npos)
    {
        return false;
    }
    std::string major_string = version.substr(0,pos);
    int major_int, lib_major_int;
    std::istringstream ( major_string ) >> major_int;
    std::istringstream ( semverMajor_ ) >> lib_major_int;
    return major_int == lib_major_int;
}

const std::string Service::getBinaryFolderPath()
{
    char buf[1024] = {0};
    ssize_t size = readlink("/proc/self/exe", buf, sizeof (buf));
    if (size == 0 || size == sizeof (buf))
    {
        throw SilecsException(__FILE__, __LINE__, "Failed to get binary folder");
    }
    std::string path(buf, size);
    boost::system::error_code ec;
    boost::filesystem::path binaryPath(boost::filesystem::canonical(path, boost::filesystem::current_path(), ec));
    boost::filesystem::path dir = binaryPath.parent_path();
    return dir.make_preferred().string();
}

const std::string Service::getParamFile(std::string controllerName)
{
    // directly next to the binary, used on GSI frontends
    std::string localParameterFile1 = getBinaryFolderPath() + "/" + controllerName + ".silecsparam";

    if (access(localParameterFile1.c_str(), F_OK) != -1)
    {
        LOG(DEBUG) << "Using local param file: " << localParameterFile1;
        return localParameterFile1;
    }

    // if FESA-class is not deployed yet and started on development-system ( used on GSI development-systems )
    std::string localParameterFile2 = getBinaryFolderPath() + "/../../../generated-silecs/client/" + controllerName + ".silecsparam";
    //for debugging printf("localParameterFile2: %s",localParameterFile2.c_str());
    if (access(localParameterFile2.c_str(), F_OK) != -1)
    {
        LOG(DEBUG) << "Using local param file: " << localParameterFile2;
        return localParameterFile2;
    }

    std::string globalParameterFile1 = paramFilesPath_ + controllerName + "/params/" + controllerName + ".silecsparam";
    if (access(globalParameterFile1.c_str(), F_OK) != -1)
    {
        LOG(DEBUG) << "Using global param file: " << globalParameterFile1;
        return globalParameterFile1;
    }

    std::string globalParameterFile2 = paramFilesPathFallback_ + controllerName + "/params/" + controllerName + ".silecsparam";
    if (access(globalParameterFile2.c_str(), F_OK) != -1)
    {
        LOG(DEBUG) << "Using global fallback param file: " << globalParameterFile2;
        return globalParameterFile2;
    }

    std::string message = "No parameter-file found for controller '" + controllerName + "'";
    throw SilecsException(__FILE__, __LINE__, message.c_str());
}

bool Service::withInputAccess(AccessType& accessType)
{
    return (accessType != Output);
}

bool Service::withOutputAccess(AccessType& accessType)
{
    return (accessType != Input);
}

bool Service::fileExists(std::string filename)
{
    struct stat fileInfo;

    // Attempt to get the file attributes
    if (stat(filename.c_str(), &fileInfo) == 0)
    { //No error: the file exists
        return true;
    }
    else
    {
        if (errno == ENOENT)
        { //No such file or directory
            return false;
        }
    }
    //Unexpected error
    throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
}

bool Service::checkArgs(int argc, char ** argv)
{
    if ( (argc >= 2) && (argv != NULL))
    {
        for (int i = 0; i < argc; i++)
            if (strcmp(argv[i], "-plcHelp") == 0)
                return false;
    }
    return true; //help is not required
}

bool Service::setArgs(int argc, char ** argv)
{
    if (checkArgs(argc, argv))
        if (Log::setLogArguments(argc, argv))
            //if ..
            //if ..
            return true;
    return false;
}

std::string Service::whichArgs()
{
    return std::string("-plcHelp") + std::string("\n") + Log::getLogArguments() + std::string("\n")
    // + ..
    ;
}

void Service::printArgs()
{
    std::cerr << std::endl;
    std::cerr << "SILECS expected arguments: " << std::endl;
    std::cerr << whichArgs() << std::endl;
}

} // namespace

