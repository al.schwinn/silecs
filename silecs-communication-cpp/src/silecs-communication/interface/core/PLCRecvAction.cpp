/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/core/PLCAction.h>
#include <silecs-communication/interface/core/PLCRecvAction.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/PLCBlock.h>
#include <silecs-communication/interface/core/Context.h>
#include <silecs-communication/interface/utility/SilecsLog.h>

namespace Silecs
{

PLCRecvBlockMode::PLCRecvBlockMode(Block* block) :
                PLCAction(block)
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCRecvBlockMode (create): " << block->getName();
}

PLCRecvBlockMode::~PLCRecvBlockMode()
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCRecvBlockMode (delete): " << theBlock_->getName();
}

PLCRecvDeviceMode::PLCRecvDeviceMode(Block* block) :
                PLCAction(block)
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCRecvDeviceMode (create): " << theBlock_->getName();
}

PLCRecvDeviceMode::~PLCRecvDeviceMode()
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCRecvDeviceMode (delete): " << theBlock_->getName();
}

int PLCRecvBlockMode::execute(Context* pContext)
{
    timeval tod; //Time-of-day for register time-stamping
    int errorCode = 0;

    Connection* pConn = theBlock_->getPLC()->getConnection();
    AccessArea area = theBlock_->getAccessArea();

    try
    {
        //if transaction is for one device only, pContext contains its reference
        if (pContext->isForOneDevice())
        {
            //Device context: get block of one device only ====================================
            Device* pDev = pContext->getDevice();

            //Base attributes of the device within the complete data block containing all devices.
            unsigned long usedAddress = theBlock_->getAddress();
            unsigned long usedSize = theBlock_->getMemSize();
            unsigned long usedDeviceOffset = pDev->getInputAddress(area) * usedSize;
            unsigned char* pBuffer = ((unsigned char*)theBlock_->getBuffer()) + usedDeviceOffset;

            // Overwrite device-block address, offset & size in case user wants to resize the block dynamically
            if (theBlock_->withCustomAttributes() == true)
            {
                usedAddress = theBlock_->getCustomAddress();
                usedSize = theBlock_->getCustomSize();
                usedDeviceOffset = theBlock_->getCustomOffset();
            }

            if (RECV & Log::topics_)
            {
                if (pConn->isEnabled())
                {
                    Log(RECV).getLog() << "RecvAction (execute/ BlockMode): " << ", device: " << pDev->getLabel() << ", block: " << theBlock_->getName();
                }
            }

            //begin critical section
            Lock lock(bufferMux_);
            switch (theBlock_->getAccessArea())
            {
                case Digital:
                    errorCode = pConn->readDIO(theBlock_->getPLC(), usedAddress, //Base address (or DBn) of the block
                    usedDeviceOffset, //Device data address within the block
                    usedSize, //Get one device-block only
                    pBuffer //Buffer to store the data
                    );
                    break;
                case Analog:
                {
                    errorCode = pConn->readAIO(theBlock_->getPLC(), usedAddress, //Base address (or DBn) of the block
                    usedDeviceOffset, //Device data address within the block
                    usedSize, //Get one device-block only
                    pBuffer //Buffer to store the data
                    );
                    break;
                }
                case Memory:
                default:
                {
                    errorCode = pConn->readMemory(theBlock_->getPLC(), usedAddress, //Base address (or DBn) of the block
                    usedDeviceOffset, //Device data address within the block
                    usedSize, //Get one device-block only
                    pBuffer //Buffer to store the data
                    );
                    break;
                }
            }

            if (pConn->isConnected() && (errorCode == 0))
            { //Data have just been received: get time-of-day to time-stamp the registers
                gettimeofday(&tod, 0);

                //Device context: Extract all the device registers from that block
                pDev->importRegisters(theBlock_, pBuffer, tod, pContext);

                LOG_DELAY(RECV) << "done for block: " << theBlock_->getName();
            }
            //end critical section
        }
        else
        {
            //Dynamic resizing of the block is not possible in BLOCK_MODE
            if (theBlock_->withCustomAttributes() == true)
                throw SilecsException(__FILE__, __LINE__, COMM_BLOCK_RESIZING_NOT_ALLOWED);

            deviceVectorType::iterator pDeviceIter;
            deviceVectorType& deviceCol = theBlock_->getPLC()->getDeviceMap();

            //Cluster/PLC context: get block of all devices in one go =========================
            if (RECV & Log::topics_)
            {
                if (pConn->isEnabled())
                {
                    Log(RECV).getLog() << "RecvAction (execute/ BlockMode): " << theBlock_->getName() << ", " << deviceCol.size() << " device(s)";
                }
            }

            //begin critical section
            Lock lock(bufferMux_);
            switch (area)
            {
                case Digital:
                    errorCode = pConn->readDIO(theBlock_->getPLC(), theBlock_->getAddress(), //Base address (or DBn) of the block
                    0, //Get all devices from the first one
                    ((PLCBlock*)theBlock_)->getBufferSize(), //Get blocks of all devices (full buffer size)
                    (unsigned char*)theBlock_->getBuffer() //Buffer to store the data (full buffer)
                    );
                    break;
                case Analog:
                {
                    errorCode = pConn->readAIO(theBlock_->getPLC(), theBlock_->getAddress(), //Base address (or DBn) of the block
                    0, //Get all devices from the first one
                    ((PLCBlock*)theBlock_)->getBufferSize(), //Get blocks of all devices (full buffer size)
                    (unsigned char*)theBlock_->getBuffer() //Buffer to store the data (full buffer)
                    );
                    break;
                }
                case Memory:
                default:
                {
                    errorCode = pConn->readMemory(theBlock_->getPLC(), theBlock_->getAddress(), //Base address (or DBn) of the block
                    0, //Get all devices from the first one
                    ((PLCBlock*)theBlock_)->getBufferSize(), //Get blocks of all devices (full buffer size)
                    (unsigned char*)theBlock_->getBuffer() //Buffer to store the data (full buffer)
                    );
                    break;
                }
            }

            if (pConn->isConnected() && (errorCode == 0))
            { //Data have just been received: get time-of-day to time-stamp the registers
                gettimeofday(&tod, 0);

                //PLC context: Extract all registers value of all devices of the PLC from that block
                for (pDeviceIter = deviceCol.begin(); pDeviceIter != deviceCol.end(); ++pDeviceIter)
                {
                    Device* pDev = pDeviceIter->second;

                    // Only extract if this device actually contains this block, otherwise skip it.
                    if (!pDev->hasBlock(theBlock_->getName()))
                    {
                        continue;
                    }
                    unsigned long deviceOffset = pDev->getInputAddress(area) * theBlock_->getMemSize();
                    unsigned char* pBuffer = ((unsigned char*)theBlock_->getBuffer()) + deviceOffset;
                    pDev->importRegisters(theBlock_, pBuffer, tod, pContext);
                }

                LOG_DELAY(RECV) << "done for block: " << theBlock_->getName();
            }
        }
    }
    catch(const SilecsException& ex)
    {
        LOG(ERROR) << ex.what();
        LOG(ERROR) << "RecvAction (execute/ BlockMode) for block " << theBlock_->getName() << " has failed.";
        return ex.getCode();
    }
    return 0;
}

int PLCRecvDeviceMode::execute(Context* pContext)
{
    timeval tod; //Time-of-day for register time-stamping
    int errorCode = 0;
    Connection* pConn = theBlock_->getPLC()->getConnection();
    AccessArea area = theBlock_->getAccessArea();

    try
    {
        //if transaction is for one device only, pContext contains its reference
        if (pContext->isForOneDevice())
        {
            //Device context: get block of one device only ====================================
            Device* pDev = pContext->getDevice();

            //Set base attributes of the device blocks
            unsigned long usedDeviceAddress = pDev->getInputAddress(area);
            unsigned long usedBlockAddress = theBlock_->getAddress();
            unsigned long usedSize = theBlock_->getMemSize();

            // Overwrite device-block address, offset & size in case user wants to resize the block dynamically
            if (theBlock_->withCustomAttributes() == true)
            {
                usedDeviceAddress = theBlock_->getCustomAddress();
                usedBlockAddress = theBlock_->getCustomOffset();
                usedSize = theBlock_->getCustomSize();
            }

            if (RECV & Log::topics_)
            {
                if (pConn->isEnabled())
                {
                    Log(RECV).getLog() << "RecvAction (execute/ DeviceMode): device: " << pDev->getLabel() << ", block: " << theBlock_->getName();
                }
            }

            //begin critical section
            Lock lock(bufferMux_);
            switch (theBlock_->getAccessArea())
            {
                case Digital:
                    errorCode = pConn->readDIO(theBlock_->getPLC(), usedDeviceAddress, //Base address (or DBn) of the device
                    usedBlockAddress, //Block data address within the device
                    usedSize, //Get one device-block only
                    (unsigned char*)theBlock_->getBuffer() //Buffer to store the data
                    );
                    break;
                case Analog:
                {
                    errorCode = pConn->readAIO(theBlock_->getPLC(), usedDeviceAddress, //Base address (or DBn) of the device
                    usedBlockAddress, //Block data address within the device
                    usedSize, //Get one device-block only
                    (unsigned char*)theBlock_->getBuffer() //Buffer to store the data
                    );
                    break;
                }
                case Memory:
                default:
                {
                    errorCode = pConn->readMemory(theBlock_->getPLC(), usedDeviceAddress, //Base address (or DBn) of the device
                    usedBlockAddress, //Block data address within the device
                    usedSize, //Get one device-block only
                    (unsigned char*)theBlock_->getBuffer() //Buffer to store the data
                    );
                    break;
                }
            }

            if (pConn->isConnected() && (errorCode == 0))
            { //Data have just been received: get time-of-day to time-stamp the registers
                gettimeofday(&tod, 0);

                //Device context: Extract all the device registers from that block
                pDev->importRegisters(theBlock_, (unsigned char*)theBlock_->getBuffer(), tod, pContext);

                LOG_DELAY(RECV) << "done for block: " << theBlock_->getName();
            }
        }
        else
        {
            //Dynamic resizing of the block is not possible in multiple access
            if (theBlock_->withCustomAttributes() == true)
                throw SilecsException(__FILE__, __LINE__, COMM_BLOCK_RESIZING_NOT_ALLOWED);

            //Cluster/PLC context: get block of all devices one by one =========================
            deviceVectorType::iterator pDeviceIter;
            deviceVectorType& deviceCol = theBlock_->getPLC()->getDeviceMap();
            for (pDeviceIter = deviceCol.begin(); pDeviceIter != deviceCol.end(); ++pDeviceIter)
            {
                Device* pDev = pDeviceIter->second;
                if (!pDev->hasBlock(theBlock_->getName()))
                {
                    return 0;
                }

                if (RECV & Log::topics_)
                {
                    if (pConn->isEnabled())
                    {
                        Log(RECV).getLog() << "RecvAction (execute/ DeviceMode): device: " << pDev->getLabel() << ", block: " << theBlock_->getName();
                    }
                }
                //begin critical section
                Lock lock(bufferMux_);
                switch (area)
                {
                    case Digital:
                        errorCode = pConn->readDIO(theBlock_->getPLC(), pDev->getInputAddress(area), //Base address (or DBn) of the device
                        theBlock_->getAddress(), //Block data address within the device
                        theBlock_->getMemSize(), //Get one device-block only
                        (unsigned char*)theBlock_->getBuffer() //Buffer to store the data
                        );
                        break;
                    case Analog:
                    {
                        errorCode = pConn->readAIO(theBlock_->getPLC(), pDev->getInputAddress(area), //Base address (or DBn) of the device
                        theBlock_->getAddress(), //Block data address within the device
                        theBlock_->getMemSize(), //Get one device-block only
                        (unsigned char*)theBlock_->getBuffer() //Buffer to store the data
                        );
                        break;
                    }
                    case Memory:
                    default:
                    {
                        errorCode = pConn->readMemory(theBlock_->getPLC(), pDev->getInputAddress(area), //Base address (or DBn) of the device
                        theBlock_->getAddress(), //Block data address within the device
                        theBlock_->getMemSize(), //Get one device-block only
                        (unsigned char*)theBlock_->getBuffer() //Buffer to store the data
                        );
                        break;
                    }
                }

                if (pConn->isConnected() && (errorCode == 0))
                { //Data have just been received: get time-of-day to time-stamp the registers
                    gettimeofday(&tod, 0);

                    //PLC context: Extract all registers value of the current device of the PLC from that block
                    pDev->importRegisters(theBlock_, (unsigned char*)theBlock_->getBuffer(), tod, pContext);

                    LOG_DELAY(RECV) << "done for block: " << theBlock_->getName();
                }
            }
        }
    }
    catch(const SilecsException& ex)
    {
        LOG(ERROR) << ex.what();
        LOG(ERROR) << "RecvAction (execute/ DeviceMode) failed.";
        return ex.getCode();
    }
    return 0;
}

} // namespace
