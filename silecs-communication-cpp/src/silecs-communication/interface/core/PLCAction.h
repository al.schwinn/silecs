/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_PLC_ACTION_H_
#define _SILECS_PLC_ACTION_H_

#include <string>
#include <silecs-communication/interface/utility/Mutex.h>
#include <silecs-communication/interface/core/SilecsAction.h>

namespace Silecs
{
class Block;
class Context;

/*!
 * \class PLCAction
 * \brief PLCAction defines the abstract level of any PLC actions (recv and send)
 */
class PLCAction : public Action
{

public:
    PLCAction(Block* block) :
                    Action(block)
    {
        bufferMux_ = new Mutex("bufferMux");
    }
    ;
    virtual ~PLCAction()
    {
        delete bufferMux_;
    }
    ;

    /*!
     * \fn execute
     * It contains the code to be executed for this action.
     * Overwrites the code from Action class
     */
    virtual int execute(Context* pContext) = 0;

protected:
    //The frame buffer update (import/export registers) and related read/write data must be done
    //in a critical section (the same buffer is used to send/recv data to all devices).
    Mutex* bufferMux_;

};

} // namespace

#endif // _SILECS_PLC_ACTION_H_
