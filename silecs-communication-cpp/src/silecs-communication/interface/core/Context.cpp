/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/core/Context.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/utility/SilecsLog.h>

namespace Silecs
{
Context::Context(Device* pDev, bool isForSynchronization) :
                pDevice_(pDev),
                isForSynchro_(isForSynchronization)
{
    //LOG(ALLOC) << "Context (create) for Device: " << pDevice_->getLabel() << ", isForSynchronization: " <<  (isForSynchro_ ? "yes" : "no");
}

Context::~Context()
{
    //LOG(ALLOC) << "Context (delete) for Device: " << pDevice_->getLabel()  << ", isForSynchronization: " <<  (isForSynchro_ ? "yes" : "no");
}

} // namespace

