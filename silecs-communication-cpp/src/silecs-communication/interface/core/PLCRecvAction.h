/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _RECV_PLC_ACTION_H_
#define _RECV_PLC_ACTION_H_

#include <silecs-communication/interface/core/PLCAction.h>

namespace Silecs
{
class Action;
class Block;
class Context;

/*!
 * \class PLCRecvBlockMode
 * \brief Concrete action responsible to get a data block from the PLC (Block mode configuration)
 */
class PLCRecvBlockMode : public PLCAction
{

public:
    PLCRecvBlockMode(Block* block);
    virtual ~PLCRecvBlockMode();

    /*!
     * \fn execute
     * Connect the PLC if necessary and Get data-block from its memory.
     * Overwrite the abstract method
     * \return 0 if no error occurred. else return a error status from low level library
     */
    int execute(Context* pContext);

private:

};

/*!
 * \class PLCRecvDeviceMode
 * \brief Concrete action responsible to get a data block from the PLC (Device mode configuration)
 */
class PLCRecvDeviceMode : public PLCAction
{

public:
    PLCRecvDeviceMode(Block* block);
    virtual ~PLCRecvDeviceMode();

    /*!
     * \fn execute
     * Connect the PLC if necessary and Get data-block from its memory.
     * Overwrite the abstract method
     * \return 0 if no error occurred. else return a error status from low level library
     */
    int execute(Context* pContext);

private:

};

} // namespace

#endif // _RECV_PLC_ACTION_H_

