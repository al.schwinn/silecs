/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/core/SilecsAction.h>
#include <silecs-communication/interface/core/PLCAction.h>
#include <silecs-communication/interface/core/PLCSendAction.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/PLCBlock.h>
#include <silecs-communication/interface/core/Context.h>
#include <silecs-communication/interface/utility/SilecsLog.h>

namespace Silecs
{

PLCSendBlockMode::PLCSendBlockMode(Block* block) :
                PLCAction(block)
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCSendBlockMode (constructor): " << block->getName();
}

PLCSendBlockMode::~PLCSendBlockMode()
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCSendBlockMode (destructor): " << theBlock_->getName();
}

PLCSendDeviceMode::PLCSendDeviceMode(Block* block) :
                PLCAction(block)
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCSendDeviceMode (constructor): " << theBlock_->getName();
}

PLCSendDeviceMode::~PLCSendDeviceMode()
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Action PLCSendDeviceMode (destructor): " << theBlock_->getName();
}

int PLCSendBlockMode::execute(Context* pContext)
{
    int errorCode = 0;
    Connection* pConn = theBlock_->getPLC()->getConnection();

    try
    {
        //if transaction is for one device only, pContext contains its reference
        if (pContext->isForOneDevice())
        {
            //Device context: send block of one device only ====================================
            Device* pDev = pContext->getDevice();

            //Base attributes of the device within the complete data block containing all devices.
            unsigned long usedAddress = theBlock_->getAddress();
            unsigned long usedSize = theBlock_->getMemSize();
            AccessArea area = theBlock_->getAccessArea();
            unsigned long usedDeviceOffset = pDev->getOutputAddress(area) * usedSize;
            unsigned char* pBuffer = ((unsigned char*)theBlock_->getBuffer()) + usedDeviceOffset;

            // Overwrite device-block address, offset & size in case user wants to resize the block dynamically
            if (theBlock_->withCustomAttributes() == true)
            {
                usedAddress = theBlock_->getCustomAddress();
                usedSize = theBlock_->getCustomSize();
                usedDeviceOffset = theBlock_->getCustomOffset();
            }

            { //critical section
                Lock lock(bufferMux_);
                if (pConn->isEnabled())
                {
                    LOG(SEND) << "SendAction (execute/ BlockMode): device: " << pDev->getLabel() << ", block: " << theBlock_->getName();

                    //Device context: Export all the device registers to that block
                    pDev->exportRegisters(theBlock_, pBuffer, pContext);
                }

                switch (area)
                {
                    case Digital:
                        errorCode = pConn->writeDIO(theBlock_->getPLC(), usedAddress, //Base address (or DBn) of the block
                        usedDeviceOffset, //Device data address within the block
                        usedSize, //Set one device-block only
                        pBuffer //Buffer which contain the data
                        );
                        break;
                    case Analog:
                    {
                        errorCode = pConn->writeAIO(theBlock_->getPLC(), usedAddress, //Base address (or DBn) of the block
                        usedDeviceOffset, //Device data address within the block
                        usedSize, //Set one device-block only
                        pBuffer //Buffer which contain the data
                        );
                        break;
                    }
                    case Memory:
                    default:
                        errorCode = pConn->writeMemory(theBlock_->getPLC(), usedAddress, //Base address (or DBn) of the block
                        usedDeviceOffset, //Device data address within the block
                        usedSize, //Set one device-block only
                        pBuffer //Buffer which contain the data
                        );
                        break;
                }
            }

            if (SEND & Log::topics_)
            {
                if (pConn->isEnabled())
                {
                    Log(SEND).getLogDelay() << "done for block: " << theBlock_->getName();
                }
            }
        }
        else
        {
            //Dynamic resizing of the block is not possible in BLOCK_MODE
            if (theBlock_->withCustomAttributes() == true)
                throw SilecsException(__FILE__, __LINE__, COMM_BLOCK_RESIZING_NOT_ALLOWED);

            { //critical section
                Lock lock(bufferMux_);
                if (pConn->isEnabled())
                {
                    deviceVectorType::iterator pDeviceIter;
                    deviceVectorType& deviceCol = theBlock_->getPLC()->getDeviceMap();

                    //Cluster/PLC context: set block of all devices in one go ===========================
                    LOG(SEND) << "SendAction (execute/ BlockMode): block: " << theBlock_->getName() << ", " << deviceCol.size() << " device(s)";

                    //PLC context: Export all registers value of all devices of the PLC to that block
                    for (pDeviceIter = deviceCol.begin(); pDeviceIter != deviceCol.end(); ++pDeviceIter)
                    {
                        Device* pDev = pDeviceIter->second;
                        AccessArea area = theBlock_->getAccessArea();
                        unsigned long deviceOffset = pDev->getOutputAddress(area) * theBlock_->getMemSize();
                        unsigned char* pBuffer = ((unsigned char*)theBlock_->getBuffer()) + deviceOffset;
                        pDev->exportRegisters(theBlock_, pBuffer, pContext);
                    }
                }

                switch (theBlock_->getAccessArea())
                {
                    case Digital:
                        errorCode = pConn->writeDIO(theBlock_->getPLC(), theBlock_->getAddress(), //Base address (or DBn) of the block
                        0, //Set all devices from the first one
                        ((PLCBlock*)theBlock_)->getBufferSize(), //Set blocks of all devices (full buffer size)
                        (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data (full buffer)
                        );
                        break;
                    case Analog:
                    {
                        errorCode = pConn->writeAIO(theBlock_->getPLC(), theBlock_->getAddress(), //Base address (or DBn) of the block
                        0, //Set all devices from the first one
                        ((PLCBlock*)theBlock_)->getBufferSize(), //Set blocks of all devices (full buffer size)
                        (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data (full buffer)
                        );
                        break;
                    }
                    case Memory:
                    default:
                        errorCode = pConn->writeMemory(theBlock_->getPLC(), theBlock_->getAddress(), //Base address (or DBn) of the block
                        0, //Set all devices from the first one
                        ((PLCBlock*)theBlock_)->getBufferSize(), //Set blocks of all devices (full buffer size)
                        (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data (full buffer)
                        );
                        break;
                }
            }

            if (SEND & Log::topics_)
            {
                if (pConn->isEnabled())
                {
                    Log(SEND).getLogDelay() << "done for block: " << theBlock_->getName();
                }
            }
        }
    }
    catch(const SilecsException& ex)
    {
        LOG(ERROR) << ex.what();
        LOG(ERROR) << "SendAction (execute/ DeviceMode) on block: " << theBlock_->getName() << " has failed";
        return ex.getCode();
    }
    return 0;
}

int PLCSendDeviceMode::execute(Context* pContext)
{
    Connection* pConn = theBlock_->getPLC()->getConnection();
    int errorCode = 0;
    try
    {
        //if transaction is for one device only, pContext contains its reference
        if (pContext->isForOneDevice())
        {
            //Device context: set block of one device only ====================================
            Device* pDev = pContext->getDevice();

            //Set base attributes of the device blocks
            AccessArea area = theBlock_->getAccessArea();
            unsigned long usedDeviceAddress = pDev->getOutputAddress(area);
            unsigned long usedBlockAddress = theBlock_->getAddress();
            unsigned long usedSize = theBlock_->getMemSize();

            // Overwrite device-block address, offset & size in case user wants to resize the block dynamically
            if (theBlock_->withCustomAttributes() == true)
            {
                usedDeviceAddress = theBlock_->getCustomAddress();
                usedBlockAddress = theBlock_->getCustomOffset();
                usedSize = theBlock_->getCustomSize();
            }

            { //critical section
                Lock lock(bufferMux_);
                if (pConn->isEnabled())
                {
                    LOG(SEND) << "SendAction (execute/ DeviceMode): device: " << pDev->getLabel() << ", block: " << theBlock_->getName();

                    //Device context: Export all the device registers to that block
                    pDev->exportRegisters(theBlock_, (unsigned char*)theBlock_->getBuffer(), pContext);
                }

                switch (area)
                {
                    case Digital:
                        errorCode = pConn->writeDIO(theBlock_->getPLC(), usedDeviceAddress, //Base address (or DBn) of the device
                        usedBlockAddress, //Block data address within the device
                        usedSize, //Set one device-block only
                        (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data
                        );
                        break;
                    case Analog:
                    {
                        errorCode = pConn->writeAIO(theBlock_->getPLC(), usedDeviceAddress, //Base address (or DBn) of the device
                        usedBlockAddress, //Block data address within the device
                        usedSize, //Set one device-block only
                        (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data
                        );
                        break;
                    }
                    case Memory:
                    default:
                        errorCode = pConn->writeMemory(theBlock_->getPLC(), usedDeviceAddress, //Base address (or DBn) of the device
                        usedBlockAddress, //Block data address within the device
                        usedSize, //Set one device-block only
                        (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data
                        );
                        break;
                }
            }

            if (SEND & Log::topics_)
            {
                if (pConn->isEnabled())
                {
                    Log(SEND).getLogDelay() << "done for block: " << theBlock_->getName();
                }
            }
        }
        else
        {
            //Dynamic resizing of the block is not possible in multiple access
            if (theBlock_->withCustomAttributes() == true)
                throw SilecsException(__FILE__, __LINE__, COMM_BLOCK_RESIZING_NOT_ALLOWED);

            //Cluster/PLC context: set block of all devices one by one =========================
            deviceVectorType::iterator pDeviceIter;
            deviceVectorType& deviceCol = theBlock_->getPLC()->getDeviceMap();
            for (pDeviceIter = deviceCol.begin(); pDeviceIter != deviceCol.end(); ++pDeviceIter)
            {
                Device* pDev = pDeviceIter->second;

                { //critical section
                    Lock lock(bufferMux_);
                    if (pConn->isEnabled())
                    {
                        LOG(SEND) << "SendAction (execute/ DeviceMode):  device: " << pDev->getLabel() << ", block: " << theBlock_->getName();

                        //PLC context: Export all registers value of the current device of the PLC to that block
                        pDev->exportRegisters(theBlock_, (unsigned char*)theBlock_->getBuffer(), pContext);
                    }

                    AccessArea area = theBlock_->getAccessArea();
                    switch (area)
                    {
                        case Digital:
                            errorCode = pConn->writeDIO(theBlock_->getPLC(), pDev->getOutputAddress(area), //Base address (or DBn) of the device
                            theBlock_->getAddress(), //Block data address within the device
                            theBlock_->getMemSize(), //Set one device-block only
                            (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data
                            );
                            break;
                        case Analog:
                        {
                            errorCode = pConn->writeAIO(theBlock_->getPLC(), pDev->getOutputAddress(area), //Base address (or DBn) of the device
                            theBlock_->getAddress(), //Block data address within the device
                            theBlock_->getMemSize(), //Set one device-block only
                            (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data
                            );
                            break;
                        }
                        case Memory:
                        default:
                            errorCode = pConn->writeMemory(theBlock_->getPLC(), pDev->getOutputAddress(area), //Base address (or DBn) of the device
                            theBlock_->getAddress(), //Block data address within the device
                            theBlock_->getMemSize(), //Set one device-block only
                            (unsigned char*)theBlock_->getBuffer() //Buffer which contain the data
                            );
                            break;
                    }
                }

                if (SEND & Log::topics_)
                {
                    if (pConn->isEnabled())
                    {
                        Log(SEND).getLogDelay() << "done for block: " << theBlock_->getName();
                    }
                }
            }
        }
    }
    catch(const SilecsException& ex)
    {
        LOG(ERROR) << ex.what();
        LOG(ERROR) << "SendAction (execute/ DeviceMode) failed";
        return ex.getCode();
    }
    return 0;
}

} // namespace
