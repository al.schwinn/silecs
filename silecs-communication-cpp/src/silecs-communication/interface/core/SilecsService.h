/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_SERVICE_H_
#define _SILECS_SERVICE_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <set>
#include <map>

namespace Silecs
{
/// @cond
// Register and Block access types
typedef enum
{
    Output,
    Input,
    InOut
} AccessType;

// Register and Block access types
typedef enum
{
    Memory,
    Digital,
    Analog
} AccessArea;
/// @endcond

/*!
 * \brief Defines the category of the SILECS Exception.
 * Can be used in the Exception catch to dispatch emergency actions.
 * Use Exception::getCategory() to retrieve it.
 */
typedef enum
{
    ///Default mode: Do not synchronize the retentive Registers at the PLC connection
    NO_SYNCHRO = 0,
    ///Synchronize the Master Registers at the PLC connection (downloaded from the PLC)
    MASTER_SYNCHRO = 1,
    ///Synchronize the Slave Registers at the PLC connection (uploaded from the client)
    SLAVE_SYNCHRO = 2,
    ///Synchronize all the retentive Registers at the PLC connection (download Masters and upload Slaves)
    FULL_SYNCHRO = 3,
} SynchroMode;
}

#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>

namespace Silecs
{
class Cluster;

/// @cond
typedef std::map<std::string, Cluster*> clusterMapType;
/// @endcond

/*!
 * \class Service
 * \brief This class is the entry point of the SILECS service.
 * Service class is a singleton that the client process must instantiate at first.
 * It provides services to set-up the general tasks (set process arguments for instance)
 * and create the required PLC Clusters and related resources of each SILECS class:
 * PLC, Device and Register.
 */
class Service
{

public:
    /*!
     * \brief Use this method to create/get the unique process instance of the SILECS Service.
     * By creating that object it's possible to transmit specific arguments to the SILECS Service,
     * typically the SILECS logging options:
     * -plcLog ERROR[,INFO,DEBUG,SETUP,ALLOC,RECV,SEND,COMM,DATA,LOCK]
     *
     * (argc=0, argv=NULL) means no arguments for SILECS Service
     */
    static Service* getInstance(int argc = 0, char ** argv = NULL);

    /*!
     * \brief This method releases all the SILECS resources and finally removes
     * the Service singleton itself.
     * Attention! This method is responsible to disconnect all the PLCs but mostly to remove
     * all the SILECS resources (Clusters and related components: PLCs, Devices, Registers, ..)
     * Client implementation must ensure that no process is currently accessing to
     * these resources before calling.
     */
    static void deleteInstance();

    /*!
     * \brief Use this method to either get an existing Cluster object,
     *  or to create a new one for a given className/version, if not existing.
     *  The client is responsible to delete all the Cluster instances and related
     *  components by deleting the Service object.
     * \param className name of the class
     * \param classVersion version number of the class
     * \return reference to the unique Cluster instance for this class/version
     */
    Silecs::Cluster* getCluster(std::string className, std::string classVersion);

    /*!
     * \brief Use this method to get the library Semantic version (Major)
     * \return the Major number of the current release: <Major>.<Minor>.<Patch>
     */
    static std::string getSemverMajor();

    /*!
     * \brief Use this method to get the library Semantic version (Minor)
     * \return the Minor number of the current release: <Major>.<Minor>.<Patch>
     */
    static std::string getSemverMinor();

    /*!
     * \brief Use this method to get the library Semantic version (Patch)
     * \return the Patch number of the current release: <Major>.<Minor>.<Patch>
     */
    static std::string getSemverPatch();

    /*!
     * \brief Returns the version of this library in the form <Major>.<Minor>.<Patch>
     * \return the version
     */
    static std::string getVersion();

    /*!
     * \brief checks if the passed version is compartible to this library
     * \return true if compartible
     */
    static bool isVersionSupported(std::string version);

    /*!
     * \brief get the parameter-file for this controller
     * \return path to the parameter-file
     */
    static const std::string getParamFile(std::string controllerName);

    /*!
     * \brief Use this method to propagate the input arguments to the SILECS library (-plcLog in particular)
     */
    void setArguments(std::string usrArgs);

    /// @cond
    // TODO: For python Diagnostic only, to be improved
    bool setLogTopics(std::string topics);
    /// @endcond

private:
    friend class PLC;
    friend class Block;
    friend class Register;
    friend class PLCRegister;
    friend class CNVRegister;

    Service(int argc, char ** argv);
    virtual ~Service();

    bool checkArgs(int argc, char ** argv);
    bool setArgs(int argc, char ** argv);
    std::string whichArgs();
    void printArgs();

    // returns folder of the binary which currently uses this library
    static const std::string getBinaryFolderPath();

    static bool withInputAccess(AccessType& accessType);
    static bool withOutputAccess(AccessType& accessType);
    static bool fileExists(std::string filename);

    /// Cluster collection of the Service
    clusterMapType clusterMap_;

    //unique instance of the Silecs service
    static Service* instance_;

    //Semantic versioning for SILECS packages (Tools, Environment and client library consistency)
    static const std::string semverMajor_; //Major release, not backward compatible
    static const std::string semverMinor_; //Minor release, backward compatible
    static const std::string semverPatch_; //Bug fixes, backward compatible
    static const std::string developmentVersion_; // Version which is used if not released

    static const std::string paramFilesPath_; // Location of all parameter-files (CERN-only)
    static const std::string paramFilesPathFallback_; // Fallback-Location of all parameter-files (CERN-only)

    static bool isShutingDown_;
};

} // namespace

#endif // _SILECS_SERVICE_H_
