/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED
#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/core/PLCAction.h>
#include <silecs-communication/interface/core/CNVRecvAction.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/core/Context.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include "../communication/CNVConnection.h.obsolete"
#include "../equipment/CNVBlock.h.obsolete"

namespace Silecs
{

    CNVRecvBlockMode::CNVRecvBlockMode(Block* block) : PLCAction(block)
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVRecvBlockMode (create): " << theBlock_->getName();
    }

    CNVRecvBlockMode::~CNVRecvBlockMode()
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVRecvBlockMode (delete): " << theBlock_->getName();
    }

    CNVRecvDeviceMode::CNVRecvDeviceMode(Block* block) : PLCAction(block)
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVRecvDeviceMode (create): " << theBlock_->getName();
    }

    CNVRecvDeviceMode::~CNVRecvDeviceMode()
    {
        if (DEBUG & Log::topics_) LOG(ALLOC) << "Action CNVRecvDeviceMode (delete): " << theBlock_->getName();
    }

    int CNVRecvBlockMode::execute(Context* pContext)
    {
        //Block mode cannot be implemented with CNV
        return 0;
    }

    int CNVRecvDeviceMode::execute(Context* pContext)
    {
        timeval tod; //Time-of-day for register time-stampng

        CNVData buffer;

        int errorCode = 0;
        Connection* pConn = theBlock_->getPLC()->getConnection();

        //(re)connect the PLC if needed and (re)synchronize the retentive registers
        if (pConn->doOpen(theBlock_->getPLC()))
        {
            try
            {
                //if transaction is for one device only, pContext contains its reference
                if (pContext->isForOneDevice())
                {
                    //Device context: get block of one device only ====================================
                    Device* pDev = pContext->getDevice();

                    if (RECV & Log::topics_)
                    {   if (pConn->isEnabled())
                        {   Log(RECV).getLog() << "RecvAction (execute/ DeviceMode): " << theBlock_->getName() << ", device: " << pDev->getLabel();
                        }
                    }

                    { //critical section
                        Lock lock(bufferMux_);

                        // read data from the buffer subscriber
                        CNVBufferedSubscriber* handle = ((CNVInputBlock*)theBlock_)->getHandle(pDev->getLabel());
                        errorCode = ((CNVConnection*)pConn)->readData(theBlock_->getPLC(), handle,&buffer);

                        if (pConn->isConnected() && (errorCode == 0))
                        { //Data have just been received: get time-of-day to time-stamp the registers
                            gettimeofday(&tod, 0);

                            //Device context: Extract all the device registers from that block
                            pDev->importRegisters(theBlock_, &buffer, tod, pContext);
                        }
                    }

                    //Deleting Null Data object raised a CNV exception
                    if (errorCode != CNVEmptyDataError) errChk(CNVDisposeData(buffer));

                    LOG_DELAY(RECV) << "done: " << theBlock_->getName();
                }
                else
                {
                    //Cluster/PLC context: get block of all devices one by one =========================
                    deviceVectorType::iterator pDeviceIter;
                    deviceVectorType& deviceCol = theBlock_->getPLC()->getDeviceMap();
                    for(pDeviceIter = deviceCol.begin(); pDeviceIter != deviceCol.end(); ++pDeviceIter)
                    {
                        Device* pDev = pDeviceIter->second;

                        if (RECV & Log::topics_)
                        {   if (pConn->isEnabled())
                            {   Log(RECV).getLog() << "RecvAction (execute/ DeviceMode): " << theBlock_->getName() << ", device: " << pDev->getLabel();
                            }
                        }

                        { //critical section
                            Lock lock(bufferMux_);
                            // read data from the buffer subscriber
                            CNVBufferedSubscriber* handle = ((CNVInputBlock*)theBlock_)->getHandle(pDev->getLabel());
                            errorCode = ((CNVConnection*)pConn)->readData(theBlock_->getPLC(), handle,&buffer);

                            if (pConn->isConnected() && (errorCode == 0))
                            { //Data have just been received: get time-of-day to time-stamp the registers
                                gettimeofday(&tod, 0);

                                //PLC context: Extract all registers value of the current device of the PLC from that block
                                pDev->importRegisters(theBlock_, &buffer, tod, pContext);
                            }
                        }

                        //Deleting Null Data object raised a CNV exception
                        if (errorCode != CNVEmptyDataError) errChk(CNVDisposeData(buffer));

                        LOG_DELAY(RECV) << "done: " << theBlock_->getName();
                    }
                }
            }
            catch (const SilecsException& ex)
            {
                errorCode = ex.getCode();
            }
        } //doOpen

        return errorCode;
    }

    void CNVRecvBlockMode::errChk(int code)
    {
        if(code != 0)
        {
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, std::string(CNVGetErrorDescription(code))+" in CNVRecvBlockMode");
        }
    }

    void CNVRecvDeviceMode::errChk(int code)
    {
        if(code != 0)
        {
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, std::string(CNVGetErrorDescription(code))+" in CNVRecvDeviceMode");
        }
    }

} // namespace
#endif //NI_SUPPORT_ENABLED
