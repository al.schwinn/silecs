/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED

#ifndef _RECV_CNV_ACTION_H_
#define _RECV_CNV_ACTION_H_

#include <silecs-communication/interface/core/PLCAction.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include <nilibnetv.h>

#ifdef __cplusplus
}
#endif

namespace Silecs
{
    class Action;
    class Block;
    class Context;

    /*!
     * \class CNVRecvBlockMode
     * \brief Concrete action responsible to get a data block from the CNV (Block mode configuration)
     */
    class CNVRecvBlockMode : public PLCAction
    {

    public:
        CNVRecvBlockMode(Block* block);
        virtual ~CNVRecvBlockMode();

        /*!
         * \fn execute
         * Connect the CNV if necessary and Get data-block from its memory.
         * Overwrite the abstract method
         * \return 0 if no error occurred. else return a error status from low level library
         */
        int execute(Context* pContext);

    private:

        /* generate a string exception out of a CNV error code*/
        void errChk(int code);

    };

    /*!
     * \class CNVRecvDeviceMode
     * \brief Concrete action responsible to get a data block from the CNV (Device mode configuration)
     */
    class CNVRecvDeviceMode : public PLCAction
    {

    public:
        CNVRecvDeviceMode(Block* block);
        virtual ~CNVRecvDeviceMode();

        /*!
         * \fn execute
         * Connect the CNV if necessary and Get data-block from its memory.
         * Overwrite the abstract method
         * \return 0 if no error occurred. else return a error status from low level library
         */
        int execute(Context* pContext);

    private:

        /* generate a string exception out of a CNV error code*/
        void errChk(int code);

    };

} // namespace

#endif // _RECV_CNV_ACTION_H_

#endif //NI_SUPPORT_ENABLED
