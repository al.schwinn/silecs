/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifdef NI_SUPPORT_ENABLED
#include <silecs-communication/interface/communication/CNVConnection.h>

#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/CNVBlock.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>

namespace Silecs
{

CNVConnection::CNVConnection(PLC* thePLC) :
                Connection(thePLC)
{
    LOG(ALLOC) << "CNVConnection (create): " << thePLC->getName();
}

CNVConnection::~CNVConnection()
{
    //Close the connection before removing resources
    //disable(); must be done before removing resource
}

/* Check for errors */
int CNVConnection::errChk(PLC* thePLC, int code) // throw (std::string*)
{
    if (code != 0)
    {
        LOG(DEBUG) << "CNV error occurred: " << code << ": " << CNVGetErrorDescription(code);
        //Do not close the connection if it's only for data missing (when deleting empty Data object)
        if (code != CNVInvalidDataHandleError)
        {
            LOG(COMM) << "Transaction failure with PXI: " << thePLC->getName() << ". CNV[" << code << "]: " << CNVGetErrorDescription(code);
            doClose(thePLC, /*withLock =*/true);
            throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, std::string(CNVGetErrorDescription(code)) + " in CNVConnection");
        }
    }
    return code;
}

/* Read Data */
int CNVConnection::readData(PLC* thePLC, CNVBufferedSubscriber *handle, CNVData *data)
{
    CNVBufferDataStatus status;

    /* Buffer is used only for performance reason.
     * Always return the last value in the buffer discarding the oldest ones.
     */

    //read the oldest data in the buffer
    int errorCode = errChk(thePLC, CNVGetDataFromBuffer(*handle, data, &status));

    if ( (errorCode == 0) && (status != CNVStaleData))
    {
        if (status != CNVNoData)
        { //buffer was not empty, try to read again to get the very last data if any
            errChk(thePLC, CNVDisposeData(*data)); //CNV requires to free the buffer on every CNVGetDataFromBuffer
            errorCode = errChk(thePLC, CNVGetDataFromBuffer(*handle, data, &status));
        }
        else
        {
            LOG(DEBUG) << "CNVConnection::readData() : buffer is empty (variables may not be initialized properly)";
            errorCode = CNVEmptyDataError;
        }
    }
    return errorCode;
}

/* Write Data */
int CNVConnection::writeData(PLC* thePLC, const char *networkVariablePathname, CNVData data)
{
    CNVWriter writer;

    int errorCode = errChk(thePLC, CNVCreateWriter(networkVariablePathname, NULL, NULL, CNVWaitForever, 0, &writer));

    if (errorCode == 0)
    {
        CNVWrite(writer, data, 0);
        CNVDispose(writer);
    }
    return errorCode;
}

int CNVConnection::readMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    return readData(thePLC, address, offset, size, pBuffer);
}
int CNVConnection::writeMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    return writeData(thePLC, address, offset, size, pBuffer);
}
int CNVConnection::readAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::string errorMsg("Read IO not supported for CNV");
    LOG(ERROR) << errorMsg;
    throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, errorMsg);
    return 0;
}

int CNVConnection::writeAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::string errorMsg("Write IO not supported for CNV");
    LOG(ERROR) << errorMsg;
    throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, errorMsg);
    return 0;
}

int CNVConnection::readDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::string errorMsg("Read IO not supported for CNV");
    LOG(ERROR) << errorMsg;
    throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, errorMsg);
    return 0;
}

int CNVConnection::writeDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    std::string errorMsg("Write IO not supported for CNV");
    LOG(ERROR) << errorMsg;
    throw SilecsException(__FILE__, __LINE__, CNV_INTERNAL_ERROR, errorMsg);
    return 0;
}

bool CNVConnection::open(PLC* thePLC)
{
    //Controller is reachable at this stage (ping done from doOpen)
    bool isOK = true; //all subscription is fine a priori

    LOG(DEBUG) << "CNVConnection::open(): Subscribe to all input variables";

    // Create all the subscriptions
    blockMapType::iterator pBlockIter;
    for (pBlockIter = thePLC->getBlockMap().begin(); pBlockIter != thePLC->getBlockMap().end(); ++pBlockIter)
    {
        //Attention: only CNVInputBlock has doSubscribe/unSubscribe feature (key-map can be used to select appropriate object)
        if (pBlockIter->first.find(Block::whichAccessType(Input)) != std::string::npos)
        {
            if (static_cast<CNVInputBlock*>(pBlockIter->second)->doSubscribe() == false)
            {
                isOK = false;
                break;
            }
        }
    }
    return isOK;
}

bool CNVConnection::close(PLC* thePLC)
{
    LOG(DEBUG) << "CNVConnection::close(): unSubscribe all input variables";

    // Delete all the subscription
    blockMapType::iterator pBlockIter;
    for (pBlockIter = thePLC->getBlockMap().begin(); pBlockIter != thePLC->getBlockMap().end(); ++pBlockIter)
    {
        //Attention: only CNVInputBlock has doSubscribe/unSubscribe feature (key-map can be used to select appropriate object)
        if (pBlockIter->first.find(Block::whichAccessType(Input)) != std::string::npos)
        {
            static_cast<CNVInputBlock*>(pBlockIter->second)->unSubscribe();
        }
    }
    isConnected_ = false;
    return true;
}

} // namespace


#endif //NI_SUPPORT_ENABLED
