/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/communication/SNAP7Connection.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/Mutex.h>

namespace Silecs
{

SNAP7Connection::SNAP7Connection(PLC* thePLC) :
                Connection(thePLC)
{
    rackNb_ = 0; //rackNb - common rack is 0 so far
    slotNb_ = -1; //slotNb - depends on hardware configuration (scan is required the first connect)

    recvClient_ = Cli_Create();
    sendClient_ = Cli_Create();

    LOG(ALLOC) << "SNAP7Connection (create) PLC/Cluster: " << thePLC->getName() << "/" << thePLC->theCluster_->getClassName();
}

SNAP7Connection::~SNAP7Connection()
{
    Cli_Destroy(&recvClient_);
    Cli_Destroy(&sendClient_);
}

bool SNAP7Connection::open(PLC* thePLC)
{
    int err = 0;
    bool ret = false;
    int slotNb;

    //Slot number is not required, so try to connect on different slots (1..31), depending on hardware layout.
    int *slotsScan;
    int slotsS7400[4] = {3, 2, 4, 1}; //slot scan - most common layout S7-400
    int slotsS71x00[4] = {1, 2, 3, 4}; //slot scan - most common layout S7-1200, S7-1500
    int slotsDefault[4] = {2, 3, 4, 1}; //slot scan - most common layout S7-other (S7-300, ET200S)
    int nbMaxSlot = ( (slotNb_ == -1) ? 4 : 1); //slots scan only the first time (slotNb_ == -1)

    switch (thePLC->getModelID())
    {
        case S7400:
            slotsScan = slotsS7400;
            break;
        case S71200:
        case S71500:
            slotsScan = slotsS71x00;
            break;
        default:
            slotsScan = slotsDefault; //S7-300, ET200S
    }

    for (int i = 0; i < nbMaxSlot; i++)
    {
        slotNb = slotsScan[i];

        err = ( (slotNb_ == -1) ? Cli_ConnectTo(recvClient_, thePLC->getIPAddress().c_str(), rackNb_, slotNb) : Cli_Connect(recvClient_));

        if (err == 0)
        {
            LOG(DEBUG) << "SNAP7 connect (channel-1) successful on PLC/rack/slot: " << thePLC->getName() << "/" << rackNb_ << "/" << slotNb;

            //We managed to open the first channel, just open the second one on the same slot.
            err = ( (slotNb_ == -1) ? Cli_ConnectTo(sendClient_, thePLC->getIPAddress().c_str(), rackNb_, slotNb) : Cli_Connect(sendClient_));

            if (err != 0)
            {
                LOG(DEBUG) << "SNAP7 connection (channel-2) failed on PLC/rack/slot: " << thePLC->getName() << "/" << rackNb_ << "/" << slotNb << ". SNAP7[" << err << "]: " << getErrorMessage(err);
                continue;
            }

            LOG(DEBUG) << "SNAP7 connect (channel-2) successful on PLC/rack/slot: " << thePLC->getName() << "/" << rackNb_ << "/" << slotNb;

            slotNb_ = slotNb; //connection is ok we can store the valid slot number for the next (re)connection (will be faster).
            ret = true;
            break;
        }
        LOG(DEBUG) << "SNAP7 connection (channel-1) failed on PLC/rack/slot: " << thePLC->getName() << "/" << rackNb_ << "/" << slotNb << ". SNAP7[" << err << "]: " << getErrorMessage(err);
    }
    return (ret);
}

bool SNAP7Connection::close(PLC* thePLC)
{
    bool ret = (Cli_Disconnect(recvClient_) == 0) && (Cli_Disconnect(sendClient_) == 0);
    return ret;
}

std::string SNAP7Connection::getErrorMessage(int err)
{
    char text[TextLen];
    Cli_ErrorText(err, text, TextLen);
    return std::string(text);
}

//-------------------------------------------------------------------------------------------------------------------
int SNAP7Connection::readUnitCode(PLC* thePLC, UnitCodeType& dataStruct)
{
    int err;
    TS7OrderCode S7data;

    if ( (err = Cli_GetOrderCode(recvClient_, &S7data)) == 0)
    {
        std::ostringstream os;
        os << S7data.Code;
        dataStruct.code = os.str();
        os.str().clear();
        os << (int)S7data.V1 << "." << (int)S7data.V2 << "." << (int)S7data.V3;
        dataStruct.version = os.str();
        LOG(DEBUG) << "Unit order-code received: " << dataStruct.code << ", " << dataStruct.version;
        return 0;
    }
    checkError(thePLC, err, false);
    return err;
}

int SNAP7Connection::readUnitStatus(PLC* thePLC, UnitStatusType& dataStruct)
{
    int err;
    int status;

    if ( (err = Cli_GetPlcStatus(recvClient_, &status)) == 0)
    {
        dataStruct.status = status;
        LOG(DEBUG) << "Unit status received: " << (int)dataStruct.status;
        return 0;
    }
    checkError(thePLC, err, false);
    return err;
}

int SNAP7Connection::readCPUInfo(PLC* thePLC, CPUInfoType& dataStruct)
{
    int err;
    TS7CpuInfo S7data;

    if ( (err = Cli_GetCpuInfo(recvClient_, &S7data)) == 0)
    {
        dataStruct.moduleName = std::string(S7data.ModuleName);
        dataStruct.moduleTypeName = std::string(S7data.ModuleTypeName);
        dataStruct.serialNumber = std::string(S7data.SerialNumber);
        dataStruct.asName = std::string(S7data.ASName);
        dataStruct.copyright = std::string(S7data.Copyright);
        LOG(DEBUG) << "CPU info received: " << dataStruct.moduleName << ", " << dataStruct.moduleTypeName << ", " << dataStruct.serialNumber << ", " << dataStruct.asName << ", " << dataStruct.copyright;
        return 0;
    }
    checkError(thePLC, err, false);
    return err;
}

int SNAP7Connection::readCPInfo(PLC* thePLC, CPInfoType& dataStruct)
{
    int err;
    TS7CpInfo S7data;

    if ( (err = Cli_GetCpInfo(recvClient_, &S7data)) == 0)
    {
        dataStruct.maxPduLength = S7data.MaxPduLengt;
        dataStruct.maxConnections = S7data.MaxConnections;
        dataStruct.maxMPIRate = S7data.MaxMpiRate;
        dataStruct.maxBusRate = S7data.MaxBusRate;
        LOG(DEBUG) << "CP info received: " << dataStruct.maxPduLength << ", " << dataStruct.maxConnections << ", " << dataStruct.maxMPIRate << ", " << dataStruct.maxBusRate;
        return 0;
    }
    checkError(thePLC, err, false);
    return err;
}

bool SNAP7Connection::isRunning(PLC* thePLC)
{
    UnitStatusType statusStruct;
    this->readUnitStatus(thePLC, statusStruct);
    switch (statusStruct.status)
    {
        case S7CpuStatusRun:
            return true;
        case S7CpuStatusStop:
            return false;
        default:
            throw SilecsException(__FILE__, __LINE__, UNKNOWN_ERROR, std::string("PLC Status is: UNKNOWN"));
    }
}
int SNAP7Connection::readMemory(PLC* thePLC, long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    int err = 1;

    if (DBn < 0)
    {
        LOG(COMM) << "Invalid DB number: " << DBn;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(DBn));
    }

    //(re)connect the PLC if needed and (re)synchronize the retentive registers
    if (doOpen(thePLC))
    {
        //connection is established then acquire data
        Lock lock(readMux_);
        //DATA topic makes sense with RECV one
        if (RECV & Log::topics_)
            LOG(DATA) << "Read memory data, DBn: " << DBn << ", ofs: " << offset << ", byte-size: " << size;

        err = Cli_DBRead(recvClient_, (int)DBn, (int)offset, (int)size, (void *)pBuffer);
        checkError(thePLC, err, false); // close the connection, will try again at the next access
    }
    return err;
}
int SNAP7Connection::writeMemory(PLC* thePLC, long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    int err = 1;

    if (DBn < 0)
    {
        LOG(COMM) << "Invalid DB number: " << DBn;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(DBn));
    }

    //(re)connect the PLC if needed and (re)synchronize the retentive registers
    if (doOpen(thePLC))
    {
        //connection is established then send data
        Lock lock(writeMux_);
        //DATA topic makes sense with SEND one
        if (SEND & Log::topics_)
            LOG(DATA) << "Write memory data, DBn: " << DBn << ", ofs: " << offset << ", byte-size: " << size;

        err = Cli_DBWrite(sendClient_, (int)DBn, (int)offset, (int)size, (void *)pBuffer);
        checkError(thePLC, err, false); // close the connection, will try again at the next access
    }
    return err;
}

int SNAP7Connection::readIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    int err = 1;

    if (address < 0)
    {
        LOG(COMM) << "Invalid address: " << address;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(address));
    }
    //(re)connect the PLC if needed and (re)synchronize the retentive registers
    if (doOpen(thePLC))
    {
        //connection is established then acquire data
        Lock lock(readMux_);
        //DATA topic makes sense with RECV one
        if (RECV & Log::topics_)
            LOG(DATA) << "Read IO data, address: " << address << ", ofs: " << offset << ", byte-size: " << size;

        address += offset;
        err = Cli_EBRead(recvClient_, (int)address, (int)size, (void *)pBuffer);
        checkError(thePLC, err, false); // close the connection, will try again at the next access
    }
    return err;
}

int SNAP7Connection::writeIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    int err = 1;

    if (address < 0)
    {
        LOG(COMM) << "Invalid address: " << address;
        throw SilecsException(__FILE__, __LINE__, PARAM_INCORRECT_BLOCK_ADDRESS, StringUtilities::toString(address));
    }
    //(re)connect the PLC if needed and (re)synchronize the retentive registers
    if (doOpen(thePLC))
    {
        //connection is established then send data
        Lock lock(writeMux_);
        //DATA topic makes sense with SEND one
        if (SEND & Log::topics_)
            LOG(DATA) << "Write IO data, address: " << address << ", ofs: " << offset << ", byte-size: " << size;

        address += offset;
        err = Cli_ABWrite(sendClient_, (int)address, (int)size, (void *)pBuffer);
        checkError(thePLC, err, false); // close the connection, will try again at the next access
    }
    return err;
}

int SNAP7Connection::readAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    return readIO(thePLC, address, offset, size, pBuffer);
}
int SNAP7Connection::writeAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    return writeIO(thePLC, address, offset, size, pBuffer);
}
int SNAP7Connection::readDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    return readIO(thePLC, address, offset, size, pBuffer);
}
int SNAP7Connection::writeDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer)
{
    return writeIO(thePLC, address, offset, size, pBuffer);
}

//-------------------------------------------------------------------------------------------------------------------
bool SNAP7Connection::checkError(PLC* thePLC, int err, bool retry)
{
    if (err != 0)
    {
        LOG(COMM) << "Transaction failure with the controller: " << thePLC->getName() << ". SNAP7[" << err << "]: " << getErrorMessage(err);

        if (retry)
        {
            LOG(COMM) << "Try to reconnect the controller: " << thePLC->getName();
            if (reOpen(thePLC))
            { // can repeat the request after the connection was successfully reopened
                return true;
            }
            // reconnection has failed again, just close the connection
            LOG(COMM) << "Unable to reconnect the controller: " << thePLC->getName();
        }
        // no retry, we just want to close (use default case)
        doClose(thePLC, /*withLock =*/true);
    }
    return false;
}

int SNAP7Connection::coldRestart(PLC* thePLC)
{
    if (doOpen(thePLC))
    {
        Lock lock(writeMux_);
        int error = Cli_PlcColdStart(sendClient_);
        if (error)
            throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, " SNAP7 Error: " + getErrorMessage(error));

    }
    return 0;
}

int SNAP7Connection::plcStop(PLC* thePLC)
{
    if (doOpen(thePLC))
    {
        Lock lock(writeMux_);
        int error = Cli_PlcStop(sendClient_);
        if (error)
            throw SilecsException(__FILE__, __LINE__, UNEXPECTED_ERROR, " SNAP7 Error: " + getErrorMessage(error));

    }
    return 0;
}

} // namespace
