/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifdef NI_SUPPORT_ENABLED
#ifndef _CNV_CONNECTION_H_
#define _CNV_CONNECTION_H_

#include <silecs-communication/interface/communication/SilecsConnection.h>

#include <iostream>
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include <nilibnetv.h>

#ifdef __cplusplus
}
#endif

namespace Silecs
{
/*!
 * \class CNVConnection
 * \brief CNV-communication object for PXI Shared Variable Protocol
 */
class CNVConnection : public Connection
{

public:
    CNVConnection(PLC* thePLC);
    virtual ~CNVConnection();

    /*!
     * \brief Read Data from the specified buffer handler
     * \return the read CNVData and error code (0 = everything is ok)
     */
    //CNVData readData(CNVBufferedSubscriber *handle);
    int readData(PLC* thePLC, CNVBufferedSubscriber *handle, CNVData *data);

    /*!
     * \brief Write the specified variable, data must be already in CNVData format.
     * In case of error raises a std::string* exception containing the error message
     * \return error code (0 = everything is ok)
     */
    int writeData(PLC* thePLC, const char *networkVariablePathname, CNVData data);

    /*!
     * \brief Subscribes for changes on the the specified variable.
     * In case of error raises a std::string* exception containing the error message
     */
    void monitorData(const char *networkVariablePathname);

    // not implemented. here because of virtual in super class
    // TODO: review in order to get read of it
    int readData(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer)
    {
        return -1;
    }
    ;

    // not implemented. here because of virtual in super class
    // TODO: review in order to get read of it
    int writeData(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* buffer)
    {
        return -1;
    }
    ;
    int readMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);

private:
    // Subscriber
    CNVSubscriber subscriber;

    /*!
     * \brief Print the error message related to the code and throw exception if needed
     * \return the error code itself (0 everything is ok)
     */
    int errChk(PLC* thePLC, int code);

    /*!
     * \brief Open the connection
     * It actually return a boolean which indicates if the server responded to a ping operation
     */
    bool open(PLC* thePLC);

    /*!
     * \brief Close the connection with the server
     */
    bool close(PLC* thePLC);

    bool checkError(PLC* thePLC, int err, bool retry)
    {
        return false;
    }

};

} // namespace

#endif // _CNV_CONNECTION_H_

#endif //NI_SUPPORT_ENABLED
