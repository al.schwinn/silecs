/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifdef MODBUS_SUPPORT_ENABLED
#include "MBHardware.h"

/*----------------------------------------------------------*/
/* Time funtion
 * IeMdbSetTime: Convert lynxos time_t format to PLC SCHNEIDER
 * format.
 *
 * Details:
 * PLC SCHNEIER format is coded on 8 bytes BCD
 * format:
 *
 *  0      1      2      3      4      5     6     7
 *  SC     00     HH     MN     MM     DD    YY    YY
 *
 *  byte 1 is used by gateway to synchronize time setting.
 */
void IeMdbSetTime(unsigned char *dt, time_t epoch) // to PLC
{
    struct tm *dscst;
    dscst = localtime(&epoch);

    /*v1.7*/
    /*In fact, each 16bits word is swapped with SCHNEIDER PLC
     1      0      3      2      5      4     7     6
     SC     00     HH     MN     MM     DD    YY    YY

     Swapping can do it directly during data transferring
     */

    dt[1] = static_cast<char>(_tobcd(dscst->tm_sec));
    dt[0] = 0x00; /*not used*/
    dt[3] = static_cast<char>(_tobcd(dscst->tm_hour));
    dt[2] = static_cast<char>(_tobcd(dscst->tm_min));
    dt[5] = static_cast<char>(_tobcd(dscst->tm_mon+1));
    dt[4] = static_cast<char>(_tobcd(dscst->tm_mday));
    dt[7] = static_cast<char>(_tobcd(2000/100));
    dt[6] = static_cast<char>(_tobcd(dscst->tm_year-100));
}

/*----------------------------------------------------------*/
/* Time funtion
 * IeMdbGetTime: Convert PLC SCHNEIDER format to time_t format.
 *
 * Details:
 * PLC SCHNEIER format is coded on 8 bytes BCD
 * format:
 *
 *  0      1        2      3      4      5     6     7
 *  SC     SC/100   HH     MN     MM     DD    YY    YY
 *
 */
double IeMdbGetTime(unsigned char *dt) // from PLC
{
    struct tm plcst;
    time_t plctm;
    double ms;
    int year;

    /*In fact, each 16bits word is swapped with SCHNEIDER PLC
     1      0        3      2      5      4     7     6
     SC     SC/100   HH     MN     MM     DD    YY    YY

     Swapping can do it directly during data transferring
     */

    plcst.tm_sec = _frombcd(dt[1]);
    plcst.tm_hour = _frombcd(dt[3]);
    plcst.tm_min = _frombcd(dt[2]);
    plcst.tm_mon = _frombcd(dt[5])-1;
    plcst.tm_mday = _frombcd(dt[4]);
    year = _frombcd(dt[6]);
    //look at Schneider DATA_AND_TIME type documentation
    plcst.tm_year = ((year >= 90) ? year : year+100);
    /*plcst.tm_wday = no used in PLC*/
    /*plcst.tm_yday = no used in PLC*/

    ms = ((double)_frombcd(dt[0])/100.);
    plcst.tm_isdst = -1; // daylight saving time unavailable
    plctm = mktime(&plcst);

    return((double)plctm + ms);
}

#endif //MODBUS_SUPPORT_ENABLED
