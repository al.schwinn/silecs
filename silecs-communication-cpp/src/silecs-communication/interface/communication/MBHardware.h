/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifdef MODBUS_SUPPORT_ENABLED
#ifndef _MBHARDWARE_H_
#define _MBHARDWARE_H_

#include <silecs-communication/interface/communication/ietype.h>
#include <modbus/modbus.h>

/*----------------------------------------------------------*/
/* Time funtion
 * IeMdbSetTime: Convert time_t epoch date to PLC SCHNEIDER
 * format.
 *
 * Details:
 * PLC SCHNEIER format is coded on 8 bytes BCD
 * format:
 *
 *  0      1      2      3      4      5     6     7
 *  SC     --     HH     MN     MM     DD    YY    YY
 *
 *  byte 1 is used by gateway to synchronize time setting.
 */
void IeMdbSetTime(unsigned char *dt, time_t epoch); // to PLC

/*----------------------------------------------------------*/
/* Time funtion
 * IeMdbGetTime: Convert PLC SCHNEIDER format to lynxOS
 * time_t format.
 *
 * Details:
 * PLC SCHNEIER format is coded on 8 bytes BCD
 * format:
 *
 *  0      1        2      3      4      5     6     7
 *  SC     SC/100   HH     MN     MM     DD    YY    YY
 *
 */
double IeMdbGetTime(unsigned char *dt); // from PLC

#endif /* _MBHARDWARE_H_ */
#endif //MODBUS_SUPPORT_ENABLED
