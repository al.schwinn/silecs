/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifdef MODBUS_SUPPORT_ENABLED
#ifndef _MB_CONNECTION_H_
#define _MB_CONNECTION_H_

#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <modbus/modbus.h>

namespace Silecs
{
class PLC;
class Connection;

/*!
 * \class MBConnection
 * \brief Plc-communication object for specific Modbus protocol
 */
class MBConnection : public Connection
{

public:
    MBConnection(PLC* thePLC);
    virtual ~MBConnection();

    int readMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeMemory(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);

private:
    modbus_t* readCtx_;
    modbus_t* writeCtx_;
    bool open(PLC* thePLC);
    bool close(PLC* thePLC);

    // This set of functions are used to address the analog and memory registers
    int readRegisters(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer, bool isIO);
    int writeRegisters(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    // read and write Frames interfere directly with the libmodbus API
    int readFrames(modbus_t* ctx, long dataAddr, uint16_t dataSize, uint8_t* dataBuffer, bool isIO);
    int writeFrames(modbus_t* ctx, long dataAddr, uint16_t dataSize, uint8_t* dataBuffer);

    // This set of functions are used to address the digital registers
    int readBits(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeBits(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    // read and write Coils interfere directly with the libmodbus API
    int readCoils(modbus_t* ctx, long dataAddr, uint16_t dataSize, uint8_t* dataBuffer);
    int writeCoils(modbus_t* ctx, long dataAddr, uint16_t dataSize, uint8_t* dataBuffer);

    bool checkError(PLC* thePLC, int err, bool retry);
};

} // namespace

#endif // _MB_CONNECTION_H_

#endif //MODBUS_SUPPORT_ENABLED
