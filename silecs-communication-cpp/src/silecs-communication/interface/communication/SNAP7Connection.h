/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SNAP7_CONNECTION_H_
#define _SNAP7_CONNECTION_H_

#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <snap7.h>

namespace Silecs
{
class PLC;

/*!
 * \class SNAP7Connection
 * \brief Plc-communication object for specific SNAP7 protocol
 */
class SNAP7Connection : public Connection
{

public:
    SNAP7Connection(PLC* thePLC);
    virtual ~SNAP7Connection();

    int readUnitCode(PLC* thePLC, UnitCodeType& dataStruct);
    int readUnitStatus(PLC* thePLC, UnitStatusType& dataStruct);
    int readCPUInfo(PLC* thePLC, CPUInfoType& dataStruct);
    int readCPInfo(PLC* thePLC, CPInfoType& dataStruct);

    bool isRunning(PLC* thePLC);

    int readData(PLC* thePLC, long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeData(PLC* thePLC, long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readMemory(PLC* thePLC, long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeMemory(PLC* thePLC, long DBn, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeAIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int readDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeDIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);

    //Extension Silecs methods
    int coldRestart(PLC* thePLC);
    int plcStop(PLC* thePLC);

private:
    S7Object recvClient_;
    S7Object sendClient_;
    bool open(PLC* thePLC);
    bool close(PLC* thePLC);
    int readIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);
    int writeIO(PLC* thePLC, long address, unsigned long offset, unsigned long size, unsigned char* pBuffer);

    int rackNb_; //rackNb - common rack is 0 by default
    int slotNb_; //slotNb - depends on hardware configuration (scan is required the first connect)

    std::string getErrorMessage(int err);
    bool checkError(PLC* thePLC, int err, bool retry);
};

} // namespace

#endif // _SNAP7_CONNECTION_H_

