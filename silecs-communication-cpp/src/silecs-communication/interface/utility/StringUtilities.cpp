/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include "StringUtilities.h"
#include <stdio.h>

namespace Silecs
{
const unsigned int StringUtilities::c_emptyStringArraySize = 1;
const char StringUtilities::c_emptyString[] = "\0";
const char* StringUtilities::c_emptyStringArray[1] = {"\0"};

void StringUtilities::tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters)
{

    size_t startPos = 0;
    size_t endPos = str.find_first_of(delimiters, 0);
    if (endPos == std::string::npos)
    {
        //no delimiters
        tokens.push_back(str);
        return;
    }
    size_t maxLenght = str.size();
    while (startPos != std::string::npos && endPos != std::string::npos)
    {
        // Found a token, add it to the vector.
        std::string s = str.substr(startPos, endPos - startPos);
        tokens.push_back(s);
        // Next start delimiter
        if (endPos < maxLenght - 1)
            startPos = endPos + 1;
        else
            // string size exceeded
            break;
        endPos = str.find_first_of(delimiters, startPos);
        if (endPos == std::string::npos)
        {
            // no delimiter at the end : last token
            std::string s = str.substr(startPos, maxLenght - startPos);
            tokens.push_back(s);
        }
    }
}

void StringUtilities::trimWhiteSpace(std::string& str)
{
    size_t pos = str.find(" ");
    while (pos != std::string::npos)
    {
        str.erase(pos, 1);
        pos = str.find(" ");
    }
}

std::string StringUtilities::toString(unsigned int data)
{
    char tmp[32];
    sprintf(tmp, "%u", data);
    return tmp;
}

std::string StringUtilities::toString(int data)
{
    char tmp[32];
    sprintf(tmp, "%d", data);
    return tmp;
}

std::string StringUtilities::toString(long data)
{
    char tmp[32];
    sprintf(tmp, "%ld", data);
    return tmp;
}

std::string StringUtilities::toString(unsigned long data)
{
    char tmp[32];
    sprintf(tmp, "%lu", data);
    return tmp;
}

std::string StringUtilities::toString(long long data)
{
    char tmp[32];
    sprintf(tmp, "%lld", data);
    return tmp;
}

std::string StringUtilities::toString(double data)
{
    char tmp[32];
    sprintf(tmp, "%f", data);
    return tmp;
}

std::string StringUtilities::toString(const void * ptr)
{
    char tmp[32];
    sprintf(tmp, "%p", ptr);
    return tmp;
}

void StringUtilities::toLower(std::string& str)
{ //cannot used std::transform() method because of ppc4 support!
    for (unsigned int i = 0; i < str.length(); i++)
    {
        str[i] = static_cast<char>(tolower(str[i]));
    }
}

} // namespace
