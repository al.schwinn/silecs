/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/utility/Mutex.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <string.h>

namespace Silecs
{

Mutex::Mutex(std::string name)
{
    int err;

    // Mutex name if any, used for logging in particular
    name_ = name;

    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Mutex (create): " << name_;

    //create the pthread-mutex using the default parameters + recursive option
    pMutex_ = new pthread_mutex_t();

    if ( (err = pthread_mutexattr_init(&attr_)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));

    //Mutex supports re-entrance for re-open mechanism inside send/recv action (doOpen() method call)
    if ( (err = pthread_mutexattr_settype(&attr_, PTHREAD_MUTEX_RECURSIVE)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));

    if ( (err = pthread_mutex_init(pMutex_, &attr_)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
}

Mutex::~Mutex()
{
    int err;

    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Mutex (delete): " << name_;

    if ( (err = pthread_mutexattr_destroy(&attr_)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));

    if ( (err = pthread_mutex_destroy(pMutex_)) != 0)
    { //can be broken by SIGKILL (just leave with no Exception)
      //throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
    }

    // patch by Stefano Magnoni (20-03-2012)
    // avoid memory leak when deallocating the mutex
    if (pMutex_ != NULL)
    {
        delete pMutex_;
        pMutex_ = NULL;
    }
}

void Mutex::lock()
{
    int err;
    LOG(LOCK) << "Waiting for lock: " << this->name_;
    if ( (err = pthread_mutex_lock(pMutex_)) != 0)
    { // lock can be broken by SIGKILL (just release it with no Exception)
        pthread_mutex_unlock(pMutex_);
        //throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
    }
    LOG(LOCK) << "Has locked: " << this->name_;
}

void Mutex::unlock()
{
    int err;
    if ( (err = pthread_mutex_unlock(pMutex_)) != 0)
    { // unlock can be broken by SIGKILL (just force release again but no Exception)
        pthread_mutex_unlock(pMutex_);
        //throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
    }
    LOG(LOCK) << "Has unlocked: " << this->name_;
}

}
