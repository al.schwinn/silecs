/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/utility/SilecsLog.h>
#include <string.h>
#include <silecs-communication/interface/utility/Condition.h>

namespace Silecs
{

Condition::Condition(std::string name)
{
    int err;

    // Condition name if any, used for logging in particular
    name_ = name;

    //create the related mutex using the same name
    pMutex_ = new Mutex(name_);

    if ( (err = pthread_cond_init(&condVar_, NULL)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
}

Condition::~Condition()
{
    int err;
    if ( (err = pthread_cond_destroy(&condVar_)) != 0)
    { //can be broken by SIGKILL (just leave with no Exception)
      //throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
    }
    delete pMutex_;
}

void Condition::lock()
{
    pMutex_->lock();
}

void Condition::unlock()
{
    pMutex_->unlock();
}

void Condition::signal()
{
    int err;
    if ( (err = pthread_cond_signal(&condVar_)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
    LOG(LOCK) << "Has signaled condition: " << this->name_;
}

void Condition::broadcast()
{
    int err;
    if ( (err = pthread_cond_broadcast(&condVar_)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
}

void Condition::wait()
{
    int err;
    LOG(LOCK) << "Waiting for condition: " << this->name_;
    if ( (err = pthread_cond_wait(&condVar_, pMutex_->pMutex_)) != 0)
        throw SilecsException(__FILE__, __LINE__, errno, strerror(errno));
    LOG(LOCK) << "Has get condition: " << this->name_;
}

} // end namespace
