/*
 *  Piece of code adapted for SILECS.
 *  Original version references:
 *
 *  Portable Agile C++ Classes (PACC)
 *  Copyright (C) 2004 by Marc Parizeau
 *  http://manitou.gel.ulaval.ca/~parizeau/PACC
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Contact:
 *  Laboratoire de Vision et Systemes Numeriques
 *  Departement de genie electrique et de genie informatique
 *  Universite Laval, Quebec, Canada, G1K 7P4
 *  http://vision.gel.ulaval.ca
 *
 */

#ifndef _TimeStamp_H_
#define _TimeStamp_H_

#include <unistd.h>
#ifdef __Lynx__
#include <time.h>
#else
#include <sys/time.h>
#endif

namespace Silecs
{

#define S_UNIT		1.0  //express delay in second
#define MS_UNIT		1.e3 //express delay in milli-second
#define YS_UNIT		1.e6 //express delay in micro-second
#define NS_UNIT		1.e9 //express delay in nano-second

class TsCounter
{

public:
    TsCounter(bool inHardware = true) :
                    mHardware(inHardware)
    {
        if (mPeriod == 0)
            calibrateCountPeriod();
        mValueCount = 0;
        reset();
    }

    void calibrateCountPeriod(unsigned int inDelay = 50/*ms*/, unsigned int inTimes = 10);
    static double getCountPeriod(void)
    {
        return mPeriod;
    }
    static void setCountPeriod(double period)
    {
        mPeriod = period;
    }
    void reset(void)
    {
        mValueCount = getCount();
        mDelayCount = 0;
    }

    unsigned long long getCount(void) const;
    double getCount(double unit) const
    {
        return (double)getCount() * mPeriod * unit;
    }

    double getValue(double unit)
    {
        unsigned long long count = getCount();
        mDelayCount = count;
        return (double) (count - mValueCount) * mPeriod * unit;
    }

    double getDelay(double unit)
    {
        return (double) (getCount() - mDelayCount) * mPeriod * unit;
    }

    double getTimeOfDay(double unit)
    {
        timeval lCurrent;
        ::gettimeofday(&lCurrent, 0);
        return ( (static_cast<double>(lCurrent.tv_sec) * YS_UNIT + static_cast<double>(lCurrent.tv_usec)) * (unit / YS_UNIT));
    }

protected:
    bool mHardware;
    unsigned long long mValueCount; //duration from start
    unsigned long long mDelayCount; //duration from previous getCount
    static double mPeriod;
};

} //namespace Silecs

#endif // _TimeStamp_H_
