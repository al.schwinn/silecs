/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _CONDITION_H_
#define _CONDITION_H_

#include <pthread.h>
#include <errno.h>

#include <silecs-communication/interface/utility/Mutex.h>

namespace Silecs
{

/*!
 * \class Condition
 * \brief This class implement the basic methods to manage the thread synchronization
 * with Conditional Variable. In particular, it manages the related mutex.
 */
class Condition
{
public:
    Condition(std::string name = "undefined");
    virtual ~Condition();

    void lock();
    void unlock();

    void signal();
    void broadcast();
    void wait();

private:
    // Condition name if any, used for logging in particular
    std::string name_;

    pthread_cond_t condVar_;
    Mutex* pMutex_;
};

} // namespace

#endif /* _CONDITION_H_ */
