/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _MUTEX_H_
#define _MUTEX_H_

#include <pthread.h>
#include <errno.h>
#include <string>
#include <map>

#include <silecs-communication/interface/utility/SilecsException.h>

namespace Silecs
{

class Condition;

class Mutex
{
public:
    Mutex(std::string name = "undefined");
    ~Mutex();

    void lock();
    void unlock();

private:
    friend class Condition;

    // Mutex name if any, used for logging in particular
    std::string name_;

    // Mutex object
    pthread_mutex_t* pMutex_;

    // Mutex Attributes
    pthread_mutexattr_t attr_;
};

// For automatic mutex-release on code-block exit ( e.g. if exceptions are used )
class Lock
{
public:

    Lock(Mutex* mutex) :
                    mutex_(mutex)
    {
        mutex_->lock();
    }

    Lock() :
                    mutex_(NULL)
    {
    }

    ~Lock()
    {
        if (mutex_ != NULL)
            mutex_->unlock();
    }

    void setMutex(Mutex* mutex)
    {
        mutex_ = mutex;
    }

protected:

    Mutex* mutex_;
};

} // namespace

#endif /* _MUTEX_H_ */
