/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/StringUtilities.h>

#include <cstring>
#include <fstream>

namespace
{
bool isInitialized_ = false;
}
namespace Silecs
{

XMLParser::XMLParser(const std::string& fileName, bool validateFile) :
                fileName_(fileName)
{
    if (!isInitialized_)
    {
        throw SilecsException(__FILE__, __LINE__, "Silecs XML-Parser needs to be initialized before usage!");
    }
    if (validateFile)
    {
        const std::ifstream file(fileName.c_str());
        if (file.fail())
        {
            std::ostringstream message;
            message << "The Parameter File " << fileName << " was not found";
            throw SilecsException(__FILE__, __LINE__, message.str().c_str());
        }
        // Check if file is valid
    }
}

XMLParser::~XMLParser()
{
}

void XMLParser::init()
{
    xmlInitParser();
    isInitialized_ = true;
}

void XMLParser::cleanUp()
{
    xmlCleanupParser();
}

void XMLParser::trimCarrierReturn(std::string& s) const
{
    size_t startPos = 0;
    while ( (startPos = s.find("\n", startPos)) != std::string::npos)
    {
        s.erase(startPos, 2);
    }
}

void XMLParser::fillElement(xmlNodePtr node, ElementXML& element) const
{
    xmlChar* value = xmlNodeListGetString(node->doc, node->children, 1);
    // If the node contains text, the carrier return is removed
    if (value)
    {
        std::string trimString(reinterpret_cast<const char*>(value));
        trimCarrierReturn(trimString);
        element.value_ = trimString.c_str();
        xmlFree(value);
        value = NULL;
    }
    element.name_ = reinterpret_cast<const char*>(node->name);
    // Loop over all the attributes of the XML element
    if (node->properties != NULL)
    {
        xmlAttrPtr curAttr;
        for (curAttr = node->properties; curAttr != NULL; curAttr = curAttr->next)
        {
            boost::shared_ptr<AttributeXML> attribute(new AttributeXML());
            attribute->name_ = reinterpret_cast<const char*>(curAttr->name);
            value = xmlGetProp(node, reinterpret_cast<const xmlChar*>(attribute->name_.c_str()));
            attribute->value_ = reinterpret_cast<const char*>(value);
            xmlFree(value);
            value = NULL;
            element.attributeList_.push_back(attribute);
        }
    }
    // Loop over all the child nodes of the XML element
    if (node->xmlChildrenNode != NULL)
    {
        xmlNodePtr curNode;
        for (curNode = node->xmlChildrenNode; curNode != NULL; curNode = curNode->next)
        {
            if ( (curNode->type == XML_ELEMENT_NODE))
            {
                boost::shared_ptr<ElementXML> child(new ElementXML());
                fillElement(curNode, *child);
                element.childList_.push_back(child);
            }
        }
    }
}

boost::optional<boost::ptr_vector<ElementXML> > XMLParser::getElementsFromXPath(const std::string& xpathExpression) const
{
    boost::optional<boost::ptr_vector<ElementXML> > elements;
    // Load the XML document
    const xmlDocPtr document = xmlParseFile(fileName_.c_str());
    if (document == NULL)
    {
        std::ostringstream message;
        message << "The Parameter File " << fileName_ << " could not be parsed";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }
    // Create an XPath evaluation context
    const xmlXPathContextPtr xpathContext = xmlXPathNewContext(document);
    if (xpathContext == NULL)
    {
        xmlFreeDoc(document);
        return elements;
    }
    // Evaluate the XPath expression
    const xmlXPathObjectPtr xpathObject = xmlXPathEvalExpression(reinterpret_cast<const xmlChar*>(xpathExpression.c_str()), xpathContext);
    if (xpathObject == NULL)
    {
        xmlXPathFreeContext(xpathContext);
        xmlFreeDoc(document);
        return elements;
    }
    // Loop over the node set returned by XPath
    if (xmlXPathNodeSetIsEmpty(xpathObject->nodesetval) == false)
    {
        for (int i = 0; i < xpathObject->nodesetval->nodeNr; ++i)
        {
            std::auto_ptr<ElementXML> element(new ElementXML());
            fillElement(xpathObject->nodesetval->nodeTab[i], *element);
            if (!elements)
            {
                elements = boost::ptr_vector<ElementXML>();
            }
            elements->push_back(element.release());
        }
    }
    // Cleanup
    xmlXPathFreeObject(xpathObject);
    xmlXPathFreeContext(xpathContext);
    xmlFreeDoc(document);
    return elements;
}

boost::ptr_vector<ElementXML> XMLParser::getElementsFromXPath_throwIfEmpty(const std::string& xpathExpression) const
{
    boost::optional<boost::ptr_vector<ElementXML> > result = getElementsFromXPath(xpathExpression);
    if (!result)
    {
        std::ostringstream message;
        message << "The xpathExpression: '" << xpathExpression << "' did not find a match";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }
    if (result.get().size() == 0)
    {
        std::ostringstream message;
        message << "The xpathExpression: '" << xpathExpression << "' returned an empty collection of xml-Elements";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }
    return result.get();
}

ElementXML XMLParser::getFirstElementFromXPath(const std::string& xpathExpression) const
{
    boost::optional<boost::ptr_vector<ElementXML> > result = getElementsFromXPath_throwIfEmpty(xpathExpression);
    return result.get()[0];
}

ElementXML XMLParser::getSingleElementFromXPath(const std::string& xpathExpression) const
{
    boost::optional<boost::ptr_vector<ElementXML> > result = getElementsFromXPath_throwIfEmpty(xpathExpression);
    if (result.get().size() > 1)
    {
        std::ostringstream message;
        message << "The xpathExpression: '" << xpathExpression << "' returns more than one XML-Elements";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }
    return result.get()[0];
}

xmlChar* XMLParser::ConvertInput(const char *in, const char *encoding)
{
    xmlChar *out;
    int32_t ret;
    int32_t size;
    int32_t out_size;
    int32_t temp;
    xmlCharEncodingHandlerPtr handler;

    if (in == 0)
        return 0;

    handler = xmlFindCharEncodingHandler(encoding);

    if (!handler)
    {
        std::ostringstream message;
        message << "The encoding: '" << encoding << "' was not found";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }

    size = (int32_t)std::strlen(in) + 1;
    out_size = size * 2 - 1;
    out = (unsigned char *)xmlMalloc((size_t)out_size);

    if (out == 0)
    {
        std::ostringstream message;
        message << "Failed to convert the xml to: '" << encoding << "'";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }

    temp = size - 1;
    ret = handler->input(out, &out_size, (const xmlChar *)in, &temp);
    if ( (ret < 0) || (temp - size + 1))
    {
        std::ostringstream message;
        message << "Failed to convert the xml to: '" << encoding << "'";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
        xmlFree(out);
        out = 0;
    }
    else
    {
        out = (unsigned char *)xmlRealloc(out, out_size + 1);
        out[out_size] = 0; /*null terminating out */
    }

    return out;
}

} // fesa
