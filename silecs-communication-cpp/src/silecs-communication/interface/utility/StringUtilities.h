/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _STRING_UTILITIES_H_
#define _STRING_UTILITIES_H_

#include <string>
#include <vector>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <sstream>

namespace Silecs
{

class StringUtilities
{
public:
    static void tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters = " ");

    static void trimWhiteSpace(std::string& str);

    // define an array of one pointer on an empty string
    static const unsigned int c_emptyStringArraySize;
    static const char* c_emptyStringArray[1];
    // define an empty string
    static const char c_emptyString[];

    static std::string toString(unsigned int data);
    static std::string toString(int data);
    static std::string toString(unsigned long data);
    static std::string toString(long data);
    static std::string toString(long long data);
    static std::string toString(double data);
    static std::string toString(const void * ptr);
    static void toLower(std::string& str);

    // TODO: Merge both fromString methods ( std::ios_base argument is anyhwo always the same )
    /*!
     * \brief transform a string into any integer-type
     * \param str the string to transform
     * \param value that will receive the conversion
     */
    template <typename T> static void fromString(T& value, const std::string& str);

    /**
     * Convert from string to a generic numeric type
     */
    template <class T> static bool from_string(T& t,const std::string& s, std::ios_base& (*f)(std::ios_base&));

};

template <typename T>
inline void StringUtilities::fromString(T& value, const std::string& str)
{
    std::istringstream iss(str);
    if ( (iss >> value).fail())
    {
        std::ostringstream message;
        message << "Failed to convert the string: '" << str << "' to numeric type";
        throw SilecsException(__FILE__, __LINE__, message.str().c_str());
    }
}

template <class T>
inline bool StringUtilities::from_string(T& t, const std::string& s, std::ios_base& (*f)(std::ios_base&))
{
    std::istringstream iss(s);
    return !(iss >> f >> t).fail();
}

} // namespace

#endif //_STRING_UTILITIES_H_
