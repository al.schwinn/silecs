/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsDevice.h>

#include <silecs-communication/interface/core/Context.h>
#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/core/WrapperAction.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/equipment/PLCRegister.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/XMLParser.h>

#include <silecs-communication/interface/equipment/CNVRegister.h>

#include <sstream>

namespace Silecs
{
Device::Device(PLC* thePLC, const ElementXML& deviceNode, boost::ptr_vector<ElementXML>& blockNodes) :
                thePLC_(thePLC)
{
    // Update Device ElementXML attributes
    label_ = deviceNode.getAttribute("label");
    StringUtilities::fromString(address_, deviceNode.getAttribute("address"));

    //For backward compatibility (IO addresses are not defined in Silecs version <1.4.0)
    //Undefined address = -1 by default.
    ai_address_ = -1;
    ao_address_ = -1;
    di_address_ = -1;
    do_address_ = -1;
    if (deviceNode.hasAttribute("AI-address"))
        StringUtilities::fromString(ai_address_, deviceNode.getAttribute("AI-address"));
    if (deviceNode.hasAttribute("AO-address"))
        StringUtilities::fromString(ao_address_, deviceNode.getAttribute("AO-address"));
    if (deviceNode.hasAttribute("DI-address"))
        StringUtilities::fromString(di_address_, deviceNode.getAttribute("DI-address"));
    if (deviceNode.hasAttribute("DO-address"))
        StringUtilities::fromString(do_address_, deviceNode.getAttribute("DO-address"));

    LOG(ALLOC) << "Creating SilecsDevice: " << label_;

    boost::ptr_vector<ElementXML>::const_iterator blockIter;
    for (blockIter = blockNodes.begin(); blockIter != blockNodes.end(); blockIter++)
    {
        std::string blockName = blockIter->getAttribute("name");
        LOG(ALLOC) << "Adding Block to Device: " << blockName;
        AccessType accessType = Block::whichAccessType(blockIter->name_);
        AccessArea accessArea = Memory;
        if (blockIter->hasAttribute("ioType")) // only IO-Blocks have this attribute
            accessArea = Block::whichAccessArea(blockIter->getAttribute("ioType"));
        std::vector<boost::shared_ptr<ElementXML> > registerNodes = blockIter->childList_;
        instantiateRegisters(blockName, accessType, accessArea, registerNodes);
    }
}

Device::~Device()
{
    LOG(ALLOC) << "Device (delete): " << label_;

    // Remove the register instances related to this Device
    registerVectorType::iterator pRegisterIter;
    for (pRegisterIter = registerCol_.begin(); pRegisterIter != registerCol_.end(); ++pRegisterIter)
        delete pRegisterIter->second;
    registerCol_.clear();

    // Register instances have been deleted before, just clear the block-register Map.
    blockRegisterMap_.clear();
}

PLC* Device::getPLC()
{
    return thePLC_;
}

Register* Device::getRegister(std::string registerName)
{
    registerVectorType::iterator iter;
    for (iter = registerCol_.begin(); iter != registerCol_.end(); ++iter)
    {
        if (iter->first == registerName)
        {
            return iter->second;
        }
    }
    throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_REGISTER_NAME, registerName);
    return NULL; // to suppress warning
}

std::vector<Register*>& Device::getRegisterCollection(std::string blockName)
{
    blockRegisterMapType::iterator iter = blockRegisterMap_.find(blockName);
    if (iter == blockRegisterMap_.end())
    {
        throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_BLOCK_NAME, blockName);
    }
    return iter->second;
}

int Device::recv(std::string blockName)
{
    //Synchronous device data receive

    //Context of the transaction:
    //this = the device to be treated,
    //false = is not for registers synchronization (normal transaction)
    Context context(this, false);

    //Execute the receive action for the required device (from the current thread)
    return getBlock(blockName, Input)->getTask()->execute(&context);
}

int Device::send(std::string blockName)
{
    //Synchronous device data send

    //Context of the transaction:
    //this = the device to be treated,
    //false = is not for registers synchronization (normal transaction)
    Context context(this, false);

    //Execute the receive action for the required device (from the current thread)
    return getBlock(blockName, Output)->getTask()->execute(&context);
}

std::string Device::getLabel()
{
    return label_;
}

std::string Device::getRegisterList()
{
    std::string registerList = "";
    registerVectorType::iterator pRegisterIter;
    for (pRegisterIter = registerCol_.begin(); pRegisterIter != registerCol_.end(); ++pRegisterIter)
    {
        if (!registerList.empty())
            registerList.append(" ");
        registerList.append(pRegisterIter->second->getName());
    }
    return registerList;
}

Register* Device::instantiateRegister(ElementXML* registerNode)
{
    switch (getPLC()->getBrandID())
    {
        // SIEMENS PLCs use Motorola memory convention (BigEndianRegister)
        case Siemens:
            return new S7Register(this, registerNode);

            // SCHNEIDER PLCs use Intel memory convention (LittleEndianRegister)
            // RABBIT microcontrollers use Intel memory convention (LittleEndianRegister)
        case Schneider:
        case Digi:
            return new UnityRegister(this, registerNode);

            // BECKHOFF PLCs use different memory convention depending on PLC type:
            // . CX model: same convention than Unity
            // . BC model: special convention for 64 bits data (float, ..): swap only the two 16bits words together (no bytes swap).
        case Beckhoff:
            return new TwinCATRegister(this, registerNode);

#ifdef NI_SUPPORT_ENABLED
            //NI Shared variables use specific convention
            case Ni:
            return new CNVRegister(this, registerNode);
#endif

        default:
            throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_PLC_MANUFACTURER, getPLC()->getBrand());

    } //switch
}

void Device::instantiateRegisters(std::string blockName, AccessType accessType, AccessArea accessArea, std::vector<boost::shared_ptr<ElementXML> >& registerNodes)
{
    std::vector<Register*> registerCol;

    //LOG(SETUP) << "Ordering registers for block: " <<  blockName << " on Device: " << getLabel();

    std::vector<boost::shared_ptr<ElementXML> >::const_iterator registerIter;
    for (registerIter = registerNodes.begin(); registerIter != registerNodes.end(); registerIter++)
    {
        std::string registerName = (*registerIter)->getAttribute("name");
        Register* pReg = instantiateRegister(registerIter->get());
        pReg->setAccessArea(accessArea);
        pReg->setAccessType(accessType);
        pReg->setBlockName(blockName);
        registerCol.push_back(pReg);
        registerCol_.push_back(std::make_pair(registerName, pReg));
    }
    blockRegisterMap_.insert(std::make_pair(blockName, registerCol));
}

Block* Device::getBlock(const std::string blockName, AccessType accessType)
{
    if (!hasBlock(blockName))
    {
        std::ostringstream errorMessage;
        errorMessage << "Block not found! The block '" << blockName << "' does not exist on the device '" << label_ << "'.";
        throw SilecsException(__FILE__, __LINE__, errorMessage.str());
    }
    return thePLC_->getBlock(blockName, accessType);
}

bool Device::hasBlock(const std::string blockName)
{
    blockRegisterMapType::iterator iter;
    iter = blockRegisterMap_.find(blockName);
    return (iter != blockRegisterMap_.end());
}

void Device::importRegisters(Block* pBlock, void* pBuffer, timeval ts, Context* pContext)
{
    std::vector<Register*>& registerCol = getRegisterCollection(pBlock->getName());
    std::vector<Register*>::iterator pReg;
    for (pReg = registerCol.begin(); pReg < registerCol.end(); pReg++)
    {
        if (!pContext->isForSynchronization())
        {
            if ( (*pReg)->getFormat() == String)
                (*pReg)->importString(pBuffer, ts);
            else
                (*pReg)->importValue(pBuffer, ts);
        }
    }
}

void Device::exportRegisters(Block* pBlock, void* pBuffer, Context* pContext)
{
    std::vector<Register*>& registerCol = getRegisterCollection(pBlock->getName());
    std::vector<Register*>::iterator pReg;
    for (pReg = registerCol.begin(); pReg < registerCol.end(); pReg++)
    {
        if (!pContext->isForSynchronization())
        {
            if ( (*pReg)->getFormat() == String)
                (*pReg)->exportString(pBuffer);
            else
                (*pReg)->exportValue(pBuffer);
        }
    }
}

void Device::copyInToOut(const std::string blockName)
{
    std::vector<Register*>& registerCol = getRegisterCollection(blockName);
    for (unsigned int i = 0; i < registerCol.size(); i++)
    {
        Register* pReg = registerCol[i];
        if (pReg->isReadable() && pReg->isWritable())
            pReg->copyValue();
    }

}

} // namespace
