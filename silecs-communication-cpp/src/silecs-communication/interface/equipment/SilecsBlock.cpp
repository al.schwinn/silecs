/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/Thread.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/core/SilecsAction.h>
#include <silecs-communication/interface/core/WrapperAction.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>

namespace Silecs
{

Block::Block(PLC* thePLC, ElementXML blockNode, AccessType accessType) :
                thePLC_(thePLC), configuration_(false)
{
    std::string designName = blockNode.getAttribute("name");


    if(blockNode.name_ == "Configuration-Block")
        configuration_ = true;

    name_ = designName;
    StringUtilities::fromString(size_, blockNode.getAttribute("size"));
    StringUtilities::fromString(memSize_, blockNode.getAttribute("mem-size"));
    StringUtilities::fromString(address_, blockNode.getAttribute("address"));
    if (blockNode.hasAttribute("ioType")) // only IO-Blocks have this attribute
        accessArea_ = Block::whichAccessArea(blockNode.getAttribute("ioType"));
    else
        accessArea_ = Memory;

    resetCustomAttributes(); //by default custom-size of the block is the maximum size

    /*In case of InOut block we will instantiate 2 different blocks (input/output)
     * and the accessType is given by the caller.
     */
    accessType_ = accessType;
}

Block::~Block()
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Block (delete): " << name_;

    if (pAction_ != NULL)
        delete pAction_;
    if (pTask_ != NULL)
        delete pTask_;
}

AccessType Block::whichAccessType(std::string type)
{
    if (type == "Acquisition-Block")
        return Input;
    else if (type == "Command-Block")
        return Output;
    else if (type == "Configuration-Block")
        return InOut;
    else if (type == "Setting-Block")
        return InOut;
    else if (type == "Setting-IO-Block")
        return InOut;
    else if (type == "Acquisition-IO-Block")
        return Input;
    else if (type == "Command-IO-Block")
        return Output;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_ACCESS_TYPE, type);
}

std::string Block::whichAccessType(AccessType type)
{
    switch (type)
    {
        case Output:
            return "WRITE-ONLY";
            break;
        case Input:
            return "READ-ONLY";
            break;
        case InOut:
            return "READ-WRITE";
            break;
        default:
            throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_ACCESS_TYPE, StringUtilities::toString(type));
    }
}

AccessArea Block::whichAccessArea(std::string area)
{
    StringUtilities::toLower(area);
    if (area == "memory")
        return Memory;
    else if (area == "digital-io")
        return Digital;
    else if (area == "analog-io")
        return Analog;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_ACCESS_AREA, area);
}

std::string Block::whichAccessArea(AccessArea area)
{
    switch (area)
    {
        case Memory:
            return "MEMORY";
            break;
        case Digital:
            return "DIGITAL-IO";
            break;
        case Analog:
            return "ANALOG-IO";
            break;
        default:
            throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_ACCESS_AREA, StringUtilities::toString(area));
    }
}

void Block::setCustomAttributes(const unsigned long customAddress, const unsigned long customOffset, const unsigned long customSize)
{
    //user wants to limit the size of data to be sent/receive - check that it is lower than design allocation size
    if (customSize > memSize_)
        throw SilecsException(__FILE__, __LINE__, PARAM_EXCEEDED_BLOCK_SIZE, StringUtilities::toString(memSize_));

    customAddress_ = customAddress;
    customOffset_ = customOffset;
    customSize_ = customSize;
    customAttributes_ = true;
    LOG(DEBUG) << "Set custom attributes of block: " << name_ << ", memSize: " << memSize_ << ", customSize: " << customSize << ", customAddress: " << customAddress << ", customOffset: " << customOffset;
}

void Block::resetCustomAttributes()
{
    customAttributes_ = false;
    LOG(DEBUG) << "Reset custom attributes of block: " << name_;
}

} // namespace
