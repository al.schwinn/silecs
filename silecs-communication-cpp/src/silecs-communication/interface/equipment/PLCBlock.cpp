/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/Thread.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/core/SilecsAction.h>
#include <silecs-communication/interface/core/PLCRecvAction.h>
#include <silecs-communication/interface/core/PLCSendAction.h>
#include <silecs-communication/interface/core/WrapperAction.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/equipment/PLCBlock.h>

namespace Silecs
{

PLCBlock::PLCBlock(PLC* thePLC, ElementXML blockNode, AccessType accessType) :
                Block(thePLC, blockNode, accessType)
{
    // Create buffer for the block exchanges
    bufferSize_ = memSize_; //size of one block type (including alignements)
    if (getPLC()->getProtocolModeID() == BlockMode)
    { //BlockMode ==> access all devices in once
        unsigned int devicesWithBlock = getPLC()->getNrDevicesWithBlock(blockNode.getAttribute("name"));
        bufferSize_ *= devicesWithBlock;
    }
    pBuffer_ = (unsigned char*)calloc(bufferSize_, sizeof(unsigned char));
}

PLCBlock::~PLCBlock()
{
    //Remove the buffer
    if (pBuffer_ != NULL)
        free(pBuffer_);
    pBuffer_ = NULL;
}

InputBlock::InputBlock(PLC* thePLC, ElementXML blockNode, AccessType accessType) :
                PLCBlock(thePLC, blockNode, accessType)
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Block (create): " << name_ << ", plc: " << getPLC()->getName() << ", access: Input" << ", address: " << address_ << ", mem-size: " << memSize_ << ", buffer-size: " << bufferSize_;

    // Creates receive task which relies on the block exchange
    if (getPLC()->getProtocolModeID() == BlockMode)
        pAction_ = new PLCRecvBlockMode(this);
    else
        pAction_ = new PLCRecvDeviceMode(this);

    WrapperAction wrapperAction(pAction_);
    pTask_ = new Task<WrapperAction>(WrapperAction::function, wrapperAction);
}

InputBlock::~InputBlock()
{
}

OutputBlock::OutputBlock(PLC* thePLC, ElementXML blockNode, AccessType accessType) :
                PLCBlock(thePLC, blockNode, accessType)
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Block (create): " << name_ << ", plc: " << getPLC()->getName() << ", access: Output" << ", address: " << address_ << ", mem-size: " << memSize_ << ", buffer-size: " << bufferSize_;

    // Creates send task which relies on the block exchange
    if (getPLC()->getProtocolModeID() == BlockMode)
        pAction_ = new PLCSendBlockMode(this);
    else
        pAction_ = new PLCSendDeviceMode(this);

    WrapperAction wrapperAction(pAction_);
    pTask_ = new Task<WrapperAction>(WrapperAction::function, wrapperAction);
}

OutputBlock::~OutputBlock()
{
}

}
