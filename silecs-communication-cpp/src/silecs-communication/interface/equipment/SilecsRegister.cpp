/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/XMLParser.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsRegister.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>

#include <arpa/inet.h>
#include <math.h>

namespace Silecs
{

Register::Register(Device* theDevice, ElementXML* registerNode) :
                theDevice_(theDevice), isConfiguration_(false)
{
    name_ = registerNode->getAttribute("name");
    length_ = 1;
    dimension1_ = 1;
    dimension2_ = 1;
    std::vector<boost::shared_ptr<ElementXML> > childNodes = registerNode->childList_;
    std::vector<boost::shared_ptr<ElementXML> >::const_iterator childIter;
    for (childIter = childNodes.begin(); childIter != childNodes.end(); childIter++)
    {
        // We have to loop, since there is as well the description-element
        format_ = whichFormatType( (*childIter)->getAttribute("format"));
        if ( (*childIter)->hasAttribute("dim"))
            StringUtilities::fromString(dimension1_, (*childIter)->getAttribute("dim")); // dim is used for 1d-arrays
        if ( (*childIter)->hasAttribute("dim1"))
            StringUtilities::fromString(dimension1_, (*childIter)->getAttribute("dim1"));
        if ( (*childIter)->hasAttribute("dim2"))
            StringUtilities::fromString(dimension2_, (*childIter)->getAttribute("dim2"));
        if ( (*childIter)->hasAttribute("string-length"))
            StringUtilities::fromString(length_, (*childIter)->getAttribute("string-length"));
    }
    StringUtilities::fromString(size_, registerNode->getAttribute("size"));
    StringUtilities::fromString(memSize_, registerNode->getAttribute("mem-size"));
    StringUtilities::fromString(address_, registerNode->getAttribute("address"));

    if(registerNode->name_ == "Configuration-Register")
        isConfiguration_ = true;

    isInitialized_ = false; //register not initialized yet (used for output registers)

    if (length_ > 1)
    {
        LOG(DEBUG) << "Register (create): " << name_ << ", format: " << getFormatAsString() << ", string-length: " << length_ << ", dim: " << dimension1_ << ", dim2: " << dimension2_ << ", offset: " << address_;
    }
    else
    {
        LOG(DEBUG) << "Register (create): " << name_ << ", format: " << getFormatAsString() << ", dim: " << dimension1_ << ", dim2: " << dimension2_ << ", offset: " << address_;
    }
}

Register::~Register()
{
    if (DEBUG & Log::topics_)
        LOG(ALLOC) << "Register (delete): " << name_;

    //Remove the value buffers
    if (pRecvValue_ != NULL)
    {
        if (format_ == String)
        {
            // loop through array of pointers and delete them
            std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
            for (unsigned int i = 0; i < dimension1_ * dimension2_; i++)
            {
                delete pRecvStringValue[i];
            }
            pRecvStringValue = NULL;
        }
        free(pRecvValue_);
        pRecvValue_ = NULL;
    }

    if (pSendValue_ != NULL)
    {
        if (format_ == String)
        {
            // loop through array of pointers and delete them
            std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
            for (unsigned int i = 0; i < dimension1_ * dimension2_; i++)
            {
                delete pSendStringValue[i];
            }
            pSendStringValue = NULL;
        }
        free(pSendValue_);
        pSendValue_ = NULL;
    }
}

PLC* Register::getPLC()
{
    return theDevice_->getPLC();
}
Device* Register::getDevice()
{
    return theDevice_;
}

// GET methods =============================================================
template<typename T>
inline T Register::getVal(FormatType F)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || !isScalar())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    return * ((T*)pRecvValue_);
}

template<typename T>
inline void Register::getValArray(FormatType F, T* pValue, uint32_t dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || !isSingleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    memcpy((void *)pValue, pRecvValue_, size_ * dimension1_);
}

template<typename T>
inline void Register::getValArray2D(FormatType F, T* pValue, uint32_t dim1, uint32_t dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || !isDoubleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    memcpy((void *)pValue, pRecvValue_, size_ * dimension1_ * dimension2_);
}

template<typename Tsrc, typename Tdst>
inline void Register::convGetValArray(FormatType F, Tdst* pValue, uint32_t dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || !isSingleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    //automatic conversion from T to F
    for (unsigned long i = 0; i < dimension1_; i++)
        pValue[i] = ((Tsrc*)pRecvValue_)[i];
}

template<typename Tsrc, typename Tdst>
inline void Register::convGetValArray2D(FormatType F, Tdst* pValue, uint32_t dim1, uint32_t dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || !isDoubleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    //automatic conversion from T to F
    for (unsigned int i = 0; i < (dimension1_ * dimension2_); i++)
        ((Tdst*)pValue)[i] = ((Tsrc*)pRecvValue_)[i];
}

template<typename T>
inline T* Register::getRefArray(FormatType F, uint32_t& dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || !isSingleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    dim = dimension1_;
    return (T*)pRecvValue_;
}

template<typename T>
inline T* Register::getRefArray2D(FormatType F, uint32_t& dim1, uint32_t& dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || !isDoubleArray())
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    dim1 = dimension1_;
    dim2 = dimension2_;
    return (T*)pRecvValue_;
}

// .........................................................................
// No template for string format
std::string Register::getValString()
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != String) || (dimension1_ != 1) || (dimension2_ != 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
    return * (pRecvStringValue[0]);
}

void Register::getValStringArray(std::string* pVal, uint32_t dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != String)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim) || (dimension2_ > 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        pVal[i] = * (pRecvStringValue[i]);
    }
}

void Register::getValStringArray2D(std::string* pVal, uint32_t dim1, uint32_t dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != String)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        for (unsigned long j = 0; j < dimension2_; j++)
        {
            pVal[ (i * dimension2_) + j] = * (pRecvStringValue[ (i * dimension2_) + j]);
        }
    }
}

const std::string** Register::getRefStringArray(uint32_t& dim)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (dimension1_ <= 1)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    dim = dimension1_;
    return (const std::string**)pRecvValue_;
}

const std::string** Register::getRefStringArray2D(uint32_t& dim1, uint32_t& dim2)
{
    if (!isReadable())
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
    if (dimension2_ <= 1)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    dim1 = dimension1_;
    dim2 = dimension2_;
    return (const std::string**)pRecvValue_;
}

// .........................................................................
//Deprecated: initially for SLC5 (32bits platform)
char Register::getValChar()
{
    return (char)getVal<int8_t>(Char);
}
void Register::getValCharArray(char* pVal, uint32_t dim)
{
    getValArray<int8_t>(Char, (int8_t*)pVal, dim);
}
void Register::getValCharArray2D(char* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<int8_t>(Char, (int8_t*)pVal, dim1, dim2);
}
const char* Register::getRefCharArray(uint32_t& dim)
{
    return (const char*)getRefArray<int8_t>(Char, dim);
}
const char* Register::getRefCharArray2D(uint32_t& dim1, uint32_t& dim2)
{
    return (const char*)getRefArray2D<int8_t>(Char, dim1, dim2);
}

unsigned char Register::getValUChar()
{
    return getVal<uint8_t>(uChar);
}
void Register::getValUCharArray(unsigned char* pVal, uint32_t dim)
{
    getValArray<uint8_t>(uChar, (uint8_t*)pVal, dim);
}
void Register::getValUCharArray2D(unsigned char* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<uint8_t>(uChar, (uint8_t*)pVal, dim1, dim2);
}
void Register::getValUCharArray(short* pVal, uint32_t dim)
{
    convGetValArray<unsigned char, short>(uChar, pVal, dim);
}
void Register::getValUCharArray2D(short* pVal, uint32_t dim1, uint32_t dim2)
{
    convGetValArray2D<unsigned char, short>(uChar, pVal, dim1, dim2);
}
const unsigned char* Register::getRefUCharArray(uint32_t& dim)
{
    return (const unsigned char*)getRefArray<uint8_t>(uChar, dim);
}
const unsigned char* Register::getRefUCharArray2D(uint32_t& dim1, uint32_t& dim2)
{
    return (const unsigned char*)getRefArray2D<uint8_t>(uChar, dim1, dim2);
}

short Register::getValShort()
{
    return (short)getVal<int16_t>(Short);
}
void Register::getValShortArray(short* pVal, uint32_t dim)
{
    getValArray<int16_t>(Short, (int16_t*)pVal, dim);
}
void Register::getValShortArray2D(short* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<int16_t>(Short, (int16_t*)pVal, dim1, dim2);
}
const short* Register::getRefShortArray(uint32_t& dim)
{
    return (short*)getRefArray<int16_t>(Short, dim);
}
short* Register::getRefShortArray2D(uint32_t& dim1, uint32_t& dim2)
{
    return (short*)getRefArray2D<int16_t>(Short, dim1, dim2);
}

unsigned short Register::getValUShort()
{
    return (unsigned short)getVal<uint16_t>(uShort);
}
void Register::getValUShortArray(unsigned short* pVal, uint32_t dim)
{
    getValArray<uint16_t>(uShort, (uint16_t*)pVal, dim);
}
void Register::getValUShortArray2D(unsigned short* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<uint16_t>(uShort, (uint16_t*)pVal, dim1, dim2);
}
void Register::getValUShortArray(long* pVal, uint32_t dim)
{
    convGetValArray<unsigned short, long>(uShort, pVal, dim);
}
void Register::getValUShortArray2D(long* pVal, uint32_t dim1, uint32_t dim2)
{
    convGetValArray2D<unsigned short, long>(uShort, pVal, dim1, dim2);
}
const unsigned short* Register::getRefUShortArray(uint32_t& dim)
{
    return (const unsigned short*)getRefArray<uint16_t>(uShort, dim);
}
const unsigned short* Register::getRefUShortArray2D(uint32_t& dim1, uint32_t& dim2)
{
    return (const unsigned short*)getRefArray2D<uint16_t>(uShort, dim1, dim2);
}

#ifndef __x86_64__
//not supported anymore from SLC6 platform (look at <.h> comments)
long Register::getValLong()
{   return (long)getVal<int32_t>(Long);}
void Register::getValLongArray(long* pVal, uint32_t dim)
{   getValArray<int32_t>(Long, (int32_t*)pVal, dim);}
void Register::getValLongArray2D(long* pVal, uint32_t dim1, uint32_t dim2)
{   getValArray2D<int32_t>(Long, (int32_t*)pVal, dim1, dim2);}
const long* Register::getRefLongArray(uint32_t& dim)
{   return (const long*)getRefArray<int32_t>(Long, dim);}
const long* Register::getRefLongArray2D(uint32_t& dim1, uint32_t& dim2)
{   return (const long*)getRefArray2D<int32_t>(Long, dim1, dim2);}

unsigned long Register::getValULong()
{   return (unsigned long)getVal<uint32_t>(uLong);}
void Register::getValULongArray(unsigned long* pVal, uint32_t dim)
{   getValArray<uint32_t>(uLong, (uint32_t*)pVal, dim);}
void Register::getValULongArray2D(unsigned long* pVal, uint32_t dim1, uint32_t dim2)
{   getValArray2D<uint32_t>(uLong, (uint32_t*)pVal, dim1, dim2);}
void Register::getValULongArray(long long* pVal, uint32_t dim)
{   convGetValArray<unsigned long, long long>(uLong, pVal, dim);}
void Register::getValULongArray2D(long long* pVal, uint32_t dim1, uint32_t dim2)
{   convGetValArray2D<unsigned long, long long>(uLong, pVal, dim1, dim2);}
const unsigned long* Register::getRefULongArray(uint32_t& dim)
{   return (const unsigned long*)getRefArray<uint32_t>(uLong, dim);}
const unsigned long* Register::getRefULongArray2D(uint32_t& dim1, uint32_t& dim2)
{   return (const unsigned long*)getRefArray2D<uint32_t>(uLong, dim1, dim2);}
#endif

float Register::getValFloat()
{
    return getVal<float>(Float);
}
void Register::getValFloatArray(float* pVal, uint32_t dim)
{
    getValArray<float>(Float, pVal, dim);
}
void Register::getValFloatArray2D(float* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<float>(Float, pVal, dim1, dim2);
}
const float* Register::getRefFloatArray(uint32_t& dim)
{
    return getRefArray<float>(Float, dim);
}
const float* Register::getRefFloatArray2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<const float>(Float, dim1, dim2);
}

double Register::getValDate()
{
    return getVal<double>(Date);
}
void Register::getValDateArray(double* pVal, uint32_t dim)
{
    getValArray<double>(Date, pVal, dim);
}
void Register::getValDateArray2D(double* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<double>(Date, pVal, dim1, dim2);
}
const double* Register::getRefDateArray(uint32_t& dim)
{
    return getRefArray<double>(Date, dim);
}
double* Register::getRefDateArray2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<double>(Date, dim1, dim2);
}

// .........................................................................
//Recommended: from SLC6 (32bits and 64bits platform)
int8_t Register::getValInt8()
{
    return getVal<int8_t>(Int8);
}
void Register::getValInt8Array(int8_t* pVal, uint32_t dim)
{
    getValArray<int8_t>(Int8, pVal, dim);
}
void Register::getValInt8Array2D(int8_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<int8_t>(Int8, pVal, dim1, dim2);
}
const int8_t* Register::getRefInt8Array(uint32_t& dim)
{
    return getRefArray<int8_t>(Int8, dim);
}
int8_t* Register::getRefInt8Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<int8_t>(Int8, dim1, dim2);
}

uint8_t Register::getValUInt8()
{
    return getVal<uint8_t>(uInt8);
}
void Register::getValUInt8Array(uint8_t* pVal, uint32_t dim)
{
    getValArray<uint8_t>(uInt8, pVal, dim);
}
void Register::getValUInt8Array2D(uint8_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<uint8_t>(uInt8, pVal, dim1, dim2);
}
void Register::getValUInt8Array(int16_t* pVal, uint32_t dim)
{
    convGetValArray<uint8_t, int16_t>(uInt8, pVal, dim);
}
void Register::getValUInt8Array2D(int16_t* pVal, uint32_t dim1, uint32_t dim2)
{
    convGetValArray2D<uint8_t, int16_t>(uInt8, pVal, dim1, dim2);
}
const uint8_t* Register::getRefUInt8Array(uint32_t& dim)
{
    return getRefArray<uint8_t>(uInt8, dim);
}
uint8_t* Register::getRefUInt8Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<uint8_t>(uInt8, dim1, dim2);
}

int16_t Register::getValInt16()
{
    return getVal<int16_t>(Int16);
}
void Register::getValInt16Array(int16_t* pVal, uint32_t dim)
{
    getValArray<int16_t>(Int16, pVal, dim);
}
void Register::getValInt16Array2D(int16_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<int16_t>(Int16, pVal, dim1, dim2);
}
const int16_t* Register::getRefInt16Array(uint32_t& dim)
{
    return getRefArray<int16_t>(Int16, dim);
}
int16_t* Register::getRefInt16Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<int16_t>(Int16, dim1, dim2);
}

uint16_t Register::getValUInt16()
{
    return getVal<uint16_t>(uInt16);
}
void Register::getValUInt16Array(uint16_t* pVal, uint32_t dim)
{
    getValArray<uint16_t>(uInt16, pVal, dim);
}
void Register::getValUInt16Array2D(uint16_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<uint16_t>(uInt16, pVal, dim1, dim2);
}
void Register::getValUInt16Array(int32_t* pVal, uint32_t dim)
{
    convGetValArray<uint16_t, int32_t>(uInt16, pVal, dim);
}
void Register::getValUInt16Array2D(int32_t* pVal, uint32_t dim1, uint32_t dim2)
{
    convGetValArray2D<uint16_t, int32_t>(uInt16, pVal, dim1, dim2);
}
const uint16_t* Register::getRefUInt16Array(uint32_t& dim)
{
    return getRefArray<uint16_t>(uInt16, dim);
}
uint16_t* Register::getRefUInt16Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<uint16_t>(uInt16, dim1, dim2);
}

int32_t Register::getValInt32()
{
    return getVal<int32_t>(Int32);
}
void Register::getValInt32Array(int32_t* pVal, uint32_t dim)
{
    getValArray<int32_t>(Int32, pVal, dim);
}
void Register::getValInt32Array2D(int32_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<int32_t>(Int32, pVal, dim1, dim2);
}
const int32_t* Register::getRefInt32Array(uint32_t& dim)
{
    return getRefArray<int32_t>(Int32, dim);
}
int32_t* Register::getRefInt32Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<int32_t>(Int32, dim1, dim2);
}

uint32_t Register::getValUInt32()
{
    return getVal<uint32_t>(uInt32);
}
void Register::getValUInt32Array(uint32_t* pVal, uint32_t dim)
{
    getValArray<uint32_t>(uInt32, pVal, dim);
}
void Register::getValUInt32Array2D(uint32_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<uint32_t>(uInt32, pVal, dim1, dim2);
}
void Register::getValUInt32Array(int64_t* pVal, uint32_t dim)
{
    convGetValArray<uint32_t, int64_t>(uInt32, pVal, dim);
}
void Register::getValUInt32Array2D(int64_t* pVal, uint32_t dim1, uint32_t dim2)
{
    convGetValArray2D<uint32_t, int64_t>(uInt32, pVal, dim1, dim2);
}
const uint32_t* Register::getRefUInt32Array(uint32_t& dim)
{
    return getRefArray<uint32_t>(uInt32, dim);
}
uint32_t* Register::getRefUInt32Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<uint32_t>(uInt32, dim1, dim2);
}

int64_t Register::getValInt64()
{
    return getVal<int64_t>(Int64);
}
void Register::getValInt64Array(int64_t* pVal, uint32_t dim)
{
    getValArray<int64_t>(Int64, pVal, dim);
}
void Register::getValInt64Array2D(int64_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<int64_t>(Int64, pVal, dim1, dim2);
}
const int64_t* Register::getRefInt64Array(uint32_t& dim)
{
    return getRefArray<int64_t>(Int64, dim);
}
int64_t* Register::getRefInt64Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<int64_t>(Int64, dim1, dim2);
}

uint64_t Register::getValUInt64()
{
    return getVal<uint64_t>(uInt64);
}
void Register::getValUInt64Array(uint64_t* pVal, uint32_t dim)
{
    getValArray<uint64_t>(uInt64, pVal, dim);
}
void Register::getValUInt64Array2D(uint64_t* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<uint64_t>(uInt64, pVal, dim1, dim2);
}
const uint64_t* Register::getRefUInt64Array(uint32_t& dim)
{
    return getRefArray<uint64_t>(uInt64, dim);
}
uint64_t* Register::getRefUInt64Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<uint64_t>(uInt64, dim1, dim2);
}

float Register::getValFloat32()
{
    return getVal<float>(Float32);
}
void Register::getValFloat32Array(float* pVal, uint32_t dim)
{
    getValArray<float>(Float32, pVal, dim);
}
void Register::getValFloat32Array2D(float* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<float>(Float32, pVal, dim1, dim2);
}
const float* Register::getRefFloat32Array(uint32_t& dim)
{
    return getRefArray<float>(Float32, dim);
}
float* Register::getRefFloat32Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<float>(Float32, dim1, dim2);
}

double Register::getValFloat64()
{
    return getVal<double>(Float64);
}
void Register::getValFloat64Array(double* pVal, uint32_t dim)
{
    getValArray<double>(Float64, pVal, dim);
}
void Register::getValFloat64Array2D(double* pVal, uint32_t dim1, uint32_t dim2)
{
    getValArray2D<double>(Float64, pVal, dim1, dim2);
}
const double* Register::getRefFloat64Array(uint32_t& dim)
{
    return getRefArray<double>(Float64, dim);
}
double* Register::getRefFloat64Array2D(uint32_t& dim1, uint32_t& dim2)
{
    return getRefArray2D<double>(Float64, dim1, dim2);
}

// SET methods =============================================================

void Register::setAccessArea(AccessArea accessArea)
{
    accessArea_ = accessArea;
}

void Register::setAccessType(AccessType accessType)
{
    accessType_ = accessType;

    // By knowing the register type we can allocate 1 or 2 buffers for data exchanges.
    pRecvValue_ = pSendValue_ = NULL;

    if (Service::withInputAccess(accessType))
    {
        if (format_ == String)
        {
            pRecvValue_ = calloc(dimension1_ * dimension2_, sizeof(void*));
            std::string** pRecvStringValue = static_cast<std::string**>(pRecvValue_);

            // allocate memory for the required pointers to the strings
            for (unsigned long i = 0; i < dimension1_ * dimension2_; i++)
            {
                pRecvStringValue[i] = new std::string();
            }
        }
        else // If not string register just allocate space for data
        {
            pRecvValue_ = calloc(dimension1_ * dimension2_, size_);
        }
    }

    if (Service::withOutputAccess(accessType))
    {
        if (format_ == String)
        {
            pSendValue_ = calloc(dimension1_ * dimension2_, sizeof(void*));
            std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);

            //allocate memory for the required pointers to the strings
            for (unsigned long i = 0; i < dimension1_ * dimension2_; i++)
            { // allocate space for the string object
                pSendStringValue[i] = new std::string();
            }
        }
        else // if not string register just allocate space for data
        {
            pSendValue_ = calloc(dimension1_ * dimension2_, size_);
        }
    }
}

template<typename T>
inline void Register::setVal(FormatType F, T val)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != F) || (dimension1_ != 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    * ((T*)pSendValue_) = val;
    isInitialized_ = true; //the register value has been set once at least
}

template<typename T>
inline void Register::setValArray(FormatType F, const T* pVal, uint32_t dim)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != F)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    memcpy(pSendValue_, (void *)pVal, size_ * dimension1_);
    isInitialized_ = true; //the register value has been set once at least
}

template<typename T>
inline void Register::setValArray2D(FormatType F, const T* pVal, uint32_t dim1, uint32_t dim2)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != F)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    memcpy(pSendValue_, (void *)pVal, size_ * dimension1_ * dimension2_);
    isInitialized_ = true; //the register value has been set once at least
}

template<typename Tsrc, typename Tdst>
inline void Register::convSetValArray(FormatType F, const Tsrc* pVal, uint32_t dim)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != F)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    // conversion from Type-source to Type-destination
    for (unsigned long i = 0; i < dimension1_; i++)
        static_cast<Tdst*>(pSendValue_)[i] = static_cast<Tdst>(pVal[i]);
    isInitialized_ = true; //the register value has been set once at least
}

template<typename Tsrc, typename Tdst>
inline void Register::convSetValArray2D(FormatType F, const Tsrc* pVal, uint32_t dim1, uint32_t dim2)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != F)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if ( (dimension1_ != dim1) || (dimension2_ != dim2))
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    // conversion from Type-source to Type-destination
    for (unsigned long i = 0; i < (dimension1_ * dimension2_); i++)
        static_cast<Tdst*>(pSendValue_)[i] = static_cast<Tdst>(pVal[i]);
    isInitialized_ = true; //the register value has been set once at least
}

// .........................................................................
// No template for string format
void Register::setValString(std::string val)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if ( (format_ != String) || (dimension1_ != 1) || (dimension2_ != 1))
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());

    std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
    * (pSendStringValue[0]) = val;
    isInitialized_ = true; //the register value has been set once at least
}

void Register::setValStringArray(const std::string* pVal, uint32_t dim)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != String)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim || dimension2_ > 1)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());

    std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        * (pSendStringValue[i]) = pVal[i];
    }
    isInitialized_ = true; //the register value has been set once at least
}

void Register::setValStringArray2D(const std::string* pVal, uint32_t dim1, uint32_t dim2)
{
    if (!isWritable())
        throw SilecsException(__FILE__, __LINE__, DATA_WRITE_ACCESS_TYPE_MISMATCH, getName());
    if (format_ != String)
        throw SilecsException(__FILE__, __LINE__, PARAM_FORMAT_TYPE_MISMATCH, getName());
    if (dimension1_ != dim1 || dimension2_ != dim2)
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());

    std::string** pSendStringValue = static_cast<std::string**>(pSendValue_);
    for (unsigned long i = 0; i < dimension1_; i++)
    {
        for (unsigned long j = 0; j < dimension2_; j++)
        {
            * (pSendStringValue[ (i * dimension2_) + j]) = pVal[ (i * dimension2_) + j];
        }
    }
    isInitialized_ = true; //the register value has been set once at least
}

// .........................................................................
//Deprecated: initially for SLC5 (32bits platform)
void Register::setValChar(char val)
{
    setVal<int8_t>(Int8, (int8_t)val);
}
void Register::setValCharArray(const char* pVal, uint32_t dim)
{
    setValArray<int8_t>(Char, (int8_t*)pVal, dim);
}
void Register::setValCharArray2D(const char* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<int8_t>(Char, (const int8_t*)pVal, dim1, dim2);
}

void Register::setValUChar(unsigned char val)
{
    setVal<uint8_t>(uChar, (uint8_t)val);
}
void Register::setValUCharArray(const unsigned char* pVal, uint32_t dim)
{
    setValArray<uint8_t>(uChar, (uint8_t*)pVal, dim);
}
void Register::setValUCharArray2D(const unsigned char* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<uint8_t>(uChar, (const uint8_t*)pVal, dim1, dim2);
}
void Register::setValUCharArray(const short* pVal, uint32_t dim)
{
    convSetValArray<short, unsigned char>(uChar, pVal, dim);
}
void Register::setValUCharArray2D(const short* pVal, uint32_t dim1, uint32_t dim2)
{
    convSetValArray2D<short, unsigned char>(uChar, pVal, dim1, dim2);
}

void Register::setValShort(short val)
{
    setVal<int16_t>(Short, (int16_t)val);
}
void Register::setValShortArray(const short* pVal, uint32_t dim)
{
    setValArray<int16_t>(Short, (int16_t*)pVal, dim);
}
void Register::setValShortArray2D(const short* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<int16_t>(Short, (const int16_t*)pVal, dim1, dim2);
}

void Register::setValUShort(unsigned short val)
{
    setVal<uint16_t>(uShort, (uint16_t)val);
}
void Register::setValUShortArray(const unsigned short* pVal, uint32_t dim)
{
    setValArray<uint16_t>(uShort, (uint16_t*)pVal, dim);
}
void Register::setValUShortArray2D(const unsigned short* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<uint16_t>(uShort, (const uint16_t*)pVal, dim1, dim2);
}
void Register::setValUShortArray(const long* pVal, uint32_t dim)
{
    convSetValArray<long, unsigned short>(uShort, pVal, dim);
}
void Register::setValUShortArray2D(const long* pVal, uint32_t dim1, uint32_t dim2)
{
    convSetValArray2D<long, unsigned short>(uShort, pVal, dim1, dim2);
}

#ifndef __x86_64__
//not supported anymore from SLC6 platform (look at <.h> comments)
void Register::setValLong(long val)
{   setVal<int32_t>(Long, (int32_t)val);}
void Register::setValLongArray(const long* pVal, uint32_t dim)
{   setValArray<int32_t>(Long, (int32_t*)pVal, dim);}
void Register::setValLongArray2D(const long* pVal, uint32_t dim1, uint32_t dim2)
{   setValArray2D<int32_t>(Long, (int32_t*)pVal, dim1, dim2);}

void Register::setValULong(unsigned long val)
{   setVal<uint32_t>(uLong, (uint32_t)val);}
void Register::setValULongArray(const unsigned long* pVal, uint32_t dim)
{   setValArray<uint32_t>(uLong, (uint32_t*)pVal, dim);}
void Register::setValULongArray2D(const unsigned long* pVal, uint32_t dim1, uint32_t dim2)
{   setValArray2D<uint32_t>(uLong, (uint32_t*)pVal, dim1, dim2);}
void Register::setValULongArray(const long long* pVal, uint32_t dim)
{   convSetValArray<long long,unsigned long>(uLong, pVal, dim);}
void Register::setValULongArray2D(const long long* pVal, uint32_t dim1, uint32_t dim2)
{   convSetValArray2D<long long,unsigned long>(uLong, pVal, dim1, dim2);}
#endif

void Register::setValFloat(float val)
{
    setVal<float>(Float, val);
}
void Register::setValFloatArray(const float* pVal, uint32_t dim)
{
    setValArray<float>(Float, pVal, dim);
}
void Register::setValFloatArray2D(const float* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<float>(Float, pVal, dim1, dim2);
}

//Set date is not required for the time being
void Register::setValDate(double val)
{
    setVal<double>(Date, val);
}
void Register::setValDateArray(const double* pVal, uint32_t dim)
{
    setValArray<double>(Date, pVal, dim);
}
void Register::setValDateArray2D(const double* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<double>(Date, pVal, dim1, dim2);
}

// .........................................................................
//Recommended: from SLC6 (32bits and 64bits platform)
void Register::setValInt8(int8_t val)
{
    setVal<int8_t>(Int8, val);
}
void Register::setValInt8Array(const int8_t* pVal, uint32_t dim)
{
    setValArray<int8_t>(Int8, pVal, dim);
}
void Register::setValInt8Array2D(const int8_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<int8_t>(Int8, pVal, dim1, dim2);
}

void Register::setValUInt8(uint8_t val)
{
    setVal<uint8_t>(uInt8, val);
}
void Register::setValUInt8Array(const uint8_t* pVal, uint32_t dim)
{
    setValArray<uint8_t>(uInt8, pVal, dim);
}
void Register::setValUInt8Array2D(const uint8_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<uint8_t>(uInt8, pVal, dim1, dim2);
}
void Register::setValUInt8Array(const int16_t* pVal, uint32_t dim)
{
    convSetValArray<int16_t, uint8_t>(uInt8, pVal, dim);
}
void Register::setValUInt8Array2D(const int16_t* pVal, uint32_t dim1, uint32_t dim2)
{
    convSetValArray2D<int16_t, uint8_t>(uInt8, pVal, dim1, dim2);
}

void Register::setValInt16(int16_t val)
{
    setVal<int16_t>(Int16, val);
}
void Register::setValInt16Array(const int16_t* pVal, uint32_t dim)
{
    setValArray<int16_t>(Int16, pVal, dim);
}
void Register::setValInt16Array2D(const int16_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<int16_t>(Int16, pVal, dim1, dim2);
}

void Register::setValUInt16(uint16_t val)
{
    setVal<uint16_t>(uInt16, val);
}
void Register::setValUInt16Array(const uint16_t* pVal, uint32_t dim)
{
    setValArray<uint16_t>(uInt16, pVal, dim);
}
void Register::setValUInt16Array2D(const uint16_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<uint16_t>(uInt16, pVal, dim1, dim2);
}
void Register::setValUInt16Array(const int32_t* pVal, uint32_t dim)
{
    convSetValArray<int32_t, uint16_t>(uInt16, pVal, dim);
}
void Register::setValUInt16Array2D(const int32_t* pVal, uint32_t dim1, uint32_t dim2)
{
    convSetValArray2D<int32_t, uint16_t>(uInt16, pVal, dim1, dim2);
}

void Register::setValInt32(int32_t val)
{
    setVal<int32_t>(Int32, val);
}
void Register::setValInt32Array(const int32_t* pVal, uint32_t dim)
{
    setValArray<int32_t>(Int32, pVal, dim);
}
void Register::setValInt32Array2D(const int32_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<int32_t>(Int32, pVal, dim1, dim2);
}

void Register::setValUInt32(uint32_t val)
{
    setVal<uint32_t>(uInt32, val);
}
void Register::setValUInt32Array(const uint32_t* pVal, uint32_t dim)
{
    setValArray<uint32_t>(uInt32, pVal, dim);
}
void Register::setValUInt32Array2D(const uint32_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<uint32_t>(uInt32, pVal, dim1, dim2);
}
void Register::setValUInt32Array(const int64_t* pVal, uint32_t dim)
{
    convSetValArray<int64_t, uint32_t>(uInt32, pVal, dim);
}
void Register::setValUInt32Array2D(const int64_t* pVal, uint32_t dim1, uint32_t dim2)
{
    convSetValArray2D<int64_t, uint32_t>(uInt32, pVal, dim1, dim2);
}

void Register::setValInt64(int64_t val)
{
    setVal<int64_t>(Int64, val);
}
void Register::setValInt64Array(const int64_t* pVal, uint32_t dim)
{
    setValArray<int64_t>(Int64, pVal, dim);
}
void Register::setValInt64Array2D(const int64_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<int64_t>(Int64, pVal, dim1, dim2);
}

void Register::setValUInt64(uint64_t val)
{
    setVal<uint64_t>(uInt64, val);
}
void Register::setValUInt64Array(const uint64_t* pVal, uint32_t dim)
{
    setValArray<uint64_t>(uInt64, pVal, dim);
}
void Register::setValUInt64Array2D(const uint64_t* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<uint64_t>(uInt64, pVal, dim1, dim2);
}

void Register::setValFloat32(float val)
{
    setVal<float>(Float32, val);
}
void Register::setValFloat32Array(const float* pVal, uint32_t dim)
{
    setValArray<float>(Float32, pVal, dim);
}
void Register::setValFloat32Array2D(const float* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<float>(Float32, pVal, dim1, dim2);
}

void Register::setValFloat64(double val)
{
    setVal<double>(Float64, val);
}
void Register::setValFloat64Array(const double* pVal, uint32_t dim)
{
    setValArray<double>(Float64, pVal, dim);
}
void Register::setValFloat64Array2D(const double* pVal, uint32_t dim1, uint32_t dim2)
{
    setValArray2D<double>(Float64, pVal, dim1, dim2);
}

// .........................................................................
std::string Register::getName()
{
    return name_;
}
AccessArea Register::getAccessArea()
{
    return accessArea_;
}
unsigned long Register::getDimension(uint16_t whichDim)
{
    return ( (whichDim == 2) ? dimension2_ : dimension1_);
}
uint32_t Register::getDimension1()
{
    return dimension1_;
}
uint32_t Register::getDimension2()
{
    return dimension2_;
}
uint32_t Register::getLength()
{
    return static_cast<uint32_t>(length_);
}
;
FormatType Register::getFormat()
{
    return format_;
}
std::string Register::getBlockName()
{
    return blockName_;
}
std::string Register::getFormatAsString()
{
    return FormatTypeString[format_];
}

bool Register::isReadable()
{
    return (accessType_ != Output);
}
bool Register::isWritable()
{
    return (accessType_ != Input);
}

bool Register::hasInputAccess()
{
    return (accessType_ != Output);
}
bool Register::hasOutputAccess()
{
    return (accessType_ != Input);
}
bool Register::isIORegister()
{
    return (accessArea_ == Digital || accessArea_ == Analog);
}
bool Register::isDIRegister()
{
    return (accessArea_ == Digital && accessType_ == Input);
}
bool Register::isDORegister()
{
    return (accessArea_ == Digital && accessType_ == Output);
}
bool Register::isAIRegister()
{
    return (accessArea_ == Analog && accessType_ == Input);
}
bool Register::isAORegister()
{
    return (accessArea_ == Analog && accessType_ == Output);
}
bool Register::isMemoryRegister()
{
    return (accessArea_ == Memory);
}
bool Register::isConfiguration()
{
    return isConfiguration_;
}
bool Register::isScalar()
{
    return ( (dimension1_ == 1) && (dimension2_ == 1));
}
bool Register::isSingleArray()
{
    return ( (dimension1_ > 1) && (dimension2_ == 1));
}
bool Register::isDoubleArray()
{
    return (dimension2_ > 1);
}
bool Register::isInitialized()
{
    return isInitialized_;
}

std::string Register::whichAccessArea(AccessArea area)
{
    return Block::whichAccessArea(area);
}

FormatType Register::whichFormatType(std::string type)
{
    StringUtilities::toLower(type);
#ifndef __x86_64__
    //SLC5 (32bits platform) still uses deprecated types ========
    if (type == "byte") return uChar;
    else if (type == "int") return Short;
    else if (type == "word") return uShort;
    else if (type == "dint") return Long;
    else if (type == "dword") return uLong;
    else if (type == "real") return Float;
#else
    //SLC6 (64bits platform) ====================================
    if (type == "byte")
        return uInt8;
    else if (type == "int")
        return Int16;
    else if (type == "word")
        return uInt16;
    else if (type == "dint")
        return Int32;
    else if (type == "dword")
        return uInt32;
    else if (type == "real")
        return Float32;
#endif
    // ANY platform =============================================
    if (type == "char")
        return Char;

    else if (type == "dt")
        return Date;
    else if (type == "date")
        return Date;

    else if (type == "int8")
        return Int8;
    else if (type == "uint8")
        return uInt8;
    else if (type == "int16")
        return Int16;
    else if (type == "uint16")
        return uInt16;
    else if (type == "int32")
        return Int32;
    else if (type == "uint32")
        return uInt32;
    else if (type == "int64")
        return Int64;
    else if (type == "uint64")
        return uInt64;
    else if (type == "float32")
        return Float32;
    else if (type == "float64")
        return Float64;
    else if (type == "string")
        return String;
    else
        throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_FORMAT_TYPE, type);
}

std::string Register::getValAsByteString(void* pValue, unsigned long maxSize)
{
    std::ostringstream os;
    std::string** pStringValue = static_cast<std::string**>(pValue);
    unsigned long nbElement = dimension1_ * dimension2_;
    unsigned long byteSize = nbElement * size_;
    unsigned long nbLoop = (getFormat() == String) ? nbElement : byteSize;

    for (unsigned int i = 0; i < nbLoop; i++)
    {
        if ( (i == maxSize) && (nbLoop > 2 * maxSize))
        {
            i = static_cast<unsigned int>(nbLoop - maxSize);
            os << ".... ";
        }

        if (getFormat() == String)
        {
            os << *pStringValue[i] << " ";
        }
        else
        {
            os << (unsigned int) ((unsigned char*)pValue)[i] << " ";
        }
    }
    return os.str();
}

void Register::setScalarfromString(std::string stringValue)
{
    if(!isScalar())
        throw SilecsException(__FILE__, __LINE__,"The type of register " + name_ + " is not a scalar type.");

    switch(format_)
    {
        case uInt8:
        {
            //Casting to uint16 and later to uint 8 to avoid it being converted for his ASCII character
            uint16_t val = 0;
            if(!StringUtilities::from_string<uint16_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to uInt8 failed for register '" + name_ + "'");
            setValUInt8((uint16_t)val);
            break;
        }
        case Int8:
        {
            int8_t val;
            if(!StringUtilities::from_string<int8_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to Int8 failed for register '" + name_ + "'");
            setValInt8(val);
            break;
        }
        case uInt16:
        {
            uint16_t val;
            if(!StringUtilities::from_string<uint16_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to uInt16 failed for register '" + name_ + "'");
            setValUInt16(val);
            break;
        }
        case Int16:
        {
            int16_t val;
            if(!StringUtilities::from_string<int16_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to Int16 failed for register '" + name_ + "'");
            setValInt16(val);
            break;
        }
        case uInt32:
        {
            uint32_t val;
            if(!StringUtilities::from_string<uint32_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to uInt32 failed for register '" + name_ + "'");
            setValUInt32(val);
            break;
        }
        case Int32:
        {
            int32_t val;
            if(!StringUtilities::from_string<int32_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to Int32 failed for register '" + name_ + "'");
            setValInt32(val);
            break;
        }
        case uInt64:
        {
            uint64_t val;
            if(!StringUtilities::from_string<uint64_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to uInt64 failed for register '" + name_ + "'");
            setValUInt64(val);
            break;
        }
        case Int64:
        {
            int64_t val;
            if(!StringUtilities::from_string<int64_t>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to Int64 failed for register '" + name_ + "'");
            setValInt64(val);
            break;
        }
        case Float32:
        {
            float val;
            if(!StringUtilities::from_string<float>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to Float32 failed for register '" + name_ + "'");
            setValFloat32(val);
            break;
        }
        case Float64:
        {
            double val;
            if(!StringUtilities::from_string<double>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to Float64 failed for register '" + name_ + "'");
            setValFloat64(val);
            break;
        }
        case String:
        {
            setValString(stringValue);
            break;
        }
        case Date:
        {
            double val;
            if(!StringUtilities::from_string<double>(val, stringValue, std::dec))
                throw SilecsException(__FILE__, __LINE__,"Conversion from string '" + stringValue + "'to Date failed for register '" + name_ + "'");
            setValDate(val);
            break;
        }
        default:
            throw SilecsException(__FILE__, __LINE__, "Register '" + name_+ "' has a unknown format type");
    }
}



std::string Register::getValAsString(void* pValue, unsigned long i, unsigned long j)
{
    struct tm dscst;
    std::ostringstream os;

    //get element whatever is the value dimension (scalar, single array or flat 2D-array)
    unsigned long index = (i * dimension2_) + j;

    if ( (i >= dimension1_) || (j >= dimension2_))
    {
        throw SilecsException(__FILE__, __LINE__, PARAM_ARRAY_DIMENSION_MISMATCH, getName());
    }

    switch (format_)
    { // -------- Deprecated types (32bits platform only)
      //case Char:	os << (char)((char*)pValue)[index]; 					break;
      //case uChar:	os << (unsigned int)((unsigned char*)pValue)[index];	break;
      //case Short:	os << (short)((short*)pValue)[index];					break;
      //case uShort:	os << (unsigned short)((unsigned short*)pValue)[index];	break;
      //case Long:	os << (long)((long*)pValue)[index];						break;
      //case uLong:	os << (unsigned long)((unsigned long*)pValue)[index];	break;
      //case Float:	os << (float)((float*)pValue)[index];					break;
      //-------- Supported types (32 and 64bits platform)
        case Int8:
            os << (int8_t) ((int8_t*)pValue)[index];
            break;
        case uInt8:
            os << (uint16_t) ((uint8_t*)pValue)[index];
            break;
        case Int16:
            os << (int16_t) ((int16_t*)pValue)[index];
            break;
        case uInt16:
            os << (uint16_t) ((uint16_t*)pValue)[index];
            break;
        case Int32:
            os << (int32_t) ((int32_t*)pValue)[index];
            break;
        case uInt32:
            os << (uint32_t) ((uint32_t*)pValue)[index];
            break;
        case Int64:
            os << (int64_t) ((int64_t*)pValue)[index];
            break;
        case uInt64:
            os << (uint64_t) ((uint64_t*)pValue)[index];
            break;
        case Float32:
            os << (float) ((float*)pValue)[index];
            break;
        case Float64:
            os << (double) ((double*)pValue)[index];
            break;
        case String:
        {
            std::string** pStringValue = static_cast<std::string**>(pValue);
            os << * (pStringValue[index]);
            break;
        }
        case Date:
        {
            dscst = getTMDate( ((double*)pValue)[index]);
            os << dscst.tm_year + 1900 << "-" << dscst.tm_mon + 1 << "-" << dscst.tm_mday << "-" << dscst.tm_hour << ":" << dscst.tm_min << ":" << dscst.tm_sec << " ";
            break;
        }
        default:
            throw SilecsException(__FILE__, __LINE__, DATA_UNKNOWN_FORMAT_TYPE, StringUtilities::toString(format_));
    }
    return os.str();
}

std::string Register::getInputValAsString()
{
    return getInputValAsString(0, 0);
}
;
std::string Register::getInputValAsString(unsigned long i)
{
    return getInputValAsString(i, 0);
}
;
std::string Register::getInputValAsString(unsigned long i, unsigned long j)
{
    if (isReadable())
    {
        return getValAsString(pRecvValue_, i, j);
    }
    else
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
}

std::string Register::getOutputValAsString()
{
    return getOutputValAsString(0, 0);
}
;
std::string Register::getOutputValAsString(unsigned long i)
{
    return getOutputValAsString(i, 0);
}
;
std::string Register::getOutputValAsString(unsigned long i, unsigned long j)
{
    if (isWritable())
    {
        return getValAsString(pSendValue_, i, j);
    }
    else
        throw SilecsException(__FILE__, __LINE__, DATA_READ_ACCESS_TYPE_MISMATCH, getName());
}

std::string Register::dumpInputVal(bool asAsciiChar)
{
    std::ostringstream os;
    unsigned long byteSize = dimension1_ * dimension2_ * size_;
    for (unsigned int i = 0; i < byteSize; i++)
    {
        if (asAsciiChar)
        {
            // Returns when end of string is found
            // TODO Release 4.0.0: add this check only for String registers
            if ( ((char) ((unsigned char*)pRecvValue_)[i]) == 0) // returns as soon as it reaches the end of the string
                break;
            os << (char) ((unsigned char*)pRecvValue_)[i];
        }
        else
            os << hex << (unsigned int) ((unsigned char*)pRecvValue_)[i] << " ";
    }
    return os.str();
}

std::string Register::dumpOutputVal(bool asAsciiChar)
{
    std::ostringstream os;
    unsigned long byteSize = dimension1_ * dimension2_ * size_;
    for (unsigned int i = 0; i < byteSize; i++)
    {
        if (asAsciiChar)
            os << (char) ((unsigned char*)pSendValue_)[i];
        else
            os << hex << (unsigned int) ((unsigned char*)pSendValue_)[i] << " ";
    }
    return os.str();
}

struct tm Register::getTMDate(double date)
{
    struct tm dscst;
    time_t dt = (time_t)date;
    localtime_r(&dt, &dscst);
    return dscst;
}

time_t Register::getEpochDate(double date)
{
    return static_cast<time_t>(round(date));
}

timeval Register::getTimeStamp()
{
    if (isReadable())
        return tod_;
    throw SilecsException(__FILE__, __LINE__, PARAM_NO_TIME_STAMP, getName());
}

std::string Register::getTimeStampAsString()
{
    if (isReadable())
    {
        std::ostringstream os;
        double ts = (double)tod_.tv_sec + (double)tod_.tv_usec / 1000000.0; //second
        os << std::setprecision(20) << ts << "s";
        return os.str();
    }
    else
        return "not defined";
}

void Register::printVal()
{
    std::ostringstream os;
    os << getName() << " " << getFormatAsString() << "[" << dimension1_ << "][" << dimension2_ << "] ";
    if (getFormat() == String)
        os << ", string-length: " << getLength();

    if (isReadable())
    {
        os << ", input: ";
        for (unsigned int i = 0; i < dimension1_; i++)
            for (unsigned int j = 0; j < dimension2_; j++)
                os << getValAsString(pRecvValue_, i, j) << " ";
    }

    if (isWritable())
    {
        os << ", output: ";
        for (unsigned int i = 0; i < dimension1_; i++)
            for (unsigned int j = 0; j < dimension2_; j++)
                os << getValAsString(pSendValue_, i, j) << " ";
    }
    std::cout << os.str() << std::endl;
}

} // namespace
