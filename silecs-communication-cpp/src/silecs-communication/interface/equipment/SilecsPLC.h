/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_PLC_H_
#define _SILECS_PLC_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/core/Diagnostic.h>
#include <silecs-communication/interface/utility/Thread.h>

#include <stdint.h>
#include <string>

namespace Silecs
{
class Connection;
class Cluster;
class Device;
class Block;
class WrapperAction;

/// @cond
typedef std::vector<std::pair<std::string, Device*> > deviceVectorType;
typedef std::vector<Block*> blockVectorType;
typedef std::map<std::string, Connection*> connMapType;

// PLC type, brand, system, protocol type and mode
typedef enum
{
    BusController,
    BusCoupler
} PLCType;

// PLC brand, system, protocol type and mode
typedef enum
{
    Siemens,
    Schneider,
    Ni,
    Digi,
    Beckhoff
} PLCBrand;

typedef enum
{
    ET200S,
    S7300,
    S7400,
    S71200,
    S71500,
    S7VIRTUAL,
    Premium,
    Quantum,
    M340,
    BC9xxx,
    CX9xxx,
    BK9xxx,
    RCM4010,
    RCM2000,
    CompactRIO,
    PXIRT,
    PXIWindows,
    PCWindows,
    OtherSupportCNV
} PLCModel;

typedef enum
{
    Step7,
    TiaPortal,
    Unity,
    TwinCat,
    StdC,
    ServerS7,
    Labview
} PLCSystem;

typedef enum
{
    MBProtocol,
    S7Protocol,
    CNVProtocol
} ProtocolType;

typedef enum
{
    BlockMode,
    DeviceMode
} ProtocolMode;
/// @endcond

// PLC unit status
typedef struct
{
    int status; //bit-pattern hardware specific (look at the related documentation)
} UnitStatusType;

// Order code
typedef struct
{
    std::string code;
    std::string version;
} UnitCodeType;

// CPU Info
typedef struct
{
    std::string moduleName;
    std::string moduleTypeName;
    std::string serialNumber;
    std::string asName;
    std::string copyright;
} CPUInfoType;

// CP Info
typedef struct
{
    int maxPduLength;
    int maxConnections;
    int maxMPIRate;
    int maxBusRate;
} CPInfoType;

/*!
 * \class PLC
 * \brief This object provides all services related to a PLC. It provides the information
 * of the PLC hardware configuration, connection status and diagnostic.
 * It maintains a reference of all the PLC devices.
 * It can be used to trig the data-block exchanges for all devices of the PLC at the same time.
 */
class PLC
{

public:
    /// @cond
    /*!
     * \return List of the related Cluster instances (Class/Version) for the PLC.
     */
    std::vector<std::string> getDeviceList();

    deviceVectorType& getDeviceMap();
    /// @endcond

    // Diagnostic features ----------------------------------------

    /*!
     * \brief Returns the PLC network name
     * \return PLC host-name
     */
    std::string getName();

    /*!
     * \brief Returns the PLC network address
     * \return PLC TCP-IP address
     */
    std::string getIPAddress();

    /*!
     * \brief Returns the PLC configuration domain: CPS, PSB, ADE, .., TST
     * \return Accelerator domain
     */
    std::string getDomain();

    /*!
     * \brief Returns the PLC manufacturer: SIEMENS, SCHNEIDER, BECKHOFF, RABBIT, NI
     * \return PLC brand
     */
    std::string getBrand();

    /*!
     * \brief Returns the PLC development system: STEP-7, TIA-PORTAL, UNITY Pro, TWINCat, Standard-C, Standard-Cpp, Labview
     * \return PLC development system
     */
    std::string getSystem();

    /*!
     * \brief Returns PLC hardware model: UNITY_Premium, SIMATIC_S7-300, ..
     * \return PLC hardware model
     */
    std::string getModel();

    /*!
     * \brief Returns PLC type: bus coupler or bus controller
     * \return PLC type
     */
    PLCType getPLCType();

    /*!
     * \brief Returns the protocol type used for the PLC communication: MODBUS-TCP, S7-TCP, CNV-TCP
     * \return PLC protocol type
     */
    std::string getProtocolType();

    /*!
     * \brief Returns the SILECS mode used to map the data within the PLC memory: DEVICE_MODE, BLOCK_MODE
     * \return PLC protocol mode
     */
    std::string getProtocolMode();

    /*!
     * \deprecated Use getMemBaseAddress() instead
     * \brief Returns the memory base address of the SILECS configuration within the PLC
     * \return DB number for SIMATIC-S7
     * \return 16bits absolute memory address for UNITY Premium/Quantum
     * \return 32bits absolute memory address for UNITY M340
     */
    unsigned long getBaseAddress() __attribute__ ((deprecated));

    /*!
     * \brief Returns the memory base address of the SILECS configuration within the PLC
     * \return DB number for SIMATIC-S7
     * \return 16bits absolute memory address for UNITY Premium/Quantum
     * \return 32bits absolute memory address for UNITY M340
     */
    long getMemBaseAddress();

    /*!
     * \brief Returns the digital input base address of the SILECS configuration within the PLC
     * \return byte address for SIMATIC-S7
     * \return byte address for MODBUS TCP
     * \return -1 in case of non existent address
     */
    long getDIBaseAddress();

    /*!
     * \brief Returns the digital output base address of the SILECS configuration within the PLC
     * \return byte address for SIMATIC-S7
     * \return byte address for MODBUS TCP
     * \return -1 in case of non existent address
     */
    long getDOBaseAddress();

    /*!
     * \brief Returns the analog input base address of the SILECS configuration within the PLC
     * \return byte address for SIMATIC-S7
     * \return byte address for MODBUS TCP
     * \return -1 in case of non existent address
     */
    long getAIBaseAddress();

    /*!
     * \brief Returns the analog output base address of the SILECS configuration within the PLC
     * \return byte address for SIMATIC-S7
     * \return byte address for MODBUS TCP
     * \return -1 in case of non existent address
     */
    long getAOBaseAddress();

    /*!
     * \brief Extracts SILECS release information from the client configuration parameters.
     * Provides detail about SILECS software used to generate the client documents.
     * \return SILECS release number
     */
    std::string getLocalRelease();

    /*!
     * \brief Extracts Owner information from the client configuration parameters.
     * Owner of the deployment document is not necessary responsible for the 'Generate' action
     * who can be done by any Editor of the document.
     * \return Owner name
     */
    std::string getLocalOwner();

    /*!
     * \brief Extracts the Date information from the client configuration parameters.
     * Date of the client parameters file generation.
     * \return Coordinate Universal Time (UTC)
     */
    std::string getLocalDate();

    /*!
     * \brief Extracts the Checksum information from the client configuration parameters.
     * CRC-32 Checksum relies on Classes and Deployment documents that have been used to
     * design the client configuration.
     * \return CRC32 checksum
     */
    unsigned long getLocalChecksum();

    // Miscellaneous features ----------------------------------------

    /*!
     * \brief Returns one device instance of that PLC by label or index
     * \param deviceName label or index of the device
     * \return Reference of the device object
     */
    Device* getDevice(std::string deviceName);

    /*!
     * \fn getBlock
     * \brief returns one instance of the requested block checking its access-type
     */
    Block* getBlock(const std::string blockName, AccessType accessType);

    /*!
     * \brief Used to get the rt-priority of the PLC related thread
     * \return rt-priority value
     */
    unsigned int getThreadPriority();

    /*!
     * \brief Used to set the real-time priority of the PLC related thread
     * \param rtPrio value of the priority
     */
    void setThreadPriority(unsigned int rtprio);

    /*!
     * \brief Used to Enable the Client/PLC connection and connect the PLC immediately if needed (connectNow=true).
     * Just Enabling the connection (connectNow=false) means the connection is allowed and systematically checked on each PLC access (send/recv).
     * In any case, connect() method downloads the Diagnostic data from the PLC Header memory and checks the Client/PLC configuration consistency.
     * The 'synchroMode' parameter is used to select the synchronization mode of the retentive Registers.
     * It must be used carefully since it can overwrite all the PLC Slave registers at the connection time.
     * \param connectNow = true: Connection is maintained immediately after the Header data update.
     * connectNow = false: delay the real data connection to the first access attempt (send/recv blocks).
     * \param synchroMode = NO_SYNCHRO: connect() method does not synchronize any retentive Registers at connection time.
     * synchroMode = MASTER_SYNCHRO: connect() method downloads all the Master Registers from the PLC memory and updates local Register values at connection time.
     * synchroMode = SLAVE_SYNCHRO: connect() method transmits all the local Slave Registers values to the PLC at connection time.
     * synchroMode = FULL_SYNCHRO: connect() method executes MASTER and SLAVE Registers synchronizationat connection time (see above).
     * \param compareChecksums: Set to false if you want to skip the checksum comparison. Default is true
     */
    void connect(SynchroMode synchroMode, bool connectNow, bool compareChecksums = true);

    /*!
     * \brief Used to disable the Client/PLC connection.
     * Connection is closed (if needed) and send/recv transaction are not possible anymore (PLC connection is Disabled).
     * The PLC object still exists on the Cluster and the Registers are available locally.
     */
    void disconnect();

    /*!
     * \brief Returns true if the PLC connection has been requested using connect() method.
     * \return Enable status
     */
    bool isEnabled();

    /*!
     * \brief Check the connection status and reconnect the controller if required (optional)
     * Returns true if the PLC connection is established.
     * \param reconnectNow can be used to force reconnection before checking (false by default).
     * \return Connection status
     */
    bool isConnected(bool reconnectNow = false);
    bool isSharedConnection()
    {
        return isSharedConn_;
    }

    int recvUnitCode(UnitCodeType& dataStruct);
    int recvUnitStatus(UnitStatusType& dataStruct);
    int recvCPUInfo(CPUInfoType& dataStruct);
    int recvCPInfo(CPInfoType& dataStruct);

    // true if the "recvUnitStatus" is RUN, false otherwise. Throws exception on failure
    bool isRunning();

    int sendColdRestart();
    int sendPlcStop();
    /*!
     * \brief Acquires one particular registers block of all devices of that PLC.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be acquired
     * \return 0 if operation was successful else an error code (when available).
     */
    int recv(std::string blockName);

    /*!
     * \brief Transmits the registers block to all devices of that PLC.
     * The method tries to (re)connect the PLC if the connection is not established yet or unexpectedly interrupted (network failure, PLC down, etc.).
     * It generates an Silecs::SilecsException if the PLC has not been enabled (using the connect() method).
     * \param blockName name of the block to be transmitted
     * \return 0 if operation was successful else an error code (when available).
     */
    int send(std::string blockName);

    /*!
     * \brief Copies input buffer in output buffer
     * The method copies the input buffer in the output buffer for each READ-WRITE register in the PLC for the block passed as parameter
     * \param blockName name of the block for which the copy must be performed
     */
    void copyInToOut(const std::string blockName);

    /*!
     * \brief Used to change designed attributes of data block.
     * In that version, can be used to limit dynamically size of data to be received (recv).
     * Interesting for specific application that uses general purpose data block with maximum size and adjusts the real data size at communication time.
     * Caution! This option can be used only with single device access (using DEVICE_MODE or send/recv from the Device level).
     * \param blockName name of the related block
     * \param customAddress base address (DBn or absolute in byte) of the data block to be received
     * \param customOffset offset (in byte) of the data block to be received
     * \param customSize real size (in byte) of the data block to be received
     */
    void setReadBlockAttributes(const std::string blockName, const unsigned long customAddress, const unsigned long customOffset, const unsigned long customSize);

    /*!
     * \brief Sets back the default attributes of the data block (the ones defined by the design)
     * \param blockName name of the related block
     */
    void resetReadBlockAttributes(const std::string blockName);

    /*!
     * \brief Used to change designed attributes of a data block.
     * In that version, can be used to limit dynamically size of data to be sent (send).
     * Interesting for specific application that uses general purpose data block with maximum size and adjusts the real data size at communication time.
     * Caution! This option can be used only with single device access (using DEVICE_MODE or send/recv from the Device level).
     * \param blockName name of the related block
     * \param customAddress base address (DBn or absolute in byte) of the data block to be sent
     * \param customOffset offset (in byte) of the data block to be sent
     * \param customSize real size (in byte) of the data block to be sent
     */
    void setWriteBlockAttributes(const std::string blockName, const unsigned long customAddress, const unsigned long customOffset, const unsigned long customSize);

    /*!
     * \brief Sets back the default attributes of the data block (the ones defined by the design)
     * \param blockName name of the related block
     */
    void resetWriteBlockAttributes(const std::string blockName);

    /// @cond
    /*!
     * \fn getConnection
     * \brief Use this method to either get an existing Connection object,
     *  or to create a new one for a given PLC, if not existing.
     *  Allow sharing connection pipe between different classes if required (from mapping configuration)
     * \return reference to the [shared-]connection instance of this PLC instance
     */
    Connection* createConnection(); //just to instantiate the object
    Connection* getConnection();
    static void deleteConnection(); //just to remove shared-connection objects
    /// @endcond

    const std::string getParamsFileName();

    /*!
     * \brief Used for detemining the required block buffer size in case of BLOCK_MODE. In BLOCK_MODE a single block contains a specific register for all the devices,
     * hence when calculating the required buffer size, we need to only reserve the space for the devices which actually contain this block.
     * \param blockName name of the block which should be used to count the devices
     */
    unsigned int getNrDevicesWithBlock(const std::string& blockName);

private:
    friend class Cluster;
    friend class Device;
    friend class Register;
    friend class Connection;
    friend class S7Connection;
    friend class MBConnection;
    friend class SNAP7Connection;

    // export plcConn_ to Silecs Action
    friend class Block;

    friend class PLCBlock;
    friend class InputBlock;
    friend class OutputBlock;
    friend class PLCRegister;
    friend class TwinCATRegister;
    friend class PLCRecvBlockMode;
    friend class PLCRecvDeviceMode;
    friend class PLCSendBlockMode;
    friend class PLCSendDeviceMode;

    friend class CNVBlock;
    friend class CNVConnection;
    friend class CNVInputBlock;
    friend class CNVOutputBlock;
    friend class CNVRegister;
    friend class CNVRecvBlockMode;
    friend class CNVRecvDeviceMode;
    friend class CNVSendBlockMode;
    friend class CNVSendDeviceMode;

    PLC(Cluster* theCluster, std::string plcName, std::string plcIPaddr, string parameterFile = "");
    virtual ~PLC();

    inline PLCType& getTypeID()
    {
        return typeID_;
    }
    inline PLCBrand& getBrandID()
    {
        return brandID_;
    }
    inline PLCModel& getModelID()
    {
        return modelID_;
    }
    inline PLCSystem& getSystemID()
    {
        return systemID_;
    }
    inline ProtocolMode& getProtocolModeID()
    {
        return protocolModeID_;
    }
    inline ProtocolType& getProtocolTypeID()
    {
        return protocolTypeID_;
    }

    void updateLocalData();

    /*!
     * \fn updateHeader
     * \brief This method is used to update the unique 'SilecsHeader' Device data of the PLC config.
     * It consists to create the corresponding Cluster if needed, connect the current PLC,
     * upload the diagnostic block registers ('hdrBlk') then disconnect the PLC.
     * \return True if uploading was correctly done.
     */
    bool updateHeader();

    /*!
     * \fn recvAsync/ recvWait
     * \brief Acquires the 'blockname' data block of all devices of that PLC using asynchronous mode
     */
    void recvAsync(std::string blockName);

    /*!
     * \fn sendAsync/ sendWait
     * \brief Sends the 'blockname' data block to all devices of that PLC using asynchronous mode
     */
    void sendAsync(std::string blockName);

    /*!
     * \fn waitAsync
     * \brief Wait for asynchronous call completion (recv/send)
     */
    void waitAsync();

    /*!
     * \fn whichPLC, whichProtocol...
     * \return the enumeration value of the given Protocol type/mode string
     */
    static PLCType whichPLCType(std::string model); //BusController, BusCoupler
    static PLCBrand whichPLCBrand(std::string brand); //SIEMENS, SCHNEIDER, BECKHOFF, ...
    static PLCModel whichPLCModel(std::string model); //S7-400, S7-300, ...
    static PLCSystem whichPLCSystem(std::string system); //STEP-7, UNITY, TWINCAT, ...
    static ProtocolType whichProtocolType(std::string system); //S7, Modbus, CNV, ...
    static ProtocolMode whichProtocolMode(std::string mode); //Device or Block

    /*!
     * \fn ExtractDatabase
     * \brief Parse the Silecs params file and extract the configuration of that PLC.
     */
    void extractDatabase();

    /*!
     * \brief Provides the  map of the Block objects attached to the underlying PLC(s)
     * \return Block object map
     */
    blockVectorType& getBlockCol();

    /*!
     * \fn updateStatus
     * \brief update all the status variables of the PLC.
     */
    void updateStatus();

    /// Parent cluster reference of that PLC
    Cluster* theCluster_;

    /// PLC attributes
    std::string name_;
    std::string IPaddr_;
    std::string domain_; //TST, ADE, PSB, CPS, ..
    std::string brand_; //SIEMENS, SCHNEIDER, ..
    std::string system_; //STEP-7, UNITY, TWINCAT, ..
    std::string model_; //SIMATIC S7-300, Premium, ..
    std::string protocolMode_; //Device, Block
    std::string protocolType_; //MODBUS-TCP, S7-TCP, ..

    PLCType typeID_; //BusController (with CPU), BusCoupler (pure IO)
    PLCBrand brandID_; //Siemens, Schneider, ... enumeration
    PLCModel modelID_; //S7-400, S7-300, ... enumeration
    PLCSystem systemID_; //Step7, Unity, TwinCat, ... enumeration
    ProtocolMode protocolModeID_; //BlockMode, DeviceMode enumeration
    ProtocolType protocolTypeID_; //Modbus, S7 enumeration

    long baseMemAddr_;
    long baseDIAddr_;
    long baseDOAddr_;
    long baseAIAddr_;
    long baseAOAddr_;
    std::string usedMem_;

    string parameterFile_;

    // Unique instance of the Header Device for diagnostic purpose
    Device* theHeader_;

    /* Diagnostic registers are used to check the consistency between the
     * PLC memory (remote data) and the client parameters (local data).
     */
    //Local data, extracted from the parameters file
    std::string localRelease_; //Release number of the SILECS software
    std::string localOwner_; //Owner of the deployment document
    std::string localDate_; //Date of the Generation (Client parameters and PLC Sources)
    uint32_t localChecksum_; //Checksum of the deployed configuration

    //Remote data, uploaded from the PLC memory
    std::string remoteVersion_;
    std::string remoteOwner_;
    std::string remoteDate_;
    uint32_t remoteChecksum_;

    Context* allDevicesTransaction_;
    Context* allDevicesSynchronization_;

    /// Related thread allowing cluster PLCs parallelism
    Thread<WrapperAction>* pThread_;

    /// Device collection of the PLC
    deviceVectorType deviceCol_;

    /// Block collection of the PLC listed by name.
    blockVectorType blockCol_;

    /// Object to manage the plc communication
    /* By default, we instantiate 1 connection (2 channels: r/w) per PLC/Cluster
     * In order to safe PLC resources it's possible to share 1 connection between clusters of the same PLC.
     * A connection ID is used to define if the connection is shared or not.
     */
    Connection* plcConn_; // the connection object that can be individual per PLC/Cluster or shared
    std::string connID_; // use to identify the connection object in the map (connMap_):
                         // individual connection:  <plcName>+<ClassName>
                         // shared connection: <plcName>+"SharedConnection"
    bool isSharedConn_; // use to know if plcConn_ is shared with others Cluster or not

    /// Connection collection to allow sharing connection channels between different clusters
    static connMapType connMap_;

    // synchronization mode for each occurence of PLC/Cluster connection: [NO|FULL|SLAVE|MASTER]_SYNCHRO
    SynchroMode synchroMode_;

    /// Diagnostic status
    Status status_;
};

} // namespace

#endif // _SILECS_PLC_H_
