/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_BLOCK_H_
#define _SILECS_BLOCK_H_

#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/utility/Thread.h>

#include <boost/shared_ptr.hpp>

namespace Silecs
{
class Action;
class WrapperAction;
class PLC;
class ElementXML;

/*! -----------------------------------------------------------------------
 * \class Block
 * \brief This object maintains a reference of all the device registers.
 * It is responsible to serialize, de-serialize data for the network transfer
 * (alignment, data swapping, ..). It can be used also to trig the communication
 * for a particular device (make sense for DEVICE-MODE only) if needed.
 */
class Block
{

public:
    Block(PLC* thePLC, ElementXML blockNode, AccessType accessType);
    virtual ~Block();

    /*!
     * \fn whichAccessType
     * \return the enumeration value of the given access-type string
     */
    static AccessType whichAccessType(std::string type);
    static std::string whichAccessType(AccessType type);

    /*!
     * \fn whichAccessArea
     * \return the enumeration value of the given access-area string
     */
    static AccessArea whichAccessArea(std::string area);
    static std::string whichAccessArea(AccessArea area);

    inline bool hasInputAccess()
    {
        return (accessType_ != Output);
    }
    inline bool hasOutputAccess()
    {
        return (accessType_ != Input);
    }

    inline bool isReadable()
    {
        return (accessType_ != Output);
    }
    inline bool isWritable()
    {
        return (accessType_ != Input);
    }
    inline bool isConfiguration()
    {
        return configuration_;
    }
    inline PLC* getPLC()
    {
        return thePLC_;
    }
    inline std::string& getName()
    {
        return name_;
    }
    inline AccessType getAccessType()
    {
        return (accessType_);
    }
    inline AccessArea getAccessArea()
    {
        return (accessArea_);
    }
    inline unsigned long& getAddress()
    {
        return address_;
    }
    inline unsigned long& getMemSize()
    {
        return memSize_;
    }
    inline Task<WrapperAction>* getTask()
    {
        return pTask_;
    }
    inline void* getBuffer()
    {
        return pBuffer_;
    }

    /// @cond
    void setCustomAttributes(const unsigned long customAddress, const unsigned long customOffset, const unsigned long customSize);
    void resetCustomAttributes();
    inline bool& withCustomAttributes()
    {
        return customAttributes_;
    }
    inline unsigned long& getCustomAddress()
    {
        return customAddress_;
    }
    inline unsigned long& getCustomOffset()
    {
        return customOffset_;
    }
    inline unsigned long& getCustomSize()
    {
        return customSize_;
    }
    /// @endcond

protected:
    friend class Device;

    /// Parent PLC reference of that device
    PLC* thePLC_;

    /// Block attributes
    std::string name_;

    /// Block access-type: Input, Output or InOut
    AccessType accessType_;

    /// Block access-type: Memory, Digital or Analog
    AccessArea accessArea_;

    /// Block only consists of configuration registers (write once to controller at startup, afterwards only read from controller)
    bool configuration_;

    /// Block size
    unsigned long size_;

    /// Block memory size (including alignments)
    unsigned long memSize_;

    /* Block custom attributes
     * Can be used to limit dynamically the size of sent/received data.
     * Interesting for specific application. (e.g. Cryo) which design maximum size of the block and adjust
     * the real data size at runtime (using PLC::setCustomBlockAttributes() method.
     * !! This mechanism can be used only with device access mode (DEVICE-MODE or BLOCK-MODE from device)
     */
    // Flag used to enable/disable use of custom attributes
    bool customAttributes_;
    unsigned long customAddress_;
    unsigned long customOffset_;
    unsigned long customSize_;

    unsigned long address_;

    /// Related PLC task to this data-block
    Action* pAction_;
    Task<WrapperAction>* pTask_;

    // Send/Receive data buffer
    void *pBuffer_;
};

} // namespace

#endif // _SILECS_BLOCK_H_
