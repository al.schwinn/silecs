/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#ifndef _SILECS_REGISTER_H_
#define _SILECS_REGISTER_H_

#include <stdint.h>
#include <boost/shared_ptr.hpp>
#include <string>

namespace Silecs
{
class PLC;
class Device;
class ElementXML;

/// @cond
// Register supported format
static const std::string FormatTypeString[] = { //!!Must be synchronized with FormatType enumeration

//"uChar",	/*!<: SLC5 deprecated: 8-bits unsigned char	 = PLC BYTE	*/
//"Char",		/*!<: 8-bits ascii char 					 = PLC CHAR	*/
//"uShort",	/*!<: SLC5 deprecated: 16-bits unsigned int	 = PLC WORD	*/
//"Short",	/*!<: SLC5 deprecated: 16-bits signed int 	 = PLC INT	*/
//"uLong",	/*!<: SLC5 deprecated: 32-bits unsigned long = PLC DWORD*/
//"Long",		/*!<: SLC5 deprecated: 32-bits signed long	 = PLC DINT	*/
//"Float",	/*!<: SLC5 deprecated: 32-bits signed float	 = PLC REAL	*/

"uInt8", /*!<: 8-bits unsigned integer*/
"Int8", /*!<: 8-bits integer*/
"uInt16", /*!<: 16-bits unsigned integer*/
"Int16", /*!<: 16-bits integer*/
"uInt32", /*!<: 32-bits unsigned integer*/
"Int32", /*!<: 32-bits integer*/
"Float32",/*!<: 32-bits signed float*/
"Date", /*!<: 64-bits time (elapsed time since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)	= dt  */

"uInt64", /*!<: 64-bits unsigned integer*/
"Int64", /*!<: 64-bits integer*/
"Float64", /*!<: 64-bits signed float*/
"String" /*!<: String of ASCII characters*/
};

/*!
 * \brief Enumeration for the Register data type. Used by Register::getFormat() method.
 * Each type has a PLC type equivalent (see below)
 */
typedef enum
{
    //!!Must be synchronized with FormatTypeString[]
    uChar = 0, /*!<: SLC5 deprecated: 8-bits unsigned char	 = PLC BYTE	*/
    Char = 1, /*!<: 8-bits ascii char 					 = PLC CHAR	*/
    uShort = 2, /*!<: SLC5 deprecated: 16-bits unsigned int	 = PLC WORD	*/
    Short = 3, /*!<: SLC5 deprecated: 16-bits signed int 	 = PLC INT	*/
    uLong = 4, /*!<: SLC5 deprecated: 32-bits unsigned long = PLC DWORD*/
    Long = 5, /*!<: SLC5 deprecated: 32-bits signed long	 = PLC DINT	*/
    Float = 6, /*!<: SLC5 deprecated: 32-bits signed float	 = PLC REAL	*/
    Date = 7, /*!<: 64-bits double (elapsed time since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)	= dt  */

    uInt8 = 0, /*!<: 8-bits unsigned integer*/
    Int8 = 1, /*!<: 8-bits integer*/
    uInt16 = 2, /*!<: 16-bits unsigned integer*/
    Int16 = 3, /*!<: 16-bits integer*/
    uInt32 = 4, /*!<: 32-bits unsigned integer*/
    Int32 = 5, /*!<: 32-bits integer*/
    Float32 = 6,/*!<: 32-bits signed float*/

    uInt64 = 8, /*!<: 64-bits unsigned integer*/
    Int64 = 9, /*!<: 64-bits integer*/
    Float64 = 10, /*!<: 64-bits signed float*/
    String = 11

} FormatType;

/*!
 * \class Register
 * \brief This object stores the final device data.
 * It provides interface to update and extract the register value.
 * The Register name is unique within the scope of the device class.
 */
class Register
{
public:

    /*!
     * \brief Returns reference of the Device where the register is instantiated
     * \return Reference of the Device object
     */
    Device* getDevice();

    /*!
     * \brief Returns the register name as defined in the Class design
     * \return Register name
     */
    std::string getName();

    /*!
     * \brief Returns the register access area (same as the related block as defined in the Class design)
     * \return Register area: Memory, Digital, Analog
     */
    AccessArea getAccessArea();

    /*!
     * \brief Returns the format of the register data
     * \return value from the FormatType enumeration
     */
    FormatType getFormat();

    /*!
     * \brief Returns number of elements in case of array register (1 in case of scalar register)
     * \return Register dimension1
     */
    unsigned long getDimension(uint16_t whichDim = 1 /*1,2*/); //for backward compatibility getDimension() ~= getDimension1()
    uint32_t getDimension1();

    /*!
     * \brief Returns second dimension in case of 2D-array (1 in case of array register)
     * \return Register dimension2
     */
    uint32_t getDimension2();

    /*!
     * \brief Returns length of string in case of a string/array of strings register
     * \return Register length
     */
    uint32_t getLength();

    /*!
     * \brief Returns the name of the block which contains the register as defined in the Class design
     * \return Block name of the register
     */
    std::string getBlockName();

    /*!
     * \brief Time-of-day: number of seconds and microseconds since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)
     * Use to time-stamp the register coming from PLC (no time-stamping on send).
     * FEC clock and Timing are synchronized using SNTP (common GPS source)
     * \return Register time-stamping as timeval structure
     */
    timeval getTimeStamp();

    /*!
     * \brief A register can have read-only, write-only or read-write access mode as defined in the Class design.
     * \return true if register has read-only or read-write access mode
     */
    bool hasInputAccess();
    bool isReadable();

    /*!
     * \brief A register can have read-only, write-only or read-write access mode as defined in the Class design.
     * \return true if the register has write-only or read-write access mode
     */
    bool hasOutputAccess();
    bool isWritable();

    /*!
     * \brief A register can be Memory or IO peripheral register.
     * \return true if the register is an IO register (Digital, Analog)
     */
    bool isIORegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is digital input register
     */
    bool isDIRegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is digital output register
     */
    bool isDORegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is analog input register
     */
    bool isAIRegister();

    /*!
     * \brief A register can belong to either digital or analog IO
     * \return true if the register is analog output register
     */
    bool isAORegister();

    /*!
     * \brief A register can be Memory or IO peripheral register.
     * \return true if the register is a Memory register
     */
    bool isMemoryRegister();

    /*!
     * \brief A register that has constant value is a configuration.
     * Its given default-value is set to the controller at start-up.
     * \return if the register is a configuration register
     */
    bool isConfiguration();

    /*!
     * \brief A scalar is a single value register (dimension1_ and dimension2_ attributes == 1)
     * \return true if register is a scalar
     */
    bool isScalar();

    /*!
     * \brief A single array is a register with dimension1_ attribute > 1
     * \return true if register is a single array
     */
    bool isSingleArray();

    /*!
     * \brief A double array is a register with dimension2_ attribute > 1
     * \return true if register has second dimension
     */
    bool isDoubleArray();

    // GET methods =============================================================

    // .........................................................................
    //Deprecated: initially for SLC5 (32bits platform)

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a char format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt8 instead.
     */
    char getValChar();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a char* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt8Array instead.
     */
    void getValCharArray(char* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a char* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt8Array2D instead.
     */
    void getValCharArray2D(char* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a char* reference directly to the memory location where the array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefInt8Array instead.
     */
    const char* getRefCharArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a char* reference directly to the memory location where the flat 2D-array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefInt8Array2D instead.
     */
    const char* getRefCharArray2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a unsigned char format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt8 instead.
     */
    unsigned char getValUChar();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a unsigned char* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt8Array instead.
     */
    void getValUCharArray(unsigned char* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a unsigned char* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt8Array2D instead.
     */
    void getValUCharArray2D(unsigned char* pVal, uint32_t dim1, uint32_t dim2);

    //specific methods with automatic conversion (JAVA does not support unsigned type)
    void getValUCharArray(short* pVal, uint32_t dim);
    void getValUCharArray2D(short* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a unsigned char* reference directly to the memory location where the array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefUInt8Array instead.
     */
    const unsigned char* getRefUCharArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a unsigned char* reference directly to the memory location where the flat 2D-array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefUInt8Array2D instead.
     */
    const unsigned char* getRefUCharArray2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a short format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt16 instead.
     */
    short getValShort();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a short* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt16Array instead.
     */
    void getValShortArray(short* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a short* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt16Array2D instead.
     */
    void getValShortArray2D(short* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a short* reference directly to the memory location where the array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefInt16Array instead.
     */
    const short* getRefShortArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array in unsigned long format
     * \return a short* reference directly to the memory location where the flat 2D-array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefInt16Array2D instead.
     */
    short* getRefShortArray2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a unsigned short format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt16 instead.
     */
    unsigned short getValUShort();
    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a unsigned short* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt16Array instead.
     */
    void getValUShortArray(unsigned short* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a unsigned short* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt16Array2D instead.
     */
    void getValUShortArray2D(unsigned short* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void getValUShortArray(long* pVal, uint32_t dim);
    void getValUShortArray2D(long* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a unsigned short* reference directly to the memory location where the array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefUInt16Array instead.
     */
    const unsigned short* getRefUShortArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array in unsigned long format
     * \return a unsigned short* reference directly to the memory location where the flat 2D-array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefUInt16Array2D instead.
     */
    const unsigned short* getRefUShortArray2D(uint32_t& dim1, uint32_t& dim2);

#ifndef __x86_64__ //long type is platform dependent (32bits or 64bits integer)
    //SLC5: support for 'long' type as 32bits
    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt32 instead.
     */
    long getValLong();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a long* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt32Array instead.
     */
    void getValLongArray(long* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a long* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValInt32Array2D instead.
     */
    void getValLongArray2D(long* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a long* reference directly to the memory location where the array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefInt32Array instead.
     */
    const long* getRefLongArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a long* reference directly to the memory location where the flat 2D-array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefInt32Array2D instead.
     */
    const long* getRefLongArray2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt32 instead.
     */
    unsigned long getValULong();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a unsigned long* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt32Array instead.
     */
    void getValULongArray(unsigned long* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a unsigned long* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValUInt32Array2D instead.
     */
    void getValULongArray2D(unsigned long* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void getValULongArray(long long* pVal, uint32_t dim);
    void getValULongArray2D(long long* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a unsigned long* reference directly to the memory location where the array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefUInt32Array instead.
     */
    const unsigned long* getRefULongArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a unsigned long* reference directly to the memory location where the flat 2D-array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefUInt32Array2D instead.
     */
    const unsigned long* getRefULongArray2D(uint32_t& dim1, uint32_t& dim2);

#else
    //from SLC6: 'long' methods still used as 32bits but with explicit typing instead
    //It's recommended to use new appropriate methods instead: Int32/UInt32
#define getValLong 			getValInt32
#define getValLongArray 	getValInt32Array
#define getRefLongArray 	getRefInt32Array

#define getValULong		 	getValUInt32
#define getValULongArray	getValUInt32Array
#define getRefULongArray	getRefUInt32Array
#endif

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a unsigned float format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValFloat32 instead.
     */
    float getValFloat();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a float* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValFloat32Array instead.
     */
    void getValFloatArray(float* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a float* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getValFloat32Array2D instead.
     */
    void getValFloatArray2D(float* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a float* reference directly to the memory location where the array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefFloat32Array instead.
     */
    const float* getRefFloatArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a float* reference directly to the memory location where the flat 2D-array begins in the buffer
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of getRefFloat32Array2D instead.
     */
    const float* getRefFloatArray2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * \return the buffer content in a unsigned double format
     */
    double getValDate();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * \param pVal a double* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValDateArray(double* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * \param pVal a double* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of 2D-array in unsigned long format
     * \param dim2 second dimension of 2D-array in unsigned long format
     */
    void getValDateArray2D(double* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim reference location where to store the array dimension in unsigned long format
     * \return a double* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    const double* getRefDateArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a double* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    double* getRefDateArray2D(uint32_t& dim1, uint32_t& dim2);

    // .........................................................................
    //Recommended: from SLC6 (32bits and 64bits platform)
    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a int8_t format
     */
    int8_t getValInt8();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a int8_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValInt8Array(int8_t* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a int8_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValInt8Array2D(int8_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in unsigned long format
     * \return a int8_t* reference directly to the memory location where the array begins in the buffer
     */
    const int8_t* getRefInt8Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 reference where to store the second dimension of flat 2D-array in unsigned long format
     * \return a int8_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    int8_t* getRefInt8Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a uint8_t format
     */
    uint8_t getValUInt8();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a uint8_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValUInt8Array(uint8_t* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a uint8_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValUInt8Array2D(uint8_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void getValUInt8Array(int16_t* pVal, uint32_t dim);
    void getValUInt8Array2D(int16_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a uint8_t* reference directly to the memory location where the array begins in the buffer
     */
    const uint8_t* getRefUInt8Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a uint8_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    uint8_t* getRefUInt8Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a int16_t format
     */
    int16_t getValInt16();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a int16_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValInt16Array(int16_t* pval, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a int16_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValInt16Array2D(int16_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a int16_t* reference directly to the memory location where the array begins in the buffer
     */
    const int16_t* getRefInt16Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a int16_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    int16_t* getRefInt16Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a uint16_t format
     */
    uint16_t getValUInt16();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a uint16_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValUInt16Array(uint16_t* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a uint16_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValUInt16Array2D(uint16_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void getValUInt16Array(int32_t* pVal, uint32_t dim);
    void getValUInt16Array2D(int32_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a uint16_t* reference directly to the memory location where the array begins in the buffer
     */
    const uint16_t* getRefUInt16Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a uint16_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    uint16_t* getRefUInt16Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a int32_t format
     */
    int32_t getValInt32();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a int32_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValInt32Array(int32_t* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a int32_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValInt32Array2D(int32_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a int32_t* reference directly to the memory location where the array begins in the buffer
     */
    const int32_t* getRefInt32Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a int32_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    int32_t* getRefInt32Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a uint32_t format
     */
    uint32_t getValUInt32();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a uint32_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValUInt32Array(uint32_t* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a uint32_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValUInt32Array2D(uint32_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void getValUInt32Array(int64_t* pVal, uint32_t dim);
    void getValUInt32Array2D(int64_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a uint32_t* reference directly to the memory location where the array begins in the buffer
     */
    const uint32_t* getRefUInt32Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a uint32_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    uint32_t* getRefUInt32Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a int64_t format
     */
    int64_t getValInt64();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a int64_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValInt64Array(int64_t* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a int8_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValInt64Array2D(int64_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a int64_t* reference directly to the memory location where the array begins in the buffer
     */
    const int64_t* getRefInt64Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a int64_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    int64_t* getRefInt64Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a uint64_t format
     */
    uint64_t getValUInt64();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a uint64_t* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValUInt64Array(uint64_t* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a uint64_t* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValUInt64Array2D(uint64_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    //void getValUInt64Array(int128_t* pVal, uint32_t dim); not supported (128bits)!
    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a uint64_t* reference directly to the memory location where the array begins in the buffer
     */
    const uint64_t* getRefUInt64Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a uint64_t* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    uint64_t* getRefUInt64Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a float format
     */
    float getValFloat32();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a float* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValFloat32Array(float* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a float* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValFloat32Array2D(float* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can sped up reading and writing operation when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in format unsigned long
     * \return a float* reference directly to the memory location where the array begins in the buffer
     */
    const float* getRefFloat32Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array  in unsigned long format
     * \return a float* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    float* getRefFloat32Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer.
     * \return the buffer content in a double format
     */
    double getValFloat64();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a double* pointer to a memory location where the method will store a copy of the array
     * \param dim vector length in unsigned long format
     */
    void getValFloat64Array(double* pVal, uint32_t dim);

    /*!
     * \brief Extracts the 2D-array of values from the register input buffer.
     * \param pVal a double* pointer to a memory location where the method will store a copy of the flat 2D-array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValFloat64Array2D(double* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method give direct access on the buffer and his adoption is not recommended.
     * \param dim Reference where to store the vector dimension in unsigned long format
     * \return a double* reference directly to the memory location where the array begins in the buffer
     */
    const double* getRefFloat64Array(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Reference where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Reference where to store the second dimension of flat 2D-array in unsigned long format
     * \return a double* reference directly to the memory location where the flat 2D-array begins in the buffer
     */
    double* getRefFloat64Array2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Extracts the current value from the register input buffer. (receive must be done ahead)
     * \return the buffer content in a string format
     */
    std::string getValString();

    /*!
     * \brief Extracts the vector of values from the register input buffer.
     * \param pVal a std::string* pointer to a memory location where the method will store a copy of the string
     * \param dim vector length in unsigned long format
     */
    void getValStringArray(std::string* pVal, uint32_t dim);

    /*!
     * \brief Extracts the string array from the register input buffer.
     * \param pVal a std::string* pointer to a memory location where the method will store a copy of the 2D (flat) array
     * \param dim1 first dimension of flat 2D-array in unsigned long format
     * \param dim2 second dimension of flat 2D-array in unsigned long format
     */
    void getValStringArray2D(std::string* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer us very big and a small part of
     * information is requested. This methos give direct access on the buffer and his adoption is not recommended.
     * \param dim Location where to store the vector dimension in unsigned long format
     * \return a str::string** reference directly to the memory location where the array of pointers begins in the buffer
     */
    const std::string** getRefStringArray(uint32_t& dim);

    /*!
     * \brief Return a reference to register input buffer.
     * The usage of this method can speed up reading and writing operations when the buffer is very big and a small part of
     * information is requested. This method gives direct access on the buffer and his adoption is not recommended.
     * \param dim1 Location where to store the first dimension of flat 2D-array in unsigned long format
     * \param dim2 Location where to store the second dimension of flat 2D-array in unsigned long format
     * \return a str::string** reference directly to the memory location where the flat 2D-array of pointers begins in the buffer
     */
    const std::string** getRefStringArray2D(uint32_t& dim1, uint32_t& dim2);

    /*!
     * \brief Converts the double Date type to tm broken-down time structure
     * conforming to POSIX convention (look at localtime() man pages)
     * \param date Floating-point number of second that has elapsed since UTC (00:00) on January 1, 1900
     * \return tm Broken-down time structure
     */
    static struct tm getTMDate(double date);

    /*!
     * \brief Converts (static-cast) the floating-point SILECS Date type to standard time_t,
     * as those returned by Linux function time().
     * \param date Floating-point number of second that has elapsed since UTC (00:00) on January 1, 1900
     * \return time_t value
     */
    static time_t getEpochDate(double date);

    // SET methods ========================================================

    // .........................................................................
    //Deprecated: initially for SLC5 (32bits platform)
    /*!
     * \brief Set the value for the register output buffer. (Send must be done afterwards)
     * \param val Value to be written in the buffer in a char format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt8 instead.
     */
    void setValChar(char val);

    /*!
     * \brief Set the value for the array register output buffer. (Send must be done afterwards)
     * \param pVal A char* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt8Array instead.
     */
    void setValCharArray(const char* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A char* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt8Array2D instead.
     */
    void setValCharArray2D(const char* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a unsigned char format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt8 instead.
     */
    void setValUChar(unsigned char val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A unsigned char* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt8Array instead.
     */
    void setValUCharArray(const unsigned char* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A unsigned char* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt8Array2D instead.
     */
    void setValUCharArray2D(const unsigned char* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void setValUCharArray(const short* pVal, uint32_t dim);
    void setValUCharArray2D(const short* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a short format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt16 instead.
     */
    void setValShort(short val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A short* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt16Array instead.
     */
    void setValShortArray(const short* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A short* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt16Array2D instead.
     */
    void setValShortArray2D(const short* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a unsigned short format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt16 instead.
     */
    void setValUShort(unsigned short val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A unsigned short* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt16Array instead.
     */
    void setValUShortArray(const unsigned short* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A unsigned short* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt16Array2D instead.
     */
    void setValUShortArray2D(const unsigned short* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void setValUShortArray(const long* pVal, uint32_t dim);
    void setValUShortArray2D(const long* pVal, uint32_t dim1, uint32_t dim2);

#ifndef __x86_64__  //long type is platform dependant (32bits or 64bits integer)
    //SLC5: support for 'long' type as 32bits
    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt32 instead.
     */
    void setValLong(long val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A long* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt32Array instead.
     */
    void setValLongArray(const long* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A long* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValInt32Array2D instead.
     */
    void setValLongArray2D(const long* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt32 instead.
     */
    void setValULong(unsigned long val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A unsigned long* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt32Array instead.
     */
    void setValULongArray(const unsigned long* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A unsigned long* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValUInt32Array2D instead.
     */
    void setValULongArray2D(const unsigned long* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void setValULongArray(const long long* pVal, uint32_t dim);
    void setValULongArray2D(const long long* pVal, uint32_t dim1, uint32_t dim2);

#else
    //from SLC6: 'long' methods still used as 32bits but with explicit typing instead
    //It's recommended to use new appropriate methods instead: Int32/UInt32
#define setValLong 			setValInt32
#define setValLongArray 	setValInt32Array
#define setValULong		 	setValUInt32
#define setValULongArray	setValUInt32Array
#endif

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a float format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValFloat32 instead.
     */
    void setValFloat(float val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A float* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValFloat32Array instead.
     */
    void setValFloatArray(const float* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A float* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     * \deprecated This method is supported for backward compatibility (version <= 3.2.*). Suggested usage of setValFloat32Array2D instead.
     */
    void setValFloatArray2D(const float* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the Date register output buffer.(Send must be done afterwards)
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * \param val Value to be written in the buffer in a double format
     */
    void setValDate(double val);

    /*!
     * \brief Set the value for the Date array register output buffer.(Send must be done afterwards)
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * \param pVal A double* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValDateArray(const double* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * Date type represents the floating-point number of second elapsed since 00:00:00
     * on January 1, 1970, Coordinated Universal Time (UTC)
     * \param pVal A double* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValDateArray2D(const double* pVal, uint32_t dim1, uint32_t dim2);

    // .........................................................................
    //Recommended: from SLC6 (32bits and 64bits platform)
    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a int8_t format
     */
    void setValInt8(int8_t val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A int8_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValInt8Array(const int8_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A int8_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValInt8Array2D(const int8_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a uint8_t format
     */
    void setValUInt8(uint8_t val);
    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A uint8_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValUInt8Array(const uint8_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A uint8_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValUInt8Array2D(const uint8_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void setValUInt8Array(const int16_t* pVal, uint32_t dim);
    void setValUInt8Array2D(const int16_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a int16_t format
     */
    void setValInt16(int16_t val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A int16_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValInt16Array(const int16_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A int16_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValInt16Array2D(const int16_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a uint16_t format
     */
    void setValUInt16(uint16_t val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A uint16_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValUInt16Array(const uint16_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A uint16_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValUInt16Array2D(const uint16_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void setValUInt16Array(const int32_t* pVal, uint32_t dim);
    void setValUInt16Array2D(const int32_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a int32_t format
     */
    void setValInt32(int32_t val);

    /*!
     * \brief Set the value for the array register output buffer. (Send must be done afterwards)
     * \param pVal A int32_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValInt32Array(const int32_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A int32_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValInt32Array2D(const int32_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer. (Send must be done afterwards)
     * \param val Value to be written in the buffer in a uint32_t format
     */
    void setValUInt32(uint32_t val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A uint32_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValUInt32Array(const uint32_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A uint32_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValUInt32Array2D(const uint32_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    void setValUInt32Array(const int64_t* pVal, uint32_t dim);
    void setValUInt32Array2D(const int64_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a int64_t format
     */
    void setValInt64(int64_t val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A int64_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValInt64Array(const int64_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A int64_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValInt64Array2D(const int64_t* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a uint64_t format
     */
    void setValUInt64(uint64_t val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A uint64_t* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValUInt64Array(const uint64_t* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A uint64_t* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValUInt64Array2D(const uint64_t* pVal, uint32_t dim1, uint32_t dim2);

    //specific method with automatic conversion (JAVA does not support unsigned type)
    //void setValUInt64Array(const int128_t* pVal, uint32_t dim); not supported (128bits)!

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a float format
     */
    void setValFloat32(float val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A float* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValFloat32Array(const float* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A float* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValFloat32Array2D(const float* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer.(Send must be done afterwards)
     * \param val Value to be written in the buffer in a double format
     */
    void setValFloat64(double val);

    /*!
     * \brief Set the value for the array register output buffer.(Send must be done afterwards)
     * \param pVal A double* pointer to a memory location from where the method will get the array values
     * \param dim Vector length in unsigned long format
     */
    void setValFloat64Array(const double* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A double* pointer to a memory location from where the method will get the flat 2D-array values
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValFloat64Array2D(const double* pVal, uint32_t dim1, uint32_t dim2);

    /*!
     * \brief Set the value for the register output buffer. (Send must be done afterwards)
     * \param val string to be written in the buffer
     */
    void setValString(std::string val);

    /*!
     * \brief Set the value for the array register output buffer. (Send must be done afterwards)
     * \param pVal A std::string* pointer to a memory location from where the method will get the array
     * \param dim Vector length in unsigned long format
     */
    void setValStringArray(const std::string* pVal, uint32_t dim);

    /*!
     * \brief Set the value for the 2D-array register output buffer. (Send must be done afterwards)
     * \param pVal A std::string* pointer to a memory location from where the method will get the flat 2D-array
     * \param dim1 First dimension of flat 2D-array in unsigned long format
     * \param dim2 Second dimension of flat 2D-array in unsigned long format
     */
    void setValStringArray2D(const std::string* pVal, uint32_t dim1, uint32_t dim2);

    // .........................................................................
    /*!
     * \brief Returns the format of the register data as string value
     * \return Format-type of the register (Enumeration: "char", "uChar", "short", "uShort", ..)
     */
    std::string getFormatAsString();

    /*!
     * \brief Returns the register time-stamping as readable value
     * \return number of seconds since the Epoch (floating point value)
     */
    std::string getTimeStampAsString();

    /*!
     * \brief Interprets the input register value and return the corresponding string.
     * In case of array or 2D-array, it return string value of the appropriate element.
     * Generates an Exception if register is write-only or if index(es) exceed the array dimension(s)
     * \param i,j indexes of the element in the (2D)array if any (optional)
     * \return input register value or element as string
     */
    std::string getInputValAsString(); //for scalar
    std::string getInputValAsString(unsigned long i); //for single array
    std::string getInputValAsString(unsigned long i, unsigned long j); //for 2D array (flat)

    /*!
     * \brief Interprets the output register value and return the corresponding string.
     * In case of array or 2D-array, it return string value of the appropriate element.
     * Generates an Exception if register is read-only or if index(es) exceed the array dimension(s)
     * \param i,j indexes of the element in the (2D)array if any (optional)
     * \return input register value or element as string
     */
    std::string getOutputValAsString(); //for scalar
    std::string getOutputValAsString(unsigned long i); //for single array
    std::string getOutputValAsString(unsigned long i, unsigned long j); //for 2D array (flat)

    /*!
     * \brief Returns the Input register value as sequence of 8-bit words
     * \param asAsciiChar = true to dump the buffer as Ascii characters (default is Hexadecimal dump)
     * \return string blank-separated value
     */
    std::string dumpInputVal(bool asAsciiChar = false);

    /*!
     * \brief Returns the Output register value as sequence of 8-bit word
     * \param asAsciiChar = true to dump the buffer as Ascii characters (default is Hexadecimal dump)
     * \return string blank-separated value
     */
    std::string dumpOutputVal(bool asAsciiChar = false);

    /*!
     * \brief Display all the register information to the standard output for debugging purpose:
     * name, format, dimension, synchro and input/output value(s).
     */
    void printVal();

    /*!
     * \fn whichAccessArea
     * \return the string of the given access-area enum
     */
    std::string whichAccessArea(AccessArea area);

    /// @cond
    /*!
     * \fn whichFormatType
     * \return Enumeration value of the given format-type string
     */
    FormatType whichFormatType(std::string type);

    bool isInitialized();
    /// @endcond

    /*!
     * \fn setScalarfromString
     * \brief transformes the passed string to a register value
     * \param stringValue a scalar value in string representation
     */
    void setScalarfromString(std::string stringValue);

    /// @cond
protected:
    Register(Device* theDevice, ElementXML* registerNode);
    virtual ~Register();

    // export register extraction methods
    friend class PLC;
    friend class Device;

    /*!
     * \brief Set access type.
     * \param accessType Input, Ouput or InOut
     */
    void setAccessType(AccessType accessType);

    /*!
     * \brief Set access area.
     * \param accessArea Memory, Digital or Analog
     */
    void setAccessArea(AccessArea accessArea);

    inline void setBlockName(const std::string blockName)
    {
        blockName_ = blockName;
    }

    /*!
     * \fn getValAsString
     * \return return a copy of the corresponding string from the buffer (scalar, array or 2D-array)
     */
    std::string getValAsString(void* pValue, unsigned long i, unsigned long j);

    /*!
     * \fn getValAsByteString
     * \brief Debug purpose: return the register value as array of bytes
     * \param maxSize is the used to limit the string size in case of huge data array.
     * \return interprets the register value and return the corresponding string
     */
    std::string getValAsByteString(void* pValue, unsigned long maxSize);

    inline unsigned long getAddress()
    {
        return address_;
    }

    /*!
     * \brief Import values from the received buffer to the register. Pure virtual to be implemented by the inheriting classes.
     * \param pBuffer received data buffer
     * \param ts register time-stamp
     */
    virtual void importValue(void* pBuffer, timeval ts) = 0;

    /*!
     * \brief Export the value from the register to the correct position in the buffer. Pure virtual to be implemented by the inheriting classes.
     * \param pBuffer the output buffer where to update the data
     */
    virtual void exportValue(void* pBuffer) = 0;

    /*!
     * \brief Import values from the received buffer to the register. Pure vistual to be implemented by the inheriting classes.
     * \param pBuffer received data buffer
     * \param ts register time-stamp
     */
    virtual void importString(void* pBuffer, timeval ts) = 0;

    /*!
     * \brief Export the string from the register to the correct position in the buffer. Pure virtual to be implemented by inheriting classes.
     * \param pBuffer the output buffer where to update the data
     */
    virtual void exportString(void* pBuffer) = 0;

    /*!
     * \brief Copies the register content from the input buffer to the output buffer. Pure virtual to be implemented by the inheriting classes.
     */
    virtual void copyValue() = 0;

    /// Parent reference of that register
    Device* theDevice_;

    /// Register name
    std::string name_;

    /// Register access-type: Input, Ouput or InOut (following its related block)
    AccessType accessType_;

    /// Register access-area: Memory, Digital or Analog (following its related block)
    AccessArea accessArea_;

    /// Enum format: char, uchar, short, ushort, long, ulong, float, double
    FormatType format_;

    /// Data first dimension (>1 for single array)
    uint32_t dimension1_;

    /// Data second dimension (>1 for double array)
    uint32_t dimension2_;

    // String length
    unsigned long length_;

    /// Data size (relies on format)
    unsigned long size_;

    /// Data memory size (relies on format, including alignments)
    unsigned long memSize_;

    /// data address within the PLC memory (from the block base-address)
    unsigned long address_;

    /// Flag used to check if the register has been setted once at least.
    /// Uploading Slave registers at connection time is allowed only if all
    /// the retentive Slave registers are initialized!
    bool isInitialized_;

    /// Buffer to store the value of the register
    /// 2 different buffers are used in case of InOut access.
    void* pRecvValue_;
    void* pSendValue_;

    /// Name of the "parent" Block name (for general information)
    std::string blockName_;

    // True if this register is a configuration register ( sent to controller only once suring startup, than only read )
    bool isConfiguration_;

    // Time-of-day: number of seconds and microseconds since the POSIX.1 Epoch (00:00:00 UTC, January 1, 1970)
    // Use to time-stamp the register coming from PLC (no time-stamping on send).
    // FEC clock and Timing are synchronized using SNTP (common GPS source)
    timeval tod_;

    PLC* getPLC();
    /// @endcond

    //private:
    //template <typename T> inline T getVal(FormatType F, unsigned char* pValue);
    template<typename T> T getVal(FormatType F);
    template<typename T> void getValArray(FormatType F, T* pValue, uint32_t dim);
    template<typename T> void getValArray2D(FormatType F, T* pValue, uint32_t dim1, uint32_t dim2);
    template<typename Tsrc, typename Tdst> void convGetValArray(FormatType F, Tdst* pValue, uint32_t dim);
    template<typename Tsrc, typename Tdst> void convGetValArray2D(FormatType F, Tdst* pValue, uint32_t dim1, uint32_t dim2);
    template<typename T> T* getRefArray(FormatType F, uint32_t& dim);
    template<typename T> T* getRefArray2D(FormatType F, uint32_t& dim1, uint32_t& dim2);

    template<typename T> void setVal(FormatType F, T val);
    template<typename T> void setValArray(FormatType F, const T* pVal, uint32_t dim);
    template<typename T> void setValArray2D(FormatType F, const T* pVal, uint32_t dim1, uint32_t dim2);
    template<typename Tsrc, typename Tdst> void convSetValArray(FormatType F, const Tsrc* pVal, uint32_t dim);
    template<typename Tsrc, typename Tdst> void convSetValArray2D(FormatType F, const Tsrc* pVal, uint32_t dim1, uint32_t dim2);
};

} // namespace

#endif // _SILECS_REGISTER_H_
