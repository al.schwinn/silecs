/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/


#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/equipment/SilecsDevice.h>
#include <silecs-communication/interface/equipment/SilecsBlock.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <arpa/inet.h>
#include <netdb.h>

namespace Silecs
{
bool ClusterConfig::getSharedConnection() const
{
    return sharedConnection;
}

void ClusterConfig::setSharedConnection(bool sharedConnectionFlag)
{
    sharedConnection = sharedConnectionFlag;
}

Cluster::Cluster(const std::string className, const std::string classVersion) :
                className_(className),
                classVersion_(classVersion)
{
    hostName_ = Log::getHost();

    //Default run-time configuration setup
    configuration.sharedConnection = false; //Do not shared connection with other clusters by default

    LOG((ALLOC)) << "CLUSTER (create): " << className_ << "/" << classVersion_;
}

Cluster::~Cluster()
{
    LOG(ALLOC) << "CLUSTER (delete): " << className_ << "/" << classVersion_;

    // Remove the PLCs instances related to this Cluster (hostName_/className_)
    plcMapType::iterator plcMapIter;
    for (plcMapIter = plcMap_.begin(); plcMapIter != plcMap_.end(); ++plcMapIter)
    {
        delete plcMapIter->second;
    }
    plcMap_.clear();
    IPAddrMap_.clear(); //this map refer the same PLC object (not necessary to delete them)
}

bool Cluster::getHostNameByAddress(const std::string IPAddress, std::string& hostName)
{
    int error;
    struct sockaddr_in ipv4addr;

    ipv4addr.sin_family = AF_INET;
    if ( (error = inet_pton(AF_INET, IPAddress.c_str(), &ipv4addr.sin_addr)) == 1)
    {
        char hname[NI_MAXHOST];
        if ( ( (error = getnameinfo((struct sockaddr*)&ipv4addr, sizeof (ipv4addr), hname, sizeof (hname), NULL, 0, 0)) == 0) && (isalpha(hname[0])))
        {
            hostName = std::string(hname);
            if (DEBUG & Log::topics_)
                LOG(COMM) << "getHostNameByAddress() successful, IPaddr: " << IPAddress << ", host-name: " << hostName;
            return true;
        }
    }
    if (DEBUG & Log::topics_)
        LOG(COMM) << "getHostNameByAddress() failed: " << gai_strerror(error);
    return false;
}

bool Cluster::getAddressByHostName(const std::string hostName, std::string& IPAddress)
{
    int error;
    struct addrinfo hints, *result = NULL;
    struct in_addr ipv4addr;

    memset(&hints, 0, sizeof (hints));
    hints.ai_family = AF_INET;
    if ( (error = getaddrinfo(hostName.c_str(), NULL, &hints, &result)) == 0)
    {
        ipv4addr.s_addr = ((struct sockaddr_in *) (result->ai_addr))->sin_addr.s_addr;
        IPAddress = std::string(inet_ntoa(ipv4addr));
        if (DEBUG & Log::topics_)
            LOG(COMM) << "getAddressByHostName() successful, host-name: " << hostName << ", IPaddr: " << IPAddress;
        if (result)
            freeaddrinfo(result);
        return true;
    }
    if (DEBUG & Log::topics_)
        LOG(COMM) << "getAddressByHostName() failed: " << gai_strerror(error);
    if (result)
        freeaddrinfo(result);
    return false;
}

Block* Cluster::getBlock(const std::string blockName, AccessType accessType)
{ // Retrieve the block from the first PLC (same for each of them)
    return plcMap_.begin()->second->getBlock(blockName, accessType);
}

std::string Cluster::getBlockList(AccessType accessType)
{
    std::string blockList = "";
    //At first, check this class/version is already deployed (one PLC in the Cluster at least)
    if (plcMap_.size() > 0)
    { // Retrieve the block-map from the first PLC (same for each of them)
        blockVectorType blockCol = plcMap_.begin()->second->getBlockCol();

        blockVectorType::iterator block;
        for (block = blockCol.begin(); block != blockCol.end(); ++block)
        {
            if ( (*block)->getAccessType() == accessType)
            {
                std::string blockName = (*block)->getName();
                blockList.append(blockName + " ");
            }
        }
    }
    return blockList;
}

std::string Cluster::getBlockList()
{
    std::string blockList = "";
    //At first, check this class/version is already deployed (one PLC in the Cluster at least)
    if (plcMap_.size() > 0)
    { // Retrieve the block-map from the first PLC (same for each of them)
        blockVectorType blockCol = plcMap_.begin()->second->getBlockCol();

        blockVectorType::iterator block;
        for (block = blockCol.begin(); block != blockCol.end(); ++block)
        {
            std::string blockName = (*block)->getName();
            blockList.append(blockName + " ");
        }
    }
    return blockList;
}

PLC* Cluster::getPLC(std::string plcID, string parameterFile)
{
    //getPLC() supports IP and host-name addressing, but both are required to register the PLC object:
    //Host-name will be used to manage related configuration documents.
    //IP-addr is used to connect PLC in order to reduce the naming-server accesses.

    //Force PLC hostname lower-case
    StringUtilities::toLower(plcID);

    //At first, check if the given ID is already registered in that Cluster (host-name or IP-address)
    bool plcNotFound = false;
    plcMapType::iterator iter = plcMap_.find(plcID); //check the host-name map at first
    if (iter == plcMap_.end())
    { //we didn't find PLC by host-name, try to find it by IP address
        iter = IPAddrMap_.find(plcID);
        plcNotFound = (iter == IPAddrMap_.end());
    }

    if (plcNotFound)
    {
        //Retrieve Host-name & IP-address of the PLC
        std::string hostName;
        std::string IPAddress;

        if (isdigit(plcID[0]))
        { //user provided an IP address (start with digit character)
            IPAddress = plcID;
            if (!getHostNameByAddress(IPAddress, hostName))
                throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_IP_ADDRESS, IPAddress);
        }
        else
        { //user provided an PLC network name (start with alpha-character)
            hostName = plcID;
            if (!getAddressByHostName(hostName, IPAddress))
                throw SilecsException(__FILE__, __LINE__, PARAM_UNKNOWN_PLC_HOSTNAME, hostName);
        }

        //Create the new instance of the PLC
        PLC* pPLC = new PLC(this, hostName, IPAddress, parameterFile);
        plcMap_.insert(std::make_pair(hostName, pPLC)); //update host-name map
        IPAddrMap_.insert(std::make_pair(IPAddress, pPLC)); //update IP address map
        return pPLC;
    }
    return iter->second;
}

std::string Cluster::getHostName()
{
    return hostName_;
}
std::string Cluster::getClassName()
{
    return className_;
}
std::string Cluster::getClassVersion()
{
    return classVersion_;
}

std::string Cluster::getPLCList()
{
    std::string PLCList = "";
    plcMapType::iterator plcMapIter;
    for (plcMapIter = plcMap_.begin(); plcMapIter != plcMap_.end(); ++plcMapIter)
    {
        if (!PLCList.empty())
            PLCList.append(" ");
        PLCList.append(plcMapIter->second->getName());
    }
    return PLCList;
}

const plcMapType& Cluster::getPLCMap()
{
    return plcMap_;
}

void Cluster::recv(std::string blockName)
{
    //Asynchronous data receive
    plcMapType::iterator plcMapIter;

    // Schedule task on each PLC using asynchronous call for parallelism
    for (plcMapIter = plcMap_.begin(); plcMapIter != plcMap_.end(); ++plcMapIter)
    {
        plcMapIter->second->recvAsync(blockName);
    }

    // then wait for the completion of each PLC thread (finally, maximum delay will be the longest one).
    for (plcMapIter = plcMap_.begin(); plcMapIter != plcMap_.end(); ++plcMapIter)
    {
        plcMapIter->second->waitAsync();
    }
}

void Cluster::send(std::string blockName)
{
    //Asynchronous data send
    plcMapType::iterator plcMapIter;

    // Schedule task on each PLC using asynchronous call for parallelism
    for (plcMapIter = plcMap_.begin(); plcMapIter != plcMap_.end(); ++plcMapIter)
    {
        plcMapIter->second->sendAsync(blockName);
    }

    // then wait for the completion of each PLC thread (finally, maximum delay will be the longest one).
    for (plcMapIter = plcMap_.begin(); plcMapIter != plcMap_.end(); ++plcMapIter)
    {
        plcMapIter->second->waitAsync();
    }
}

void Cluster::copyInToOut(const std::string blockName)
{
    plcMapType::iterator plcMapIter;
    for (plcMapIter = plcMap_.begin(); plcMapIter != plcMap_.end(); ++plcMapIter)
        plcMapIter->second->copyInToOut(blockName);
}

} // namespace

