/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifndef CFP774SIMATIC400_1_0_0_H_
#define CFP774SIMATIC400_1_0_0_H_

#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>

#include "SilecsHeader_1_0_0.h"
#include "SilecsValid_1_0_0.h"

namespace Cfp774Simatic400_1_0_0
{

typedef SilecsWrapper::DeployConfig DeployConfig;
typedef SilecsWrapper::DesignConfig DesignConfig;
class DeployUnit : public SilecsWrapper::DeployUnit
{
public:

    /*!
     * \brief Use this method to create/get the unique instance of the Deploy Unit.
     *
     * \param logTopics This parameter can be used to enable/disable log topics
     * valid topics are ERROR[,INFO,DEBUG,SETUP,ALLOC,RECV,SEND,COMM,DATA,LOCK].
     *
     * \param globalConfig This parameter can be used to pass different parameters to
     * the library. I.e. enabling automatic connection.
     */
    static DeployUnit* getInstance(const std::string& logTopics = "",
            const SilecsWrapper::DeployConfig& globalConfig = SilecsWrapper::DeployConfig())
    {
        if (_instance == NULL)
        {
            _instance = new DeployUnit(logTopics, globalConfig);
        }
        else
        {
            if (not logTopics.empty())
            {
                _instance->getService()->setLogTopics(logTopics);
            }
        }
//        DeployUnit::_globalConfig = globalConfig;
        return dynamic_cast<DeployUnit*>(_instance);
    }
    
    /*!
     * \brief Use this method to create/get the unique instance of the Deploy Unit.
     *
     * \param globalConfig This parameter can be used to pass different parameters to
     * the library. I.e. enabling automatic connection.
     */
    static DeployUnit* getInstance(const SilecsWrapper::DeployConfig& globalConfig)
    {
        return getInstance("", globalConfig);
    }
    
    /*!
     * \brief Return pointer to the deployed design SilecsHeader.
     */
    SilecsHeader_1_0_0::Design* getSilecsHeader()
    {
        return _SilecsHeader;
    }

    /*!
     * \brief Return pointer to the deployed design SilecsValid.
     */
    SilecsValid_1_0_0::Design* getSilecsValid()
    {
        return _SilecsValid;
    }

private:

    SilecsHeader_1_0_0::Design* _SilecsHeader;
    SilecsValid_1_0_0::Design* _SilecsValid;

    DeployUnit(const std::string& logTopics, const SilecsWrapper::DeployConfig& globalConfig) :
                    SilecsWrapper::DeployUnit("Cfp774Simatic400_1_0_0", "1.0.0", logTopics, globalConfig)
    {
        // Construct Design SilecsHeader_1_0_0
        _SilecsHeader = new SilecsHeader_1_0_0::Design((SilecsWrapper::DeployUnit*) this);
        
        // Construct Design SilecsValid_1_0_0
        _SilecsValid = new SilecsValid_1_0_0::Design((SilecsWrapper::DeployUnit*) this);
        
    }
    
    ~DeployUnit()
    {
        delete _SilecsHeader;
        delete _SilecsValid;
    }
};

} /* namespace Cfp774Simatic400_1_0_0 */

#endif /* CFP774SIMATIC400_1_0_0_H_ */
