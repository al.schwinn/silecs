#include <silecs-communication/interface/core/SilecsService.h>
#include <silecs-communication/interface/equipment/SilecsCluster.h>
#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/communication/SNAP7Connection.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/protocol/core/silecs.h>

#include <iostream>
#include <numeric>
#include <vector>
#include <string>

int main(int argc, char **argv)

{
    Silecs::Service* pService = NULL;
    Silecs::Cluster* pCluster  = NULL;
    int rackNb = 0;
    int slotNb = 2;
    uint8_t EBbuffer[10];
    uint8_t ABbuffer[10];

    memset(EBbuffer, 0, sizeof(EBbuffer));

    try

    {
        // Instantiate the singleton of the SILECS Service

        pService = Silecs::Service::getInstance();

        pService->setArguments(string(argv[1]));
        pCluster = pService->getCluster("SilecsValid", "1.1.0");

        cout << string(argv[1]) << endl;

        // Retrieve the PLC related to the current FESA device

        // (from 'plcHostName' FESA field defined on that purpose).

        Silecs::PLC* pPLC = pCluster->getPLC("cfp-774-csimatic-s7-01");

        //pPLC->connect(/*synchroMode=*/Silecs::NO_SYNCHRO, /*connectNow=*/false);

       // S7Object recvClient = Cli_Create();
       // Cli_ConnectTo(recvClient, pPLC->getIPAddress().c_str(), rackNb, slotNb);


       // Cli_EBRead(recvClient, 72, 1, EBbuffer);

       // std::cout << std::hex << EBbuffer[0] << std::endl;

       // Cli_ABRead(recvClient, 72, 1, EBbuffer);

       // std::cout << std::hex << EBbuffer[0] << std::endl;

       // ABbuffer[0] = EBbuffer[0] + 1;

       // Cli_ABWrite(recvClient, 72, 1, ABbuffer);

       // std::cout << std::hex << ABbuffer[0] << std::endl;

       // Cli_EBRead(recvClient, 72, 1, EBbuffer);

       // std::cout << std::hex << EBbuffer[0] << std::endl;

       // Cli_Disconnect(recvClient);
	   // Cli_Destroy(&recvClient);

        pService->deleteInstance();
   }

   catch (const Silecs::SilecsException& ex)
   {
       throw Silecs::SilecsException(__FILE__, __LINE__, ex.getMessage());
   }

   pService->deleteInstance();
}
