#include <silecs-communication/interface/utility/SilecsLog.h>
#include <silecs-communication/interface/utility/StringUtilities.h>
#include <silecs-communication/interface/communication/SilecsConnection.h>
#include <silecs-communication/interface/equipment/SilecsPLC.h>
#include <silecs-communication/interface/communication/MBConnection.h>
#include <silecs-communication/interface/utility/SilecsException.h>
#include <silecs-communication/protocol/modbus/iemdb.h>

#include <iostream>

int main(void)
{
    Silecs::Service* pService = NULL;
    Silecs::Cluster* pCluster  = NULL;
    modbus_t *readCtx;
    modbus_t *writeCtx;
    uint16_t reg[64];
    int rc, i;

    memset(reg, 0, sizeof(reg));
    try
    {
        // Instantiate the singleton of the SILECS Service

        pService = Silecs::Service::getInstance();
        pCluster = pService->getCluster("BK9Design", "1.0.0");

        // Retrieve the PLC related to the current FESA device

        // (from 'plcHostName' FESA field defined on that purpose).

        Silecs::PLC* pPLC = pCluster->getPLC("cfp-774-ctwincat-mb-03");

        //pPLC->connect(/*synchroMode=*/Silecs::NO_SYNCHRO, /*connectNow=*/false);

        readCtx = modbus_new_tcp((char *) pPLC->getIPAddress().c_str(), MODBUS_TCP_DEFAULT_PORT /*=502*/);
        writeCtx = modbus_new_tcp((char *) pPLC->getIPAddress().c_str(), MODBUS_TCP_DEFAULT_PORT /*=502*/);
        modbus_connect(readCtx);
        modbus_connect(writeCtx);


        rc = modbus_read_input_registers(readCtx, 24, 1, reg);

        std::cout << "Reading input from address 0" << std::endl;

        if (rc == -1) {
            fprintf(stderr, "%s\n", modbus_strerror(errno));
            return -1;
        }


        for (i=0; i < rc; i++) {
            printf("reg[%d]=%d (0x%X)\n", i, reg[i], reg[i]);
        }

        modbus_close(readCtx);
        modbus_close(writeCtx);
        modbus_free(readCtx);
        modbus_free(writeCtx);

        pService->deleteInstance();
   }

   catch (const Silecs::SilecsException& ex)
   {
       throw Silecs::SilecsException(__FILE__, __LINE__, ex.getMessage());
   }

   pService->deleteInstance();
}
