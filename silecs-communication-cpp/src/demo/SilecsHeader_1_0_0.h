/*
Copyright (c) 2017 European Organization for Nuclear Research (CERN).
All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html

Contributors:
    .European Organization for Nuclear Research    (CERN) - initial API and implementation
    .GSI Helmholtzzentrum für Schwerionenforschung (GSI)  - features and bugfixes
*/

#ifndef SILECSHEADER_1_0_0_H_
#define SILECSHEADER_1_0_0_H_

#include <silecs-communication/wrapper/Block.h>
#include <silecs-communication/wrapper/DeployUnit.h>
#include <silecs-communication/wrapper/Design.h>
#include <silecs-communication/wrapper/Device.h>

namespace SilecsHeader_1_0_0
{

class Design;

class HdrBlk : public SilecsWrapper::Block
{
public:
    /*!
     * \brief hdrBlk constructor. It creates an empty block.
     */
    HdrBlk() :
                    SilecsWrapper::Block("hdrBlk"),
                    _checksum(0),
                    _date(0)
    {
    }

    ~HdrBlk()
    {
    }

    /*!
     * \brief Get _version register.
     * \return value.
     */
    const std::string& get_version() const
    {
        return _version;
    }

    /*!
     * \brief Set _version register.
     * \param value to be set.
     */
    void set_version(const std::string& value)
    {
        _version = value;
    }

    /*!
     * \brief Get _checksum register.
     * \return value.
     */
    uint32_t get_checksum() const
    {
        return _checksum;
    }

    /*!
     * \brief Set _checksum register.
     * \param value to be set.
     */
    void set_checksum(uint32_t value)
    {
        _checksum = value;
    }

    /*!
     * \brief Get _user register.
     * \return value.
     */
    const std::string& get_user() const
    {
        return _user;
    }

    /*!
     * \brief Set _user register.
     * \param value to be set.
     */
    void set_user(const std::string& value)
    {
        _user = value;
    }

    /*!
     * \brief Get _date register.
     * \return value.
     */
    double get_date() const
    {
        return _date;
    }

    /*!
     * \brief Set _date register.
     * \param value to be set.
     */
    void set_date(double value)
    {
        _date = value;
    }
private:
    std::string _version;
    uint32_t _checksum;
    std::string _user;
    double _date;

};
class Device : public SilecsWrapper::Device
{
public:
    Device(const std::string& label, SilecsWrapper::Controller *controller) :
                    SilecsWrapper::Device(label, controller)
    {
    }

    ~Device()
    {
    }
    
    /*!
     * \brief Get hdrBlk block. 
     * \param block HdrBlk reference where to store returned value.
     */
    void getHdrBlk(HdrBlk &block)
    {
        // Copy register _version
        block.set_version(getSilecsDevice()->getRegister("_version")->getValString());
        // Copy register _checksum
        block.set_checksum(getSilecsDevice()->getRegister("_checksum")->getValUInt32());
        // Copy register _user
        block.set_user(getSilecsDevice()->getRegister("_user")->getValString());
        // Copy register _date
        block.set_date(getSilecsDevice()->getRegister("_date")->getValDate());
    }
    

    /*!
     * \brief Receive hdrBlk block for current device.
     */
    void receiveHdrBlk()
    {
        _silecsDevice->recv("hdrBlk");
    }
    

};

class Controller : public SilecsWrapper::Controller
{
public:
    Controller(const std::string& name, const std::string& domain, Design *design) :
                    SilecsWrapper::Controller(name, domain, (SilecsWrapper::Design*) design)
    {
            _deviceMap.insert(std::pair<std::string, Device*>("0", new Device("0", this)));
    }

    ~Controller()
    {
        map<std::string, Device*>::iterator it;
        for (it = _deviceMap.begin(); it != _deviceMap.end(); it++)
        {
            delete it->second;
        }
    }

    /*!
     * \brief Return pointer to the requested device.
     * \param label Device label.
     */
    Device* getDevice(const std::string& label)
    {
        if (_deviceMap.find(label) != _deviceMap.end())
        {
            return _deviceMap[label];
        }
        throw Silecs::SilecsException(__FILE__, __LINE__, Silecs::PARAM_UNKNOWN_DEVICE_NAME, label);
    }

    std::map<std::string, Device*>& getDeviceMap()
    {
        return _deviceMap;
    }
    /*!
     * \brief Receive hdrBlk blocks from all devices of current controller.
     */
    void receiveHdrBlkAllDevices()
    {
        _silecsPLC->recv("hdrBlk");
    }
    
private:
    std::map<std::string, Device*> _deviceMap;
};

class Design : public SilecsWrapper::Design
{
public:

    Design(SilecsWrapper::DeployUnit *deployUnit) :
                    SilecsWrapper::Design("SilecsHeader", "1.0.0", deployUnit)
    {

            _controllerMap.insert(
                std::pair<std::string, Controller*>("cfp-774-csimatic-s7-02",
                        new Controller("cfp-774-csimatic-s7-02", "TST", this)));

    }

    ~Design()
    {
        map<std::string, Controller*>::iterator it;
        for (it = _controllerMap.begin(); it != _controllerMap.end(); it++)
        {
            delete it->second;
        }
    }

    /*!
     * \brief Return pointer to the requested controller.
     * \param name Controller's name.
     */
    Controller* getController(const std::string& name)
    {
        if (_controllerMap.find(name) != _controllerMap.end())
        {
            return _controllerMap[name];
        }
        throw Silecs::SilecsException(__FILE__, __LINE__, Silecs::PARAM_UNKNOWN_PLC_HOSTNAME, name);
    }

    /*!
     * \brief Return pointer to the controller cfp-774-csimatic-s7-02.
     */
    Controller* getCfp_774_csimatic_s7_02()
    {
        return _controllerMap["cfp-774-csimatic-s7-02"];
    }
    
    /*!
     * \brief Receive hdrBlk blocks from all connected controllers.
     */
    void receiveHdrBlkAllControllers()
    {
        _silecsCluster->recv("hdrBlk");
    }
    
    std::map<std::string, Controller*>& getControllerMap()
    {
        return _controllerMap;
    }

private:
    std::map<std::string, Controller*> _controllerMap;

};

} /* namespace SilecsHeader_1_0_0 */

#endif /* SILECSHEADER_1_0_0_H_ */
