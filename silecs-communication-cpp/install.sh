#!/bin/sh
set -e

RELEASE_DIR_BASE=$1
SILECS_VERSION=$2

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

CPU=x86_64

RELEASE_DIR=${RELEASE_DIR_BASE}/${SILECS_VERSION}/silecs-communication-cpp
if [ -d ${RELEASE_DIR} ]; then 
    echo "Error: ${RELEASE_DIR} already exists ...skipping"
    exit 1
fi

MAJOR=`echo $SILECS_VERSION | cut -d. -f1`
MINOR=`echo $SILECS_VERSION | cut -d. -f2`
PATCH=`echo $SILECS_VERSION | cut -d. -f3`

make -C ${SCRIPTPATH} clean
MODBUS_SUPPORT_ENABLED=true make -C ${SCRIPTPATH} CPU=${CPU} MAJOR=${MAJOR} MINOR=${MINOR} PATCH=${PATCH} -j4

# Squash the silecs library with the modbus library, so no extra-lining will be required
SILECS_LIB=${SCRIPTPATH}/build/lib/${CPU}/libsilecs-comm.a
SQUASHED_LIB=${SCRIPTPATH}/build/lib/${CPU}/libsquashed.a
cd ${SCRIPTPATH}
ar -M <squash_modbus_lib.mri
rm  ${SILECS_LIB}
mv ${SQUASHED_LIB} ${SILECS_LIB}

mkdir -p ${RELEASE_DIR}
cp -r ${SCRIPTPATH}/build/include ${RELEASE_DIR}
cp -r ${SCRIPTPATH}/build/lib ${RELEASE_DIR}

# Make all files write-protected to prevent overwriting an old version by accident
chmod a-w -R ${RELEASE_DIR}