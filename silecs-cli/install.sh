#!/bin/sh
set -e

RELEASE_DIR_BASE=$1
SILECS_VERSION=$2

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

RELEASE_DIR=${RELEASE_DIR_BASE}/${SILECS_VERSION}/silecs-cli
if [ -d ${RELEASE_DIR} ]; then 
    echo "Error: ${RELEASE_DIR} already exists ...skipping"
    exit 1
fi

mkdir -p ${RELEASE_DIR}

DEST_FILE=${RELEASE_DIR}/silecs

cp -r ${SCRIPTPATH}/silecs.sh ${DEST_FILE}

# Make all files write-protected to prevent overwriting an old version by accident
chmod a-w -R ${RELEASE_DIR}