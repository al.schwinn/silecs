#!/bin/sh
set -e

RELEASE_DIR_BASE=$1
SILECS_VERSION=$2

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

RELEASE_DIR=${RELEASE_DIR_BASE}/${SILECS_VERSION}/silecs-cli-client
if [ -d ${RELEASE_DIR} ]; then 
    echo "Error: ${RELEASE_DIR} already exists ...skipping"
    exit 1
fi

SNAP7_BASE=${RELEASE_DIR_BASE}/snap7/1.4/x86_64-linux

make -C ${SCRIPTPATH} clean
make -C ${SCRIPTPATH} CPU=x86_64 MAJOR=${MAJOR} MINOR=${MINOR} PATCH=${PATCH} SNAP7_BASE=${SNAP7_BASE} -j4

mkdir -p ${RELEASE_DIR}
cp -r ${SCRIPTPATH}/build/bin ${RELEASE_DIR}
cp -r ${SCRIPTPATH}/examples ${RELEASE_DIR}

# Make all files write-protected to prevent overwriting an old version by accident
chmod a-w -R ${RELEASE_DIR}