#!/bin/sh
set -e

PARAM_FILE=/common/home/bel/schwinn/lnx/workspace-silecs-neon/SiemensTestDU/generated/client/tsts7001.silecsparam

# config for SiemensTestDU
ARGUMENTS="-f $PARAM_FILE -i -c"
COMMAND="$BINARY $ARGUMENTS"
$COMMAND
