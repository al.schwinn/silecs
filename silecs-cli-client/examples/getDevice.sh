#!/bin/sh
set -e

PARAM_FILE=/common/home/bel/schwinn/lnx/workspace-silecs-neon/SiemensTestDU/generated/client/tsts7001.silecsparam
DEVICE=MyDevice1

# config for SiemensTestDU
ARGUMENTS="-f $PARAM_FILE -d $DEVICE -m GET_DEVICE"
COMMAND="$BINARY $ARGUMENTS"
$COMMAND

