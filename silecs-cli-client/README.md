# silecs-cli-client

This component of the SILECS PLC-framework allows to connect to a silecs-configured PLC by using the command-line

Options: 

 * -b silecs-block to get
 * -c ignore silecs-checksum, force connect to PLC
 * -d silecs-device to request
 * -f path to silecs-parameter-file of the plc
 * -h print this help
 * -i start interactive session
 * -v verbose, all LOGTOPICS are enabled
 * -m mode, can be 'GET_DEVICE', 'GET_BLOCK' or 'GET_REGISTER'
 * -p periodic Interval(ms) us ? to measure
 * -r optional, register to get/set
 * -s silent, minimal output (only the register-values, no meta information)

# Disclaimer

The GSI Version of Silecs is a fork of the CERN Silecs version. It was forked 2018 and has evolved into a slightly different framework. The fork was required because CERN did not merge GSI merge requests, and generally does not respect the GPL v3 licence (source code and issue tracker only visible in the CERN walled garden, CERN account and special privileges required, software only builds and runs within CERN infrastructure ). That makes a collaboration/contributions impossible. 

## Getting Started

In order to use the silecs command line client, best first check the scripts in the [examples](examples) folder.

You can copy these scripts and adjust them to your personal needs.

For more information, check the [GSI SILECS Wiki Page][GSI_Wiki]

Check the section 'Configuration of the SILECS development Environment' in order to add `silecs-cli-client` to your `$PATH` variable

## License

Licensed under the GNU GENERAL PUBLIC LICENSE Version 3. See the [LICENSE file][license] for details.

[license]: LICENSE
[GSI_Wiki]: https://www-acc.gsi.de/wiki/Frontend/SILECS
