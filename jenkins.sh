#!/bin/sh
set -e

echo "##############################################"
echo "configuration of build environment"
echo "##############################################"

# $WORKSPACE is the jenkins workspace, will be filled by jenkins

export COMMON_MAKE_PATH="$WORKSPACE/../../generics/generic-makefiles"
export SNAP7_BASE="$WORKSPACE/snap7/snap7-full"
export BOOST_HOME="$WORKSPACE/../../generics/boost_1.54.0/boost/1.54.0"
export SILECS_COMM_HOME="$WORKSPACE/silecs-communication-cpp/build"

echo "##############################################"
echo "building snap7" 
echo "##############################################"
cd snap7
./build.sh
cd ..
echo "##############################################"
echo "building silecs-communication-cpp" 
echo "##############################################"
cd silecs-communication-cpp
make clean
make all CPU=x86_64 -j4 
cd ..
echo "##############################################"
echo "building silecs-codegen" 
echo "##############################################"
cd silecs-codegen/src/xml
python runTests.py
cd ../../..
echo "##############################################"
echo "building silecs-diagnostic-cpp" 
echo "##############################################"
cd silecs-diagnostic-cpp
make clean
make CPU=x86_64 -j1
cd ..
echo "##############################################"
echo "building silecs-cli-client" 
echo "##############################################"
cd silecs-cli-client
make clean
make CPU=x86_64 -j1
cd ..
echo "##############################################"
echo "build finished sucessfully" 
echo "##############################################"

